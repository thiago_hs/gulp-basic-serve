(function(){

    /* TODO - precisa refazer a estrutura desse codigo */
    var components = {

          collapse:{
            'js-md':{
              match:function(){

                //temp
                $('.js-md[data-toggle="collapse"]').each(function(){

                  var $element = $(this),
                  $content = $($element.attr('href')),
                  unmatchClasses = ['model']

                  $element.removeClass('collapsed');
                  $content
                  .removeClass('collapse')
                  .removeAttr('style');

                  if($element.hasClass('model1')){
                    $element.data('js-md-unmatch-classes','model1')
                    $element.removeClass('model1')
                  }

                  $element.data('original-href',$element.attr('href'));
                  $element.removeAttr('href');


                });

                console.log('match dropdown medium');
              },
              unmatch:function(){

                //temp
                $('.js-md[data-toggle="collapse"]').each(function(){

                  var $element = $(this),
                  $content = $($element.data('original-href'));

                  $element.addClass('collapsed');
                  $content
                  .addClass('collapse');

                  if($element.data('js-md-unmatch-classes') == 'model1'){
                    $element.addClass('model1')
                  }

                  $element.attr('href',$element.data('original-href'));


                });

                console.log('unmatch dropdown medium');
              },
              setup:function(){
                //console.log('iniciou dropdown medium');
              }
            },
            'js-lg':{
              match:function(){

                //temp
                $('.js-lg[data-toggle="collapse"]').each(function(){

                  var $element = $(this),
                  $content = $($element.attr('href')),
                  unmatchClasses = ['model']

                  $element.removeClass('collapsed');
                  $content
                  .removeClass('collapse')
                  .removeAttr('style');

                  if($element.hasClass('model1')){
                    $element.data('js-lg-unmatch-classes','model1')
                    $element.removeClass('model1')
                  }

                  $element.data('original-href',$element.attr('href'));
                  $element.removeAttr('href');


                });

                console.log('match dropdown large');
              },
              unmatch:function(){

                //temp
                $('.js-lg[data-toggle="collapse"]').each(function(){

                  var $element = $(this),
                  $content = $($element.data('original-href'));

                  $element.addClass('collapsed');
                  $content
                  .addClass('collapse');

                  if($element.data('js-lg-unmatch-classes') == 'model1'){
                    $element.addClass('model1')

                  }
                  $element.attr('href',$element.data('original-href'));


                });

                console.log('unmatch dropdown large');
              },
              setup:function(){
                //console.log('iniciou dropdown medium');
              }
            },
            'js-sm':{
              match:function(){

                console.log('match dropdown small')
              },
              unmatch:function(){
                console.log('unmatch dropdown small');
              },
              setup:function(){
                //console.log('iniciou dropdown small');
              }
            }
          },
          /*carousel:{
            'js-md':{
              match:function(){
                console.log('match carousel medium');
              },
              unmatch:function(){
                console.log('unmatch carousel medium');
              },
              setup:function(){
                console.log('iniciou carousel medium');
              }
            },
            'js-sm':{
              match:function(){
                console.log('match carousel small');
              },
              unmatch:function(){
                console.log('unmatch carousel small');
              },
              setup:function(){
                console.log('iniciou carousel small');
              }
            }
          }*/
        }
        var medias = {
          small:'(min-width:568px)',
          medium:'(min-width:769px)',
          large:'(min-width:992px)'
        };

        for(var media in medias){

          var this_media_components = [];

          /*
            para cada componente, verifica-se quais batem com a midia atual
            reune-se todos os componentes dessa media para ser executado posteriormente
            no local adequado
          */
          for (var component in components){

            if(media == 'small'){

              this_media_components.push(components[component]['js-sm']);
            }
            else if(media == 'medium'){

              this_media_components.push(components[component]['js-md']);
            }
            else if(media == 'large'){

              this_media_components.push(components[component]['js-lg']);
            }
          }
          console.log('executando media '+media);
          //para cada media, executa-se equire.register()
          enquire.register(medias[media],this_media_components);

          // reseting
          this_media_components = [];
        };
  })();

  // swiper
  ;(function(){
    $('[data-ride="swiper-ingresso"], [data-ride="swiper-ingresso-side"], [data-ride="swiper-ingresso-list"]').each(function () {
      var $elm = $(this);
      var $body = $('body');
      var $nextButton = $('.swiper-button-next', $elm);
      var $prevButton = $('.swiper-button-prev', $elm);

      var options = {
        carousel:{
          nextButton: $nextButton,
          prevButton: $prevButton,
          paginationClickable:true,
          preloadImages:false,
          simulateTouch: false,
          speed:500,
          preventClicksPropagation: false,
          pagination:$('.swiper-pagination', $elm).length ? $('.swiper-pagination', $elm) : null
        },
        thumbs:{
          slideToClickedSlide: true,
          slidesPerView: 4,
          spaceBetween: 10,
          touchRatio: 0.2,
          centeredSlides:true
        }
      };

      if ($body.hasClass('atm-v2')) {
        options.carousel = $.extend({},
          { onReachBeginning: function(swiper){
            console.log("beginning")
            var $carousel = $(swiper.container).parent();
            $carousel.addClass('is-beginning');
          },
          onSlideChangeEnd:function(swiper){
            var $carousel = $(swiper.container).parent();
            if(!swiper.isBeginning && !swiper.isEnd){
              $carousel.removeClass('is-beginning is-end');
            } else if(!swiper.isBeginning){
              $carousel.removeClass('is-beginning');
            } else if(!swiper.isEnd){
              $carousel.removeClass('is-end');
            }
          },
          onReachEnd:function(swiper){
            var $carousel = $(swiper.container).parent();
            $carousel.addClass('is-end');
          }, simulateTouch: false, }, options.carousel);
      }

      if ($elm.attr('data-ride') == 'swiper-ingresso-side') {
        options.thumbs = $.extend({}, { direction:'vertical', setWrapperSize:true }, options.thumbs);
      }

      else if($elm.attr('data-ride') == 'swiper-ingresso-list'){
        options.carousel = $.extend({}, {
          slidesPerView:'auto',
          slidesPerGroup:1,
          speed:800,
          //loop:true,
          freeMode:true,
          mousewheelControl:true,
          mousewheelForceToAxis:true,
          preloadImages:false,
          //lazyLoadingInPrevNext:true,
          watchSlidesVisibility:true,
          simulateTouch: false
        }, options.carousel);
        /* CORRECAO PARA O BANNER WALLPAPER */
        if($('body.home-page').hasClass('has-wallpaper-banner')){
          options.carousel = $.extend(options.carousel, {
            breakpoints:{
              543:{
                slidesPerGroup:1
              },
              767:{
                slidesPerGroup:2
              },
              1919:{
                slidesPerGroup:3
              }
            }
          });
        }
      }
      else {
        options.carousel = $.extend(options.carousel, {
          autoplay:8000,
          loop:true,
          lazyLoading:true,
          lazyLoadingOnTransitionStart:true
        });

        options.thumbs = $.extend({}, {
          centeredSlides: true,
          breakpoints: {
            // >= 320
            320: {
              slidesPerView: 2,
              spaceBetweenSlides: 5
            },
            // >= 544
            544:{
              slidesPerView: 3,
              spaceBetweenSlides: 5
            },
            // >= 768
            768: {
              slidesPerView: 4,
              spaceBetweenSlides: 10
            }
          }
        }, options.thumbs);
      }

      if ($elm.hasClass('swiper-ingresso-column')) {
        options.carousel = $.extend({}, { 
          slidesPerColumn: 2,
          slidesPerGroup:2 
          }, options.carousel);
      }

      if($('.gallery-top', $elm).length){
        var galleryTop = new Swiper($('.gallery-top', $elm), options.carousel);
      }
      if($('.gallery-thumbs', $elm).length){
        var galleryThumbs = new Swiper($('.gallery-thumbs', $elm), options.thumbs);
      }

      if($('.gallery-thumbs', $elm).length && $('.gallery-top', $elm).length){
        galleryTop.params.control = galleryThumbs;
        galleryThumbs.params.control = galleryTop;
      }

      if ($elm.hasClass('swiper-ingresso-column')) {
        $nextButton.click(function() {
            watchButtons($elm, $prevButton, $nextButton);
        });

        $prevButton.click(function() {
            watchButtons($elm, $prevButton, $nextButton);
        });
      }

    });

    //Swiper ATM Vertical
    $('.swipe-vertical').each(function () {
      var $elm = $(this);
      var $swiperWide = $('.swiper-vertical-wide');

      var $bottomButton = $elm.hasClass('swiper-vertical-wide') ? $('.swipe-btn-bottom', $swiperWide) : $('.swipe-btn-bottom', $elm);
      var $topButton = $elm.hasClass('swiper-vertical-wide') ? $('.swipe-btn-top', $swiperWide) : $('.swipe-btn-top', $elm);

      options = {
        slidesPerView: $elm.hasClass('swiper-vertical-wide') ?  'auto' : 5,
        nextButton: $bottomButton,
        prevButton: $topButton,
        direction: 'vertical',

      };

      if ($elm.hasClass('swiper-vertical-wide')) {
        options = $.extend({}, {
              slidesPerColumnFill: 'row',
              slidesPerColumn: 3,
              onReachBeginning: function(swiper){
                var $carousel = $(swiper.container).parent();
                $carousel.addClass('is-beginning');
              },
              onSlideChangeEnd:function(swiper){
                var $carousel = $(swiper.container).parent();

                if(!swiper.isBeginning && !swiper.isEnd){
                  $carousel.removeClass('is-beginning is-end');
                } else if(!swiper.isBeginning){
                  $carousel.removeClass('is-beginning');
                } else if(!swiper.isEnd){
                  $carousel.removeClass('is-end');
                }
              },
              onReachEnd:function(swiper){
                var $carousel = $(swiper.container).parent();
                $carousel.addClass('is-end');
              }
        }, options);

      };

      var verticalSwiper = new Swiper($('.swiper-vertical-container', $elm), options);

      if ($elm.hasClass('swiper-vertical-column')) {
        $bottomButton.click(function() {
            watchButtons($swiperWide, $topButton, $bottomButton);
        });

        $topButton.click(function() {
            watchButtons($swiperWide, $topButton, $bottomButton);
        });
      }
    });

    watchButtons = function($carousel, $rightButton, $leftButton) {
      if (!$leftButton.hasClass('swiper-button-disabled') &&
          !$rightButton.hasClass('swiper-button-disabled')) {
        $carousel.removeClass('is-beginning');
        $carousel.removeClass('is-end');
        return;
      }

      if ($leftButton.hasClass('swiper-button-disabled')) {
        $carousel.addClass('is-end');
        $carousel.removeClass('is-beginning');
      } 
      
      if ($rightButton.hasClass('swiper-button-disabled')) {
        $carousel.addClass('is-beginning');
        $carousel.removeClass('is-end');
      }
    }

    var $win = $(window);
    //Swiper Home ATM
    $('.swiper-ingresso-atm').each(function () {
      var $elm = $(this);

      var verticalSwiper = new Swiper($('.swiper-container-atm', $elm), {
        slidesPerView:  $($win).width() >= 1366 ? 'auto' : 3,
        spaceBetween: 20,
        simulateTouch: false,
        centeredSlides: true,
        speed: 600,
        nextButton: $('.swiper-button-next', $elm),
        prevButton: $('.swiper-button-prev', $elm),
        onReachBeginning:function(swiper){
          var $carousel = $(swiper.container).parent();
          $carousel.addClass('is-beginning');
        },
        onSlideChangeEnd:function(swiper){
          var $carousel = $(swiper.container).parent();
          if(!swiper.isBeginning && !swiper.isEnd){
            $carousel.removeClass('is-beginning is-end');
          } else if(!swiper.isBeginning){
            $carousel.removeClass('is-beginning');
          } else if(!swiper.isEnd){
            $carousel.removeClass('is-end');
          }
        },
        onReachEnd:function(swiper){
          var $carousel = $(swiper.container).parent();
          $carousel.addClass('is-end');
        }
      });
    });

    //Swiper Home ATM
    $('.swiper-ingresso-atm-v2').each(function () {
      var $elm = $(this);
      var $body = $('body');

      options = {
        slidesPerView: 'auto',
        initialSlide: 0,
        spaceBetween: 40,
        centeredSlides: true,
        autoplay: 2000,
        autoplayDisableOnInteraction: false,
        speed: 400,
        nextButton: $('.swiper-button-next', $elm),
        prevButton: $('.swiper-button-prev', $elm),
        onReachBeginning:function(swiper){
          var $carousel = $(swiper.container).parent();
          $carousel.addClass('is-beginning');
        },
        onSlideChangeEnd:function(swiper){
          var $swiper = $('.swiper-atm-wapper');
          addClassInactive( $swiper.children() );
          addClassSlideVisible( $swiper.children() );
          var $carousel = $(swiper.container).parent();
          if(!swiper.isBeginning && !swiper.isEnd){
            $carousel.removeClass('is-beginning is-end');
          } else if(!swiper.isBeginning){
            $carousel.removeClass('is-beginning');
          } else if(!swiper.isEnd){
            $carousel.removeClass('is-end');
          }
        },
        onReachEnd:function(swiper){
          var $carousel = $(swiper.container).parent();
          $carousel.addClass('is-end');
        }
      };

      if ($body.hasClass('home-page-v2')) {
        options = $.extend({}, {
          
        }, options);
      } 
      var verticalSwiper = new Swiper($('.swiper-container-atm-v2', $elm), options);
    });

    self.addClassInactive = function ($childrens) {

        for (var i = 0; i < $childrens.length; i++) {

            if (!$childrens.eq(i).hasClass('swiper-slide-inactive') &&
                !$childrens.eq(i).hasClass('swiper-slide-active')) {

                $childrens.eq(i).addClass('swiper-slide-inactive');
            } 

            if ($childrens.eq(i).hasClass('swiper-slide-activ')) {
                $childrens.eq(i).removeClass('swiper-slide-activ');
            }
        }

    }

    self.addClassSlideVisible = function ($childrens) {

        for (var i = 0; i < $childrens.length; i++) {

            if ($childrens.eq(i).hasClass('swiper-slide-active')) {
                
                $childrens.eq(i)
                    .removeClass('swiper-slide-inactive')
                    .addClass('swiper-slide-activ');
                break;
            } 

        }

    }

    self.addClassSlideTwoVisible = function ($childrens) {

        for (var i = 0; i < $childrens.length; i++) {

            if ($childrens.eq(i).hasClass('swiper-slide-active')) {

                $childrens.eq(i)
                    .removeClass('swiper-slide-inactive')
                    .addClass('swiper-slide-activ');
                break;
            }
        }
    }

    var $swiper = $('.swiper-atm-wapper');
    var $win = $(window);

    addClassInactive( $swiper.children() );
    if ($($win).width() >= 1366) {  
        addClassSlideTwoVisible( $swiper.children() );
    } else {
        addClassSlideVisible( $swiper.children() );
    }

    $('.swiper-button-next').click(function() {
      addClassInactive( $swiper.children() );

      if ($($win).width() >= 1366) {  
         addClassSlideTwoVisible( $swiper.children() );
      } else {
         addClassSlideVisible( $swiper.children() );
      }
    });

    $('.swiper-button-prev').click(function() {
      addClassInactive( $swiper.children() );

      if ($($win).width() >= 1366) {  
         addClassSlideTwoVisible( $swiper.children() );
      } else {
         addClassSlideVisible( $swiper.children() );
      }
    });

    $($win).resize(function(){

       if ($($win).width() >= 1366) {  
         addClassInactive( $swiper.children() );
         addClassSlideTwoVisible( $swiper.children() );
       }     
    });

    var swiper = new Swiper('.swiper-timeline .swiper-container', {
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      initialSlide: 12,
      spaceBetween: 10,
      centeredSlides: true,
      slidesPerView: 'auto',
      slideToClickedSlide: true,
      simulateTouch: false,
      onReachBeginning:function(swiper){
        var $carousel = $(swiper.container).parent();
        $carousel.addClass('is-beginning');
      },
      onSlideChangeEnd:function(swiper){
        var $carousel = $(swiper.container).parent();
        if(!swiper.isBeginning && !swiper.isEnd){
          $carousel.removeClass('is-beginning is-end');
        } else if(!swiper.isBeginning){
          $carousel.removeClass('is-beginning');
        } else if(!swiper.isEnd){
          $carousel.removeClass('is-end');
        }
      },
      onReachEnd:function(swiper){
        var $carousel = $(swiper.container).parent();
        $carousel.addClass('is-end');
      }
      });

  })();

  /* temp js to show filter beahavior */
  ;(function(){
    /* temp code - for behavior purpose */
    $('.filter').each(function(){
      var $filter = $(this);
      var $head = $('[data-toggle="collapse"]',this);
      var $options = $('[role=option]',this);
      var $removeButton = $('.filter-action',this);
      var $filterContent = $(".filter-content",this);

      function _constructor(){
        $('a',$options).click(function(){
          _addItem.call(this,$(this).attr('data-val') || $(this).text());
          $head.closest('.tab-accordion').addClass('filter-selected');
        });

        $removeButton.on('click',function(e){
          _removeItem.call(this);
          e.stopPropagation();
        });
      }

      function _closeCollapse(){
        $filterContent.collapse('hide');
      }

      function _getFilterValue(){
        return $head.children('.filter-val').text();
      }

      function _addItem(newValue){
        if(_getFilterValue() !== newValue){
          $head.children('.filter-val').text(newValue);
          $options.filter('.is-selected').removeClass('is-selected');
          $filter.addClass('filter-has-val');

          $(this).closest('.filter-it')
          .addClass('is-selected')
          .trigger('filter.added');

          _closeCollapse();
        }

      }
      function _removeItem(){
        $head.children('.filter-val').text('');
        $options.filter('.is-selected').removeClass('is-selected');
        $filter.removeClass('filter-has-val');
      }

      _constructor.call(this);

    });

  })();

  ;(function(){
    /* esse timeout e por causa do do static. Tudo por causa de um plugin chamado w3-include.js*/
    setTimeout(function(){
      // Slide page component
      $('[data-slide-page-link], [data-slide-page-back]').each(function () {
        var $el = $(this);


        if (typeof $el.attr('data-slide-page-link') == 'string'){

          var _$page = $($el.attr('data-slide-page-link'));

          _$page.on('transitionend',function(){
            _$page.removeClass('is-animating');
          });

          $el.click(function (e) {
            _$page
            .addClass('is-animating')
            .toggleClass('active')
            .find('> .slide-page-wp > [data-slide-page-side]')
            .toggleClass('active')
          })


        }
        else {
          var _$page = $($el.attr('data-slide-page-back'));
          $el.click(function (e) {
            _$page
            .addClass('is-animating')
            .toggleClass('active')
            .find('> .slide-page-wp > [data-slide-page-side]')
            .toggleClass('active')
          })
        }

      })
    },3000)
  })()

  ;(function(){
    if ('addEventListener' in document) {
      document.addEventListener('DOMContentLoaded', function() {
          FastClick.attach(document.body);
      }, false);
  }
  })();

  ;(function(){

        var $body = $('body');
        var $header = $('#header');
        var $headerSearchInput = $('#search-ipt',$header);
        var $headerSearch = $headerSearchInput.closest('.hd-search');
        var $toggleItems = $('#header .hd-mm-lnk, #header .hd-local-link, #header .hd-um-link');
        var $window = $(window);
        var resolutions = {
          small:568,
          medium:769,
          large:992
        };
        var $bannerTop = $('#banner-top');

        function toggleCloseButton(e){
          if($(e.target).is($('#'+$(this).attr('id')))){
            $('[href="#'+$(this).attr('id')+'"]').not('.hd-local-link').find('.svg-icon');
          }
        }

        function toggleFilter(){
          $('.collapse.in',this).collapse('hide');
        }

        // generic close collapse button
        $('[data-close-collapse]').click(function(){
          $(this).closest('.collapse.in').collapse('hide');
        });

        // search suggestions
        $headerSearchInput
        .focus(function(){
          $headerSearch.addClass('is-opened');
          console.log('focus');
        });
        // remove classes and collapse items when click out
        $(document).on('click',function(e){
          // if not clicking on search content
          if(!$(e.target).closest('.hd-search').length){
            $headerSearch.removeClass('is-opened');
          }
          // if not clicking on some header collapsable content
          if($('#header .hd-cont.collapse.in, #header .hd-cont.collapsing').length && !$(e.target).closest('#header .hd-cont.collapse.in').length){
            $('#header .hd-cont.collapse.in').collapse('hide');
          }
        });
        // end - search suggestions

        // toggle items
        $('.hd-cont',$header)
        .on('show.bs.collapse',function(e){
          toggleCloseButton.call(this, e);
          if(!$('body.page-overlay').length && $(window).width() < resolutions.large){
            $('body').addClass('page-overlay');
          }
        })
        .on('hide.bs.collapse',function(e){
          toggleCloseButton.call(this, e);
        })
        .on('hidden.bs.collapse',function(e){
          if($('body.page-overlay').length && !$('.hd-cont.collapse.in',$header).length){
            $('body').removeClass('page-overlay');
          }
        });

        if ($window.width() > resolutions.small) {
          $bannerTop.removeClass('hide-banner-top');
        }

        $window.resize(function(){
          if($window.width() < resolutions.large){

            // overlay class
            if($('.hd-cont.collapse.in',$header).length){
              $('body').addClass('page-overlay');
            }else{
              $('body').removeClass('page-overlay');
            }

            // header
            if(typeof $header.attr('data-fixed') !== 'undefined') {
              if ($window.width() < resolutions.small) {
                $header.addClass('is-fixed');

                if (!$bannerTop.hasClass('hide-banner-top'))
                    $bannerTop.addClass('hide-banner-top');

              } else {
                  $header.removeClass('is-fixed');
                  $bannerTop.removeClass('hide-banner-top');
              }
            }

            // filter
          }else{
            $('.filter').off('filter.added').on('filter.added',toggleFilter);
          }
        })
        // end - toggle items

        // filter
        $('.filter',$header).on('filter.added',toggleFilter);
        // end -filter

        // fixed header
        if (typeof $header.attr('data-fixed') !== 'undefined') {

          if($body.hasClass('home-page')) {
            var $homeFilter = $('#filter-home-main');
          } else {
            $header.addClass('is-fixed');
            $bannerTop.addClass('banner-top');
          }

        }

        var headerTopOffset = $("#header").offset().top;

        $window.scroll(function () {

          if(typeof $header.attr('data-fixed') !== 'undefined') {
            if ($window.scrollTop() > headerTopOffset) {
              if (!$header.hasClass('is-fixed')) {
                  $header.addClass('is-fixed');
              }

              if (!$header.hasClass('is-reduced')) {
                  $header.addClass('is-reduced is-reducing');
              }
            } else {
                $header
                    .removeClass('is-reduced')
                    .addClass('is-reducing');

                if ($window.width() > resolutions.small && $body.hasClass('home-page')) {
                  $header.removeClass('is-fixed');
                }
            }
          }


            if ($body.hasClass('home-page')) {

                if (parseInt($window.scrollTop()) > parseInt($homeFilter.offset().top) + parseInt($homeFilter.outerHeight())) {

                    if (!$header.hasClass('has-search-icon')) {
                        $header.addClass('has-search-icon');
                    }
                } else if ($header.hasClass('has-search-icon')) {

                    $header.removeClass('has-search-icon');
                }
            }
        })

        // end - fixed header

    })();

  ;(function() {
    var $body = $('body');
    var TABLET_MAX_WIDTH = 767;

    $(document).on('click', '.js-video-poster', function(ev) {
      ev.preventDefault();
      var $poster = $(this);
      var $wrapper = $poster.closest('.js-video-wrapper');
      videoPlay($wrapper);
    });

    $(document).on('click', '#trailer-close-button', function(ev) {
      ev.preventDefault();
      var $wrapper = $('.js-video-wrapper');
      var $trailers = $(this).closest('.trailers');
      videoStop($wrapper);
      resetTrailers($trailers);
    });

    $(document).on('click', '.trailers .trailer-button',function(){
      var $trailerButton = $(this);
      var $iframe = $();
      var trailerURL = $trailerButton.attr('data-trailer');
      var $trailers = $trailerButton.closest('.trailers');

      $trailers.find('.trailer-button').removeClass('active');
      $trailerButton .addClass('active');

      if(typeof $trailerButton.attr('data-control-player') !== 'undefined'){
        $iframe = $($trailerButton.attr('data-control-player'));
      } else{
        $iframe = parseInt(window.innerWidth) > TABLET_MAX_WIDTH ? $('#movie-trailer-desktop') : $('#movie-trailer-mobile');
      }

      $iframe.attr('src',trailerURL);
    });

    function videoPlay($wrapper) {
      var $iframe = $wrapper.find('.js-video-iframe');
      var src = $iframe.data('src');
      $wrapper.addClass('video-wrapper-active');
      $body.addClass('is-playing-video');
      $iframe.attr('src', src);
    }

    function videoStop($wrapper) {
      if (!$wrapper) {
        var $wrapper = $('.js-video-wrapper');
        var $iframe = $('.js-video-iframe');
      } else {
        var $iframe = $wrapper.find('.js-video-iframe');
      }
      $body.removeClass('is-playing-video');
      $wrapper.removeClass('video-wrapper-active');
      $iframe.attr('src', '');

    }

    function resetTrailers($wrapper){
      $wrapper.find('.trailer-button')
      .removeClass('active')
      .eq(0)
      .addClass('active');
    }


  })();
