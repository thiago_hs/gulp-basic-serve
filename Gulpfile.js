var gulp = require('gulp'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	jshint = require('gulp-jshint'),
	uglify = require('gulp-uglify'),
	imagemin = require('gulp-imagemin'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	notify = require('gulp-notify'),
	cache = require('gulp-cache'),
	livereload = require('gulp-livereload'),
	connect = require('gulp-connect'),
  nunjucksRender = require('gulp-nunjucks-render'),
	lr = require('tiny-lr'),
	server = lr();

const browserSync = require('browser-sync').create();
const reload = browserSync.reload;

var folder = {
  dist:'dist',
  html:{
    src:"src/pages/**/*.html",
    includes:"src/templates"
  },
  styles:{
    scss:"src/assets/scss/**/*.scss",
    css:"src/assets/css/**/*.{css,map}",
    dist:"dist/assets/css"
  },
  fonts:{
    src:"src/assets/fonts/**/*.{ttf,woff,eof,svg,otf}",
    dist:"dist/assets/fonts"
  },
  images:{
    src:"src/assets/img/**/*.{svg,gif,jpg,png}",
    dist:"dist/assets/img"
  },
  scripts:{
    src:"src/assets/js/**/*.js",
    dist:"dist/assets/js"
  }

};

// Server - listed on localhost:8080
gulp.task('webserver', function() {
  connect.server();
});

// Gets .html files in pages
gulp.task('pages', function() {
  // Gets .html and .nunjucks files in pages
  return gulp.src(folder.html.src)
  // Renders template with nunjucks
  .pipe(nunjucksRender({
    path: [folder.html.includes],
    envOptions:{
      tags:{
        variableStart: '@{{',
        variableEnd: '}}',
      }
    }
  }))
  // output files in app folder
  .pipe(gulp.dest(folder.dist))
  .pipe(browserSync.stream());
});

gulp.task('styles', function() {
  return gulp.src(folder.styles.scss)
	.pipe(sass({ style: 'expanded' }))
  .pipe(concat('components.css'))
	.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
	.pipe(gulp.dest(folder.styles.dist))
	.pipe(notify({ message: 'Styles task complete' }))
    .pipe(browserSync.stream());
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
	return gulp.src(folder.scripts.src)
		/*.pipe(concat('script.js'))
		.pipe(gulp.dest(folder.scripts.dist))
		.pipe(rename('script.min.js'))
		.pipe(uglify())*/
		.pipe(gulp.dest(folder.scripts.dist))
        .pipe(browserSync.stream());
});

// Images
gulp.task('images', function() {
  return gulp.src(folder.images.src)
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest(folder.images.dist))
    .pipe(notify({ message: 'Images task complete' }))
    .pipe(browserSync.stream());
});

// Copy files
gulp.task('copy',function(){

  var copyOpts = {prefix:3}
  
  // copy styles
  gulp.src(folder.styles.css)
  .pipe(gulp.dest(folder.styles.dist))

  // copy images
  gulp.src(folder.images.src)
  .pipe(gulp.dest(folder.images.dist))

  // copy fonts
  return gulp.src(folder.fonts.src)
  .pipe(gulp.dest(folder.fonts.dist))
  .pipe(browserSync.stream());
});

gulp.task('serve', ['webserver','styles', 'scripts', 'pages', 'copy'], function() {

  gulp.watch(folder.styles.scss, ['styles']);
  
  // Watch .html files
  gulp.watch([folder.html.src],['pages']);

  // Watch .js files
  gulp.watch(folder.scripts.src, ['scripts']);

  // Watch image files
  gulp.watch(folder.images.src, ['images']);

  // Watch css and fonts files
  gulp.watch([folder.styles.css,folder.fonts.src],['copy']);

  // Create LiveReload server
  var server = livereload();

  // Watch any files in dist/, reload on change
  gulp.watch([folder.styles.dist + '/**',folder.scripts.dist+'/**',folder.images.dist+'/**','*.html']).on('change', function(file) {
	server.changed(file.path);
  });

    browserSync.init({
        server: {
            baseDir: "./" + folder.dist,
            directory: true
        },
        ghostMode: false
    });
});