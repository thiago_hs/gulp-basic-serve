/*!
 * enquire.js v2.1.1 - Awesome Media Queries in JavaScript
 * Copyright (c) 2014 Nick Williams - http://wicky.nillia.ms/enquire.js
 * License: MIT (http://www.opensource.org/licenses/mit-license.php)
 */

!function(a,b,c){var d=window.matchMedia;"undefined"!=typeof module&&module.exports?module.exports=c(d):"function"==typeof define&&define.amd?define(function(){return b[a]=c(d)}):b[a]=c(d)}("enquire",this,function(a){"use strict";function b(a,b){var c,d=0,e=a.length;for(d;e>d&&(c=b(a[d],d),c!==!1);d++);}function c(a){return"[object Array]"===Object.prototype.toString.apply(a)}function d(a){return"function"==typeof a}function e(a){this.options=a,!a.deferSetup&&this.setup()}function f(b,c){this.query=b,this.isUnconditional=c,this.handlers=[],this.mql=a(b);var d=this;this.listener=function(a){d.mql=a,d.assess()},this.mql.addListener(this.listener)}function g(){if(!a)throw new Error("matchMedia not present, legacy browsers require a polyfill");this.queries={},this.browserIsIncapable=!a("only all").matches}return e.prototype={setup:function(){this.options.setup&&this.options.setup(),this.initialised=!0},on:function(){!this.initialised&&this.setup(),this.options.match&&this.options.match()},off:function(){this.options.unmatch&&this.options.unmatch()},destroy:function(){this.options.destroy?this.options.destroy():this.off()},equals:function(a){return this.options===a||this.options.match===a}},f.prototype={addHandler:function(a){var b=new e(a);this.handlers.push(b),this.matches()&&b.on()},removeHandler:function(a){var c=this.handlers;b(c,function(b,d){return b.equals(a)?(b.destroy(),!c.splice(d,1)):void 0})},matches:function(){return this.mql.matches||this.isUnconditional},clear:function(){b(this.handlers,function(a){a.destroy()}),this.mql.removeListener(this.listener),this.handlers.length=0},assess:function(){var a=this.matches()?"on":"off";b(this.handlers,function(b){b[a]()})}},g.prototype={register:function(a,e,g){var h=this.queries,i=g&&this.browserIsIncapable;return h[a]||(h[a]=new f(a,i)),d(e)&&(e={match:e}),c(e)||(e=[e]),b(e,function(b){h[a].addHandler(b)}),this},unregister:function(a,b){var c=this.queries[a];return c&&(b?c.removeHandler(b):(c.clear(),delete this.queries[a])),this}},new g});
!function(a,b,c,d){var e=a(b);a.fn.lazyload=function(f){function g(){var b=0;i.each(function(){var c=a(this);if(!j.skip_invisible||c.is(":visible"))if(a.abovethetop(this,j)||a.leftofbegin(this,j));else if(a.belowthefold(this,j)||a.rightoffold(this,j)){if(++b>j.failure_limit)return!1}else c.trigger("appear"),b=0})}var h,i=this,j={threshold:0,failure_limit:0,event:"scroll",effect:"show",container:b,data_attribute:"original",skip_invisible:!1,appear:null,load:null,placeholder:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"};return f&&(d!==f.failurelimit&&(f.failure_limit=f.failurelimit,delete f.failurelimit),d!==f.effectspeed&&(f.effect_speed=f.effectspeed,delete f.effectspeed),a.extend(j,f)),h=j.container===d||j.container===b?e:a(j.container),0===j.event.indexOf("scroll")&&h.bind(j.event,function(){return g()}),this.each(function(){var b=this,c=a(b);b.loaded=!1,(c.attr("src")===d||c.attr("src")===!1)&&c.is("img")&&c.attr("src",j.placeholder),c.one("appear",function(){if(!this.loaded){if(j.appear){var d=i.length;j.appear.call(b,d,j)}a("<img />").bind("load",function(){var d=c.attr("data-"+j.data_attribute);c.hide(),c.is("img")?c.attr("src",d):c.css("background-image","url('"+d+"')"),c[j.effect](j.effect_speed),b.loaded=!0;var e=a.grep(i,function(a){return!a.loaded});if(i=a(e),j.load){var f=i.length;j.load.call(b,f,j)}}).attr("src",c.attr("data-"+j.data_attribute))}}),0!==j.event.indexOf("scroll")&&c.bind(j.event,function(){b.loaded||c.trigger("appear")})}),e.bind("resize",function(){g()}),/(?:iphone|ipod|ipad).*os 5/gi.test(navigator.appVersion)&&e.bind("pageshow",function(b){b.originalEvent&&b.originalEvent.persisted&&i.each(function(){a(this).trigger("appear")})}),a(c).ready(function(){g()}),this},a.belowthefold=function(c,f){var g;return g=f.container===d||f.container===b?(b.innerHeight?b.innerHeight:e.height())+e.scrollTop():a(f.container).offset().top+a(f.container).height(),g<=a(c).offset().top-f.threshold},a.rightoffold=function(c,f){var g;return g=f.container===d||f.container===b?e.width()+e.scrollLeft():a(f.container).offset().left+a(f.container).width(),g<=a(c).offset().left-f.threshold},a.abovethetop=function(c,f){var g;return g=f.container===d||f.container===b?e.scrollTop():a(f.container).offset().top,g>=a(c).offset().top+f.threshold+a(c).height()},a.leftofbegin=function(c,f){var g;return g=f.container===d||f.container===b?e.scrollLeft():a(f.container).offset().left,g>=a(c).offset().left+f.threshold+a(c).width()},a.inviewport=function(b,c){return!(a.rightoffold(b,c)||a.leftofbegin(b,c)||a.belowthefold(b,c)||a.abovethetop(b,c))},a.extend(a.expr[":"],{"below-the-fold":function(b){return a.belowthefold(b,{threshold:0})},"above-the-top":function(b){return!a.belowthefold(b,{threshold:0})},"right-of-screen":function(b){return a.rightoffold(b,{threshold:0})},"left-of-screen":function(b){return!a.rightoffold(b,{threshold:0})},"in-viewport":function(b){return a.inviewport(b,{threshold:0})},"above-the-fold":function(b){return!a.belowthefold(b,{threshold:0})},"right-of-fold":function(b){return a.rightoffold(b,{threshold:0})},"left-of-fold":function(b){return!a.rightoffold(b,{threshold:0})}})}(jQuery,window,document);
/*!
 * Bootstrap v4.0.0-alpha.2 (http://getbootstrap.com)
 * Copyright 2011-2017 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

if (typeof jQuery === 'undefined') {
  throw new Error('Bootstrap\'s JavaScript requires jQuery')
}

+function ($) {
  var version = $.fn.jquery.split(' ')[0].split('.')
  if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1) || (version[0] >= 3)) {
    throw new Error('Bootstrap\'s JavaScript requires at least jQuery v1.9.1 but less than v3.0.0')
  }
}(jQuery);


+function ($) {

/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-alpha.2): util.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */'use strict';var _get=function get(_x,_x2,_x3){var _again=true;_function: while(_again) {var object=_x,property=_x2,receiver=_x3;_again = false;if(object === null)object = Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc === undefined){var parent=Object.getPrototypeOf(object);if(parent === null){return undefined;}else {_x = parent;_x2 = property;_x3 = receiver;_again = true;desc = parent = undefined;continue _function;}}else if('value' in desc){return desc.value;}else {var getter=desc.get;if(getter === undefined){return undefined;}return getter.call(receiver);}}};var _createClass=(function(){function defineProperties(target,props){for(var i=0;i < props.length;i++) {var descriptor=props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if('value' in descriptor)descriptor.writable = true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};})();function _inherits(subClass,superClass){if(typeof superClass !== 'function' && superClass !== null){throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__ = superClass;}function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError('Cannot call a class as a function');}}var Util=(function($){ /**
   * ------------------------------------------------------------------------
   * Private TransitionEnd Helpers
   * ------------------------------------------------------------------------
   */var transition=false;var TransitionEndEvent={WebkitTransition:'webkitTransitionEnd',MozTransition:'transitionend',OTransition:'oTransitionEnd otransitionend',transition:'transitionend'}; // shoutout AngusCroll (https://goo.gl/pxwQGp)
function toType(obj){return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();}function isElement(obj){return (obj[0] || obj).nodeType;}function getSpecialTransitionEndEvent(){return {bindType:transition.end,delegateType:transition.end,handle:function handle(event){if($(event.target).is(this)){return event.handleObj.handler.apply(this,arguments);}}};}function transitionEndTest(){if(window.QUnit){return false;}var el=document.createElement('bootstrap');for(var _name in TransitionEndEvent) {if(el.style[_name] !== undefined){return {end:TransitionEndEvent[_name]};}}return false;}function transitionEndEmulator(duration){var _this=this;var called=false;$(this).one(Util.TRANSITION_END,function(){called = true;});setTimeout(function(){if(!called){Util.triggerTransitionEnd(_this);}},duration);return this;}function setTransitionEndSupport(){transition = transitionEndTest();$.fn.emulateTransitionEnd = transitionEndEmulator;if(Util.supportsTransitionEnd()){$.event.special[Util.TRANSITION_END] = getSpecialTransitionEndEvent();}} /**
   * --------------------------------------------------------------------------
   * Public Util Api
   * --------------------------------------------------------------------------
   */var Util={TRANSITION_END:'bsTransitionEnd',getUID:function getUID(prefix){do { /* eslint-disable no-bitwise */prefix += ~ ~(Math.random() * 1000000); // "~~" acts like a faster Math.floor() here
/* eslint-enable no-bitwise */}while(document.getElementById(prefix));return prefix;},getSelectorFromElement:function getSelectorFromElement(element){var selector=element.getAttribute('data-target');if(!selector){selector = element.getAttribute('href') || '';selector = /^#[a-z]/i.test(selector)?selector:null;}return selector;},reflow:function reflow(element){new Function('bs','return bs')(element.offsetHeight);},triggerTransitionEnd:function triggerTransitionEnd(element){$(element).trigger(transition.end);},supportsTransitionEnd:function supportsTransitionEnd(){return Boolean(transition);},typeCheckConfig:function typeCheckConfig(componentName,config,configTypes){for(var property in configTypes) {if(configTypes.hasOwnProperty(property)){var expectedTypes=configTypes[property];var value=config[property];var valueType=undefined;if(value && isElement(value)){valueType = 'element';}else {valueType = toType(value);}if(!new RegExp(expectedTypes).test(valueType)){throw new Error(componentName.toUpperCase() + ': ' + ('Option "' + property + '" provided type "' + valueType + '" ') + ('but expected type "' + expectedTypes + '".'));}}}}};setTransitionEndSupport();return Util;})(jQuery); /**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-alpha.2): alert.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */var Alert=(function($){ /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */var NAME='alert';var VERSION='4.0.0-alpha.2';var DATA_KEY='bs.alert';var EVENT_KEY='.' + DATA_KEY;var DATA_API_KEY='.data-api';var JQUERY_NO_CONFLICT=$.fn[NAME];var TRANSITION_DURATION=150;var Selector={DISMISS:'[data-dismiss="alert"]'};var Event={CLOSE:'close' + EVENT_KEY,CLOSED:'closed' + EVENT_KEY,CLICK_DATA_API:'click' + EVENT_KEY + DATA_API_KEY};var ClassName={ALERT:'alert',FADE:'fade',IN:'in'}; /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */var Alert=(function(){function Alert(element){_classCallCheck(this,Alert);this._element = element;} /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */ // getters
_createClass(Alert,[{key:'close', // public
value:function close(element){element = element || this._element;var rootElement=this._getRootElement(element);var customEvent=this._triggerCloseEvent(rootElement);if(customEvent.isDefaultPrevented()){return;}this._removeElement(rootElement);}},{key:'dispose',value:function dispose(){$.removeData(this._element,DATA_KEY);this._element = null;} // private
},{key:'_getRootElement',value:function _getRootElement(element){var selector=Util.getSelectorFromElement(element);var parent=false;if(selector){parent = $(selector)[0];}if(!parent){parent = $(element).closest('.' + ClassName.ALERT)[0];}return parent;}},{key:'_triggerCloseEvent',value:function _triggerCloseEvent(element){var closeEvent=$.Event(Event.CLOSE);$(element).trigger(closeEvent);return closeEvent;}},{key:'_removeElement',value:function _removeElement(element){$(element).removeClass(ClassName.IN);if(!Util.supportsTransitionEnd() || !$(element).hasClass(ClassName.FADE)){this._destroyElement(element);return;}$(element).one(Util.TRANSITION_END,$.proxy(this._destroyElement,this,element)).emulateTransitionEnd(TRANSITION_DURATION);}},{key:'_destroyElement',value:function _destroyElement(element){$(element).detach().trigger(Event.CLOSED).remove();} // static
}],[{key:'_jQueryInterface',value:function _jQueryInterface(config){return this.each(function(){var $element=$(this);var data=$element.data(DATA_KEY);if(!data){data = new Alert(this);$element.data(DATA_KEY,data);}if(config === 'close'){data[config](this);}});}},{key:'_handleDismiss',value:function _handleDismiss(alertInstance){return function(event){if(event){event.preventDefault();}alertInstance.close(this);};}},{key:'VERSION',get:function get(){return VERSION;}}]);return Alert;})();$(document).on(Event.CLICK_DATA_API,Selector.DISMISS,Alert._handleDismiss(new Alert())); /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */$.fn[NAME] = Alert._jQueryInterface;$.fn[NAME].Constructor = Alert;$.fn[NAME].noConflict = function(){$.fn[NAME] = JQUERY_NO_CONFLICT;return Alert._jQueryInterface;};return Alert;})(jQuery); /**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-alpha.2): button.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */var Button=(function($){ /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */var NAME='button';var VERSION='4.0.0-alpha.2';var DATA_KEY='bs.button';var EVENT_KEY='.' + DATA_KEY;var DATA_API_KEY='.data-api';var JQUERY_NO_CONFLICT=$.fn[NAME];var ClassName={ACTIVE:'active',BUTTON:'btn',FOCUS:'focus'};var Selector={DATA_TOGGLE_CARROT:'[data-toggle^="button"]',DATA_TOGGLE:'[data-toggle="buttons"]',INPUT:'input',ACTIVE:'.active',BUTTON:'.btn'};var Event={CLICK_DATA_API:'click' + EVENT_KEY + DATA_API_KEY,FOCUS_BLUR_DATA_API:'focus' + EVENT_KEY + DATA_API_KEY + ' ' + ('blur' + EVENT_KEY + DATA_API_KEY)}; /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */var Button=(function(){function Button(element){_classCallCheck(this,Button);this._element = element;} /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */ // getters
_createClass(Button,[{key:'toggle', // public
value:function toggle(){var triggerChangeEvent=true;var rootElement=$(this._element).closest(Selector.DATA_TOGGLE)[0];if(rootElement){var input=$(this._element).find(Selector.INPUT)[0];if(input){if(input.type === 'radio'){if(input.checked && $(this._element).hasClass(ClassName.ACTIVE)){triggerChangeEvent = false;}else {var activeElement=$(rootElement).find(Selector.ACTIVE)[0];if(activeElement){$(activeElement).removeClass(ClassName.ACTIVE);}}}if(triggerChangeEvent){input.checked = !$(this._element).hasClass(ClassName.ACTIVE);$(this._element).trigger('change');}input.focus();}}else {this._element.setAttribute('aria-pressed',!$(this._element).hasClass(ClassName.ACTIVE));}if(triggerChangeEvent){$(this._element).toggleClass(ClassName.ACTIVE);}}},{key:'dispose',value:function dispose(){$.removeData(this._element,DATA_KEY);this._element = null;} // static
}],[{key:'_jQueryInterface',value:function _jQueryInterface(config){return this.each(function(){var data=$(this).data(DATA_KEY);if(!data){data = new Button(this);$(this).data(DATA_KEY,data);}if(config === 'toggle'){data[config]();}});}},{key:'VERSION',get:function get(){return VERSION;}}]);return Button;})();$(document).on(Event.CLICK_DATA_API,Selector.DATA_TOGGLE_CARROT,function(event){event.preventDefault();var button=event.target;if(!$(button).hasClass(ClassName.BUTTON)){button = $(button).closest(Selector.BUTTON);}Button._jQueryInterface.call($(button),'toggle');}).on(Event.FOCUS_BLUR_DATA_API,Selector.DATA_TOGGLE_CARROT,function(event){var button=$(event.target).closest(Selector.BUTTON)[0];$(button).toggleClass(ClassName.FOCUS,/^focus(in)?$/.test(event.type));}); /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */$.fn[NAME] = Button._jQueryInterface;$.fn[NAME].Constructor = Button;$.fn[NAME].noConflict = function(){$.fn[NAME] = JQUERY_NO_CONFLICT;return Button._jQueryInterface;};return Button;})(jQuery); /**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-alpha.2): carousel.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */var Carousel=(function($){ /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */var NAME='carousel';var VERSION='4.0.0-alpha.2';var DATA_KEY='bs.carousel';var EVENT_KEY='.' + DATA_KEY;var DATA_API_KEY='.data-api';var JQUERY_NO_CONFLICT=$.fn[NAME];var TRANSITION_DURATION=600;var Default={interval:5000,keyboard:true,slide:false,pause:'hover',wrap:true};var DefaultType={interval:'(number|boolean)',keyboard:'boolean',slide:'(boolean|string)',pause:'(string|boolean)',wrap:'boolean'};var Direction={NEXT:'next',PREVIOUS:'prev'};var Event={SLIDE:'slide' + EVENT_KEY,SLID:'slid' + EVENT_KEY,KEYDOWN:'keydown' + EVENT_KEY,MOUSEENTER:'mouseenter' + EVENT_KEY,MOUSELEAVE:'mouseleave' + EVENT_KEY,LOAD_DATA_API:'load' + EVENT_KEY + DATA_API_KEY,CLICK_DATA_API:'click' + EVENT_KEY + DATA_API_KEY};var ClassName={CAROUSEL:'carousel',ACTIVE:'active',SLIDE:'slide',RIGHT:'right',LEFT:'left',ITEM:'carousel-item'};var Selector={ACTIVE:'.active',ACTIVE_ITEM:'.active.carousel-item',ITEM:'.carousel-item',NEXT_PREV:'.next, .prev',INDICATORS:'.carousel-indicators',DATA_SLIDE:'[data-slide], [data-slide-to]',DATA_RIDE:'[data-ride="carousel"]'}; /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */var Carousel=(function(){function Carousel(element,config){_classCallCheck(this,Carousel);this._items = null;this._interval = null;this._activeElement = null;this._isPaused = false;this._isSliding = false;this._config = this._getConfig(config);this._element = $(element)[0];this._indicatorsElement = $(this._element).find(Selector.INDICATORS)[0];this._addEventListeners();} /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */ // getters
_createClass(Carousel,[{key:'next', // public
value:function next(){if(!this._isSliding){this._slide(Direction.NEXT);}}},{key:'nextWhenVisible',value:function nextWhenVisible(){ // Don't call next when the page isn't visible
if(!document.hidden){this.next();}}},{key:'prev',value:function prev(){if(!this._isSliding){this._slide(Direction.PREVIOUS);}}},{key:'pause',value:function pause(event){if(!event){this._isPaused = true;}if($(this._element).find(Selector.NEXT_PREV)[0] && Util.supportsTransitionEnd()){Util.triggerTransitionEnd(this._element);this.cycle(true);}clearInterval(this._interval);this._interval = null;}},{key:'cycle',value:function cycle(event){if(!event){this._isPaused = false;}if(this._interval){clearInterval(this._interval);this._interval = null;}if(this._config.interval && !this._isPaused){this._interval = setInterval($.proxy(document.visibilityState?this.nextWhenVisible:this.next,this),this._config.interval);}}},{key:'to',value:function to(index){var _this2=this;this._activeElement = $(this._element).find(Selector.ACTIVE_ITEM)[0];var activeIndex=this._getItemIndex(this._activeElement);if(index > this._items.length - 1 || index < 0){return;}if(this._isSliding){$(this._element).one(Event.SLID,function(){return _this2.to(index);});return;}if(activeIndex === index){this.pause();this.cycle();return;}var direction=index > activeIndex?Direction.NEXT:Direction.PREVIOUS;this._slide(direction,this._items[index]);}},{key:'dispose',value:function dispose(){$(this._element).off(EVENT_KEY);$.removeData(this._element,DATA_KEY);this._items = null;this._config = null;this._element = null;this._interval = null;this._isPaused = null;this._isSliding = null;this._activeElement = null;this._indicatorsElement = null;} // private
},{key:'_getConfig',value:function _getConfig(config){config = $.extend({},Default,config);Util.typeCheckConfig(NAME,config,DefaultType);return config;}},{key:'_addEventListeners',value:function _addEventListeners(){if(this._config.keyboard){$(this._element).on(Event.KEYDOWN,$.proxy(this._keydown,this));}if(this._config.pause === 'hover' && !('ontouchstart' in document.documentElement)){$(this._element).on(Event.MOUSEENTER,$.proxy(this.pause,this)).on(Event.MOUSELEAVE,$.proxy(this.cycle,this));}}},{key:'_keydown',value:function _keydown(event){event.preventDefault();if(/input|textarea/i.test(event.target.tagName)){return;}switch(event.which){case 37:this.prev();break;case 39:this.next();break;default:return;}}},{key:'_getItemIndex',value:function _getItemIndex(element){this._items = $.makeArray($(element).parent().find(Selector.ITEM));return this._items.indexOf(element);}},{key:'_getItemByDirection',value:function _getItemByDirection(direction,activeElement){var isNextDirection=direction === Direction.NEXT;var isPrevDirection=direction === Direction.PREVIOUS;var activeIndex=this._getItemIndex(activeElement);var lastItemIndex=this._items.length - 1;var isGoingToWrap=isPrevDirection && activeIndex === 0 || isNextDirection && activeIndex === lastItemIndex;if(isGoingToWrap && !this._config.wrap){return activeElement;}var delta=direction === Direction.PREVIOUS?-1:1;var itemIndex=(activeIndex + delta) % this._items.length;return itemIndex === -1?this._items[this._items.length - 1]:this._items[itemIndex];}},{key:'_triggerSlideEvent',value:function _triggerSlideEvent(relatedTarget,directionalClassname){var slideEvent=$.Event(Event.SLIDE,{relatedTarget:relatedTarget,direction:directionalClassname});$(this._element).trigger(slideEvent);return slideEvent;}},{key:'_setActiveIndicatorElement',value:function _setActiveIndicatorElement(element){if(this._indicatorsElement){$(this._indicatorsElement).find(Selector.ACTIVE).removeClass(ClassName.ACTIVE);var nextIndicator=this._indicatorsElement.children[this._getItemIndex(element)];if(nextIndicator){$(nextIndicator).addClass(ClassName.ACTIVE);}}}},{key:'_slide',value:function _slide(direction,element){var _this3=this;var activeElement=$(this._element).find(Selector.ACTIVE_ITEM)[0];var nextElement=element || activeElement && this._getItemByDirection(direction,activeElement);var isCycling=Boolean(this._interval);var directionalClassName=direction === Direction.NEXT?ClassName.LEFT:ClassName.RIGHT;if(nextElement && $(nextElement).hasClass(ClassName.ACTIVE)){this._isSliding = false;return;}var slideEvent=this._triggerSlideEvent(nextElement,directionalClassName);if(slideEvent.isDefaultPrevented()){return;}if(!activeElement || !nextElement){ // some weirdness is happening, so we bail
return;}this._isSliding = true;if(isCycling){this.pause();}this._setActiveIndicatorElement(nextElement);var slidEvent=$.Event(Event.SLID,{relatedTarget:nextElement,direction:directionalClassName});if(Util.supportsTransitionEnd() && $(this._element).hasClass(ClassName.SLIDE)){$(nextElement).addClass(direction);Util.reflow(nextElement);$(activeElement).addClass(directionalClassName);$(nextElement).addClass(directionalClassName);$(activeElement).one(Util.TRANSITION_END,function(){$(nextElement).removeClass(directionalClassName).removeClass(direction);$(nextElement).addClass(ClassName.ACTIVE);$(activeElement).removeClass(ClassName.ACTIVE).removeClass(direction).removeClass(directionalClassName);_this3._isSliding = false;setTimeout(function(){return $(_this3._element).trigger(slidEvent);},0);}).emulateTransitionEnd(TRANSITION_DURATION);}else {$(activeElement).removeClass(ClassName.ACTIVE);$(nextElement).addClass(ClassName.ACTIVE);this._isSliding = false;$(this._element).trigger(slidEvent);}if(isCycling){this.cycle();}} // static
}],[{key:'_jQueryInterface',value:function _jQueryInterface(config){return this.each(function(){var data=$(this).data(DATA_KEY);var _config=$.extend({},Default,$(this).data());if(typeof config === 'object'){$.extend(_config,config);}var action=typeof config === 'string'?config:_config.slide;if(!data){data = new Carousel(this,_config);$(this).data(DATA_KEY,data);}if(typeof config === 'number'){data.to(config);}else if(typeof action === 'string'){if(data[action] === undefined){throw new Error('No method named "' + action + '"');}data[action]();}else if(_config.interval){data.pause();data.cycle();}});}},{key:'_dataApiClickHandler',value:function _dataApiClickHandler(event){var selector=Util.getSelectorFromElement(this);if(!selector){return;}var target=$(selector)[0];if(!target || !$(target).hasClass(ClassName.CAROUSEL)){return;}var config=$.extend({},$(target).data(),$(this).data());var slideIndex=this.getAttribute('data-slide-to');if(slideIndex){config.interval = false;}Carousel._jQueryInterface.call($(target),config);if(slideIndex){$(target).data(DATA_KEY).to(slideIndex);}event.preventDefault();}},{key:'VERSION',get:function get(){return VERSION;}},{key:'Default',get:function get(){return Default;}}]);return Carousel;})();$(document).on(Event.CLICK_DATA_API,Selector.DATA_SLIDE,Carousel._dataApiClickHandler);$(window).on(Event.LOAD_DATA_API,function(){$(Selector.DATA_RIDE).each(function(){var $carousel=$(this);Carousel._jQueryInterface.call($carousel,$carousel.data());});}); /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */$.fn[NAME] = Carousel._jQueryInterface;$.fn[NAME].Constructor = Carousel;$.fn[NAME].noConflict = function(){$.fn[NAME] = JQUERY_NO_CONFLICT;return Carousel._jQueryInterface;};return Carousel;})(jQuery); /**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-alpha.2): collapse.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */var Collapse=(function($){ /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */var NAME='collapse';var VERSION='4.0.0-alpha.2';var DATA_KEY='bs.collapse';var EVENT_KEY='.' + DATA_KEY;var DATA_API_KEY='.data-api';var JQUERY_NO_CONFLICT=$.fn[NAME];var TRANSITION_DURATION=600;var Default={toggle:true,parent:''};var DefaultType={toggle:'boolean',parent:'string'};var Event={SHOW:'show' + EVENT_KEY,SHOWN:'shown' + EVENT_KEY,HIDE:'hide' + EVENT_KEY,HIDDEN:'hidden' + EVENT_KEY,CLICK_DATA_API:'click' + EVENT_KEY + DATA_API_KEY};var ClassName={IN:'in',COLLAPSE:'collapse',COLLAPSING:'collapsing',COLLAPSED:'collapsed'};var Dimension={WIDTH:'width',HEIGHT:'height'};var Selector={ACTIVES:'.panel > .in, .panel > .collapsing',DATA_TOGGLE:'[data-toggle="collapse"]'}; /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */var Collapse=(function(){function Collapse(element,config){_classCallCheck(this,Collapse);this._isTransitioning = false;this._element = element;this._config = this._getConfig(config);this._triggerArray = $.makeArray($('[data-toggle="collapse"][href="#' + element.id + '"],' + ('[data-toggle="collapse"][data-target="#' + element.id + '"]')));this._parent = this._config.parent?this._getParent():null;if(!this._config.parent){this._addAriaAndCollapsedClass(this._element,this._triggerArray);}if(this._config.toggle){this.toggle();}} /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */ // getters
_createClass(Collapse,[{key:'toggle', // public
value:function toggle(){if($(this._element).hasClass(ClassName.IN)){this.hide();}else {this.show();}}},{key:'show',value:function show(){var _this4=this;if(this._isTransitioning || $(this._element).hasClass(ClassName.IN)){return;}var actives=undefined;var activesData=undefined;if(this._parent){actives = $.makeArray($(Selector.ACTIVES));if(!actives.length){actives = null;}}if(actives){activesData = $(actives).data(DATA_KEY);if(activesData && activesData._isTransitioning){return;}}var startEvent=$.Event(Event.SHOW);$(this._element).trigger(startEvent);if(startEvent.isDefaultPrevented()){return;}if(actives){Collapse._jQueryInterface.call($(actives),'hide');if(!activesData){$(actives).data(DATA_KEY,null);}}var dimension=this._getDimension();$(this._element).removeClass(ClassName.COLLAPSE).addClass(ClassName.COLLAPSING);this._element.style[dimension] = 0;this._element.setAttribute('aria-expanded',true);if(this._triggerArray.length){$(this._triggerArray).removeClass(ClassName.COLLAPSED).attr('aria-expanded',true);}this.setTransitioning(true);var complete=function complete(){$(_this4._element).removeClass(ClassName.COLLAPSING).addClass(ClassName.COLLAPSE).addClass(ClassName.IN);_this4._element.style[dimension] = '';_this4.setTransitioning(false);$(_this4._element).trigger(Event.SHOWN);};if(!Util.supportsTransitionEnd()){complete();return;}var capitalizedDimension=dimension[0].toUpperCase() + dimension.slice(1);var scrollSize='scroll' + capitalizedDimension;$(this._element).one(Util.TRANSITION_END,complete).emulateTransitionEnd(TRANSITION_DURATION);this._element.style[dimension] = this._element[scrollSize] + 'px';}},{key:'hide',value:function hide(){var _this5=this;if(this._isTransitioning || !$(this._element).hasClass(ClassName.IN)){return;}var startEvent=$.Event(Event.HIDE);$(this._element).trigger(startEvent);if(startEvent.isDefaultPrevented()){return;}var dimension=this._getDimension();var offsetDimension=dimension === Dimension.WIDTH?'offsetWidth':'offsetHeight';this._element.style[dimension] = this._element[offsetDimension] + 'px';Util.reflow(this._element);$(this._element).addClass(ClassName.COLLAPSING).removeClass(ClassName.COLLAPSE).removeClass(ClassName.IN);this._element.setAttribute('aria-expanded',false);if(this._triggerArray.length){$(this._triggerArray).addClass(ClassName.COLLAPSED).attr('aria-expanded',false);}this.setTransitioning(true);var complete=function complete(){_this5.setTransitioning(false);$(_this5._element).removeClass(ClassName.COLLAPSING).addClass(ClassName.COLLAPSE).trigger(Event.HIDDEN);};this._element.style[dimension] = 0;if(!Util.supportsTransitionEnd()){complete();return;}$(this._element).one(Util.TRANSITION_END,complete).emulateTransitionEnd(TRANSITION_DURATION);}},{key:'setTransitioning',value:function setTransitioning(isTransitioning){this._isTransitioning = isTransitioning;}},{key:'dispose',value:function dispose(){$.removeData(this._element,DATA_KEY);this._config = null;this._parent = null;this._element = null;this._triggerArray = null;this._isTransitioning = null;} // private
},{key:'_getConfig',value:function _getConfig(config){config = $.extend({},Default,config);config.toggle = Boolean(config.toggle); // coerce string values
Util.typeCheckConfig(NAME,config,DefaultType);return config;}},{key:'_getDimension',value:function _getDimension(){var hasWidth=$(this._element).hasClass(Dimension.WIDTH);return hasWidth?Dimension.WIDTH:Dimension.HEIGHT;}},{key:'_getParent',value:function _getParent(){var _this6=this;var parent=$(this._config.parent)[0];var selector='[data-toggle="collapse"][data-parent="' + this._config.parent + '"]';$(parent).find(selector).each(function(i,element){_this6._addAriaAndCollapsedClass(Collapse._getTargetFromElement(element),[element]);});return parent;}},{key:'_addAriaAndCollapsedClass',value:function _addAriaAndCollapsedClass(element,triggerArray){if(element){var isOpen=$(element).hasClass(ClassName.IN);element.setAttribute('aria-expanded',isOpen);if(triggerArray.length){$(triggerArray).toggleClass(ClassName.COLLAPSED,!isOpen).attr('aria-expanded',isOpen);}}} // static
}],[{key:'_getTargetFromElement',value:function _getTargetFromElement(element){var selector=Util.getSelectorFromElement(element);return selector?$(selector)[0]:null;}},{key:'_jQueryInterface',value:function _jQueryInterface(config){return this.each(function(){var $this=$(this);var data=$this.data(DATA_KEY);var _config=$.extend({},Default,$this.data(),typeof config === 'object' && config);if(!data && _config.toggle && /show|hide/.test(config)){_config.toggle = false;}if(!data){data = new Collapse(this,_config);$this.data(DATA_KEY,data);}if(typeof config === 'string'){if(data[config] === undefined){throw new Error('No method named "' + config + '"');}data[config]();}});}},{key:'VERSION',get:function get(){return VERSION;}},{key:'Default',get:function get(){return Default;}}]);return Collapse;})();$(document).on(Event.CLICK_DATA_API,Selector.DATA_TOGGLE,function(event){event.preventDefault();var target=Collapse._getTargetFromElement(this);var data=$(target).data(DATA_KEY);var config=data?'toggle':$(this).data();Collapse._jQueryInterface.call($(target),config);}); /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */$.fn[NAME] = Collapse._jQueryInterface;$.fn[NAME].Constructor = Collapse;$.fn[NAME].noConflict = function(){$.fn[NAME] = JQUERY_NO_CONFLICT;return Collapse._jQueryInterface;};return Collapse;})(jQuery); /**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-alpha.2): dropdown.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */var Dropdown=(function($){ /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */var NAME='dropdown';var VERSION='4.0.0-alpha.2';var DATA_KEY='bs.dropdown';var EVENT_KEY='.' + DATA_KEY;var DATA_API_KEY='.data-api';var JQUERY_NO_CONFLICT=$.fn[NAME];var Event={HIDE:'hide' + EVENT_KEY,HIDDEN:'hidden' + EVENT_KEY,SHOW:'show' + EVENT_KEY,SHOWN:'shown' + EVENT_KEY,CLICK:'click' + EVENT_KEY,CLICK_DATA_API:'click' + EVENT_KEY + DATA_API_KEY,KEYDOWN_DATA_API:'keydown' + EVENT_KEY + DATA_API_KEY};var ClassName={BACKDROP:'dropdown-backdrop',DISABLED:'disabled',OPEN:'open'};var Selector={BACKDROP:'.dropdown-backdrop',DATA_TOGGLE:'[data-toggle="dropdown"]',FORM_CHILD:'.dropdown form',ROLE_MENU:'[role="menu"]',ROLE_LISTBOX:'[role="listbox"]',NAVBAR_NAV:'.navbar-nav',VISIBLE_ITEMS:'[role="menu"] li:not(.disabled) a, ' + '[role="listbox"] li:not(.disabled) a'}; /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */var Dropdown=(function(){function Dropdown(element){_classCallCheck(this,Dropdown);this._element = element;this._addEventListeners();} /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */ // getters
_createClass(Dropdown,[{key:'toggle', // public
value:function toggle(){if(this.disabled || $(this).hasClass(ClassName.DISABLED)){return false;}var parent=Dropdown._getParentFromElement(this);var isActive=$(parent).hasClass(ClassName.OPEN);Dropdown._clearMenus();if(isActive){return false;}if('ontouchstart' in document.documentElement && !$(parent).closest(Selector.NAVBAR_NAV).length){ // if mobile we use a backdrop because click events don't delegate
var dropdown=document.createElement('div');dropdown.className = ClassName.BACKDROP;$(dropdown).insertBefore(this);$(dropdown).on('click',Dropdown._clearMenus);}var relatedTarget={relatedTarget:this};var showEvent=$.Event(Event.SHOW,relatedTarget);$(parent).trigger(showEvent);if(showEvent.isDefaultPrevented()){return false;}this.focus();this.setAttribute('aria-expanded','true');$(parent).toggleClass(ClassName.OPEN);$(parent).trigger($.Event(Event.SHOWN,relatedTarget));return false;}},{key:'dispose',value:function dispose(){$.removeData(this._element,DATA_KEY);$(this._element).off(EVENT_KEY);this._element = null;} // private
},{key:'_addEventListeners',value:function _addEventListeners(){$(this._element).on(Event.CLICK,this.toggle);} // static
}],[{key:'_jQueryInterface',value:function _jQueryInterface(config){return this.each(function(){var data=$(this).data(DATA_KEY);if(!data){$(this).data(DATA_KEY,data = new Dropdown(this));}if(typeof config === 'string'){if(data[config] === undefined){throw new Error('No method named "' + config + '"');}data[config].call(this);}});}},{key:'_clearMenus',value:function _clearMenus(event){if(event && event.which === 3){return;}var backdrop=$(Selector.BACKDROP)[0];if(backdrop){backdrop.parentNode.removeChild(backdrop);}var toggles=$.makeArray($(Selector.DATA_TOGGLE));for(var i=0;i < toggles.length;i++) {var _parent=Dropdown._getParentFromElement(toggles[i]);var relatedTarget={relatedTarget:toggles[i]};if(!$(_parent).hasClass(ClassName.OPEN)){continue;}if(event && event.type === 'click' && /input|textarea/i.test(event.target.tagName) && $.contains(_parent,event.target)){continue;}var hideEvent=$.Event(Event.HIDE,relatedTarget);$(_parent).trigger(hideEvent);if(hideEvent.isDefaultPrevented()){continue;}toggles[i].setAttribute('aria-expanded','false');$(_parent).removeClass(ClassName.OPEN).trigger($.Event(Event.HIDDEN,relatedTarget));}}},{key:'_getParentFromElement',value:function _getParentFromElement(element){var parent=undefined;var selector=Util.getSelectorFromElement(element);if(selector){parent = $(selector)[0];}return parent || element.parentNode;}},{key:'_dataApiKeydownHandler',value:function _dataApiKeydownHandler(event){if(!/(38|40|27|32)/.test(event.which) || /input|textarea/i.test(event.target.tagName)){return;}event.preventDefault();event.stopPropagation();if(this.disabled || $(this).hasClass(ClassName.DISABLED)){return;}var parent=Dropdown._getParentFromElement(this);var isActive=$(parent).hasClass(ClassName.OPEN);if(!isActive && event.which !== 27 || isActive && event.which === 27){if(event.which === 27){var toggle=$(parent).find(Selector.DATA_TOGGLE)[0];$(toggle).trigger('focus');}$(this).trigger('click');return;}var items=$.makeArray($(Selector.VISIBLE_ITEMS));items = items.filter(function(item){return item.offsetWidth || item.offsetHeight;});if(!items.length){return;}var index=items.indexOf(event.target);if(event.which === 38 && index > 0){ // up
index--;}if(event.which === 40 && index < items.length - 1){ // down
index++;}if(index < 0){index = 0;}items[index].focus();}},{key:'VERSION',get:function get(){return VERSION;}}]);return Dropdown;})();$(document).on(Event.KEYDOWN_DATA_API,Selector.DATA_TOGGLE,Dropdown._dataApiKeydownHandler).on(Event.KEYDOWN_DATA_API,Selector.ROLE_MENU,Dropdown._dataApiKeydownHandler).on(Event.KEYDOWN_DATA_API,Selector.ROLE_LISTBOX,Dropdown._dataApiKeydownHandler).on(Event.CLICK_DATA_API,Dropdown._clearMenus).on(Event.CLICK_DATA_API,Selector.DATA_TOGGLE,Dropdown.prototype.toggle).on(Event.CLICK_DATA_API,Selector.FORM_CHILD,function(e){e.stopPropagation();}); /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */$.fn[NAME] = Dropdown._jQueryInterface;$.fn[NAME].Constructor = Dropdown;$.fn[NAME].noConflict = function(){$.fn[NAME] = JQUERY_NO_CONFLICT;return Dropdown._jQueryInterface;};return Dropdown;})(jQuery); /**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-alpha.2): modal.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */var Modal=(function($){ /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */var NAME='modal';var VERSION='4.0.0-alpha.2';var DATA_KEY='bs.modal';var EVENT_KEY='.' + DATA_KEY;var DATA_API_KEY='.data-api';var JQUERY_NO_CONFLICT=$.fn[NAME];var TRANSITION_DURATION=300;var BACKDROP_TRANSITION_DURATION=150;var Default={backdrop:true,keyboard:true,focus:true,show:true};var DefaultType={backdrop:'(boolean|string)',keyboard:'boolean',focus:'boolean',show:'boolean'};var Event={HIDE:'hide' + EVENT_KEY,HIDDEN:'hidden' + EVENT_KEY,SHOW:'show' + EVENT_KEY,SHOWN:'shown' + EVENT_KEY,FOCUSIN:'focusin' + EVENT_KEY,RESIZE:'resize' + EVENT_KEY,CLICK_DISMISS:'click.dismiss' + EVENT_KEY,KEYDOWN_DISMISS:'keydown.dismiss' + EVENT_KEY,MOUSEUP_DISMISS:'mouseup.dismiss' + EVENT_KEY,MOUSEDOWN_DISMISS:'mousedown.dismiss' + EVENT_KEY,CLICK_DATA_API:'click' + EVENT_KEY + DATA_API_KEY};var ClassName={SCROLLBAR_MEASURER:'modal-scrollbar-measure',BACKDROP:'modal-backdrop',OPEN:'modal-open',FADE:'fade',IN:'in'};var Selector={DIALOG:'.modal-dialog',DATA_TOGGLE:'[data-toggle="modal"]',DATA_DISMISS:'[data-dismiss="modal"]',FIXED_CONTENT:'.navbar-fixed-top, .navbar-fixed-bottom, .is-fixed'}; /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */var Modal=(function(){function Modal(element,config){_classCallCheck(this,Modal);this._config = this._getConfig(config);this._element = element;this._dialog = $(element).find(Selector.DIALOG)[0];this._backdrop = null;this._isShown = false;this._isBodyOverflowing = false;this._ignoreBackdropClick = false;this._originalBodyPadding = 0;this._scrollbarWidth = 0;} /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */ // getters
_createClass(Modal,[{key:'toggle', // public
value:function toggle(relatedTarget){return this._isShown?this.hide():this.show(relatedTarget);}},{key:'show',value:function show(relatedTarget){var _this7=this;var showEvent=$.Event(Event.SHOW,{relatedTarget:relatedTarget});$(this._element).trigger(showEvent);if(this._isShown || showEvent.isDefaultPrevented()){return;}this._isShown = true;this._checkScrollbar();this._setScrollbar();$(document.body).addClass(ClassName.OPEN);this._setEscapeEvent();this._setResizeEvent();$(this._element).on(Event.CLICK_DISMISS,Selector.DATA_DISMISS,$.proxy(this.hide,this));$(this._dialog).on(Event.MOUSEDOWN_DISMISS,function(){$(_this7._element).one(Event.MOUSEUP_DISMISS,function(event){if($(event.target).is(_this7._element)){_this7._ignoreBackdropClick = true;}});});this._showBackdrop($.proxy(this._showElement,this,relatedTarget));}},{key:'hide',value:function hide(event){if(event){event.preventDefault();}var hideEvent=$.Event(Event.HIDE);$(this._element).trigger(hideEvent);if(!this._isShown || hideEvent.isDefaultPrevented()){return;}this._isShown = false;this._setEscapeEvent();this._setResizeEvent();$(document).off(Event.FOCUSIN);$(this._element).removeClass(ClassName.IN);$(this._element).off(Event.CLICK_DISMISS);$(this._dialog).off(Event.MOUSEDOWN_DISMISS);if(Util.supportsTransitionEnd() && $(this._element).hasClass(ClassName.FADE)){$(this._element).one(Util.TRANSITION_END,$.proxy(this._hideModal,this)).emulateTransitionEnd(TRANSITION_DURATION);}else {this._hideModal();}}},{key:'dispose',value:function dispose(){$.removeData(this._element,DATA_KEY);$(window).off(EVENT_KEY);$(document).off(EVENT_KEY);$(this._element).off(EVENT_KEY);$(this._backdrop).off(EVENT_KEY);this._config = null;this._element = null;this._dialog = null;this._backdrop = null;this._isShown = null;this._isBodyOverflowing = null;this._ignoreBackdropClick = null;this._originalBodyPadding = null;this._scrollbarWidth = null;} // private
},{key:'_getConfig',value:function _getConfig(config){config = $.extend({},Default,config);Util.typeCheckConfig(NAME,config,DefaultType);return config;}},{key:'_showElement',value:function _showElement(relatedTarget){var _this8=this;var transition=Util.supportsTransitionEnd() && $(this._element).hasClass(ClassName.FADE);if(!this._element.parentNode || this._element.parentNode.nodeType !== Node.ELEMENT_NODE){ // don't move modals dom position
document.body.appendChild(this._element);}this._element.style.display = 'block';this._element.removeAttribute('aria-hidden');this._element.scrollTop = 0;if(transition){Util.reflow(this._element);}$(this._element).addClass(ClassName.IN);if(this._config.focus){this._enforceFocus();}var shownEvent=$.Event(Event.SHOWN,{relatedTarget:relatedTarget});var transitionComplete=function transitionComplete(){if(_this8._config.focus){_this8._element.focus();}$(_this8._element).trigger(shownEvent);};if(transition){$(this._dialog).one(Util.TRANSITION_END,transitionComplete).emulateTransitionEnd(TRANSITION_DURATION);}else {transitionComplete();}}},{key:'_enforceFocus',value:function _enforceFocus(){var _this9=this;$(document).off(Event.FOCUSIN) // guard against infinite focus loop
.on(Event.FOCUSIN,function(event){if(document !== event.target && _this9._element !== event.target && !$(_this9._element).has(event.target).length){_this9._element.focus();}});}},{key:'_setEscapeEvent',value:function _setEscapeEvent(){var _this10=this;if(this._isShown && this._config.keyboard){$(this._element).on(Event.KEYDOWN_DISMISS,function(event){if(event.which === 27){_this10.hide();}});}else if(!this._isShown){$(this._element).off(Event.KEYDOWN_DISMISS);}}},{key:'_setResizeEvent',value:function _setResizeEvent(){if(this._isShown){$(window).on(Event.RESIZE,$.proxy(this._handleUpdate,this));}else {$(window).off(Event.RESIZE);}}},{key:'_hideModal',value:function _hideModal(){var _this11=this;this._element.style.display = 'none';this._element.setAttribute('aria-hidden','true');this._showBackdrop(function(){$(document.body).removeClass(ClassName.OPEN);_this11._resetAdjustments();_this11._resetScrollbar();$(_this11._element).trigger(Event.HIDDEN);});}},{key:'_removeBackdrop',value:function _removeBackdrop(){if(this._backdrop){$(this._backdrop).remove();this._backdrop = null;}}},{key:'_showBackdrop',value:function _showBackdrop(callback){var _this12=this;var animate=$(this._element).hasClass(ClassName.FADE)?ClassName.FADE:'';if(this._isShown && this._config.backdrop){var doAnimate=Util.supportsTransitionEnd() && animate;this._backdrop = document.createElement('div');this._backdrop.className = ClassName.BACKDROP;if(animate){$(this._backdrop).addClass(animate);}$(this._backdrop).appendTo(document.body);$(this._element).on(Event.CLICK_DISMISS,function(event){if(_this12._ignoreBackdropClick){_this12._ignoreBackdropClick = false;return;}if(event.target !== event.currentTarget){return;}if(_this12._config.backdrop === 'static'){_this12._element.focus();}else {_this12.hide();}});if(doAnimate){Util.reflow(this._backdrop);}$(this._backdrop).addClass(ClassName.IN);if(!callback){return;}if(!doAnimate){callback();return;}$(this._backdrop).one(Util.TRANSITION_END,callback).emulateTransitionEnd(BACKDROP_TRANSITION_DURATION);}else if(!this._isShown && this._backdrop){$(this._backdrop).removeClass(ClassName.IN);var callbackRemove=function callbackRemove(){_this12._removeBackdrop();if(callback){callback();}};if(Util.supportsTransitionEnd() && $(this._element).hasClass(ClassName.FADE)){$(this._backdrop).one(Util.TRANSITION_END,callbackRemove).emulateTransitionEnd(BACKDROP_TRANSITION_DURATION);}else {callbackRemove();}}else if(callback){callback();}} // ----------------------------------------------------------------------
// the following methods are used to handle overflowing modals
// todo (fat): these should probably be refactored out of modal.js
// ----------------------------------------------------------------------
},{key:'_handleUpdate',value:function _handleUpdate(){this._adjustDialog();}},{key:'_adjustDialog',value:function _adjustDialog(){var isModalOverflowing=this._element.scrollHeight > document.documentElement.clientHeight;if(!this._isBodyOverflowing && isModalOverflowing){this._element.style.paddingLeft = this._scrollbarWidth + 'px';}if(this._isBodyOverflowing && !isModalOverflowing){this._element.style.paddingRight = this._scrollbarWidth + 'px~';}}},{key:'_resetAdjustments',value:function _resetAdjustments(){this._element.style.paddingLeft = '';this._element.style.paddingRight = '';}},{key:'_checkScrollbar',value:function _checkScrollbar(){this._isBodyOverflowing = document.body.clientWidth < window.innerWidth;this._scrollbarWidth = this._getScrollbarWidth();}},{key:'_setScrollbar',value:function _setScrollbar(){var bodyPadding=parseInt($(Selector.FIXED_CONTENT).css('padding-right') || 0,10);this._originalBodyPadding = document.body.style.paddingRight || '';if(this._isBodyOverflowing){document.body.style.paddingRight = bodyPadding + this._scrollbarWidth + 'px';}}},{key:'_resetScrollbar',value:function _resetScrollbar(){document.body.style.paddingRight = this._originalBodyPadding;}},{key:'_getScrollbarWidth',value:function _getScrollbarWidth(){ // thx d.walsh
var scrollDiv=document.createElement('div');scrollDiv.className = ClassName.SCROLLBAR_MEASURER;document.body.appendChild(scrollDiv);var scrollbarWidth=scrollDiv.offsetWidth - scrollDiv.clientWidth;document.body.removeChild(scrollDiv);return scrollbarWidth;} // static
}],[{key:'_jQueryInterface',value:function _jQueryInterface(config,relatedTarget){return this.each(function(){var data=$(this).data(DATA_KEY);var _config=$.extend({},Modal.Default,$(this).data(),typeof config === 'object' && config);if(!data){data = new Modal(this,_config);$(this).data(DATA_KEY,data);}if(typeof config === 'string'){if(data[config] === undefined){throw new Error('No method named "' + config + '"');}data[config](relatedTarget);}else if(_config.show){data.show(relatedTarget);}});}},{key:'VERSION',get:function get(){return VERSION;}},{key:'Default',get:function get(){return Default;}}]);return Modal;})();$(document).on(Event.CLICK_DATA_API,Selector.DATA_TOGGLE,function(event){var _this13=this;var target=undefined;var selector=Util.getSelectorFromElement(this);if(selector){target = $(selector)[0];}var config=$(target).data(DATA_KEY)?'toggle':$.extend({},$(target).data(),$(this).data());if(this.tagName === 'A'){event.preventDefault();}var $target=$(target).one(Event.SHOW,function(showEvent){if(showEvent.isDefaultPrevented()){ // only register focus restorer if modal will actually get shown
return;}$target.one(Event.HIDDEN,function(){if($(_this13).is(':visible')){_this13.focus();}});});Modal._jQueryInterface.call($(target),config,this);}); /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */$.fn[NAME] = Modal._jQueryInterface;$.fn[NAME].Constructor = Modal;$.fn[NAME].noConflict = function(){$.fn[NAME] = JQUERY_NO_CONFLICT;return Modal._jQueryInterface;};return Modal;})(jQuery); /**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-alpha.2): scrollspy.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */var ScrollSpy=(function($){ /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */var NAME='scrollspy';var VERSION='4.0.0-alpha.2';var DATA_KEY='bs.scrollspy';var EVENT_KEY='.' + DATA_KEY;var DATA_API_KEY='.data-api';var JQUERY_NO_CONFLICT=$.fn[NAME];var Default={offset:10,method:'auto',target:''};var DefaultType={offset:'number',method:'string',target:'(string|element)'};var Event={ACTIVATE:'activate' + EVENT_KEY,SCROLL:'scroll' + EVENT_KEY,LOAD_DATA_API:'load' + EVENT_KEY + DATA_API_KEY};var ClassName={DROPDOWN_ITEM:'dropdown-item',DROPDOWN_MENU:'dropdown-menu',NAV_LINK:'nav-link',NAV:'nav',ACTIVE:'active'};var Selector={DATA_SPY:'[data-spy="scroll"]',ACTIVE:'.active',LIST_ITEM:'.list-item',LI:'li',LI_DROPDOWN:'li.dropdown',NAV_LINKS:'.nav-link',DROPDOWN:'.dropdown',DROPDOWN_ITEMS:'.dropdown-item',DROPDOWN_TOGGLE:'.dropdown-toggle'};var OffsetMethod={OFFSET:'offset',POSITION:'position'}; /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */var ScrollSpy=(function(){function ScrollSpy(element,config){_classCallCheck(this,ScrollSpy);this._element = element;this._scrollElement = element.tagName === 'BODY'?window:element;this._config = this._getConfig(config);this._selector = this._config.target + ' ' + Selector.NAV_LINKS + ',' + (this._config.target + ' ' + Selector.DROPDOWN_ITEMS);this._offsets = [];this._targets = [];this._activeTarget = null;this._scrollHeight = 0;$(this._scrollElement).on(Event.SCROLL,$.proxy(this._process,this));this.refresh();this._process();} /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */ // getters
_createClass(ScrollSpy,[{key:'refresh', // public
value:function refresh(){var _this14=this;var autoMethod=this._scrollElement !== this._scrollElement.window?OffsetMethod.POSITION:OffsetMethod.OFFSET;var offsetMethod=this._config.method === 'auto'?autoMethod:this._config.method;var offsetBase=offsetMethod === OffsetMethod.POSITION?this._getScrollTop():0;this._offsets = [];this._targets = [];this._scrollHeight = this._getScrollHeight();var targets=$.makeArray($(this._selector));targets.map(function(element){var target=undefined;var targetSelector=Util.getSelectorFromElement(element);if(targetSelector){target = $(targetSelector)[0];}if(target && (target.offsetWidth || target.offsetHeight)){ // todo (fat): remove sketch reliance on jQuery position/offset
return [$(target)[offsetMethod]().top + offsetBase,targetSelector];}}).filter(function(item){return item;}).sort(function(a,b){return a[0] - b[0];}).forEach(function(item){_this14._offsets.push(item[0]);_this14._targets.push(item[1]);});}},{key:'dispose',value:function dispose(){$.removeData(this._element,DATA_KEY);$(this._scrollElement).off(EVENT_KEY);this._element = null;this._scrollElement = null;this._config = null;this._selector = null;this._offsets = null;this._targets = null;this._activeTarget = null;this._scrollHeight = null;} // private
},{key:'_getConfig',value:function _getConfig(config){config = $.extend({},Default,config);if(typeof config.target !== 'string'){var id=$(config.target).attr('id');if(!id){id = Util.getUID(NAME);$(config.target).attr('id',id);}config.target = '#' + id;}Util.typeCheckConfig(NAME,config,DefaultType);return config;}},{key:'_getScrollTop',value:function _getScrollTop(){return this._scrollElement === window?this._scrollElement.scrollY:this._scrollElement.scrollTop;}},{key:'_getScrollHeight',value:function _getScrollHeight(){return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight,document.documentElement.scrollHeight);}},{key:'_process',value:function _process(){var scrollTop=this._getScrollTop() + this._config.offset;var scrollHeight=this._getScrollHeight();var maxScroll=this._config.offset + scrollHeight - this._scrollElement.offsetHeight;if(this._scrollHeight !== scrollHeight){this.refresh();}if(scrollTop >= maxScroll){var target=this._targets[this._targets.length - 1];if(this._activeTarget !== target){this._activate(target);}}if(this._activeTarget && scrollTop < this._offsets[0]){this._activeTarget = null;this._clear();return;}for(var i=this._offsets.length;i--;) {var isActiveTarget=this._activeTarget !== this._targets[i] && scrollTop >= this._offsets[i] && (this._offsets[i + 1] === undefined || scrollTop < this._offsets[i + 1]);if(isActiveTarget){this._activate(this._targets[i]);}}}},{key:'_activate',value:function _activate(target){this._activeTarget = target;this._clear();var queries=this._selector.split(',');queries = queries.map(function(selector){return selector + '[data-target="' + target + '"],' + (selector + '[href="' + target + '"]');});var $link=$(queries.join(','));if($link.hasClass(ClassName.DROPDOWN_ITEM)){$link.closest(Selector.DROPDOWN).find(Selector.DROPDOWN_TOGGLE).addClass(ClassName.ACTIVE);$link.addClass(ClassName.ACTIVE);}else { // todo (fat) this is kinda sus...
// recursively add actives to tested nav-links
$link.parents(Selector.LI).find(Selector.NAV_LINKS).addClass(ClassName.ACTIVE);}$(this._scrollElement).trigger(Event.ACTIVATE,{relatedTarget:target});}},{key:'_clear',value:function _clear(){$(this._selector).filter(Selector.ACTIVE).removeClass(ClassName.ACTIVE);} // static
}],[{key:'_jQueryInterface',value:function _jQueryInterface(config){return this.each(function(){var data=$(this).data(DATA_KEY);var _config=typeof config === 'object' && config || null;if(!data){data = new ScrollSpy(this,_config);$(this).data(DATA_KEY,data);}if(typeof config === 'string'){if(data[config] === undefined){throw new Error('No method named "' + config + '"');}data[config]();}});}},{key:'VERSION',get:function get(){return VERSION;}},{key:'Default',get:function get(){return Default;}}]);return ScrollSpy;})();$(window).on(Event.LOAD_DATA_API,function(){var scrollSpys=$.makeArray($(Selector.DATA_SPY));for(var i=scrollSpys.length;i--;) {var $spy=$(scrollSpys[i]);ScrollSpy._jQueryInterface.call($spy,$spy.data());}}); /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */$.fn[NAME] = ScrollSpy._jQueryInterface;$.fn[NAME].Constructor = ScrollSpy;$.fn[NAME].noConflict = function(){$.fn[NAME] = JQUERY_NO_CONFLICT;return ScrollSpy._jQueryInterface;};return ScrollSpy;})(jQuery); /**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-alpha.2): tab.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */var Tab=(function($){ /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */var NAME='tab';var VERSION='4.0.0-alpha.2';var DATA_KEY='bs.tab';var EVENT_KEY='.' + DATA_KEY;var DATA_API_KEY='.data-api';var JQUERY_NO_CONFLICT=$.fn[NAME];var TRANSITION_DURATION=150;var Event={HIDE:'hide' + EVENT_KEY,HIDDEN:'hidden' + EVENT_KEY,SHOW:'show' + EVENT_KEY,SHOWN:'shown' + EVENT_KEY,CLICK_DATA_API:'click' + EVENT_KEY + DATA_API_KEY};var ClassName={DROPDOWN_MENU:'dropdown-menu',ACTIVE:'active',FADE:'fade',IN:'in'};var Selector={A:'a',LI:'li',DROPDOWN:'.dropdown',UL:'ul:not(.dropdown-menu)',FADE_CHILD:'> .nav-item .fade, > .fade',ACTIVE:'.active',ACTIVE_CHILD:'> .nav-item > .active, > .active',DATA_TOGGLE:'[data-toggle="tab"], [data-toggle="pill"]',DROPDOWN_TOGGLE:'.dropdown-toggle',DROPDOWN_ACTIVE_CHILD:'> .dropdown-menu .active'}; /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */var Tab=(function(){function Tab(element){_classCallCheck(this,Tab);this._element = element;} /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */ // getters
_createClass(Tab,[{key:'show', // public
value:function show(){var _this15=this;if(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && $(this._element).hasClass(ClassName.ACTIVE)){return;}var target=undefined;var previous=undefined;var ulElement=$(this._element).closest(Selector.UL)[0];var selector=Util.getSelectorFromElement(this._element);if(ulElement){previous = $.makeArray($(ulElement).find(Selector.ACTIVE));previous = previous[previous.length - 1];}var hideEvent=$.Event(Event.HIDE,{relatedTarget:this._element});var showEvent=$.Event(Event.SHOW,{relatedTarget:previous});if(previous){$(previous).trigger(hideEvent);}$(this._element).trigger(showEvent);if(showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()){return;}if(selector){target = $(selector)[0];}this._activate(this._element,ulElement);var complete=function complete(){var hiddenEvent=$.Event(Event.HIDDEN,{relatedTarget:_this15._element});var shownEvent=$.Event(Event.SHOWN,{relatedTarget:previous});$(previous).trigger(hiddenEvent);$(_this15._element).trigger(shownEvent);};if(target){this._activate(target,target.parentNode,complete);}else {complete();}}},{key:'dispose',value:function dispose(){$.removeClass(this._element,DATA_KEY);this._element = null;} // private
},{key:'_activate',value:function _activate(element,container,callback){var active=$(container).find(Selector.ACTIVE_CHILD)[0];var isTransitioning=callback && Util.supportsTransitionEnd() && (active && $(active).hasClass(ClassName.FADE) || Boolean($(container).find(Selector.FADE_CHILD)[0]));var complete=$.proxy(this._transitionComplete,this,element,active,isTransitioning,callback);if(active && isTransitioning){$(active).one(Util.TRANSITION_END,complete).emulateTransitionEnd(TRANSITION_DURATION);}else {complete();}if(active){$(active).removeClass(ClassName.IN);}}},{key:'_transitionComplete',value:function _transitionComplete(element,active,isTransitioning,callback){if(active){$(active).removeClass(ClassName.ACTIVE);var dropdownChild=$(active).find(Selector.DROPDOWN_ACTIVE_CHILD)[0];if(dropdownChild){$(dropdownChild).removeClass(ClassName.ACTIVE);}active.setAttribute('aria-expanded',false);}$(element).addClass(ClassName.ACTIVE);element.setAttribute('aria-expanded',true);if(isTransitioning){Util.reflow(element);$(element).addClass(ClassName.IN);}else {$(element).removeClass(ClassName.FADE);}if(element.parentNode && $(element.parentNode).hasClass(ClassName.DROPDOWN_MENU)){var dropdownElement=$(element).closest(Selector.DROPDOWN)[0];if(dropdownElement){$(dropdownElement).find(Selector.DROPDOWN_TOGGLE).addClass(ClassName.ACTIVE);}element.setAttribute('aria-expanded',true);}if(callback){callback();}} // static
}],[{key:'_jQueryInterface',value:function _jQueryInterface(config){return this.each(function(){var $this=$(this);var data=$this.data(DATA_KEY);if(!data){data = data = new Tab(this);$this.data(DATA_KEY,data);}if(typeof config === 'string'){if(data[config] === undefined){throw new Error('No method named "' + config + '"');}data[config]();}});}},{key:'VERSION',get:function get(){return VERSION;}}]);return Tab;})();$(document).on(Event.CLICK_DATA_API,Selector.DATA_TOGGLE,function(event){event.preventDefault();Tab._jQueryInterface.call($(this),'show');}); /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */$.fn[NAME] = Tab._jQueryInterface;$.fn[NAME].Constructor = Tab;$.fn[NAME].noConflict = function(){$.fn[NAME] = JQUERY_NO_CONFLICT;return Tab._jQueryInterface;};return Tab;})(jQuery); /* global Tether */ /**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-alpha.2): tooltip.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */var Tooltip=(function($){ /**
   * Check for Tether dependency
   * Tether - http://github.hubspot.com/tether/
   */if(window.Tether === undefined){throw new Error('Bootstrap tooltips require Tether (http://github.hubspot.com/tether/)');} /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */var NAME='tooltip';var VERSION='4.0.0-alpha.2';var DATA_KEY='bs.tooltip';var EVENT_KEY='.' + DATA_KEY;var JQUERY_NO_CONFLICT=$.fn[NAME];var TRANSITION_DURATION=150;var CLASS_PREFIX='bs-tether';var Default={animation:true,template:'<div class="tooltip" role="tooltip">' + '<div class="tooltip-arrow"></div>' + '<div class="tooltip-inner"></div></div>',trigger:'hover focus',title:'',delay:0,html:false,selector:false,placement:'top',offset:'0 0',constraints:[]};var DefaultType={animation:'boolean',template:'string',title:'(string|element|function)',trigger:'string',delay:'(number|object)',html:'boolean',selector:'(string|boolean)',placement:'(string|function)',offset:'string',constraints:'array'};var AttachmentMap={TOP:'bottom center',RIGHT:'middle left',BOTTOM:'top center',LEFT:'middle right'};var HoverState={IN:'in',OUT:'out'};var Event={HIDE:'hide' + EVENT_KEY,HIDDEN:'hidden' + EVENT_KEY,SHOW:'show' + EVENT_KEY,SHOWN:'shown' + EVENT_KEY,INSERTED:'inserted' + EVENT_KEY,CLICK:'click' + EVENT_KEY,FOCUSIN:'focusin' + EVENT_KEY,FOCUSOUT:'focusout' + EVENT_KEY,MOUSEENTER:'mouseenter' + EVENT_KEY,MOUSELEAVE:'mouseleave' + EVENT_KEY};var ClassName={FADE:'fade',IN:'in'};var Selector={TOOLTIP:'.tooltip',TOOLTIP_INNER:'.tooltip-inner'};var TetherClass={element:false,enabled:false};var Trigger={HOVER:'hover',FOCUS:'focus',CLICK:'click',MANUAL:'manual'}; /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */var Tooltip=(function(){function Tooltip(element,config){_classCallCheck(this,Tooltip); // private
this._isEnabled = true;this._timeout = 0;this._hoverState = '';this._activeTrigger = {};this._tether = null; // protected
this.element = element;this.config = this._getConfig(config);this.tip = null;this._setListeners();} /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */ // getters
_createClass(Tooltip,[{key:'enable', // public
value:function enable(){this._isEnabled = true;}},{key:'disable',value:function disable(){this._isEnabled = false;}},{key:'toggleEnabled',value:function toggleEnabled(){this._isEnabled = !this._isEnabled;}},{key:'toggle',value:function toggle(event){if(event){var dataKey=this.constructor.DATA_KEY;var context=$(event.currentTarget).data(dataKey);if(!context){context = new this.constructor(event.currentTarget,this._getDelegateConfig());$(event.currentTarget).data(dataKey,context);}context._activeTrigger.click = !context._activeTrigger.click;if(context._isWithActiveTrigger()){context._enter(null,context);}else {context._leave(null,context);}}else {if($(this.getTipElement()).hasClass(ClassName.IN)){this._leave(null,this);return;}this._enter(null,this);}}},{key:'dispose',value:function dispose(){clearTimeout(this._timeout);this.cleanupTether();$.removeData(this.element,this.constructor.DATA_KEY);$(this.element).off(this.constructor.EVENT_KEY);if(this.tip){$(this.tip).remove();}this._isEnabled = null;this._timeout = null;this._hoverState = null;this._activeTrigger = null;this._tether = null;this.element = null;this.config = null;this.tip = null;}},{key:'show',value:function show(){var _this16=this;var showEvent=$.Event(this.constructor.Event.SHOW);if(this.isWithContent() && this._isEnabled){$(this.element).trigger(showEvent);var isInTheDom=$.contains(this.element.ownerDocument.documentElement,this.element);if(showEvent.isDefaultPrevented() || !isInTheDom){return;}var tip=this.getTipElement();var tipId=Util.getUID(this.constructor.NAME);tip.setAttribute('id',tipId);this.element.setAttribute('aria-describedby',tipId);this.setContent();if(this.config.animation){$(tip).addClass(ClassName.FADE);}var placement=typeof this.config.placement === 'function'?this.config.placement.call(this,tip,this.element):this.config.placement;var attachment=this._getAttachment(placement);$(tip).data(this.constructor.DATA_KEY,this).appendTo(document.body);$(this.element).trigger(this.constructor.Event.INSERTED);this._tether = new Tether({attachment:attachment,element:tip,target:this.element,classes:TetherClass,classPrefix:CLASS_PREFIX,offset:this.config.offset,constraints:this.config.constraints,addTargetClasses:false});Util.reflow(tip);this._tether.position();$(tip).addClass(ClassName.IN);var complete=function complete(){var prevHoverState=_this16._hoverState;_this16._hoverState = null;$(_this16.element).trigger(_this16.constructor.Event.SHOWN);if(prevHoverState === HoverState.OUT){_this16._leave(null,_this16);}};if(Util.supportsTransitionEnd() && $(this.tip).hasClass(ClassName.FADE)){$(this.tip).one(Util.TRANSITION_END,complete).emulateTransitionEnd(Tooltip._TRANSITION_DURATION);return;}complete();}}},{key:'hide',value:function hide(callback){var _this17=this;var tip=this.getTipElement();var hideEvent=$.Event(this.constructor.Event.HIDE);var complete=function complete(){if(_this17._hoverState !== HoverState.IN && tip.parentNode){tip.parentNode.removeChild(tip);}_this17.element.removeAttribute('aria-describedby');$(_this17.element).trigger(_this17.constructor.Event.HIDDEN);_this17.cleanupTether();if(callback){callback();}};$(this.element).trigger(hideEvent);if(hideEvent.isDefaultPrevented()){return;}$(tip).removeClass(ClassName.IN);if(Util.supportsTransitionEnd() && $(this.tip).hasClass(ClassName.FADE)){$(tip).one(Util.TRANSITION_END,complete).emulateTransitionEnd(TRANSITION_DURATION);}else {complete();}this._hoverState = '';} // protected
},{key:'isWithContent',value:function isWithContent(){return Boolean(this.getTitle());}},{key:'getTipElement',value:function getTipElement(){return this.tip = this.tip || $(this.config.template)[0];}},{key:'setContent',value:function setContent(){var $tip=$(this.getTipElement());this.setElementContent($tip.find(Selector.TOOLTIP_INNER),this.getTitle());$tip.removeClass(ClassName.FADE).removeClass(ClassName.IN);this.cleanupTether();}},{key:'setElementContent',value:function setElementContent($element,content){var html=this.config.html;if(typeof content === 'object' && (content.nodeType || content.jquery)){ // content is a DOM node or a jQuery
if(html){if(!$(content).parent().is($element)){$element.empty().append(content);}}else {$element.text($(content).text());}}else {$element[html?'html':'text'](content);}}},{key:'getTitle',value:function getTitle(){var title=this.element.getAttribute('data-original-title');if(!title){title = typeof this.config.title === 'function'?this.config.title.call(this.element):this.config.title;}return title;}},{key:'cleanupTether',value:function cleanupTether(){if(this._tether){this._tether.destroy();}} // private
},{key:'_getAttachment',value:function _getAttachment(placement){return AttachmentMap[placement.toUpperCase()];}},{key:'_setListeners',value:function _setListeners(){var _this18=this;var triggers=this.config.trigger.split(' ');triggers.forEach(function(trigger){if(trigger === 'click'){$(_this18.element).on(_this18.constructor.Event.CLICK,_this18.config.selector,$.proxy(_this18.toggle,_this18));}else if(trigger !== Trigger.MANUAL){var eventIn=trigger === Trigger.HOVER?_this18.constructor.Event.MOUSEENTER:_this18.constructor.Event.FOCUSIN;var eventOut=trigger === Trigger.HOVER?_this18.constructor.Event.MOUSELEAVE:_this18.constructor.Event.FOCUSOUT;$(_this18.element).on(eventIn,_this18.config.selector,$.proxy(_this18._enter,_this18)).on(eventOut,_this18.config.selector,$.proxy(_this18._leave,_this18));}});if(this.config.selector){this.config = $.extend({},this.config,{trigger:'manual',selector:''});}else {this._fixTitle();}}},{key:'_fixTitle',value:function _fixTitle(){var titleType=typeof this.element.getAttribute('data-original-title');if(this.element.getAttribute('title') || titleType !== 'string'){this.element.setAttribute('data-original-title',this.element.getAttribute('title') || '');this.element.setAttribute('title','');}}},{key:'_enter',value:function _enter(event,context){var dataKey=this.constructor.DATA_KEY;context = context || $(event.currentTarget).data(dataKey);if(!context){context = new this.constructor(event.currentTarget,this._getDelegateConfig());$(event.currentTarget).data(dataKey,context);}if(event){context._activeTrigger[event.type === 'focusin'?Trigger.FOCUS:Trigger.HOVER] = true;}if($(context.getTipElement()).hasClass(ClassName.IN) || context._hoverState === HoverState.IN){context._hoverState = HoverState.IN;return;}clearTimeout(context._timeout);context._hoverState = HoverState.IN;if(!context.config.delay || !context.config.delay.show){context.show();return;}context._timeout = setTimeout(function(){if(context._hoverState === HoverState.IN){context.show();}},context.config.delay.show);}},{key:'_leave',value:function _leave(event,context){var dataKey=this.constructor.DATA_KEY;context = context || $(event.currentTarget).data(dataKey);if(!context){context = new this.constructor(event.currentTarget,this._getDelegateConfig());$(event.currentTarget).data(dataKey,context);}if(event){context._activeTrigger[event.type === 'focusout'?Trigger.FOCUS:Trigger.HOVER] = false;}if(context._isWithActiveTrigger()){return;}clearTimeout(context._timeout);context._hoverState = HoverState.OUT;if(!context.config.delay || !context.config.delay.hide){context.hide();return;}context._timeout = setTimeout(function(){if(context._hoverState === HoverState.OUT){context.hide();}},context.config.delay.hide);}},{key:'_isWithActiveTrigger',value:function _isWithActiveTrigger(){for(var trigger in this._activeTrigger) {if(this._activeTrigger[trigger]){return true;}}return false;}},{key:'_getConfig',value:function _getConfig(config){config = $.extend({},this.constructor.Default,$(this.element).data(),config);if(config.delay && typeof config.delay === 'number'){config.delay = {show:config.delay,hide:config.delay};}Util.typeCheckConfig(NAME,config,this.constructor.DefaultType);return config;}},{key:'_getDelegateConfig',value:function _getDelegateConfig(){var config={};if(this.config){for(var key in this.config) {if(this.constructor.Default[key] !== this.config[key]){config[key] = this.config[key];}}}return config;} // static
}],[{key:'_jQueryInterface',value:function _jQueryInterface(config){return this.each(function(){var data=$(this).data(DATA_KEY);var _config=typeof config === 'object'?config:null;if(!data && /destroy|hide/.test(config)){return;}if(!data){data = new Tooltip(this,_config);$(this).data(DATA_KEY,data);}if(typeof config === 'string'){if(data[config] === undefined){throw new Error('No method named "' + config + '"');}data[config]();}});}},{key:'VERSION',get:function get(){return VERSION;}},{key:'Default',get:function get(){return Default;}},{key:'NAME',get:function get(){return NAME;}},{key:'DATA_KEY',get:function get(){return DATA_KEY;}},{key:'Event',get:function get(){return Event;}},{key:'EVENT_KEY',get:function get(){return EVENT_KEY;}},{key:'DefaultType',get:function get(){return DefaultType;}}]);return Tooltip;})();$.fn[NAME] = Tooltip._jQueryInterface;$.fn[NAME].Constructor = Tooltip;$.fn[NAME].noConflict = function(){$.fn[NAME] = JQUERY_NO_CONFLICT;return Tooltip._jQueryInterface;};return Tooltip;})(jQuery); /**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-alpha.2): popover.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */var Popover=(function($){ /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */var NAME='popover';var VERSION='4.0.0-alpha.2';var DATA_KEY='bs.popover';var EVENT_KEY='.' + DATA_KEY;var JQUERY_NO_CONFLICT=$.fn[NAME];var Default=$.extend({},Tooltip.Default,{placement:'right',trigger:'click',content:'',template:'<div class="popover" role="tooltip">' + '<div class="popover-arrow"></div>' + '<h3 class="popover-title"></h3>' + '<div class="popover-content"></div></div>'});var DefaultType=$.extend({},Tooltip.DefaultType,{content:'(string|element|function)'});var ClassName={FADE:'fade',IN:'in'};var Selector={TITLE:'.popover-title',CONTENT:'.popover-content',ARROW:'.popover-arrow'};var Event={HIDE:'hide' + EVENT_KEY,HIDDEN:'hidden' + EVENT_KEY,SHOW:'show' + EVENT_KEY,SHOWN:'shown' + EVENT_KEY,INSERTED:'inserted' + EVENT_KEY,CLICK:'click' + EVENT_KEY,FOCUSIN:'focusin' + EVENT_KEY,FOCUSOUT:'focusout' + EVENT_KEY,MOUSEENTER:'mouseenter' + EVENT_KEY,MOUSELEAVE:'mouseleave' + EVENT_KEY}; /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */var Popover=(function(_Tooltip){_inherits(Popover,_Tooltip);function Popover(){_classCallCheck(this,Popover);_get(Object.getPrototypeOf(Popover.prototype),'constructor',this).apply(this,arguments);} /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */_createClass(Popover,[{key:'isWithContent', // overrides
value:function isWithContent(){return this.getTitle() || this._getContent();}},{key:'getTipElement',value:function getTipElement(){return this.tip = this.tip || $(this.config.template)[0];}},{key:'setContent',value:function setContent(){var $tip=$(this.getTipElement()); // we use append for html objects to maintain js events
this.setElementContent($tip.find(Selector.TITLE),this.getTitle());this.setElementContent($tip.find(Selector.CONTENT),this._getContent());$tip.removeClass(ClassName.FADE).removeClass(ClassName.IN);this.cleanupTether();} // private
},{key:'_getContent',value:function _getContent(){return this.element.getAttribute('data-content') || (typeof this.config.content === 'function'?this.config.content.call(this.element):this.config.content);} // static
}],[{key:'_jQueryInterface',value:function _jQueryInterface(config){return this.each(function(){var data=$(this).data(DATA_KEY);var _config=typeof config === 'object'?config:null;if(!data && /destroy|hide/.test(config)){return;}if(!data){data = new Popover(this,_config);$(this).data(DATA_KEY,data);}if(typeof config === 'string'){if(data[config] === undefined){throw new Error('No method named "' + config + '"');}data[config]();}});}},{key:'VERSION', // getters
get:function get(){return VERSION;}},{key:'Default',get:function get(){return Default;}},{key:'NAME',get:function get(){return NAME;}},{key:'DATA_KEY',get:function get(){return DATA_KEY;}},{key:'Event',get:function get(){return Event;}},{key:'EVENT_KEY',get:function get(){return EVENT_KEY;}},{key:'DefaultType',get:function get(){return DefaultType;}}]);return Popover;})(Tooltip);$.fn[NAME] = Popover._jQueryInterface;$.fn[NAME].Constructor = Popover;$.fn[NAME].noConflict = function(){$.fn[NAME] = JQUERY_NO_CONFLICT;return Popover._jQueryInterface;};return Popover;})(jQuery); /**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-alpha.2): carousel.js
 * @author Arthur Franco
 * --------------------------------------------------------------------------
 */var CarouselIngresso=(function($){ /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */var NAME='carouselIngresso';var VERSION='4.0.0-alpha.2';var DATA_KEY='bs.ingresso.carousel';var EVENT_KEY='.' + DATA_KEY;var DATA_API_KEY='.data-api';var JQUERY_NO_CONFLICT=$.fn[NAME];var TRANSITION_DURATION=600;var Default={interval:5000,keyboard:true,slide:false,pause:'hover',wrap:true};var DefaultType={interval:'(number|boolean)',keyboard:'boolean',slide:'(boolean|string)',pause:'(string|boolean)',wrap:'boolean'};var Direction={NEXT:'next',PREVIOUS:'prev'};var Event={SLIDE:'slide' + EVENT_KEY,SLID:'slid' + EVENT_KEY,KEYDOWN:'keydown' + EVENT_KEY,MOUSEENTER:'mouseenter' + EVENT_KEY,MOUSELEAVE:'mouseleave' + EVENT_KEY,LOAD_DATA_API:'load' + EVENT_KEY + DATA_API_KEY,CLICK_DATA_API:'click' + EVENT_KEY + DATA_API_KEY};var ClassName={CAROUSEL:'carousel-ingresso',ACTIVE:'active',SLIDE:'slide',RIGHT:'right',LEFT:'left',ITEM:'carousel-item'};var Selector={ACTIVE:'.active',ACTIVE_ITEM:'.active.carousel-item',ITEM:'.carousel-item',NEXT_PREV:'.next, .prev',INDICATORS:'.carousel-indicators',DATA_SLIDE:'[data-slide], [data-slide-to]',DATA_RIDE:'[data-ride="carousel-ingresso"]'}; /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */var CarouselIngresso=(function(_Carousel){_inherits(CarouselIngresso,_Carousel);function CarouselIngresso(element,config){_classCallCheck(this,CarouselIngresso);_get(Object.getPrototypeOf(CarouselIngresso.prototype),'constructor',this).call(this,element,config);this._showThumbnails();} /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */ // getters
_createClass(CarouselIngresso,[{key:'_showThumbnails', // private
value:function _showThumbnails(){$(Selector.DATA_SLIDE,this._indicatorsElement).each(function(i){var $indicatorElement=$(this);var $slideImage=$(Selector.ITEM).eq(i).children('img');$indicatorElement.css('background-image','url(' + $slideImage.attr('src') + ')');});} // static
}],[{key:'_jQueryInterface',value:function _jQueryInterface(config){return this.each(function(){var data=$(this).data(DATA_KEY);var _config=$.extend({},Default,$(this).data());if(typeof config === 'object'){$.extend(_config,config);}var action=typeof config === 'string'?config:_config.slide;if(!data){data = new CarouselIngresso(this,_config);$(this).data(DATA_KEY,data);}if(typeof config === 'number'){data.to(config);}else if(typeof action === 'string'){if(data[action] === undefined){throw new Error('No method named "' + action + '"');}data[action]();}else if(_config.interval){data.pause();data.cycle();}});}},{key:'_dataApiClickHandler',value:function _dataApiClickHandler(event){var selector=Util.getSelectorFromElement(this);if(!selector){return;}var target=$(selector)[0];if(!target || !$(target).hasClass(ClassName.CAROUSEL)){return;}var config=$.extend({},$(target).data(),$(this).data());var slideIndex=this.getAttribute('data-slide-to');if(slideIndex){config.interval = false;}CarouselIngresso._jQueryInterface.call($(target),config);if(slideIndex){$(target).data(DATA_KEY).to(slideIndex);}event.preventDefault();}},{key:'VERSION',get:function get(){return VERSION;}},{key:'Default',get:function get(){return Default;}}]);return CarouselIngresso;})(Carousel);$(document).on(Event.CLICK_DATA_API,Selector.DATA_SLIDE,CarouselIngresso._dataApiClickHandler);$(window).on(Event.LOAD_DATA_API,function(){$(Selector.DATA_RIDE).each(function(){var $carousel=$(this);CarouselIngresso._jQueryInterface.call($carousel,$carousel.data());});}); /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */$.fn[NAME] = CarouselIngresso._jQueryInterface;$.fn[NAME].Constructor = CarouselIngresso;$.fn[NAME].noConflict = function(){$.fn[NAME] = JQUERY_NO_CONFLICT;return CarouselIngresso._jQueryInterface;};return CarouselIngresso;})(jQuery);(function(){window.lazySizesConfig = window.lazySizesConfig || {};window.lazySizesConfig.expand = 10;window.lazySizesConfig.expFactor = 1.5;window.lazySizesConfig.hFac = 1.5;window.lazySizesConfig.loadMode = 1; /* these configs follows bootstrap's breakpoints  */window.lazySizesConfig.customMedia = {'--small':'(min-width: 544px)','--medium':'(min-width: 768px)','--large':'(min-width: 992px)','--xlarge':'(min-width: 1440px)'};})(); /**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-alpha.2): modal.js
 * @author Arthur Franco
 * --------------------------------------------------------------------------
 */var ModalIngresso=(function($){ /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */var NAME='modalIngresso';var VERSION='4.0.0-alpha.2';var DATA_KEY='bs.ingresso.modal';var EVENT_KEY='.' + DATA_KEY;var DATA_API_KEY='.data-api';var JQUERY_NO_CONFLICT=$.fn[NAME];var TRANSITION_DURATION=300;var BACKDROP_TRANSITION_DURATION=150;var Default={backdrop:true,keyboard:true,focus:true,show:true};var DefaultType={backdrop:'(boolean|string)',keyboard:'boolean',focus:'boolean',show:'boolean'};var Event={HIDE:'hide' + EVENT_KEY,HIDDEN:'hidden' + EVENT_KEY,SHOW:'show' + EVENT_KEY,SHOWN:'shown' + EVENT_KEY,FOCUSIN:'focusin' + EVENT_KEY,RESIZE:'resize' + EVENT_KEY,CLICK_DISMISS:'click.dismiss' + EVENT_KEY,KEYDOWN_DISMISS:'keydown.dismiss' + EVENT_KEY,MOUSEUP_DISMISS:'mouseup.dismiss' + EVENT_KEY,MOUSEDOWN_DISMISS:'mousedown.dismiss' + EVENT_KEY,CLICK_DATA_API:'click' + EVENT_KEY + DATA_API_KEY};var ClassName={SCROLLBAR_MEASURER:'modal-scrollbar-measure',BACKDROP:'modal-backdrop',OPEN:'modal-open',FADE:'fade',IN:'in'};var Selector={DIALOG:'.modal-dialog',DATA_TOGGLE:'[data-toggle="modal-ingresso"]',DATA_DISMISS:'[data-dismiss="modal"]',FIXED_CONTENT:'.navbar-fixed-top, .navbar-fixed-bottom, .is-fixed',IFRAMES:'iframe[data-src]'}; /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */var ModalIngresso=(function(_Modal){_inherits(ModalIngresso,_Modal);function ModalIngresso(element,config){_classCallCheck(this,ModalIngresso);_get(Object.getPrototypeOf(ModalIngresso.prototype),'constructor',this).call(this,element,config);} /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */ // getters
_createClass(ModalIngresso,[{key:'show', // public
value:function show(relatedTarget){_get(Object.getPrototypeOf(ModalIngresso.prototype),'show',this).call(this,relatedTarget);this._loadSource();}},{key:'hide',value:function hide(event){var _this19=this;_get(Object.getPrototypeOf(ModalIngresso.prototype),'hide',this).call(this,event);var player;$(Selector.IFRAMES,this._dialog).each(function(i,elem){var $elem=$(elem);var sourceVideoURL=$elem.attr('src') || $elem.attr('data-src');console.log(sourceVideoURL);if(_this19._matchYouTube(sourceVideoURL)){player = _this19._youTubePlayer(elem);player.stop();}else if(_this19._matchDailymotion(sourceVideoURL)){player = _this19._dailyMotionPlayer(elem);player.pause();}});} // private
},{key:'_loadSource',value:function _loadSource(){$(Selector.IFRAMES,this._dialog).each(function(i,elem){var $elem=$(elem);if(typeof $elem.attr('src') != 'string'){var srcURL=$elem.attr('data-src');$elem.attr('src',srcURL);}});}},{key:'_youTubePlayer',value:function _youTubePlayer(iframe){var player=iframe.contentWindow;function _postMessage(func){player.postMessage('{"event":"command","func":"' + func + '","args":""}','*');}return {play:function play(){_postMessage('playVideo');},pause:function pause(){_postMessage('pauseVideo');},stop:function stop(){_postMessage('stopVideo');}};}},{key:'_dailyMotionPlayer',value:function _dailyMotionPlayer(iframe){var player=iframe.contentWindow;function _postMessage(func){player.postMessage('{"command":"' + func + '","parameters":[]}',"*");}return {play:function play(){_postMessage('play');},pause:function pause(){_postMessage('pause');}};}},{key:'_matchYouTube',value:function _matchYouTube(url){var URLRule=new RegExp("^(https?\:)?(\/\/)?(www\.youtube\.com|youtu\.?be|youtube\.com)\/.+$");if(url.match(URLRule)){return url.match(URLRule).length?true:false;}else {return false;}}},{key:'_matchDailymotion',value:function _matchDailymotion(url){var URLRule=new RegExp("^(https?\:)?(\/\/)?(www\.dailymotion\.com|dailymotion\.com)\/.+$");if(url.match(URLRule)){return url.match(URLRule).length?true:false;}else {return false;}} // static
}],[{key:'_jQueryInterface',value:function _jQueryInterface(config,relatedTarget){return this.each(function(){var data=$(this).data(DATA_KEY);var _config=$.extend({},ModalIngresso.Default,$(this).data(),typeof config === 'object' && config);if(!data){data = new ModalIngresso(this,_config);$(this).data(DATA_KEY,data);}if(typeof config === 'string'){if(data[config] === undefined){throw new Error('No method named "' + config + '"');}data[config](relatedTarget);}else if(_config.show){data.show(relatedTarget);}});}},{key:'VERSION',get:function get(){return VERSION;}},{key:'Default',get:function get(){return Default;}}]);return ModalIngresso;})(Modal);$(document).on(Event.CLICK_DATA_API,Selector.DATA_TOGGLE,function(event){var _this20=this;var target=undefined;var selector=Util.getSelectorFromElement(this);if(selector){target = $(selector)[0];}var config=$(target).data(DATA_KEY)?'toggle':$.extend({},$(target).data(),$(this).data());if(this.tagName === 'A'){event.preventDefault();}var $target=$(target).one(Event.SHOW,function(showEvent){if(showEvent.isDefaultPrevented()){ // only register focus restorer if modal will actually get shown
return;}$target.one(Event.HIDDEN,function(){if($(_this20).is(':visible')){_this20.focus();}});});ModalIngresso._jQueryInterface.call($(target),config,this);}); /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */$.fn[NAME] = ModalIngresso._jQueryInterface;$.fn[NAME].Constructor = ModalIngresso;$.fn[NAME].noConflict = function(){$.fn[NAME] = JQUERY_NO_CONFLICT;return ModalIngresso._jQueryInterface;};return ModalIngresso;})(jQuery);

}(jQuery);

/*!
 * Bootstrap v4.0.0-alpha.2 (http://getbootstrap.com)
 * Copyright 2011-2017 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */
if("undefined"==typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");+function(a){var b=a.fn.jquery.split(" ")[0].split(".");if(b[0]<2&&b[1]<9||1==b[0]&&9==b[1]&&b[2]<1||b[0]>=3)throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v3.0.0")}(jQuery),+function(a){"use strict";function b(a,b){if("function"!=typeof b&&null!==b)throw new TypeError("Super expression must either be null or a function, not "+typeof b);a.prototype=Object.create(b&&b.prototype,{constructor:{value:a,enumerable:!1,writable:!0,configurable:!0}}),b&&(Object.setPrototypeOf?Object.setPrototypeOf(a,b):a.__proto__=b)}function c(a,b){if(!(a instanceof b))throw new TypeError("Cannot call a class as a function")}var d=function(a,b,c){for(var d=!0;d;){var e=a,f=b,g=c;d=!1,null===e&&(e=Function.prototype);var h=Object.getOwnPropertyDescriptor(e,f);if(void 0!==h){if("value"in h)return h.value;var i=h.get;if(void 0===i)return;return i.call(g)}var j=Object.getPrototypeOf(e);if(null===j)return;a=j,b=f,c=g,d=!0,h=j=void 0}},e=function(){function a(a,b){for(var c=0;c<b.length;c++){var d=b[c];d.enumerable=d.enumerable||!1,d.configurable=!0,"value"in d&&(d.writable=!0),Object.defineProperty(a,d.key,d)}}return function(b,c,d){return c&&a(b.prototype,c),d&&a(b,d),b}}(),f=function(a){function b(a){return{}.toString.call(a).match(/\s([a-zA-Z]+)/)[1].toLowerCase()}function c(a){return(a[0]||a).nodeType}function d(){return{bindType:h.end,delegateType:h.end,handle:function(b){if(a(b.target).is(this))return b.handleObj.handler.apply(this,arguments)}}}function e(){if(window.QUnit)return!1;var a=document.createElement("bootstrap");for(var b in i)if(void 0!==a.style[b])return{end:i[b]};return!1}function f(b){var c=this,d=!1;return a(this).one(j.TRANSITION_END,function(){d=!0}),setTimeout(function(){d||j.triggerTransitionEnd(c)},b),this}function g(){h=e(),a.fn.emulateTransitionEnd=f,j.supportsTransitionEnd()&&(a.event.special[j.TRANSITION_END]=d())}var h=!1,i={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"},j={TRANSITION_END:"bsTransitionEnd",getUID:function(a){do a+=~~(1e6*Math.random());while(document.getElementById(a));return a},getSelectorFromElement:function(a){var b=a.getAttribute("data-target");return b||(b=a.getAttribute("href")||"",b=/^#[a-z]/i.test(b)?b:null),b},reflow:function(a){new Function("bs","return bs")(a.offsetHeight)},triggerTransitionEnd:function(b){a(b).trigger(h.end)},supportsTransitionEnd:function(){return Boolean(h)},typeCheckConfig:function(a,d,e){for(var f in e)if(e.hasOwnProperty(f)){var g=e[f],h=d[f],i=void 0;if(i=h&&c(h)?"element":b(h),!new RegExp(g).test(i))throw new Error(a.toUpperCase()+": "+('Option "'+f+'" provided type "'+i+'" ')+('but expected type "'+g+'".'))}}};return g(),j}(jQuery),g=(function(a){var b="alert",d="4.0.0-alpha.2",g="bs.alert",h="."+g,i=".data-api",j=a.fn[b],k=150,l={DISMISS:'[data-dismiss="alert"]'},m={CLOSE:"close"+h,CLOSED:"closed"+h,CLICK_DATA_API:"click"+h+i},n={ALERT:"alert",FADE:"fade",IN:"in"},o=function(){function b(a){c(this,b),this._element=a}return e(b,[{key:"close",value:function(a){a=a||this._element;var b=this._getRootElement(a),c=this._triggerCloseEvent(b);c.isDefaultPrevented()||this._removeElement(b)}},{key:"dispose",value:function(){a.removeData(this._element,g),this._element=null}},{key:"_getRootElement",value:function(b){var c=f.getSelectorFromElement(b),d=!1;return c&&(d=a(c)[0]),d||(d=a(b).closest("."+n.ALERT)[0]),d}},{key:"_triggerCloseEvent",value:function(b){var c=a.Event(m.CLOSE);return a(b).trigger(c),c}},{key:"_removeElement",value:function(b){return a(b).removeClass(n.IN),f.supportsTransitionEnd()&&a(b).hasClass(n.FADE)?void a(b).one(f.TRANSITION_END,a.proxy(this._destroyElement,this,b)).emulateTransitionEnd(k):void this._destroyElement(b)}},{key:"_destroyElement",value:function(b){a(b).detach().trigger(m.CLOSED).remove()}}],[{key:"_jQueryInterface",value:function(c){return this.each(function(){var d=a(this),e=d.data(g);e||(e=new b(this),d.data(g,e)),"close"===c&&e[c](this)})}},{key:"_handleDismiss",value:function(a){return function(b){b&&b.preventDefault(),a.close(this)}}},{key:"VERSION",get:function(){return d}}]),b}();return a(document).on(m.CLICK_DATA_API,l.DISMISS,o._handleDismiss(new o)),a.fn[b]=o._jQueryInterface,a.fn[b].Constructor=o,a.fn[b].noConflict=function(){return a.fn[b]=j,o._jQueryInterface},o}(jQuery),function(a){var b="button",d="4.0.0-alpha.2",f="bs.button",g="."+f,h=".data-api",i=a.fn[b],j={ACTIVE:"active",BUTTON:"btn",FOCUS:"focus"},k={DATA_TOGGLE_CARROT:'[data-toggle^="button"]',DATA_TOGGLE:'[data-toggle="buttons"]',INPUT:"input",ACTIVE:".active",BUTTON:".btn"},l={CLICK_DATA_API:"click"+g+h,FOCUS_BLUR_DATA_API:"focus"+g+h+" "+("blur"+g+h)},m=function(){function b(a){c(this,b),this._element=a}return e(b,[{key:"toggle",value:function(){var b=!0,c=a(this._element).closest(k.DATA_TOGGLE)[0];if(c){var d=a(this._element).find(k.INPUT)[0];if(d){if("radio"===d.type)if(d.checked&&a(this._element).hasClass(j.ACTIVE))b=!1;else{var e=a(c).find(k.ACTIVE)[0];e&&a(e).removeClass(j.ACTIVE)}b&&(d.checked=!a(this._element).hasClass(j.ACTIVE),a(this._element).trigger("change")),d.focus()}}else this._element.setAttribute("aria-pressed",!a(this._element).hasClass(j.ACTIVE));b&&a(this._element).toggleClass(j.ACTIVE)}},{key:"dispose",value:function(){a.removeData(this._element,f),this._element=null}}],[{key:"_jQueryInterface",value:function(c){return this.each(function(){var d=a(this).data(f);d||(d=new b(this),a(this).data(f,d)),"toggle"===c&&d[c]()})}},{key:"VERSION",get:function(){return d}}]),b}();return a(document).on(l.CLICK_DATA_API,k.DATA_TOGGLE_CARROT,function(b){b.preventDefault();var c=b.target;a(c).hasClass(j.BUTTON)||(c=a(c).closest(k.BUTTON)),m._jQueryInterface.call(a(c),"toggle")}).on(l.FOCUS_BLUR_DATA_API,k.DATA_TOGGLE_CARROT,function(b){var c=a(b.target).closest(k.BUTTON)[0];a(c).toggleClass(j.FOCUS,/^focus(in)?$/.test(b.type))}),a.fn[b]=m._jQueryInterface,a.fn[b].Constructor=m,a.fn[b].noConflict=function(){return a.fn[b]=i,m._jQueryInterface},m}(jQuery),function(a){var b="carousel",d="4.0.0-alpha.2",g="bs.carousel",h="."+g,i=".data-api",j=a.fn[b],k=600,l={interval:5e3,keyboard:!0,slide:!1,pause:"hover",wrap:!0},m={interval:"(number|boolean)",keyboard:"boolean",slide:"(boolean|string)",pause:"(string|boolean)",wrap:"boolean"},n={NEXT:"next",PREVIOUS:"prev"},o={SLIDE:"slide"+h,SLID:"slid"+h,KEYDOWN:"keydown"+h,MOUSEENTER:"mouseenter"+h,MOUSELEAVE:"mouseleave"+h,LOAD_DATA_API:"load"+h+i,CLICK_DATA_API:"click"+h+i},p={CAROUSEL:"carousel",ACTIVE:"active",SLIDE:"slide",RIGHT:"right",LEFT:"left",ITEM:"carousel-item"},q={ACTIVE:".active",ACTIVE_ITEM:".active.carousel-item",ITEM:".carousel-item",NEXT_PREV:".next, .prev",INDICATORS:".carousel-indicators",DATA_SLIDE:"[data-slide], [data-slide-to]",DATA_RIDE:'[data-ride="carousel"]'},r=function(){function i(b,d){c(this,i),this._items=null,this._interval=null,this._activeElement=null,this._isPaused=!1,this._isSliding=!1,this._config=this._getConfig(d),this._element=a(b)[0],this._indicatorsElement=a(this._element).find(q.INDICATORS)[0],this._addEventListeners()}return e(i,[{key:"next",value:function(){this._isSliding||this._slide(n.NEXT)}},{key:"nextWhenVisible",value:function(){document.hidden||this.next()}},{key:"prev",value:function(){this._isSliding||this._slide(n.PREVIOUS)}},{key:"pause",value:function(b){b||(this._isPaused=!0),a(this._element).find(q.NEXT_PREV)[0]&&f.supportsTransitionEnd()&&(f.triggerTransitionEnd(this._element),this.cycle(!0)),clearInterval(this._interval),this._interval=null}},{key:"cycle",value:function(b){b||(this._isPaused=!1),this._interval&&(clearInterval(this._interval),this._interval=null),this._config.interval&&!this._isPaused&&(this._interval=setInterval(a.proxy(document.visibilityState?this.nextWhenVisible:this.next,this),this._config.interval))}},{key:"to",value:function(b){var c=this;this._activeElement=a(this._element).find(q.ACTIVE_ITEM)[0];var d=this._getItemIndex(this._activeElement);if(!(b>this._items.length-1||b<0)){if(this._isSliding)return void a(this._element).one(o.SLID,function(){return c.to(b)});if(d===b)return this.pause(),void this.cycle();var e=b>d?n.NEXT:n.PREVIOUS;this._slide(e,this._items[b])}}},{key:"dispose",value:function(){a(this._element).off(h),a.removeData(this._element,g),this._items=null,this._config=null,this._element=null,this._interval=null,this._isPaused=null,this._isSliding=null,this._activeElement=null,this._indicatorsElement=null}},{key:"_getConfig",value:function(c){return c=a.extend({},l,c),f.typeCheckConfig(b,c,m),c}},{key:"_addEventListeners",value:function(){this._config.keyboard&&a(this._element).on(o.KEYDOWN,a.proxy(this._keydown,this)),"hover"!==this._config.pause||"ontouchstart"in document.documentElement||a(this._element).on(o.MOUSEENTER,a.proxy(this.pause,this)).on(o.MOUSELEAVE,a.proxy(this.cycle,this))}},{key:"_keydown",value:function(a){if(a.preventDefault(),!/input|textarea/i.test(a.target.tagName))switch(a.which){case 37:this.prev();break;case 39:this.next();break;default:return}}},{key:"_getItemIndex",value:function(b){return this._items=a.makeArray(a(b).parent().find(q.ITEM)),this._items.indexOf(b)}},{key:"_getItemByDirection",value:function(a,b){var c=a===n.NEXT,d=a===n.PREVIOUS,e=this._getItemIndex(b),f=this._items.length-1,g=d&&0===e||c&&e===f;if(g&&!this._config.wrap)return b;var h=a===n.PREVIOUS?-1:1,i=(e+h)%this._items.length;return i===-1?this._items[this._items.length-1]:this._items[i]}},{key:"_triggerSlideEvent",value:function(b,c){var d=a.Event(o.SLIDE,{relatedTarget:b,direction:c});return a(this._element).trigger(d),d}},{key:"_setActiveIndicatorElement",value:function(b){if(this._indicatorsElement){a(this._indicatorsElement).find(q.ACTIVE).removeClass(p.ACTIVE);var c=this._indicatorsElement.children[this._getItemIndex(b)];c&&a(c).addClass(p.ACTIVE)}}},{key:"_slide",value:function(b,c){var d=this,e=a(this._element).find(q.ACTIVE_ITEM)[0],g=c||e&&this._getItemByDirection(b,e),h=Boolean(this._interval),i=b===n.NEXT?p.LEFT:p.RIGHT;if(g&&a(g).hasClass(p.ACTIVE))return void(this._isSliding=!1);var j=this._triggerSlideEvent(g,i);if(!j.isDefaultPrevented()&&e&&g){this._isSliding=!0,h&&this.pause(),this._setActiveIndicatorElement(g);var l=a.Event(o.SLID,{relatedTarget:g,direction:i});f.supportsTransitionEnd()&&a(this._element).hasClass(p.SLIDE)?(a(g).addClass(b),f.reflow(g),a(e).addClass(i),a(g).addClass(i),a(e).one(f.TRANSITION_END,function(){a(g).removeClass(i).removeClass(b),a(g).addClass(p.ACTIVE),a(e).removeClass(p.ACTIVE).removeClass(b).removeClass(i),d._isSliding=!1,setTimeout(function(){return a(d._element).trigger(l)},0)}).emulateTransitionEnd(k)):(a(e).removeClass(p.ACTIVE),a(g).addClass(p.ACTIVE),this._isSliding=!1,a(this._element).trigger(l)),h&&this.cycle()}}}],[{key:"_jQueryInterface",value:function(b){return this.each(function(){var c=a(this).data(g),d=a.extend({},l,a(this).data());"object"==typeof b&&a.extend(d,b);var e="string"==typeof b?b:d.slide;if(c||(c=new i(this,d),a(this).data(g,c)),"number"==typeof b)c.to(b);else if("string"==typeof e){if(void 0===c[e])throw new Error('No method named "'+e+'"');c[e]()}else d.interval&&(c.pause(),c.cycle())})}},{key:"_dataApiClickHandler",value:function(b){var c=f.getSelectorFromElement(this);if(c){var d=a(c)[0];if(d&&a(d).hasClass(p.CAROUSEL)){var e=a.extend({},a(d).data(),a(this).data()),h=this.getAttribute("data-slide-to");h&&(e.interval=!1),i._jQueryInterface.call(a(d),e),h&&a(d).data(g).to(h),b.preventDefault()}}}},{key:"VERSION",get:function(){return d}},{key:"Default",get:function(){return l}}]),i}();return a(document).on(o.CLICK_DATA_API,q.DATA_SLIDE,r._dataApiClickHandler),a(window).on(o.LOAD_DATA_API,function(){a(q.DATA_RIDE).each(function(){var b=a(this);r._jQueryInterface.call(b,b.data())})}),a.fn[b]=r._jQueryInterface,a.fn[b].Constructor=r,a.fn[b].noConflict=function(){return a.fn[b]=j,r._jQueryInterface},r}(jQuery)),h=(function(a){var b="collapse",d="4.0.0-alpha.2",g="bs.collapse",h="."+g,i=".data-api",j=a.fn[b],k=600,l={toggle:!0,parent:""},m={toggle:"boolean",parent:"string"},n={SHOW:"show"+h,SHOWN:"shown"+h,HIDE:"hide"+h,HIDDEN:"hidden"+h,CLICK_DATA_API:"click"+h+i},o={IN:"in",COLLAPSE:"collapse",COLLAPSING:"collapsing",COLLAPSED:"collapsed"},p={WIDTH:"width",HEIGHT:"height"},q={ACTIVES:".panel > .in, .panel > .collapsing",DATA_TOGGLE:'[data-toggle="collapse"]'},r=function(){function h(b,d){c(this,h),this._isTransitioning=!1,this._element=b,this._config=this._getConfig(d),this._triggerArray=a.makeArray(a('[data-toggle="collapse"][href="#'+b.id+'"],'+('[data-toggle="collapse"][data-target="#'+b.id+'"]'))),this._parent=this._config.parent?this._getParent():null,this._config.parent||this._addAriaAndCollapsedClass(this._element,this._triggerArray),this._config.toggle&&this.toggle()}return e(h,[{key:"toggle",value:function(){a(this._element).hasClass(o.IN)?this.hide():this.show()}},{key:"show",value:function(){var b=this;if(!this._isTransitioning&&!a(this._element).hasClass(o.IN)){var c=void 0,d=void 0;if(this._parent&&(c=a.makeArray(a(q.ACTIVES)),c.length||(c=null)),!(c&&(d=a(c).data(g),d&&d._isTransitioning))){var e=a.Event(n.SHOW);if(a(this._element).trigger(e),!e.isDefaultPrevented()){c&&(h._jQueryInterface.call(a(c),"hide"),d||a(c).data(g,null));var i=this._getDimension();a(this._element).removeClass(o.COLLAPSE).addClass(o.COLLAPSING),this._element.style[i]=0,this._element.setAttribute("aria-expanded",!0),this._triggerArray.length&&a(this._triggerArray).removeClass(o.COLLAPSED).attr("aria-expanded",!0),this.setTransitioning(!0);var j=function(){a(b._element).removeClass(o.COLLAPSING).addClass(o.COLLAPSE).addClass(o.IN),b._element.style[i]="",b.setTransitioning(!1),a(b._element).trigger(n.SHOWN)};if(!f.supportsTransitionEnd())return void j();var l=i[0].toUpperCase()+i.slice(1),m="scroll"+l;a(this._element).one(f.TRANSITION_END,j).emulateTransitionEnd(k),this._element.style[i]=this._element[m]+"px"}}}}},{key:"hide",value:function(){var b=this;if(!this._isTransitioning&&a(this._element).hasClass(o.IN)){var c=a.Event(n.HIDE);if(a(this._element).trigger(c),!c.isDefaultPrevented()){var d=this._getDimension(),e=d===p.WIDTH?"offsetWidth":"offsetHeight";this._element.style[d]=this._element[e]+"px",f.reflow(this._element),a(this._element).addClass(o.COLLAPSING).removeClass(o.COLLAPSE).removeClass(o.IN),this._element.setAttribute("aria-expanded",!1),this._triggerArray.length&&a(this._triggerArray).addClass(o.COLLAPSED).attr("aria-expanded",!1),this.setTransitioning(!0);var g=function(){b.setTransitioning(!1),a(b._element).removeClass(o.COLLAPSING).addClass(o.COLLAPSE).trigger(n.HIDDEN)};return this._element.style[d]=0,f.supportsTransitionEnd()?void a(this._element).one(f.TRANSITION_END,g).emulateTransitionEnd(k):void g()}}}},{key:"setTransitioning",value:function(a){this._isTransitioning=a}},{key:"dispose",value:function(){a.removeData(this._element,g),this._config=null,this._parent=null,this._element=null,this._triggerArray=null,this._isTransitioning=null}},{key:"_getConfig",value:function(c){return c=a.extend({},l,c),c.toggle=Boolean(c.toggle),f.typeCheckConfig(b,c,m),c}},{key:"_getDimension",value:function(){var b=a(this._element).hasClass(p.WIDTH);return b?p.WIDTH:p.HEIGHT}},{key:"_getParent",value:function(){var b=this,c=a(this._config.parent)[0],d='[data-toggle="collapse"][data-parent="'+this._config.parent+'"]';return a(c).find(d).each(function(a,c){b._addAriaAndCollapsedClass(h._getTargetFromElement(c),[c])}),c}},{key:"_addAriaAndCollapsedClass",value:function(b,c){if(b){var d=a(b).hasClass(o.IN);b.setAttribute("aria-expanded",d),c.length&&a(c).toggleClass(o.COLLAPSED,!d).attr("aria-expanded",d)}}}],[{key:"_getTargetFromElement",value:function(b){var c=f.getSelectorFromElement(b);return c?a(c)[0]:null}},{key:"_jQueryInterface",value:function(b){return this.each(function(){var c=a(this),d=c.data(g),e=a.extend({},l,c.data(),"object"==typeof b&&b);if(!d&&e.toggle&&/show|hide/.test(b)&&(e.toggle=!1),d||(d=new h(this,e),c.data(g,d)),"string"==typeof b){if(void 0===d[b])throw new Error('No method named "'+b+'"');d[b]()}})}},{key:"VERSION",get:function(){return d}},{key:"Default",get:function(){return l}}]),h}();return a(document).on(n.CLICK_DATA_API,q.DATA_TOGGLE,function(b){b.preventDefault();var c=r._getTargetFromElement(this),d=a(c).data(g),e=d?"toggle":a(this).data();r._jQueryInterface.call(a(c),e)}),a.fn[b]=r._jQueryInterface,a.fn[b].Constructor=r,a.fn[b].noConflict=function(){return a.fn[b]=j,r._jQueryInterface},r}(jQuery),function(a){var b="dropdown",d="4.0.0-alpha.2",g="bs.dropdown",h="."+g,i=".data-api",j=a.fn[b],k={HIDE:"hide"+h,HIDDEN:"hidden"+h,SHOW:"show"+h,SHOWN:"shown"+h,CLICK:"click"+h,CLICK_DATA_API:"click"+h+i,KEYDOWN_DATA_API:"keydown"+h+i},l={BACKDROP:"dropdown-backdrop",DISABLED:"disabled",OPEN:"open"},m={BACKDROP:".dropdown-backdrop",DATA_TOGGLE:'[data-toggle="dropdown"]',FORM_CHILD:".dropdown form",ROLE_MENU:'[role="menu"]',ROLE_LISTBOX:'[role="listbox"]',NAVBAR_NAV:".navbar-nav",VISIBLE_ITEMS:'[role="menu"] li:not(.disabled) a, [role="listbox"] li:not(.disabled) a'},n=function(){function b(a){c(this,b),this._element=a,this._addEventListeners()}return e(b,[{key:"toggle",value:function(){if(this.disabled||a(this).hasClass(l.DISABLED))return!1;var c=b._getParentFromElement(this),d=a(c).hasClass(l.OPEN);if(b._clearMenus(),d)return!1;if("ontouchstart"in document.documentElement&&!a(c).closest(m.NAVBAR_NAV).length){var e=document.createElement("div");e.className=l.BACKDROP,a(e).insertBefore(this),a(e).on("click",b._clearMenus)}var f={relatedTarget:this},g=a.Event(k.SHOW,f);return a(c).trigger(g),!g.isDefaultPrevented()&&(this.focus(),this.setAttribute("aria-expanded","true"),a(c).toggleClass(l.OPEN),a(c).trigger(a.Event(k.SHOWN,f)),!1)}},{key:"dispose",value:function(){a.removeData(this._element,g),a(this._element).off(h),this._element=null}},{key:"_addEventListeners",value:function(){a(this._element).on(k.CLICK,this.toggle)}}],[{key:"_jQueryInterface",value:function(c){return this.each(function(){var d=a(this).data(g);if(d||a(this).data(g,d=new b(this)),"string"==typeof c){if(void 0===d[c])throw new Error('No method named "'+c+'"');d[c].call(this)}})}},{key:"_clearMenus",value:function(c){if(!c||3!==c.which){var d=a(m.BACKDROP)[0];d&&d.parentNode.removeChild(d);for(var e=a.makeArray(a(m.DATA_TOGGLE)),f=0;f<e.length;f++){var g=b._getParentFromElement(e[f]),h={relatedTarget:e[f]};if(a(g).hasClass(l.OPEN)&&!(c&&"click"===c.type&&/input|textarea/i.test(c.target.tagName)&&a.contains(g,c.target))){var i=a.Event(k.HIDE,h);a(g).trigger(i),i.isDefaultPrevented()||(e[f].setAttribute("aria-expanded","false"),a(g).removeClass(l.OPEN).trigger(a.Event(k.HIDDEN,h)))}}}}},{key:"_getParentFromElement",value:function(b){var c=void 0,d=f.getSelectorFromElement(b);return d&&(c=a(d)[0]),c||b.parentNode}},{key:"_dataApiKeydownHandler",value:function(c){if(/(38|40|27|32)/.test(c.which)&&!/input|textarea/i.test(c.target.tagName)&&(c.preventDefault(),c.stopPropagation(),!this.disabled&&!a(this).hasClass(l.DISABLED))){var d=b._getParentFromElement(this),e=a(d).hasClass(l.OPEN);if(!e&&27!==c.which||e&&27===c.which){if(27===c.which){var f=a(d).find(m.DATA_TOGGLE)[0];a(f).trigger("focus")}return void a(this).trigger("click")}var g=a.makeArray(a(m.VISIBLE_ITEMS));if(g=g.filter(function(a){return a.offsetWidth||a.offsetHeight}),g.length){var h=g.indexOf(c.target);38===c.which&&h>0&&h--,40===c.which&&h<g.length-1&&h++,h<0&&(h=0),g[h].focus()}}}},{key:"VERSION",get:function(){return d}}]),b}();return a(document).on(k.KEYDOWN_DATA_API,m.DATA_TOGGLE,n._dataApiKeydownHandler).on(k.KEYDOWN_DATA_API,m.ROLE_MENU,n._dataApiKeydownHandler).on(k.KEYDOWN_DATA_API,m.ROLE_LISTBOX,n._dataApiKeydownHandler).on(k.CLICK_DATA_API,n._clearMenus).on(k.CLICK_DATA_API,m.DATA_TOGGLE,n.prototype.toggle).on(k.CLICK_DATA_API,m.FORM_CHILD,function(a){a.stopPropagation()}),a.fn[b]=n._jQueryInterface,a.fn[b].Constructor=n,a.fn[b].noConflict=function(){return a.fn[b]=j,n._jQueryInterface},n}(jQuery),function(a){var b="modal",d="4.0.0-alpha.2",g="bs.modal",h="."+g,i=".data-api",j=a.fn[b],k=300,l=150,m={backdrop:!0,keyboard:!0,focus:!0,show:!0},n={backdrop:"(boolean|string)",keyboard:"boolean",focus:"boolean",show:"boolean"},o={HIDE:"hide"+h,HIDDEN:"hidden"+h,SHOW:"show"+h,SHOWN:"shown"+h,FOCUSIN:"focusin"+h,RESIZE:"resize"+h,CLICK_DISMISS:"click.dismiss"+h,KEYDOWN_DISMISS:"keydown.dismiss"+h,MOUSEUP_DISMISS:"mouseup.dismiss"+h,MOUSEDOWN_DISMISS:"mousedown.dismiss"+h,CLICK_DATA_API:"click"+h+i},p={SCROLLBAR_MEASURER:"modal-scrollbar-measure",BACKDROP:"modal-backdrop",OPEN:"modal-open",FADE:"fade",IN:"in"},q={DIALOG:".modal-dialog",DATA_TOGGLE:'[data-toggle="modal"]',DATA_DISMISS:'[data-dismiss="modal"]',FIXED_CONTENT:".navbar-fixed-top, .navbar-fixed-bottom, .is-fixed"},r=function(){function i(b,d){c(this,i),this._config=this._getConfig(d),this._element=b,this._dialog=a(b).find(q.DIALOG)[0],this._backdrop=null,this._isShown=!1,this._isBodyOverflowing=!1,this._ignoreBackdropClick=!1,this._originalBodyPadding=0,this._scrollbarWidth=0}return e(i,[{key:"toggle",value:function(a){return this._isShown?this.hide():this.show(a)}},{key:"show",value:function(b){var c=this,d=a.Event(o.SHOW,{relatedTarget:b});a(this._element).trigger(d),this._isShown||d.isDefaultPrevented()||(this._isShown=!0,this._checkScrollbar(),this._setScrollbar(),a(document.body).addClass(p.OPEN),this._setEscapeEvent(),this._setResizeEvent(),a(this._element).on(o.CLICK_DISMISS,q.DATA_DISMISS,a.proxy(this.hide,this)),a(this._dialog).on(o.MOUSEDOWN_DISMISS,function(){a(c._element).one(o.MOUSEUP_DISMISS,function(b){a(b.target).is(c._element)&&(c._ignoreBackdropClick=!0)})}),this._showBackdrop(a.proxy(this._showElement,this,b)))}},{key:"hide",value:function(b){b&&b.preventDefault();var c=a.Event(o.HIDE);a(this._element).trigger(c),this._isShown&&!c.isDefaultPrevented()&&(this._isShown=!1,this._setEscapeEvent(),this._setResizeEvent(),a(document).off(o.FOCUSIN),a(this._element).removeClass(p.IN),a(this._element).off(o.CLICK_DISMISS),a(this._dialog).off(o.MOUSEDOWN_DISMISS),f.supportsTransitionEnd()&&a(this._element).hasClass(p.FADE)?a(this._element).one(f.TRANSITION_END,a.proxy(this._hideModal,this)).emulateTransitionEnd(k):this._hideModal())}},{key:"dispose",value:function(){a.removeData(this._element,g),a(window).off(h),a(document).off(h),a(this._element).off(h),a(this._backdrop).off(h),this._config=null,this._element=null,this._dialog=null,this._backdrop=null,this._isShown=null,this._isBodyOverflowing=null,this._ignoreBackdropClick=null,this._originalBodyPadding=null,this._scrollbarWidth=null}},{key:"_getConfig",value:function(c){return c=a.extend({},m,c),f.typeCheckConfig(b,c,n),c}},{key:"_showElement",value:function(b){var c=this,d=f.supportsTransitionEnd()&&a(this._element).hasClass(p.FADE);this._element.parentNode&&this._element.parentNode.nodeType===Node.ELEMENT_NODE||document.body.appendChild(this._element),this._element.style.display="block",this._element.removeAttribute("aria-hidden"),this._element.scrollTop=0,d&&f.reflow(this._element),a(this._element).addClass(p.IN),this._config.focus&&this._enforceFocus();var e=a.Event(o.SHOWN,{relatedTarget:b}),g=function(){c._config.focus&&c._element.focus(),a(c._element).trigger(e)};d?a(this._dialog).one(f.TRANSITION_END,g).emulateTransitionEnd(k):g()}},{key:"_enforceFocus",value:function(){var b=this;a(document).off(o.FOCUSIN).on(o.FOCUSIN,function(c){document===c.target||b._element===c.target||a(b._element).has(c.target).length||b._element.focus()})}},{key:"_setEscapeEvent",value:function(){var b=this;this._isShown&&this._config.keyboard?a(this._element).on(o.KEYDOWN_DISMISS,function(a){27===a.which&&b.hide()}):this._isShown||a(this._element).off(o.KEYDOWN_DISMISS)}},{key:"_setResizeEvent",value:function(){this._isShown?a(window).on(o.RESIZE,a.proxy(this._handleUpdate,this)):a(window).off(o.RESIZE)}},{key:"_hideModal",value:function(){var b=this;this._element.style.display="none",this._element.setAttribute("aria-hidden","true"),this._showBackdrop(function(){a(document.body).removeClass(p.OPEN),b._resetAdjustments(),b._resetScrollbar(),a(b._element).trigger(o.HIDDEN)})}},{key:"_removeBackdrop",value:function(){this._backdrop&&(a(this._backdrop).remove(),this._backdrop=null)}},{key:"_showBackdrop",value:function(b){var c=this,d=a(this._element).hasClass(p.FADE)?p.FADE:"";if(this._isShown&&this._config.backdrop){var e=f.supportsTransitionEnd()&&d;if(this._backdrop=document.createElement("div"),this._backdrop.className=p.BACKDROP,d&&a(this._backdrop).addClass(d),a(this._backdrop).appendTo(document.body),a(this._element).on(o.CLICK_DISMISS,function(a){return c._ignoreBackdropClick?void(c._ignoreBackdropClick=!1):void(a.target===a.currentTarget&&("static"===c._config.backdrop?c._element.focus():c.hide()))}),e&&f.reflow(this._backdrop),a(this._backdrop).addClass(p.IN),!b)return;if(!e)return void b();a(this._backdrop).one(f.TRANSITION_END,b).emulateTransitionEnd(l)}else if(!this._isShown&&this._backdrop){a(this._backdrop).removeClass(p.IN);var g=function(){c._removeBackdrop(),b&&b()};f.supportsTransitionEnd()&&a(this._element).hasClass(p.FADE)?a(this._backdrop).one(f.TRANSITION_END,g).emulateTransitionEnd(l):g()}else b&&b()}},{key:"_handleUpdate",value:function(){this._adjustDialog()}},{key:"_adjustDialog",value:function(){var a=this._element.scrollHeight>document.documentElement.clientHeight;!this._isBodyOverflowing&&a&&(this._element.style.paddingLeft=this._scrollbarWidth+"px"),this._isBodyOverflowing&&!a&&(this._element.style.paddingRight=this._scrollbarWidth+"px~")}},{key:"_resetAdjustments",value:function(){this._element.style.paddingLeft="",this._element.style.paddingRight=""}},{key:"_checkScrollbar",value:function(){this._isBodyOverflowing=document.body.clientWidth<window.innerWidth,this._scrollbarWidth=this._getScrollbarWidth()}},{key:"_setScrollbar",value:function(){var b=parseInt(a(q.FIXED_CONTENT).css("padding-right")||0,10);this._originalBodyPadding=document.body.style.paddingRight||"",this._isBodyOverflowing&&(document.body.style.paddingRight=b+this._scrollbarWidth+"px")}},{key:"_resetScrollbar",value:function(){document.body.style.paddingRight=this._originalBodyPadding}},{key:"_getScrollbarWidth",value:function(){var a=document.createElement("div");a.className=p.SCROLLBAR_MEASURER,document.body.appendChild(a);var b=a.offsetWidth-a.clientWidth;return document.body.removeChild(a),b}}],[{key:"_jQueryInterface",value:function(b,c){return this.each(function(){var d=a(this).data(g),e=a.extend({},i.Default,a(this).data(),"object"==typeof b&&b);if(d||(d=new i(this,e),a(this).data(g,d)),"string"==typeof b){if(void 0===d[b])throw new Error('No method named "'+b+'"');d[b](c)}else e.show&&d.show(c)})}},{key:"VERSION",get:function(){return d}},{key:"Default",get:function(){return m}}]),i}();return a(document).on(o.CLICK_DATA_API,q.DATA_TOGGLE,function(b){var c=this,d=void 0,e=f.getSelectorFromElement(this);e&&(d=a(e)[0]);var h=a(d).data(g)?"toggle":a.extend({},a(d).data(),a(this).data());"A"===this.tagName&&b.preventDefault();var i=a(d).one(o.SHOW,function(b){b.isDefaultPrevented()||i.one(o.HIDDEN,function(){a(c).is(":visible")&&c.focus()})});r._jQueryInterface.call(a(d),h,this)}),a.fn[b]=r._jQueryInterface,a.fn[b].Constructor=r,a.fn[b].noConflict=function(){return a.fn[b]=j,r._jQueryInterface},r}(jQuery)),i=(function(a){var b="scrollspy",d="4.0.0-alpha.2",g="bs.scrollspy",h="."+g,i=".data-api",j=a.fn[b],k={offset:10,method:"auto",target:""},l={offset:"number",method:"string",target:"(string|element)"},m={ACTIVATE:"activate"+h,SCROLL:"scroll"+h,LOAD_DATA_API:"load"+h+i},n={DROPDOWN_ITEM:"dropdown-item",DROPDOWN_MENU:"dropdown-menu",NAV_LINK:"nav-link",NAV:"nav",ACTIVE:"active"},o={DATA_SPY:'[data-spy="scroll"]',ACTIVE:".active",LIST_ITEM:".list-item",LI:"li",LI_DROPDOWN:"li.dropdown",NAV_LINKS:".nav-link",DROPDOWN:".dropdown",DROPDOWN_ITEMS:".dropdown-item",DROPDOWN_TOGGLE:".dropdown-toggle"},p={OFFSET:"offset",POSITION:"position"},q=function(){function i(b,d){c(this,i),this._element=b,this._scrollElement="BODY"===b.tagName?window:b,this._config=this._getConfig(d),this._selector=this._config.target+" "+o.NAV_LINKS+","+(this._config.target+" "+o.DROPDOWN_ITEMS),this._offsets=[],this._targets=[],this._activeTarget=null,this._scrollHeight=0,a(this._scrollElement).on(m.SCROLL,a.proxy(this._process,this)),this.refresh(),this._process()}return e(i,[{key:"refresh",value:function(){var b=this,c=this._scrollElement!==this._scrollElement.window?p.POSITION:p.OFFSET,d="auto"===this._config.method?c:this._config.method,e=d===p.POSITION?this._getScrollTop():0;this._offsets=[],this._targets=[],this._scrollHeight=this._getScrollHeight();var g=a.makeArray(a(this._selector));g.map(function(b){var c=void 0,g=f.getSelectorFromElement(b);if(g&&(c=a(g)[0]),c&&(c.offsetWidth||c.offsetHeight))return[a(c)[d]().top+e,g]}).filter(function(a){return a}).sort(function(a,b){return a[0]-b[0]}).forEach(function(a){b._offsets.push(a[0]),b._targets.push(a[1])})}},{key:"dispose",value:function(){a.removeData(this._element,g),a(this._scrollElement).off(h),this._element=null,this._scrollElement=null,this._config=null,this._selector=null,this._offsets=null,this._targets=null,this._activeTarget=null,this._scrollHeight=null}},{key:"_getConfig",value:function(c){if(c=a.extend({},k,c),"string"!=typeof c.target){var d=a(c.target).attr("id");d||(d=f.getUID(b),a(c.target).attr("id",d)),c.target="#"+d}return f.typeCheckConfig(b,c,l),c}},{key:"_getScrollTop",value:function(){return this._scrollElement===window?this._scrollElement.scrollY:this._scrollElement.scrollTop}},{key:"_getScrollHeight",value:function(){return this._scrollElement.scrollHeight||Math.max(document.body.scrollHeight,document.documentElement.scrollHeight)}},{key:"_process",value:function(){var a=this._getScrollTop()+this._config.offset,b=this._getScrollHeight(),c=this._config.offset+b-this._scrollElement.offsetHeight;if(this._scrollHeight!==b&&this.refresh(),a>=c){var d=this._targets[this._targets.length-1];this._activeTarget!==d&&this._activate(d)}if(this._activeTarget&&a<this._offsets[0])return this._activeTarget=null,void this._clear();for(var e=this._offsets.length;e--;){var f=this._activeTarget!==this._targets[e]&&a>=this._offsets[e]&&(void 0===this._offsets[e+1]||a<this._offsets[e+1]);f&&this._activate(this._targets[e])}}},{key:"_activate",value:function(b){this._activeTarget=b,this._clear();var c=this._selector.split(",");c=c.map(function(a){return a+'[data-target="'+b+'"],'+(a+'[href="'+b+'"]')});var d=a(c.join(","));d.hasClass(n.DROPDOWN_ITEM)?(d.closest(o.DROPDOWN).find(o.DROPDOWN_TOGGLE).addClass(n.ACTIVE),d.addClass(n.ACTIVE)):d.parents(o.LI).find(o.NAV_LINKS).addClass(n.ACTIVE),a(this._scrollElement).trigger(m.ACTIVATE,{relatedTarget:b})}},{key:"_clear",value:function(){a(this._selector).filter(o.ACTIVE).removeClass(n.ACTIVE)}}],[{key:"_jQueryInterface",value:function(b){return this.each(function(){var c=a(this).data(g),d="object"==typeof b&&b||null;if(c||(c=new i(this,d),a(this).data(g,c)),"string"==typeof b){if(void 0===c[b])throw new Error('No method named "'+b+'"');c[b]()}})}},{key:"VERSION",get:function(){return d}},{key:"Default",get:function(){return k}}]),i}();return a(window).on(m.LOAD_DATA_API,function(){for(var b=a.makeArray(a(o.DATA_SPY)),c=b.length;c--;){var d=a(b[c]);q._jQueryInterface.call(d,d.data())}}),a.fn[b]=q._jQueryInterface,a.fn[b].Constructor=q,a.fn[b].noConflict=function(){return a.fn[b]=j,q._jQueryInterface},q}(jQuery),function(a){var b="tab",d="4.0.0-alpha.2",g="bs.tab",h="."+g,i=".data-api",j=a.fn[b],k=150,l={HIDE:"hide"+h,HIDDEN:"hidden"+h,SHOW:"show"+h,SHOWN:"shown"+h,CLICK_DATA_API:"click"+h+i},m={DROPDOWN_MENU:"dropdown-menu",ACTIVE:"active",FADE:"fade",IN:"in"},n={A:"a",LI:"li",DROPDOWN:".dropdown",UL:"ul:not(.dropdown-menu)",FADE_CHILD:"> .nav-item .fade, > .fade",ACTIVE:".active",ACTIVE_CHILD:"> .nav-item > .active, > .active",
DATA_TOGGLE:'[data-toggle="tab"], [data-toggle="pill"]',DROPDOWN_TOGGLE:".dropdown-toggle",DROPDOWN_ACTIVE_CHILD:"> .dropdown-menu .active"},o=function(){function b(a){c(this,b),this._element=a}return e(b,[{key:"show",value:function(){var b=this;if(!this._element.parentNode||this._element.parentNode.nodeType!==Node.ELEMENT_NODE||!a(this._element).hasClass(m.ACTIVE)){var c=void 0,d=void 0,e=a(this._element).closest(n.UL)[0],g=f.getSelectorFromElement(this._element);e&&(d=a.makeArray(a(e).find(n.ACTIVE)),d=d[d.length-1]);var h=a.Event(l.HIDE,{relatedTarget:this._element}),i=a.Event(l.SHOW,{relatedTarget:d});if(d&&a(d).trigger(h),a(this._element).trigger(i),!i.isDefaultPrevented()&&!h.isDefaultPrevented()){g&&(c=a(g)[0]),this._activate(this._element,e);var j=function(){var c=a.Event(l.HIDDEN,{relatedTarget:b._element}),e=a.Event(l.SHOWN,{relatedTarget:d});a(d).trigger(c),a(b._element).trigger(e)};c?this._activate(c,c.parentNode,j):j()}}}},{key:"dispose",value:function(){a.removeClass(this._element,g),this._element=null}},{key:"_activate",value:function(b,c,d){var e=a(c).find(n.ACTIVE_CHILD)[0],g=d&&f.supportsTransitionEnd()&&(e&&a(e).hasClass(m.FADE)||Boolean(a(c).find(n.FADE_CHILD)[0])),h=a.proxy(this._transitionComplete,this,b,e,g,d);e&&g?a(e).one(f.TRANSITION_END,h).emulateTransitionEnd(k):h(),e&&a(e).removeClass(m.IN)}},{key:"_transitionComplete",value:function(b,c,d,e){if(c){a(c).removeClass(m.ACTIVE);var g=a(c).find(n.DROPDOWN_ACTIVE_CHILD)[0];g&&a(g).removeClass(m.ACTIVE),c.setAttribute("aria-expanded",!1)}if(a(b).addClass(m.ACTIVE),b.setAttribute("aria-expanded",!0),d?(f.reflow(b),a(b).addClass(m.IN)):a(b).removeClass(m.FADE),b.parentNode&&a(b.parentNode).hasClass(m.DROPDOWN_MENU)){var h=a(b).closest(n.DROPDOWN)[0];h&&a(h).find(n.DROPDOWN_TOGGLE).addClass(m.ACTIVE),b.setAttribute("aria-expanded",!0)}e&&e()}}],[{key:"_jQueryInterface",value:function(c){return this.each(function(){var d=a(this),e=d.data(g);if(e||(e=e=new b(this),d.data(g,e)),"string"==typeof c){if(void 0===e[c])throw new Error('No method named "'+c+'"');e[c]()}})}},{key:"VERSION",get:function(){return d}}]),b}();return a(document).on(l.CLICK_DATA_API,n.DATA_TOGGLE,function(b){b.preventDefault(),o._jQueryInterface.call(a(this),"show")}),a.fn[b]=o._jQueryInterface,a.fn[b].Constructor=o,a.fn[b].noConflict=function(){return a.fn[b]=j,o._jQueryInterface},o}(jQuery),function(a){if(void 0===window.Tether)throw new Error("Bootstrap tooltips require Tether (http://github.hubspot.com/tether/)");var b="tooltip",d="4.0.0-alpha.2",g="bs.tooltip",h="."+g,i=a.fn[b],j=150,k="bs-tether",l={animation:!0,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,selector:!1,placement:"top",offset:"0 0",constraints:[]},m={animation:"boolean",template:"string",title:"(string|element|function)",trigger:"string",delay:"(number|object)",html:"boolean",selector:"(string|boolean)",placement:"(string|function)",offset:"string",constraints:"array"},n={TOP:"bottom center",RIGHT:"middle left",BOTTOM:"top center",LEFT:"middle right"},o={IN:"in",OUT:"out"},p={HIDE:"hide"+h,HIDDEN:"hidden"+h,SHOW:"show"+h,SHOWN:"shown"+h,INSERTED:"inserted"+h,CLICK:"click"+h,FOCUSIN:"focusin"+h,FOCUSOUT:"focusout"+h,MOUSEENTER:"mouseenter"+h,MOUSELEAVE:"mouseleave"+h},q={FADE:"fade",IN:"in"},r={TOOLTIP:".tooltip",TOOLTIP_INNER:".tooltip-inner"},s={element:!1,enabled:!1},t={HOVER:"hover",FOCUS:"focus",CLICK:"click",MANUAL:"manual"},u=function(){function i(a,b){c(this,i),this._isEnabled=!0,this._timeout=0,this._hoverState="",this._activeTrigger={},this._tether=null,this.element=a,this.config=this._getConfig(b),this.tip=null,this._setListeners()}return e(i,[{key:"enable",value:function(){this._isEnabled=!0}},{key:"disable",value:function(){this._isEnabled=!1}},{key:"toggleEnabled",value:function(){this._isEnabled=!this._isEnabled}},{key:"toggle",value:function(b){if(b){var c=this.constructor.DATA_KEY,d=a(b.currentTarget).data(c);d||(d=new this.constructor(b.currentTarget,this._getDelegateConfig()),a(b.currentTarget).data(c,d)),d._activeTrigger.click=!d._activeTrigger.click,d._isWithActiveTrigger()?d._enter(null,d):d._leave(null,d)}else{if(a(this.getTipElement()).hasClass(q.IN))return void this._leave(null,this);this._enter(null,this)}}},{key:"dispose",value:function(){clearTimeout(this._timeout),this.cleanupTether(),a.removeData(this.element,this.constructor.DATA_KEY),a(this.element).off(this.constructor.EVENT_KEY),this.tip&&a(this.tip).remove(),this._isEnabled=null,this._timeout=null,this._hoverState=null,this._activeTrigger=null,this._tether=null,this.element=null,this.config=null,this.tip=null}},{key:"show",value:function(){var b=this,c=a.Event(this.constructor.Event.SHOW);if(this.isWithContent()&&this._isEnabled){a(this.element).trigger(c);var d=a.contains(this.element.ownerDocument.documentElement,this.element);if(c.isDefaultPrevented()||!d)return;var e=this.getTipElement(),g=f.getUID(this.constructor.NAME);e.setAttribute("id",g),this.element.setAttribute("aria-describedby",g),this.setContent(),this.config.animation&&a(e).addClass(q.FADE);var h="function"==typeof this.config.placement?this.config.placement.call(this,e,this.element):this.config.placement,j=this._getAttachment(h);a(e).data(this.constructor.DATA_KEY,this).appendTo(document.body),a(this.element).trigger(this.constructor.Event.INSERTED),this._tether=new Tether({attachment:j,element:e,target:this.element,classes:s,classPrefix:k,offset:this.config.offset,constraints:this.config.constraints,addTargetClasses:!1}),f.reflow(e),this._tether.position(),a(e).addClass(q.IN);var l=function(){var c=b._hoverState;b._hoverState=null,a(b.element).trigger(b.constructor.Event.SHOWN),c===o.OUT&&b._leave(null,b)};if(f.supportsTransitionEnd()&&a(this.tip).hasClass(q.FADE))return void a(this.tip).one(f.TRANSITION_END,l).emulateTransitionEnd(i._TRANSITION_DURATION);l()}}},{key:"hide",value:function(b){var c=this,d=this.getTipElement(),e=a.Event(this.constructor.Event.HIDE),g=function(){c._hoverState!==o.IN&&d.parentNode&&d.parentNode.removeChild(d),c.element.removeAttribute("aria-describedby"),a(c.element).trigger(c.constructor.Event.HIDDEN),c.cleanupTether(),b&&b()};a(this.element).trigger(e),e.isDefaultPrevented()||(a(d).removeClass(q.IN),f.supportsTransitionEnd()&&a(this.tip).hasClass(q.FADE)?a(d).one(f.TRANSITION_END,g).emulateTransitionEnd(j):g(),this._hoverState="")}},{key:"isWithContent",value:function(){return Boolean(this.getTitle())}},{key:"getTipElement",value:function(){return this.tip=this.tip||a(this.config.template)[0]}},{key:"setContent",value:function(){var b=a(this.getTipElement());this.setElementContent(b.find(r.TOOLTIP_INNER),this.getTitle()),b.removeClass(q.FADE).removeClass(q.IN),this.cleanupTether()}},{key:"setElementContent",value:function(b,c){var d=this.config.html;"object"==typeof c&&(c.nodeType||c.jquery)?d?a(c).parent().is(b)||b.empty().append(c):b.text(a(c).text()):b[d?"html":"text"](c)}},{key:"getTitle",value:function(){var a=this.element.getAttribute("data-original-title");return a||(a="function"==typeof this.config.title?this.config.title.call(this.element):this.config.title),a}},{key:"cleanupTether",value:function(){this._tether&&this._tether.destroy()}},{key:"_getAttachment",value:function(a){return n[a.toUpperCase()]}},{key:"_setListeners",value:function(){var b=this,c=this.config.trigger.split(" ");c.forEach(function(c){if("click"===c)a(b.element).on(b.constructor.Event.CLICK,b.config.selector,a.proxy(b.toggle,b));else if(c!==t.MANUAL){var d=c===t.HOVER?b.constructor.Event.MOUSEENTER:b.constructor.Event.FOCUSIN,e=c===t.HOVER?b.constructor.Event.MOUSELEAVE:b.constructor.Event.FOCUSOUT;a(b.element).on(d,b.config.selector,a.proxy(b._enter,b)).on(e,b.config.selector,a.proxy(b._leave,b))}}),this.config.selector?this.config=a.extend({},this.config,{trigger:"manual",selector:""}):this._fixTitle()}},{key:"_fixTitle",value:function(){var a=typeof this.element.getAttribute("data-original-title");(this.element.getAttribute("title")||"string"!==a)&&(this.element.setAttribute("data-original-title",this.element.getAttribute("title")||""),this.element.setAttribute("title",""))}},{key:"_enter",value:function(b,c){var d=this.constructor.DATA_KEY;return c=c||a(b.currentTarget).data(d),c||(c=new this.constructor(b.currentTarget,this._getDelegateConfig()),a(b.currentTarget).data(d,c)),b&&(c._activeTrigger["focusin"===b.type?t.FOCUS:t.HOVER]=!0),a(c.getTipElement()).hasClass(q.IN)||c._hoverState===o.IN?void(c._hoverState=o.IN):(clearTimeout(c._timeout),c._hoverState=o.IN,c.config.delay&&c.config.delay.show?void(c._timeout=setTimeout(function(){c._hoverState===o.IN&&c.show()},c.config.delay.show)):void c.show())}},{key:"_leave",value:function(b,c){var d=this.constructor.DATA_KEY;if(c=c||a(b.currentTarget).data(d),c||(c=new this.constructor(b.currentTarget,this._getDelegateConfig()),a(b.currentTarget).data(d,c)),b&&(c._activeTrigger["focusout"===b.type?t.FOCUS:t.HOVER]=!1),!c._isWithActiveTrigger())return clearTimeout(c._timeout),c._hoverState=o.OUT,c.config.delay&&c.config.delay.hide?void(c._timeout=setTimeout(function(){c._hoverState===o.OUT&&c.hide()},c.config.delay.hide)):void c.hide()}},{key:"_isWithActiveTrigger",value:function(){for(var a in this._activeTrigger)if(this._activeTrigger[a])return!0;return!1}},{key:"_getConfig",value:function(c){return c=a.extend({},this.constructor.Default,a(this.element).data(),c),c.delay&&"number"==typeof c.delay&&(c.delay={show:c.delay,hide:c.delay}),f.typeCheckConfig(b,c,this.constructor.DefaultType),c}},{key:"_getDelegateConfig",value:function(){var a={};if(this.config)for(var b in this.config)this.constructor.Default[b]!==this.config[b]&&(a[b]=this.config[b]);return a}}],[{key:"_jQueryInterface",value:function(b){return this.each(function(){var c=a(this).data(g),d="object"==typeof b?b:null;if((c||!/destroy|hide/.test(b))&&(c||(c=new i(this,d),a(this).data(g,c)),"string"==typeof b)){if(void 0===c[b])throw new Error('No method named "'+b+'"');c[b]()}})}},{key:"VERSION",get:function(){return d}},{key:"Default",get:function(){return l}},{key:"NAME",get:function(){return b}},{key:"DATA_KEY",get:function(){return g}},{key:"Event",get:function(){return p}},{key:"EVENT_KEY",get:function(){return h}},{key:"DefaultType",get:function(){return m}}]),i}();return a.fn[b]=u._jQueryInterface,a.fn[b].Constructor=u,a.fn[b].noConflict=function(){return a.fn[b]=i,u._jQueryInterface},u}(jQuery));(function(a){var f="popover",g="4.0.0-alpha.2",h="bs.popover",j="."+h,k=a.fn[f],l=a.extend({},i.Default,{placement:"right",trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="popover-arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'}),m=a.extend({},i.DefaultType,{content:"(string|element|function)"}),n={FADE:"fade",IN:"in"},o={TITLE:".popover-title",CONTENT:".popover-content",ARROW:".popover-arrow"},p={HIDE:"hide"+j,HIDDEN:"hidden"+j,SHOW:"show"+j,SHOWN:"shown"+j,INSERTED:"inserted"+j,CLICK:"click"+j,FOCUSIN:"focusin"+j,FOCUSOUT:"focusout"+j,MOUSEENTER:"mouseenter"+j,MOUSELEAVE:"mouseleave"+j},q=function(i){function k(){c(this,k),d(Object.getPrototypeOf(k.prototype),"constructor",this).apply(this,arguments)}return b(k,i),e(k,[{key:"isWithContent",value:function(){return this.getTitle()||this._getContent()}},{key:"getTipElement",value:function(){return this.tip=this.tip||a(this.config.template)[0]}},{key:"setContent",value:function(){var b=a(this.getTipElement());this.setElementContent(b.find(o.TITLE),this.getTitle()),this.setElementContent(b.find(o.CONTENT),this._getContent()),b.removeClass(n.FADE).removeClass(n.IN),this.cleanupTether()}},{key:"_getContent",value:function(){return this.element.getAttribute("data-content")||("function"==typeof this.config.content?this.config.content.call(this.element):this.config.content)}}],[{key:"_jQueryInterface",value:function(b){return this.each(function(){var c=a(this).data(h),d="object"==typeof b?b:null;if((c||!/destroy|hide/.test(b))&&(c||(c=new k(this,d),a(this).data(h,c)),"string"==typeof b)){if(void 0===c[b])throw new Error('No method named "'+b+'"');c[b]()}})}},{key:"VERSION",get:function(){return g}},{key:"Default",get:function(){return l}},{key:"NAME",get:function(){return f}},{key:"DATA_KEY",get:function(){return h}},{key:"Event",get:function(){return p}},{key:"EVENT_KEY",get:function(){return j}},{key:"DefaultType",get:function(){return m}}]),k}(i);return a.fn[f]=q._jQueryInterface,a.fn[f].Constructor=q,a.fn[f].noConflict=function(){return a.fn[f]=k,q._jQueryInterface},q})(jQuery),function(a){var h="carouselIngresso",i="4.0.0-alpha.2",j="bs.ingresso.carousel",k="."+j,l=".data-api",m=a.fn[h],n={interval:5e3,keyboard:!0,slide:!1,pause:"hover",wrap:!0},o={SLIDE:"slide"+k,SLID:"slid"+k,KEYDOWN:"keydown"+k,MOUSEENTER:"mouseenter"+k,MOUSELEAVE:"mouseleave"+k,LOAD_DATA_API:"load"+k+l,CLICK_DATA_API:"click"+k+l},p={CAROUSEL:"carousel-ingresso",ACTIVE:"active",SLIDE:"slide",RIGHT:"right",LEFT:"left",ITEM:"carousel-item"},q={ACTIVE:".active",ACTIVE_ITEM:".active.carousel-item",ITEM:".carousel-item",NEXT_PREV:".next, .prev",INDICATORS:".carousel-indicators",DATA_SLIDE:"[data-slide], [data-slide-to]",DATA_RIDE:'[data-ride="carousel-ingresso"]'},r=function(g){function h(a,b){c(this,h),d(Object.getPrototypeOf(h.prototype),"constructor",this).call(this,a,b),this._showThumbnails()}return b(h,g),e(h,[{key:"_showThumbnails",value:function(){a(q.DATA_SLIDE,this._indicatorsElement).each(function(b){var c=a(this),d=a(q.ITEM).eq(b).children("img");c.css("background-image","url("+d.attr("src")+")")})}}],[{key:"_jQueryInterface",value:function(b){return this.each(function(){var c=a(this).data(j),d=a.extend({},n,a(this).data());"object"==typeof b&&a.extend(d,b);var e="string"==typeof b?b:d.slide;if(c||(c=new h(this,d),a(this).data(j,c)),"number"==typeof b)c.to(b);else if("string"==typeof e){if(void 0===c[e])throw new Error('No method named "'+e+'"');c[e]()}else d.interval&&(c.pause(),c.cycle())})}},{key:"_dataApiClickHandler",value:function(b){var c=f.getSelectorFromElement(this);if(c){var d=a(c)[0];if(d&&a(d).hasClass(p.CAROUSEL)){var e=a.extend({},a(d).data(),a(this).data()),g=this.getAttribute("data-slide-to");g&&(e.interval=!1),h._jQueryInterface.call(a(d),e),g&&a(d).data(j).to(g),b.preventDefault()}}}},{key:"VERSION",get:function(){return i}},{key:"Default",get:function(){return n}}]),h}(g);return a(document).on(o.CLICK_DATA_API,q.DATA_SLIDE,r._dataApiClickHandler),a(window).on(o.LOAD_DATA_API,function(){a(q.DATA_RIDE).each(function(){var b=a(this);r._jQueryInterface.call(b,b.data())})}),a.fn[h]=r._jQueryInterface,a.fn[h].Constructor=r,a.fn[h].noConflict=function(){return a.fn[h]=m,r._jQueryInterface},r}(jQuery);!function(){window.lazySizesConfig=window.lazySizesConfig||{},window.lazySizesConfig.expand=10,window.lazySizesConfig.expFactor=1.5,window.lazySizesConfig.hFac=1.5,window.lazySizesConfig.loadMode=1,window.lazySizesConfig.customMedia={"--small":"(min-width: 544px)","--medium":"(min-width: 768px)","--large":"(min-width: 992px)","--xlarge":"(min-width: 1440px)"}}();(function(a){var g="modalIngresso",i="4.0.0-alpha.2",j="bs.ingresso.modal",k="."+j,l=".data-api",m=a.fn[g],n={backdrop:!0,keyboard:!0,focus:!0,show:!0},o={HIDE:"hide"+k,HIDDEN:"hidden"+k,SHOW:"show"+k,SHOWN:"shown"+k,FOCUSIN:"focusin"+k,RESIZE:"resize"+k,CLICK_DISMISS:"click.dismiss"+k,KEYDOWN_DISMISS:"keydown.dismiss"+k,MOUSEUP_DISMISS:"mouseup.dismiss"+k,MOUSEDOWN_DISMISS:"mousedown.dismiss"+k,CLICK_DATA_API:"click"+k+l},p={DIALOG:".modal-dialog",DATA_TOGGLE:'[data-toggle="modal-ingresso"]',DATA_DISMISS:'[data-dismiss="modal"]',FIXED_CONTENT:".navbar-fixed-top, .navbar-fixed-bottom, .is-fixed",IFRAMES:"iframe[data-src]"},q=function(f){function g(a,b){c(this,g),d(Object.getPrototypeOf(g.prototype),"constructor",this).call(this,a,b)}return b(g,f),e(g,[{key:"show",value:function(a){d(Object.getPrototypeOf(g.prototype),"show",this).call(this,a),this._loadSource()}},{key:"hide",value:function(b){var c=this;d(Object.getPrototypeOf(g.prototype),"hide",this).call(this,b);var e;a(p.IFRAMES,this._dialog).each(function(b,d){var f=a(d),g=f.attr("src")||f.attr("data-src");console.log(g),c._matchYouTube(g)?(e=c._youTubePlayer(d),e.stop()):c._matchDailymotion(g)&&(e=c._dailyMotionPlayer(d),e.pause())})}},{key:"_loadSource",value:function(){a(p.IFRAMES,this._dialog).each(function(b,c){var d=a(c);if("string"!=typeof d.attr("src")){var e=d.attr("data-src");d.attr("src",e)}})}},{key:"_youTubePlayer",value:function(a){function b(a){c.postMessage('{"event":"command","func":"'+a+'","args":""}',"*")}var c=a.contentWindow;return{play:function(){b("playVideo")},pause:function(){b("pauseVideo")},stop:function(){b("stopVideo")}}}},{key:"_dailyMotionPlayer",value:function(a){function b(a){c.postMessage('{"command":"'+a+'","parameters":[]}',"*")}var c=a.contentWindow;return{play:function(){b("play")},pause:function(){b("pause")}}}},{key:"_matchYouTube",value:function(a){var b=new RegExp("^(https?:)?(//)?(www.youtube.com|youtu.?be|youtube.com)/.+$");return!!a.match(b)&&!!a.match(b).length}},{key:"_matchDailymotion",value:function(a){var b=new RegExp("^(https?:)?(//)?(www.dailymotion.com|dailymotion.com)/.+$");return!!a.match(b)&&!!a.match(b).length}}],[{key:"_jQueryInterface",value:function(b,c){return this.each(function(){var d=a(this).data(j),e=a.extend({},g.Default,a(this).data(),"object"==typeof b&&b);if(d||(d=new g(this,e),a(this).data(j,d)),"string"==typeof b){if(void 0===d[b])throw new Error('No method named "'+b+'"');d[b](c)}else e.show&&d.show(c)})}},{key:"VERSION",get:function(){return i}},{key:"Default",get:function(){return n}}]),g}(h);return a(document).on(o.CLICK_DATA_API,p.DATA_TOGGLE,function(b){var c=this,d=void 0,e=f.getSelectorFromElement(this);e&&(d=a(e)[0]);var g=a(d).data(j)?"toggle":a.extend({},a(d).data(),a(this).data());"A"===this.tagName&&b.preventDefault();var h=a(d).one(o.SHOW,function(b){b.isDefaultPrevented()||h.one(o.HIDDEN,function(){a(c).is(":visible")&&c.focus()})});q._jQueryInterface.call(a(d),g,this)}),a.fn[g]=q._jQueryInterface,a.fn[g].Constructor=q,a.fn[g].noConflict=function(){return a.fn[g]=m,q._jQueryInterface},q})(jQuery)}(jQuery);
(function(){

    /* TODO - precisa refazer a estrutura desse codigo */
    var components = {

          collapse:{
            'js-md':{
              match:function(){

                //temp
                $('.js-md[data-toggle="collapse"]').each(function(){

                  var $element = $(this),
                  $content = $($element.attr('href')),
                  unmatchClasses = ['model']

                  $element.removeClass('collapsed');
                  $content
                  .removeClass('collapse')
                  .removeAttr('style');

                  if($element.hasClass('model1')){
                    $element.data('js-md-unmatch-classes','model1')
                    $element.removeClass('model1')
                  }

                  $element.data('original-href',$element.attr('href'));
                  $element.removeAttr('href');


                });

                console.log('match dropdown medium');
              },
              unmatch:function(){

                //temp
                $('.js-md[data-toggle="collapse"]').each(function(){

                  var $element = $(this),
                  $content = $($element.data('original-href'));

                  $element.addClass('collapsed');
                  $content
                  .addClass('collapse');

                  if($element.data('js-md-unmatch-classes') == 'model1'){
                    $element.addClass('model1')
                  }

                  $element.attr('href',$element.data('original-href'));


                });

                console.log('unmatch dropdown medium');
              },
              setup:function(){
                //console.log('iniciou dropdown medium');
              }
            },
            'js-lg':{
              match:function(){

                //temp
                $('.js-lg[data-toggle="collapse"]').each(function(){

                  var $element = $(this),
                  $content = $($element.attr('href')),
                  unmatchClasses = ['model']

                  $element.removeClass('collapsed');
                  $content
                  .removeClass('collapse')
                  .removeAttr('style');

                  if($element.hasClass('model1')){
                    $element.data('js-lg-unmatch-classes','model1')
                    $element.removeClass('model1')
                  }

                  $element.data('original-href',$element.attr('href'));
                  $element.removeAttr('href');


                });

                console.log('match dropdown large');
              },
              unmatch:function(){

                //temp
                $('.js-lg[data-toggle="collapse"]').each(function(){

                  var $element = $(this),
                  $content = $($element.data('original-href'));

                  $element.addClass('collapsed');
                  $content
                  .addClass('collapse');

                  if($element.data('js-lg-unmatch-classes') == 'model1'){
                    $element.addClass('model1')

                  }
                  $element.attr('href',$element.data('original-href'));


                });

                console.log('unmatch dropdown large');
              },
              setup:function(){
                //console.log('iniciou dropdown medium');
              }
            },
            'js-sm':{
              match:function(){

                console.log('match dropdown small')
              },
              unmatch:function(){
                console.log('unmatch dropdown small');
              },
              setup:function(){
                //console.log('iniciou dropdown small');
              }
            }
          },
          /*carousel:{
            'js-md':{
              match:function(){
                console.log('match carousel medium');
              },
              unmatch:function(){
                console.log('unmatch carousel medium');
              },
              setup:function(){
                console.log('iniciou carousel medium');
              }
            },
            'js-sm':{
              match:function(){
                console.log('match carousel small');
              },
              unmatch:function(){
                console.log('unmatch carousel small');
              },
              setup:function(){
                console.log('iniciou carousel small');
              }
            }
          }*/
        }
        var medias = {
          small:'(min-width:568px)',
          medium:'(min-width:769px)',
          large:'(min-width:992px)'
        };

        for(var media in medias){

          var this_media_components = [];

          /*
            para cada componente, verifica-se quais batem com a midia atual
            reune-se todos os componentes dessa media para ser executado posteriormente
            no local adequado
          */
          for (var component in components){

            if(media == 'small'){

              this_media_components.push(components[component]['js-sm']);
            }
            else if(media == 'medium'){

              this_media_components.push(components[component]['js-md']);
            }
            else if(media == 'large'){

              this_media_components.push(components[component]['js-lg']);
            }
          }
          console.log('executando media '+media);
          //para cada media, executa-se equire.register()
          enquire.register(medias[media],this_media_components);

          // reseting
          this_media_components = [];
        };
  })();

  // swiper
  ;(function(){
    $('[data-ride="swiper-ingresso"], [data-ride="swiper-ingresso-side"], [data-ride="swiper-ingresso-list"]').each(function () {
      var $elm = $(this);
      var $body = $('body');
      var $nextButton = $('.swiper-button-next', $elm);
      var $prevButton = $('.swiper-button-prev', $elm);

      var options = {
        carousel:{
          nextButton: $nextButton,
          prevButton: $prevButton,
          paginationClickable:true,
          preloadImages:false,
          simulateTouch: false,
          speed:500,
          preventClicksPropagation: false,
          pagination:$('.swiper-pagination', $elm).length ? $('.swiper-pagination', $elm) : null
        },
        thumbs:{
          slideToClickedSlide: true,
          slidesPerView: 4,
          spaceBetween: 10,
          touchRatio: 0.2,
          centeredSlides:true
        }
      };

      if ($body.hasClass('atm-v2')) {
        options.carousel = $.extend({},
          { onReachBeginning: function(swiper){
            console.log("beginning")
            var $carousel = $(swiper.container).parent();
            $carousel.addClass('is-beginning');
          },
          onSlideChangeEnd:function(swiper){
            var $carousel = $(swiper.container).parent();
            if(!swiper.isBeginning && !swiper.isEnd){
              $carousel.removeClass('is-beginning is-end');
            } else if(!swiper.isBeginning){
              $carousel.removeClass('is-beginning');
            } else if(!swiper.isEnd){
              $carousel.removeClass('is-end');
            }
          },
          onReachEnd:function(swiper){
            var $carousel = $(swiper.container).parent();
            $carousel.addClass('is-end');
          }, simulateTouch: false, }, options.carousel);
      }

      if ($elm.attr('data-ride') == 'swiper-ingresso-side') {
        options.thumbs = $.extend({}, { direction:'vertical', setWrapperSize:true }, options.thumbs);
      }

      else if($elm.attr('data-ride') == 'swiper-ingresso-list'){
        options.carousel = $.extend({}, {
          slidesPerView:'auto',
          slidesPerGroup:1,
          speed:800,
          //loop:true,
          freeMode:true,
          mousewheelControl:true,
          mousewheelForceToAxis:true,
          preloadImages:false,
          //lazyLoadingInPrevNext:true,
          watchSlidesVisibility:true,
          simulateTouch: false
        }, options.carousel);
        /* CORRECAO PARA O BANNER WALLPAPER */
        if($('body.home-page').hasClass('has-wallpaper-banner')){
          options.carousel = $.extend(options.carousel, {
            breakpoints:{
              543:{
                slidesPerGroup:1
              },
              767:{
                slidesPerGroup:2
              },
              1919:{
                slidesPerGroup:3
              }
            }
          });
        }
      }
      else {
        options.carousel = $.extend(options.carousel, {
          autoplay:8000,
          loop:true,
          lazyLoading:true,
          lazyLoadingOnTransitionStart:true
        });

        options.thumbs = $.extend({}, {
          centeredSlides: true,
          breakpoints: {
            // >= 320
            320: {
              slidesPerView: 2,
              spaceBetweenSlides: 5
            },
            // >= 544
            544:{
              slidesPerView: 3,
              spaceBetweenSlides: 5
            },
            // >= 768
            768: {
              slidesPerView: 4,
              spaceBetweenSlides: 10
            }
          }
        }, options.thumbs);
      }

      if ($elm.hasClass('swiper-ingresso-column')) {
        options.carousel = $.extend({}, { 
          slidesPerColumn: 2,
          slidesPerGroup:2 
          }, options.carousel);
      }

      if($('.gallery-top', $elm).length){
        var galleryTop = new Swiper($('.gallery-top', $elm), options.carousel);
      }
      if($('.gallery-thumbs', $elm).length){
        var galleryThumbs = new Swiper($('.gallery-thumbs', $elm), options.thumbs);
      }

      if($('.gallery-thumbs', $elm).length && $('.gallery-top', $elm).length){
        galleryTop.params.control = galleryThumbs;
        galleryThumbs.params.control = galleryTop;
      }

      if ($elm.hasClass('swiper-ingresso-column')) {
        $nextButton.click(function() {
            watchButtons($elm, $prevButton, $nextButton);
        });

        $prevButton.click(function() {
            watchButtons($elm, $prevButton, $nextButton);
        });
      }

    });

    //Swiper ATM Vertical
    $('.swipe-vertical').each(function () {
      var $elm = $(this);
      var $swiperWide = $('.swiper-vertical-wide');

      var $bottomButton = $elm.hasClass('swiper-vertical-wide') ? $('.swipe-btn-bottom', $swiperWide) : $('.swipe-btn-bottom', $elm);
      var $topButton = $elm.hasClass('swiper-vertical-wide') ? $('.swipe-btn-top', $swiperWide) : $('.swipe-btn-top', $elm);

      options = {
        slidesPerView: $elm.hasClass('swiper-vertical-wide') ?  'auto' : 5,
        nextButton: $bottomButton,
        prevButton: $topButton,
        direction: 'vertical',

      };

      if ($elm.hasClass('swiper-vertical-wide')) {
        options = $.extend({}, {
              slidesPerColumnFill: 'row',
              slidesPerColumn: 3,
              onReachBeginning: function(swiper){
                var $carousel = $(swiper.container).parent();
                $carousel.addClass('is-beginning');
              },
              onSlideChangeEnd:function(swiper){
                var $carousel = $(swiper.container).parent();

                if(!swiper.isBeginning && !swiper.isEnd){
                  $carousel.removeClass('is-beginning is-end');
                } else if(!swiper.isBeginning){
                  $carousel.removeClass('is-beginning');
                } else if(!swiper.isEnd){
                  $carousel.removeClass('is-end');
                }
              },
              onReachEnd:function(swiper){
                var $carousel = $(swiper.container).parent();
                $carousel.addClass('is-end');
              }
        }, options);

      };

      var verticalSwiper = new Swiper($('.swiper-vertical-container', $elm), options);

      if ($elm.hasClass('swiper-vertical-column')) {
        $bottomButton.click(function() {
            watchButtons($swiperWide, $topButton, $bottomButton);
        });

        $topButton.click(function() {
            watchButtons($swiperWide, $topButton, $bottomButton);
        });
      }
    });

    watchButtons = function($carousel, $rightButton, $leftButton) {
      if (!$leftButton.hasClass('swiper-button-disabled') &&
          !$rightButton.hasClass('swiper-button-disabled')) {
        $carousel.removeClass('is-beginning');
        $carousel.removeClass('is-end');
        return;
      }

      if ($leftButton.hasClass('swiper-button-disabled')) {
        $carousel.addClass('is-end');
        $carousel.removeClass('is-beginning');
      } 
      
      if ($rightButton.hasClass('swiper-button-disabled')) {
        $carousel.addClass('is-beginning');
        $carousel.removeClass('is-end');
      }
    }

    var $win = $(window);
    //Swiper Home ATM
    $('.swiper-ingresso-atm').each(function () {
      var $elm = $(this);

      var verticalSwiper = new Swiper($('.swiper-container-atm', $elm), {
        slidesPerView:  $($win).width() >= 1366 ? 'auto' : 3,
        spaceBetween: 20,
        simulateTouch: false,
        centeredSlides: true,
        speed: 600,
        nextButton: $('.swiper-button-next', $elm),
        prevButton: $('.swiper-button-prev', $elm),
        onReachBeginning:function(swiper){
          var $carousel = $(swiper.container).parent();
          $carousel.addClass('is-beginning');
        },
        onSlideChangeEnd:function(swiper){
          var $carousel = $(swiper.container).parent();
          if(!swiper.isBeginning && !swiper.isEnd){
            $carousel.removeClass('is-beginning is-end');
          } else if(!swiper.isBeginning){
            $carousel.removeClass('is-beginning');
          } else if(!swiper.isEnd){
            $carousel.removeClass('is-end');
          }
        },
        onReachEnd:function(swiper){
          var $carousel = $(swiper.container).parent();
          $carousel.addClass('is-end');
        }
      });
    });

    //Swiper Home ATM
    $('.swiper-ingresso-atm-v2').each(function () {
      var $elm = $(this);
      var $body = $('body');

      options = {
        slidesPerView: 'auto',
        initialSlide: 0,
        spaceBetween: 40,
        centeredSlides: true,
        autoplay: 2000,
        autoplayDisableOnInteraction: false,
        speed: 400,
        nextButton: $('.swiper-button-next', $elm),
        prevButton: $('.swiper-button-prev', $elm),
        onReachBeginning:function(swiper){
          var $carousel = $(swiper.container).parent();
          $carousel.addClass('is-beginning');
        },
        onSlideChangeEnd:function(swiper){
          var $swiper = $('.swiper-atm-wapper');
          addClassInactive( $swiper.children() );
          addClassSlideVisible( $swiper.children() );
          var $carousel = $(swiper.container).parent();
          if(!swiper.isBeginning && !swiper.isEnd){
            $carousel.removeClass('is-beginning is-end');
          } else if(!swiper.isBeginning){
            $carousel.removeClass('is-beginning');
          } else if(!swiper.isEnd){
            $carousel.removeClass('is-end');
          }
        },
        onReachEnd:function(swiper){
          var $carousel = $(swiper.container).parent();
          $carousel.addClass('is-end');
        }
      };

      if ($body.hasClass('home-page-v2')) {
        options = $.extend({}, {
          
        }, options);
      } 
      var verticalSwiper = new Swiper($('.swiper-container-atm-v2', $elm), options);
    });

    self.addClassInactive = function ($childrens) {

        for (var i = 0; i < $childrens.length; i++) {

            if (!$childrens.eq(i).hasClass('swiper-slide-inactive') &&
                !$childrens.eq(i).hasClass('swiper-slide-active')) {

                $childrens.eq(i).addClass('swiper-slide-inactive');
            } 

            if ($childrens.eq(i).hasClass('swiper-slide-activ')) {
                $childrens.eq(i).removeClass('swiper-slide-activ');
            }
        }

    }

    self.addClassSlideVisible = function ($childrens) {

        for (var i = 0; i < $childrens.length; i++) {

            if ($childrens.eq(i).hasClass('swiper-slide-active')) {
                
                $childrens.eq(i)
                    .removeClass('swiper-slide-inactive')
                    .addClass('swiper-slide-activ');
                break;
            } 

        }

    }

    self.addClassSlideTwoVisible = function ($childrens) {

        for (var i = 0; i < $childrens.length; i++) {

            if ($childrens.eq(i).hasClass('swiper-slide-active')) {

                $childrens.eq(i)
                    .removeClass('swiper-slide-inactive')
                    .addClass('swiper-slide-activ');
                break;
            }
        }
    }

    var $swiper = $('.swiper-atm-wapper');
    var $win = $(window);

    addClassInactive( $swiper.children() );
    if ($($win).width() >= 1366) {  
        addClassSlideTwoVisible( $swiper.children() );
    } else {
        addClassSlideVisible( $swiper.children() );
    }

    $('.swiper-button-next').click(function() {
      addClassInactive( $swiper.children() );

      if ($($win).width() >= 1366) {  
         addClassSlideTwoVisible( $swiper.children() );
      } else {
         addClassSlideVisible( $swiper.children() );
      }
    });

    $('.swiper-button-prev').click(function() {
      addClassInactive( $swiper.children() );

      if ($($win).width() >= 1366) {  
         addClassSlideTwoVisible( $swiper.children() );
      } else {
         addClassSlideVisible( $swiper.children() );
      }
    });

    $($win).resize(function(){

       if ($($win).width() >= 1366) {  
         addClassInactive( $swiper.children() );
         addClassSlideTwoVisible( $swiper.children() );
       }     
    });

    var swiper = new Swiper('.swiper-timeline .swiper-container', {
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      initialSlide: 12,
      spaceBetween: 10,
      centeredSlides: true,
      slidesPerView: 'auto',
      slideToClickedSlide: true,
      simulateTouch: false,
      onReachBeginning:function(swiper){
        var $carousel = $(swiper.container).parent();
        $carousel.addClass('is-beginning');
      },
      onSlideChangeEnd:function(swiper){
        var $carousel = $(swiper.container).parent();
        if(!swiper.isBeginning && !swiper.isEnd){
          $carousel.removeClass('is-beginning is-end');
        } else if(!swiper.isBeginning){
          $carousel.removeClass('is-beginning');
        } else if(!swiper.isEnd){
          $carousel.removeClass('is-end');
        }
      },
      onReachEnd:function(swiper){
        var $carousel = $(swiper.container).parent();
        $carousel.addClass('is-end');
      }
      });

  })();

  /* temp js to show filter beahavior */
  ;(function(){
    /* temp code - for behavior purpose */
    $('.filter').each(function(){
      var $filter = $(this);
      var $head = $('[data-toggle="collapse"]',this);
      var $options = $('[role=option]',this);
      var $removeButton = $('.filter-action',this);
      var $filterContent = $(".filter-content",this);

      function _constructor(){
        $('a',$options).click(function(){
          _addItem.call(this,$(this).attr('data-val') || $(this).text());
          $head.closest('.tab-accordion').addClass('filter-selected');
        });

        $removeButton.on('click',function(e){
          _removeItem.call(this);
          e.stopPropagation();
        });
      }

      function _closeCollapse(){
        $filterContent.collapse('hide');
      }

      function _getFilterValue(){
        return $head.children('.filter-val').text();
      }

      function _addItem(newValue){
        if(_getFilterValue() !== newValue){
          $head.children('.filter-val').text(newValue);
          $options.filter('.is-selected').removeClass('is-selected');
          $filter.addClass('filter-has-val');

          $(this).closest('.filter-it')
          .addClass('is-selected')
          .trigger('filter.added');

          _closeCollapse();
        }

      }
      function _removeItem(){
        $head.children('.filter-val').text('');
        $options.filter('.is-selected').removeClass('is-selected');
        $filter.removeClass('filter-has-val');
      }

      _constructor.call(this);

    });

  })();

  ;(function(){
    /* esse timeout e por causa do do static. Tudo por causa de um plugin chamado w3-include.js*/
    setTimeout(function(){
      // Slide page component
      $('[data-slide-page-link], [data-slide-page-back]').each(function () {
        var $el = $(this);


        if (typeof $el.attr('data-slide-page-link') == 'string'){

          var _$page = $($el.attr('data-slide-page-link'));

          _$page.on('transitionend',function(){
            _$page.removeClass('is-animating');
          });

          $el.click(function (e) {
            _$page
            .addClass('is-animating')
            .toggleClass('active')
            .find('> .slide-page-wp > [data-slide-page-side]')
            .toggleClass('active')
          })


        }
        else {
          var _$page = $($el.attr('data-slide-page-back'));
          $el.click(function (e) {
            _$page
            .addClass('is-animating')
            .toggleClass('active')
            .find('> .slide-page-wp > [data-slide-page-side]')
            .toggleClass('active')
          })
        }

      })
    },3000)
  })()

  ;(function(){
    if ('addEventListener' in document) {
      document.addEventListener('DOMContentLoaded', function() {
          FastClick.attach(document.body);
      }, false);
  }
  })();

  ;(function(){

        var $body = $('body');
        var $header = $('#header');
        var $headerSearchInput = $('#search-ipt',$header);
        var $headerSearch = $headerSearchInput.closest('.hd-search');
        var $toggleItems = $('#header .hd-mm-lnk, #header .hd-local-link, #header .hd-um-link');
        var $window = $(window);
        var resolutions = {
          small:568,
          medium:769,
          large:992
        };
        var $bannerTop = $('#banner-top');

        function toggleCloseButton(e){
          if($(e.target).is($('#'+$(this).attr('id')))){
            $('[href="#'+$(this).attr('id')+'"]').not('.hd-local-link').find('.svg-icon');
          }
        }

        function toggleFilter(){
          $('.collapse.in',this).collapse('hide');
        }

        // generic close collapse button
        $('[data-close-collapse]').click(function(){
          $(this).closest('.collapse.in').collapse('hide');
        });

        // search suggestions
        $headerSearchInput
        .focus(function(){
          $headerSearch.addClass('is-opened');
          console.log('focus');
        });
        // remove classes and collapse items when click out
        $(document).on('click',function(e){
          // if not clicking on search content
          if(!$(e.target).closest('.hd-search').length){
            $headerSearch.removeClass('is-opened');
          }
          // if not clicking on some header collapsable content
          if($('#header .hd-cont.collapse.in, #header .hd-cont.collapsing').length && !$(e.target).closest('#header .hd-cont.collapse.in').length){
            $('#header .hd-cont.collapse.in').collapse('hide');
          }
        });
        // end - search suggestions

        // toggle items
        $('.hd-cont',$header)
        .on('show.bs.collapse',function(e){
          toggleCloseButton.call(this, e);
          if(!$('body.page-overlay').length && $(window).width() < resolutions.large){
            $('body').addClass('page-overlay');
          }
        })
        .on('hide.bs.collapse',function(e){
          toggleCloseButton.call(this, e);
        })
        .on('hidden.bs.collapse',function(e){
          if($('body.page-overlay').length && !$('.hd-cont.collapse.in',$header).length){
            $('body').removeClass('page-overlay');
          }
        });

        if ($window.width() > resolutions.small) {
          $bannerTop.removeClass('hide-banner-top');
        }

        $window.resize(function(){
          if($window.width() < resolutions.large){

            // overlay class
            if($('.hd-cont.collapse.in',$header).length){
              $('body').addClass('page-overlay');
            }else{
              $('body').removeClass('page-overlay');
            }

            // header
            if(typeof $header.attr('data-fixed') !== 'undefined') {
              if ($window.width() < resolutions.small) {
                $header.addClass('is-fixed');

                if (!$bannerTop.hasClass('hide-banner-top'))
                    $bannerTop.addClass('hide-banner-top');

              } else {
                  $header.removeClass('is-fixed');
                  $bannerTop.removeClass('hide-banner-top');
              }
            }

            // filter
          }else{
            $('.filter').off('filter.added').on('filter.added',toggleFilter);
          }
        })
        // end - toggle items

        // filter
        $('.filter',$header).on('filter.added',toggleFilter);
        // end -filter

        // fixed header
        if (typeof $header.attr('data-fixed') !== 'undefined') {

          if($body.hasClass('home-page')) {
            var $homeFilter = $('#filter-home-main');
          } else {
            $header.addClass('is-fixed');
            $bannerTop.addClass('banner-top');
          }

        }

        var headerTopOffset = $("#header").offset().top;

        $window.scroll(function () {

          if(typeof $header.attr('data-fixed') !== 'undefined') {
            if ($window.scrollTop() > headerTopOffset) {
              if (!$header.hasClass('is-fixed')) {
                  $header.addClass('is-fixed');
              }

              if (!$header.hasClass('is-reduced')) {
                  $header.addClass('is-reduced is-reducing');
              }
            } else {
                $header
                    .removeClass('is-reduced')
                    .addClass('is-reducing');

                if ($window.width() > resolutions.small && $body.hasClass('home-page')) {
                  $header.removeClass('is-fixed');
                }
            }
          }


            if ($body.hasClass('home-page')) {

                if (parseInt($window.scrollTop()) > parseInt($homeFilter.offset().top) + parseInt($homeFilter.outerHeight())) {

                    if (!$header.hasClass('has-search-icon')) {
                        $header.addClass('has-search-icon');
                    }
                } else if ($header.hasClass('has-search-icon')) {

                    $header.removeClass('has-search-icon');
                }
            }
        })

        // end - fixed header

    })();

  ;(function() {
    var $body = $('body');
    var TABLET_MAX_WIDTH = 767;

    $(document).on('click', '.js-video-poster', function(ev) {
      ev.preventDefault();
      var $poster = $(this);
      var $wrapper = $poster.closest('.js-video-wrapper');
      videoPlay($wrapper);
    });

    $(document).on('click', '#trailer-close-button', function(ev) {
      ev.preventDefault();
      var $wrapper = $('.js-video-wrapper');
      var $trailers = $(this).closest('.trailers');
      videoStop($wrapper);
      resetTrailers($trailers);
    });

    $(document).on('click', '.trailers .trailer-button',function(){
      var $trailerButton = $(this);
      var $iframe = $();
      var trailerURL = $trailerButton.attr('data-trailer');
      var $trailers = $trailerButton.closest('.trailers');

      $trailers.find('.trailer-button').removeClass('active');
      $trailerButton .addClass('active');

      if(typeof $trailerButton.attr('data-control-player') !== 'undefined'){
        $iframe = $($trailerButton.attr('data-control-player'));
      } else{
        $iframe = parseInt(window.innerWidth) > TABLET_MAX_WIDTH ? $('#movie-trailer-desktop') : $('#movie-trailer-mobile');
      }

      $iframe.attr('src',trailerURL);
    });

    function videoPlay($wrapper) {
      var $iframe = $wrapper.find('.js-video-iframe');
      var src = $iframe.data('src');
      $wrapper.addClass('video-wrapper-active');
      $body.addClass('is-playing-video');
      $iframe.attr('src', src);
    }

    function videoStop($wrapper) {
      if (!$wrapper) {
        var $wrapper = $('.js-video-wrapper');
        var $iframe = $('.js-video-iframe');
      } else {
        var $iframe = $wrapper.find('.js-video-iframe');
      }
      $body.removeClass('is-playing-video');
      $wrapper.removeClass('video-wrapper-active');
      $iframe.attr('src', '');

    }

    function resetTrailers($wrapper){
      $wrapper.find('.trailer-button')
      .removeClass('active')
      .eq(0)
      .addClass('active');
    }


  })();

!function(){var a={collapse:{"js-md":{match:function(){$('.js-md[data-toggle="collapse"]').each(function(){var a=$(this),b=$(a.attr("href"));a.removeClass("collapsed"),b.removeClass("collapse").removeAttr("style"),a.hasClass("model1")&&(a.data("js-md-unmatch-classes","model1"),a.removeClass("model1")),a.data("original-href",a.attr("href")),a.removeAttr("href")}),console.log("match dropdown medium")},unmatch:function(){$('.js-md[data-toggle="collapse"]').each(function(){var a=$(this),b=$(a.data("original-href"));a.addClass("collapsed"),b.addClass("collapse"),"model1"==a.data("js-md-unmatch-classes")&&a.addClass("model1"),a.attr("href",a.data("original-href"))}),console.log("unmatch dropdown medium")},setup:function(){}},"js-lg":{match:function(){$('.js-lg[data-toggle="collapse"]').each(function(){var a=$(this),b=$(a.attr("href"));a.removeClass("collapsed"),b.removeClass("collapse").removeAttr("style"),a.hasClass("model1")&&(a.data("js-lg-unmatch-classes","model1"),a.removeClass("model1")),a.data("original-href",a.attr("href")),a.removeAttr("href")}),console.log("match dropdown large")},unmatch:function(){$('.js-lg[data-toggle="collapse"]').each(function(){var a=$(this),b=$(a.data("original-href"));a.addClass("collapsed"),b.addClass("collapse"),"model1"==a.data("js-lg-unmatch-classes")&&a.addClass("model1"),a.attr("href",a.data("original-href"))}),console.log("unmatch dropdown large")},setup:function(){}},"js-sm":{match:function(){console.log("match dropdown small")},unmatch:function(){console.log("unmatch dropdown small")},setup:function(){}}}},b={small:"(min-width:568px)",medium:"(min-width:769px)",large:"(min-width:992px)"};for(var c in b){var d=[];for(var e in a)"small"==c?d.push(a[e]["js-sm"]):"medium"==c?d.push(a[e]["js-md"]):"large"==c&&d.push(a[e]["js-lg"]);console.log("executando media "+c),enquire.register(b[c],d),d=[]}}(),function(){$('[data-ride="swiper-ingresso"], [data-ride="swiper-ingresso-side"], [data-ride="swiper-ingresso-list"]').each(function(){var a=$(this),b={carousel:{nextButton:$(".swiper-button-next",a),prevButton:$(".swiper-button-prev",a),paginationClickable:!0,preloadImages:!1,simulateTouch:!1,speed:500,preventClicksPropagation:!1,pagination:$(".swiper-pagination",a).length?$(".swiper-pagination",a):null},thumbs:{slideToClickedSlide:!0,slidesPerView:4,spaceBetween:10,touchRatio:.2,centeredSlides:!0}};if("swiper-ingresso-side"==a.attr("data-ride")?b.thumbs=$.extend({},{direction:"vertical",setWrapperSize:!0},b.thumbs):"swiper-ingresso-list"==a.attr("data-ride")?(b.carousel=$.extend({},{slidesPerView:"auto",slidesPerGroup:5,speed:800,freeMode:!0,mousewheelControl:!0,mousewheelForceToAxis:!0,preloadImages:!1,watchSlidesVisibility:!0,simulateTouch:!1,breakpoints:{543:{slidesPerGroup:1},767:{slidesPerGroup:2},991:{slidesPerGroup:3},1199:{slidesPerGroup:4}}},b.carousel),$("body.home-page").hasClass("has-wallpaper-banner")&&(b.carousel=$.extend(b.carousel,{breakpoints:{543:{slidesPerGroup:1},767:{slidesPerGroup:2},1919:{slidesPerGroup:3}}}))):(b.carousel=$.extend(b.carousel,{autoplay:8e3,loop:!0,lazyLoading:!0,lazyLoadingOnTransitionStart:!0}),b.thumbs=$.extend({},{centeredSlides:!0,breakpoints:{320:{slidesPerView:2,spaceBetweenSlides:5},544:{slidesPerView:3,spaceBetweenSlides:5},768:{slidesPerView:4,spaceBetweenSlides:10}}},b.thumbs)),$(".gallery-top",a).length)var c=new Swiper($(".gallery-top",a),b.carousel);if($(".gallery-thumbs",a).length)var d=new Swiper($(".gallery-thumbs",a),b.thumbs);$(".gallery-thumbs",a).length&&$(".gallery-top",a).length&&(c.params.control=d,d.params.control=c)}),$(".swipe-vertical").each(function(){var a=$(this);new Swiper($(".swiper-vertical-container",a),{slidesPerView:5,nextButton:$(".swipe-btn-bottom",a),prevButton:$(".swipe-btn-top",a),direction:"vertical"})}),$(".swiper-ingresso-atm").each(function(){var a=$(this);new Swiper($(".swiper-container-atm",a),{slidesPerView:3,loopedSlides:3,initialSlide:-1,spaceBetween:20,simulateTouch:!1,loop:!0,speed:600,nextButton:$(".swiper-button-next",a),prevButton:$(".swiper-button-prev",a),onReachBeginning:function(a){var b=$(a.container).parent();b.addClass("is-beginning")},onSlideChangeEnd:function(a){var b=$(a.container).parent();a.isBeginning||a.isEnd?a.isBeginning?a.isEnd||b.removeClass("is-end"):b.removeClass("is-beginning"):b.removeClass("is-beginning is-end")},onReachEnd:function(a){var b=$(a.container).parent();b.addClass("is-end")}})}),addClassInactive=function(a){for(var b=0;b<a.length;b++)a.eq(b).hasClass("swiper-slide-inactive")||a.eq(b).hasClass("swiper-slide-active")||a.eq(b).addClass("swiper-slide-inactive")},addClassSlideVisible=function(a){for(var b=0;b<a.length;b++)if(a.eq(b).hasClass("swiper-slide-active")){a.eq(b).addClass("swiper-slide-inactive").removeClass("swiper-slide-active"),a.eq(b+1).addClass("swiper-slide-active").removeClass("swiper-slide-inactive");break}};var a=$(".swiper-atm-wapper");addClassSlideVisible(a.children()),addClassInactive(a.children()),$(".swiper-button-next").click(function(){addClassSlideVisible(a.children()),addClassInactive(a.children())}),$(".swiper-button-prev").click(function(){addClassSlideVisible(a.children()),addClassInactive(a.children())});new Swiper(".swiper-timeline .swiper-container",{nextButton:".swiper-button-next",prevButton:".swiper-button-prev",initialSlide:12,spaceBetween:10,centeredSlides:!0,slidesPerView:"auto",slideToClickedSlide:!0,simulateTouch:!1,onReachBeginning:function(a){var b=$(a.container).parent();b.addClass("is-beginning")},onSlideChangeEnd:function(a){var b=$(a.container).parent();a.isBeginning||a.isEnd?a.isBeginning?a.isEnd||b.removeClass("is-end"):b.removeClass("is-beginning"):b.removeClass("is-beginning is-end")},onReachEnd:function(a){var b=$(a.container).parent();b.addClass("is-end")}})}(),function(){$(".filter").each(function(){function a(){$("a",h).click(function(){d.call(this,$(this).attr("data-val")||$(this).text()),g.closest(".tab-accordion").addClass("filter-selected")}),i.on("click",function(a){e.call(this),a.stopPropagation()})}function b(){j.collapse("hide")}function c(){return g.children(".filter-val").text()}function d(a){c()!==a&&(g.children(".filter-val").text(a),h.filter(".is-selected").removeClass("is-selected"),f.addClass("filter-has-val"),$(this).closest(".filter-it").addClass("is-selected").trigger("filter.added"),b())}function e(){g.children(".filter-val").text(""),h.filter(".is-selected").removeClass("is-selected"),f.removeClass("filter-has-val")}var f=$(this),g=$('[data-toggle="collapse"]',this),h=$("[role=option]",this),i=$(".filter-action",this),j=$(".filter-content",this);a.call(this)})}(),function(){setTimeout(function(){$("[data-slide-page-link], [data-slide-page-back]").each(function(){var a=$(this);if("string"==typeof a.attr("data-slide-page-link")){var b=$(a.attr("data-slide-page-link"));b.on("transitionend",function(){b.removeClass("is-animating")}),a.click(function(a){b.addClass("is-animating").toggleClass("active").find("> .slide-page-wp > [data-slide-page-side]").toggleClass("active")})}else{var b=$(a.attr("data-slide-page-back"));a.click(function(a){b.addClass("is-animating").toggleClass("active").find("> .slide-page-wp > [data-slide-page-side]").toggleClass("active")})}})},3e3)}(),function(){"addEventListener"in document&&document.addEventListener("DOMContentLoaded",function(){FastClick.attach(document.body)},!1)}(),function(){function a(a){$(a.target).is($("#"+$(this).attr("id")))&&$('[href="#'+$(this).attr("id")+'"]').not(".hd-local-link").find(".svg-icon")}function b(){$(".collapse.in",this).collapse("hide")}var c=$("body"),d=$("#header"),e=$("#search-ipt",d),f=e.closest(".hd-search"),g=($("#header .hd-mm-lnk, #header .hd-local-link, #header .hd-um-link"),$(window)),h={small:568,medium:769,large:992},i=$("#banner-top");if($("[data-close-collapse]").click(function(){$(this).closest(".collapse.in").collapse("hide")}),e.focus(function(){f.addClass("is-opened"),console.log("focus")}),$(document).on("click",function(a){$(a.target).closest(".hd-search").length||f.removeClass("is-opened"),$("#header .hd-cont.collapse.in, #header .hd-cont.collapsing").length&&!$(a.target).closest("#header .hd-cont.collapse.in").length&&$("#header .hd-cont.collapse.in").collapse("hide")}),$(".hd-cont",d).on("show.bs.collapse",function(b){a.call(this,b),!$("body.page-overlay").length&&$(window).width()<h.large&&$("body").addClass("page-overlay")}).on("hide.bs.collapse",function(b){a.call(this,b)}).on("hidden.bs.collapse",function(a){$("body.page-overlay").length&&!$(".hd-cont.collapse.in",d).length&&$("body").removeClass("page-overlay")}),g.width()>h.small&&i.removeClass("hide-banner-top"),g.resize(function(){g.width()<h.large?($(".hd-cont.collapse.in",d).length?$("body").addClass("page-overlay"):$("body").removeClass("page-overlay"),"undefined"!=typeof d.attr("data-fixed")&&(g.width()<h.small?(d.addClass("is-fixed"),i.hasClass("hide-banner-top")||i.addClass("hide-banner-top")):(d.removeClass("is-fixed"),i.removeClass("hide-banner-top")))):$(".filter").off("filter.added").on("filter.added",b)}),$(".filter",d).on("filter.added",b),"undefined"!=typeof d.attr("data-fixed"))if(c.hasClass("home-page"))var j=$("#filter-home-main");else d.addClass("is-fixed"),i.addClass("banner-top");var k=$("#header").offset().top;g.scroll(function(){"undefined"!=typeof d.attr("data-fixed")&&(g.scrollTop()>k?(d.hasClass("is-fixed")||d.addClass("is-fixed"),d.hasClass("is-reduced")||d.addClass("is-reduced is-reducing")):(d.removeClass("is-reduced").addClass("is-reducing"),g.width()>h.small&&c.hasClass("home-page")&&d.removeClass("is-fixed"))),c.hasClass("home-page")&&(parseInt(g.scrollTop())>parseInt(j.offset().top)+parseInt(j.outerHeight())?d.hasClass("has-search-icon")||d.addClass("has-search-icon"):d.hasClass("has-search-icon")&&d.removeClass("has-search-icon"))})}(),function(){function a(a){var b=a.find(".js-video-iframe"),c=b.data("src");a.addClass("video-wrapper-active"),d.addClass("is-playing-video"),b.attr("src",c)}function b(a){if(a)var b=a.find(".js-video-iframe");else var a=$(".js-video-wrapper"),b=$(".js-video-iframe");d.removeClass("is-playing-video"),a.removeClass("video-wrapper-active"),b.attr("src","")}function c(a){a.find(".trailer-button").removeClass("active").eq(0).addClass("active")}var d=$("body"),e=767;$(document).on("click",".js-video-poster",function(b){b.preventDefault();var c=$(this),d=c.closest(".js-video-wrapper");a(d)}),$(document).on("click","#trailer-close-button",function(a){a.preventDefault();var d=$(".js-video-wrapper"),e=$(this).closest(".trailers");b(d),c(e)}),$(document).on("click",".trailers .trailer-button",function(){var a=$(this),b=$(),c=a.attr("data-trailer"),d=a.closest(".trailers");d.find(".trailer-button").removeClass("active"),a.addClass("active"),b="undefined"!=typeof a.attr("data-control-player")?$(a.attr("data-control-player")):parseInt(window.innerWidth)>e?$("#movie-trailer-desktop"):$("#movie-trailer-mobile"),b.attr("src",c)})}();
// This file is autogenerated via the `commonjs` Grunt task. You can require() this file in a CommonJS environment.
require('./umd/util.js')
require('./umd/alert.js')
require('./umd/button.js')
require('./umd/carousel.js')
require('./umd/collapse.js')
require('./umd/dropdown.js')
require('./umd/modal.js')
require('./umd/scrollspy.js')
require('./umd/tab.js')
require('./umd/tooltip.js')
require('./umd/popover.js')
/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas, David Knight. Dual MIT/BSD license */

window.matchMedia || (window.matchMedia = function() {
    "use strict";

    // For browsers that support matchMedium api such as IE 9 and webkit
    var styleMedia = (window.styleMedia || window.media);

    // For those that don't support matchMedium
    if (!styleMedia) {
        var style       = document.createElement('style'),
            script      = document.getElementsByTagName('script')[0],
            info        = null;

        style.type  = 'text/css';
        style.id    = 'matchmediajs-test';

        script.parentNode.insertBefore(style, script);

        // 'style.currentStyle' is used by IE <= 8 and 'window.getComputedStyle' for all other browsers
        info = ('getComputedStyle' in window) && window.getComputedStyle(style, null) || style.currentStyle;

        styleMedia = {
            matchMedium: function(media) {
                var text = '@media ' + media + '{ #matchmediajs-test { width: 1px; } }';

                // 'style.styleSheet' is used by IE <= 8 and 'style.textContent' for all other browsers
                if (style.styleSheet) {
                    style.styleSheet.cssText = text;
                } else {
                    style.textContent = text;
                }

                // Test if media query is true or false
                return info.width === '1px';
            }
        };
    }

    return function(media) {
        return {
            matches: styleMedia.matchMedium(media || 'all'),
            media: media || 'all'
        };
    };
}());

/*!
 * enquire.js v2.1.2 - Awesome Media Queries in JavaScript
 * Copyright (c) 2014 Nick Williams - http://wicky.nillia.ms/enquire.js
 * License: MIT (http://www.opensource.org/licenses/mit-license.php)
 */

;(function (name, context, factory) {
	var matchMedia = window.matchMedia;

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = factory(matchMedia);
	}
	else if (typeof define === 'function' && define.amd) {
		define(function() {
			return (context[name] = factory(matchMedia));
		});
	}
	else {
		context[name] = factory(matchMedia);
	}
}('enquire', this, function (matchMedia) {

	'use strict';

    /*jshint unused:false */
    /**
     * Helper function for iterating over a collection
     *
     * @param collection
     * @param fn
     */
    function each(collection, fn) {
        var i      = 0,
            length = collection.length,
            cont;

        for(i; i < length; i++) {
            cont = fn(collection[i], i);
            if(cont === false) {
                break; //allow early exit
            }
        }
    }

    /**
     * Helper function for determining whether target object is an array
     *
     * @param target the object under test
     * @return {Boolean} true if array, false otherwise
     */
    function isArray(target) {
        return Object.prototype.toString.apply(target) === '[object Array]';
    }

    /**
     * Helper function for determining whether target object is a function
     *
     * @param target the object under test
     * @return {Boolean} true if function, false otherwise
     */
    function isFunction(target) {
        return typeof target === 'function';
    }

    /**
     * Delegate to handle a media query being matched and unmatched.
     *
     * @param {object} options
     * @param {function} options.match callback for when the media query is matched
     * @param {function} [options.unmatch] callback for when the media query is unmatched
     * @param {function} [options.setup] one-time callback triggered the first time a query is matched
     * @param {boolean} [options.deferSetup=false] should the setup callback be run immediately, rather than first time query is matched?
     * @constructor
     */
    function QueryHandler(options) {
        this.options = options;
        !options.deferSetup && this.setup();
    }
    QueryHandler.prototype = {

        /**
         * coordinates setup of the handler
         *
         * @function
         */
        setup : function() {
            if(this.options.setup) {
                this.options.setup();
            }
            this.initialised = true;
        },

        /**
         * coordinates setup and triggering of the handler
         *
         * @function
         */
        on : function() {
            !this.initialised && this.setup();
            this.options.match && this.options.match();
        },

        /**
         * coordinates the unmatch event for the handler
         *
         * @function
         */
        off : function() {
            this.options.unmatch && this.options.unmatch();
        },

        /**
         * called when a handler is to be destroyed.
         * delegates to the destroy or unmatch callbacks, depending on availability.
         *
         * @function
         */
        destroy : function() {
            this.options.destroy ? this.options.destroy() : this.off();
        },

        /**
         * determines equality by reference.
         * if object is supplied compare options, if function, compare match callback
         *
         * @function
         * @param {object || function} [target] the target for comparison
         */
        equals : function(target) {
            return this.options === target || this.options.match === target;
        }

    };
    /**
     * Represents a single media query, manages it's state and registered handlers for this query
     *
     * @constructor
     * @param {string} query the media query string
     * @param {boolean} [isUnconditional=false] whether the media query should run regardless of whether the conditions are met. Primarily for helping older browsers deal with mobile-first design
     */
    function MediaQuery(query, isUnconditional) {
        this.query = query;
        this.isUnconditional = isUnconditional;
        this.handlers = [];
        this.mql = matchMedia(query);

        var self = this;
        this.listener = function(mql) {
            self.mql = mql;
            self.assess();
        };
        this.mql.addListener(this.listener);
    }
    MediaQuery.prototype = {

        /**
         * add a handler for this query, triggering if already active
         *
         * @param {object} handler
         * @param {function} handler.match callback for when query is activated
         * @param {function} [handler.unmatch] callback for when query is deactivated
         * @param {function} [handler.setup] callback for immediate execution when a query handler is registered
         * @param {boolean} [handler.deferSetup=false] should the setup callback be deferred until the first time the handler is matched?
         */
        addHandler : function(handler) {
            var qh = new QueryHandler(handler);
            this.handlers.push(qh);

            this.matches() && qh.on();
        },

        /**
         * removes the given handler from the collection, and calls it's destroy methods
         * 
         * @param {object || function} handler the handler to remove
         */
        removeHandler : function(handler) {
            var handlers = this.handlers;
            each(handlers, function(h, i) {
                if(h.equals(handler)) {
                    h.destroy();
                    return !handlers.splice(i,1); //remove from array and exit each early
                }
            });
        },

        /**
         * Determine whether the media query should be considered a match
         * 
         * @return {Boolean} true if media query can be considered a match, false otherwise
         */
        matches : function() {
            return this.mql.matches || this.isUnconditional;
        },

        /**
         * Clears all handlers and unbinds events
         */
        clear : function() {
            each(this.handlers, function(handler) {
                handler.destroy();
            });
            this.mql.removeListener(this.listener);
            this.handlers.length = 0; //clear array
        },

        /*
         * Assesses the query, turning on all handlers if it matches, turning them off if it doesn't match
         */
        assess : function() {
            var action = this.matches() ? 'on' : 'off';

            each(this.handlers, function(handler) {
                handler[action]();
            });
        }
    };
    /**
     * Allows for registration of query handlers.
     * Manages the query handler's state and is responsible for wiring up browser events
     *
     * @constructor
     */
    function MediaQueryDispatch () {
        if(!matchMedia) {
            throw new Error('matchMedia not present, legacy browsers require a polyfill');
        }

        this.queries = {};
        this.browserIsIncapable = !matchMedia('only all').matches;
    }

    MediaQueryDispatch.prototype = {

        /**
         * Registers a handler for the given media query
         *
         * @param {string} q the media query
         * @param {object || Array || Function} options either a single query handler object, a function, or an array of query handlers
         * @param {function} options.match fired when query matched
         * @param {function} [options.unmatch] fired when a query is no longer matched
         * @param {function} [options.setup] fired when handler first triggered
         * @param {boolean} [options.deferSetup=false] whether setup should be run immediately or deferred until query is first matched
         * @param {boolean} [shouldDegrade=false] whether this particular media query should always run on incapable browsers
         */
        register : function(q, options, shouldDegrade) {
            var queries         = this.queries,
                isUnconditional = shouldDegrade && this.browserIsIncapable;

            if(!queries[q]) {
                queries[q] = new MediaQuery(q, isUnconditional);
            }

            //normalise to object in an array
            if(isFunction(options)) {
                options = { match : options };
            }
            if(!isArray(options)) {
                options = [options];
            }
            each(options, function(handler) {
                if (isFunction(handler)) {
                    handler = { match : handler };
                }
                queries[q].addHandler(handler);
            });

            return this;
        },

        /**
         * unregisters a query and all it's handlers, or a specific handler for a query
         *
         * @param {string} q the media query to target
         * @param {object || function} [handler] specific handler to unregister
         */
        unregister : function(q, handler) {
            var query = this.queries[q];

            if(query) {
                if(handler) {
                    query.removeHandler(handler);
                }
                else {
                    query.clear();
                    delete this.queries[q];
                }
            }

            return this;
        }
    };

	return new MediaQueryDispatch();

}));

$("body").on("input propertychange", ".fl-form-group", function(e) {
  $(this).toggleClass("fl-form-group-with-value", !!$(e.target).val());
}).on("focus", ".fl-form-group", function() {
  $(this).addClass("fl-form-group-with-focus");
}).on("blur", ".fl-form-group", function() {
  $(this).removeClass("fl-form-group-with-focus");
});


/**
 * Swiper 3.3.1
 * Most modern mobile touch slider and framework with hardware accelerated transitions
 *
 * http://www.idangero.us/swiper/
 *
 * Copyright 2016, Vladimir Kharlampidi
 * The iDangero.us
 * http://www.idangero.us/
 *
 * Licensed under MIT
 *
 * Released on: February 7, 2016
 */
(function () {
    'use strict';
    var $;
    /*===========================
    Swiper
    ===========================*/
    var Swiper = function (container, params) {
        if (!(this instanceof Swiper)) return new Swiper(container, params);

        var defaults = {
            direction: 'horizontal',
            touchEventsTarget: 'container',
            initialSlide: 0,
            speed: 300,
            // autoplay
            autoplay: false,
            autoplayDisableOnInteraction: true,
            autoplayStopOnLast: false,
            // To support iOS's swipe-to-go-back gesture (when being used in-app, with UIWebView).
            iOSEdgeSwipeDetection: false,
            iOSEdgeSwipeThreshold: 20,
            // Free mode
            freeMode: false,
            freeModeMomentum: true,
            freeModeMomentumRatio: 1,
            freeModeMomentumBounce: true,
            freeModeMomentumBounceRatio: 1,
            freeModeSticky: false,
            freeModeMinimumVelocity: 0.02,
            // Autoheight
            autoHeight: false,
            // Set wrapper width
            setWrapperSize: false,
            // Virtual Translate
            virtualTranslate: false,
            // Effects
            effect: 'slide', // 'slide' or 'fade' or 'cube' or 'coverflow' or 'flip'
            coverflow: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows : true
            },
            flip: {
                slideShadows : true,
                limitRotation: true
            },
            cube: {
                slideShadows: true,
                shadow: true,
                shadowOffset: 20,
                shadowScale: 0.94
            },
            fade: {
                crossFade: false
            },
            // Parallax
            parallax: false,
            // Scrollbar
            scrollbar: null,
            scrollbarHide: true,
            scrollbarDraggable: false,
            scrollbarSnapOnRelease: false,
            // Keyboard Mousewheel
            keyboardControl: false,
            mousewheelControl: false,
            mousewheelReleaseOnEdges: false,
            mousewheelInvert: false,
            mousewheelForceToAxis: false,
            mousewheelSensitivity: 1,
            // Hash Navigation
            hashnav: false,
            // Breakpoints
            breakpoints: undefined,
            // Slides grid
            spaceBetween: 0,
            slidesPerView: 1,
            slidesPerColumn: 1,
            slidesPerColumnFill: 'column',
            slidesPerGroup: 1,
            centeredSlides: false,
            slidesOffsetBefore: 0, // in px
            slidesOffsetAfter: 0, // in px
            // Round length
            roundLengths: false,
            // Touches
            touchRatio: 1,
            touchAngle: 45,
            simulateTouch: true,
            shortSwipes: true,
            longSwipes: true,
            longSwipesRatio: 0.5,
            longSwipesMs: 300,
            followFinger: true,
            onlyExternal: false,
            threshold: 0,
            touchMoveStopPropagation: true,
            // Unique Navigation Elements
            uniqueNavElements: true,
            // Pagination
            pagination: null,
            paginationElement: 'span',
            paginationClickable: false,
            paginationHide: false,
            paginationBulletRender: null,
            paginationProgressRender: null,
            paginationFractionRender: null,
            paginationCustomRender: null,
            paginationType: 'bullets', // 'bullets' or 'progress' or 'fraction' or 'custom'
            // Resistance
            resistance: true,
            resistanceRatio: 0.85,
            // Next/prev buttons
            nextButton: null,
            prevButton: null,
            // Progress
            watchSlidesProgress: false,
            watchSlidesVisibility: false,
            // Cursor
            grabCursor: false,
            // Clicks
            preventClicks: true,
            preventClicksPropagation: true,
            slideToClickedSlide: false,
            // Lazy Loading
            lazyLoading: false,
            lazyLoadingInPrevNext: false,
            lazyLoadingInPrevNextAmount: 1,
            lazyLoadingOnTransitionStart: false,
            // Images
            preloadImages: true,
            updateOnImagesReady: true,
            // loop
            loop: false,
            loopAdditionalSlides: 0,
            loopedSlides: null,
            // Control
            control: undefined,
            controlInverse: false,
            controlBy: 'slide', //or 'container'
            // Swiping/no swiping
            allowSwipeToPrev: true,
            allowSwipeToNext: true,
            swipeHandler: null, //'.swipe-handler',
            noSwiping: true,
            noSwipingClass: 'swiper-no-swiping',
            // NS
            slideClass: 'swiper-slide',
            slideActiveClass: 'swiper-slide-active',
            slideVisibleClass: 'swiper-slide-visible',
            slideDuplicateClass: 'swiper-slide-duplicate',
            slideNextClass: 'swiper-slide-next',
            slidePrevClass: 'swiper-slide-prev',
            wrapperClass: 'swiper-wrapper',
            bulletClass: 'swiper-pagination-bullet',
            bulletActiveClass: 'swiper-pagination-bullet-active',
            buttonDisabledClass: 'swiper-button-disabled',
            paginationCurrentClass: 'swiper-pagination-current',
            paginationTotalClass: 'swiper-pagination-total',
            paginationHiddenClass: 'swiper-pagination-hidden',
            paginationProgressbarClass: 'swiper-pagination-progressbar',
            // Observer
            observer: false,
            observeParents: false,
            // Accessibility
            a11y: false,
            prevSlideMessage: 'Previous slide',
            nextSlideMessage: 'Next slide',
            firstSlideMessage: 'This is the first slide',
            lastSlideMessage: 'This is the last slide',
            paginationBulletMessage: 'Go to slide {{index}}',
            // Callbacks
            runCallbacksOnInit: true
            /*
            Callbacks:
            onInit: function (swiper)
            onDestroy: function (swiper)
            onClick: function (swiper, e)
            onTap: function (swiper, e)
            onDoubleTap: function (swiper, e)
            onSliderMove: function (swiper, e)
            onSlideChangeStart: function (swiper)
            onSlideChangeEnd: function (swiper)
            onTransitionStart: function (swiper)
            onTransitionEnd: function (swiper)
            onImagesReady: function (swiper)
            onProgress: function (swiper, progress)
            onTouchStart: function (swiper, e)
            onTouchMove: function (swiper, e)
            onTouchMoveOpposite: function (swiper, e)
            onTouchEnd: function (swiper, e)
            onReachBeginning: function (swiper)
            onReachEnd: function (swiper)
            onSetTransition: function (swiper, duration)
            onSetTranslate: function (swiper, translate)
            onAutoplayStart: function (swiper)
            onAutoplayStop: function (swiper),
            onLazyImageLoad: function (swiper, slide, image)
            onLazyImageReady: function (swiper, slide, image)
            */

        };
        var initialVirtualTranslate = params && params.virtualTranslate;

        params = params || {};
        var originalParams = {};
        for (var param in params) {
            if (typeof params[param] === 'object' && params[param] !== null && !(params[param].nodeType || params[param] === window || params[param] === document || (typeof Dom7 !== 'undefined' && params[param] instanceof Dom7) || (typeof jQuery !== 'undefined' && params[param] instanceof jQuery))) {
                originalParams[param] = {};
                for (var deepParam in params[param]) {
                    originalParams[param][deepParam] = params[param][deepParam];
                }
            }
            else {
                originalParams[param] = params[param];
            }
        }
        for (var def in defaults) {
            if (typeof params[def] === 'undefined') {
                params[def] = defaults[def];
            }
            else if (typeof params[def] === 'object') {
                for (var deepDef in defaults[def]) {
                    if (typeof params[def][deepDef] === 'undefined') {
                        params[def][deepDef] = defaults[def][deepDef];
                    }
                }
            }
        }

        // Swiper
        var s = this;

        // Params
        s.params = params;
        s.originalParams = originalParams;

        // Classname
        s.classNames = [];
        /*=========================
          Dom Library and plugins
          ===========================*/
        if (typeof $ !== 'undefined' && typeof Dom7 !== 'undefined'){
            $ = Dom7;
        }
        if (typeof $ === 'undefined') {
            if (typeof Dom7 === 'undefined') {
                $ = window.Dom7 || window.Zepto || window.jQuery;
            }
            else {
                $ = Dom7;
            }
            if (!$) return;
        }
        // Export it to Swiper instance
        s.$ = $;

        /*=========================
          Breakpoints
          ===========================*/
        s.currentBreakpoint = undefined;
        s.getActiveBreakpoint = function () {
            //Get breakpoint for window width
            if (!s.params.breakpoints) return false;
            var breakpoint = false;
            var points = [], point;
            for ( point in s.params.breakpoints ) {
                if (s.params.breakpoints.hasOwnProperty(point)) {
                    points.push(point);
                }
            }
            points.sort(function (a, b) {
                return parseInt(a, 10) > parseInt(b, 10);
            });
            for (var i = 0; i < points.length; i++) {
                point = points[i];
                if (point >= window.innerWidth && !breakpoint) {
                    breakpoint = point;
                }
            }
            return breakpoint || 'max';
        };
        s.setBreakpoint = function () {
            //Set breakpoint for window width and update parameters
            var breakpoint = s.getActiveBreakpoint();
            if (breakpoint && s.currentBreakpoint !== breakpoint) {
                var breakPointsParams = breakpoint in s.params.breakpoints ? s.params.breakpoints[breakpoint] : s.originalParams;
                var needsReLoop = s.params.loop && (breakPointsParams.slidesPerView !== s.params.slidesPerView);
                for ( var param in breakPointsParams ) {
                    s.params[param] = breakPointsParams[param];
                }
                s.currentBreakpoint = breakpoint;
                if(needsReLoop && s.destroyLoop) {
                    s.reLoop(true);
                }
            }
        };
        // Set breakpoint on load
        if (s.params.breakpoints) {
            s.setBreakpoint();
        }

        /*=========================
          Preparation - Define Container, Wrapper and Pagination
          ===========================*/
        s.container = $(container);
        if (s.container.length === 0) return;
        if (s.container.length > 1) {
            var swipers = [];
            s.container.each(function () {
                var container = this;
                swipers.push(new Swiper(this, params));
            });
            return swipers;
        }

        // Save instance in container HTML Element and in data
        s.container[0].swiper = s;
        s.container.data('swiper', s);

        s.classNames.push('swiper-container-' + s.params.direction);

        if (s.params.freeMode) {
            s.classNames.push('swiper-container-free-mode');
        }
        if (!s.support.flexbox) {
            s.classNames.push('swiper-container-no-flexbox');
            s.params.slidesPerColumn = 1;
        }
        if (s.params.autoHeight) {
            s.classNames.push('swiper-container-autoheight');
        }
        // Enable slides progress when required
        if (s.params.parallax || s.params.watchSlidesVisibility) {
            s.params.watchSlidesProgress = true;
        }
        // Coverflow / 3D
        if (['cube', 'coverflow', 'flip'].indexOf(s.params.effect) >= 0) {
            if (s.support.transforms3d) {
                s.params.watchSlidesProgress = true;
                s.classNames.push('swiper-container-3d');
            }
            else {
                s.params.effect = 'slide';
            }
        }
        if (s.params.effect !== 'slide') {
            s.classNames.push('swiper-container-' + s.params.effect);
        }
        if (s.params.effect === 'cube') {
            s.params.resistanceRatio = 0;
            s.params.slidesPerView = 1;
            s.params.slidesPerColumn = 1;
            s.params.slidesPerGroup = 1;
            s.params.centeredSlides = false;
            s.params.spaceBetween = 0;
            s.params.virtualTranslate = true;
            s.params.setWrapperSize = false;
        }
        if (s.params.effect === 'fade' || s.params.effect === 'flip') {
            s.params.slidesPerView = 1;
            s.params.slidesPerColumn = 1;
            s.params.slidesPerGroup = 1;
            s.params.watchSlidesProgress = true;
            s.params.spaceBetween = 0;
            s.params.setWrapperSize = false;
            if (typeof initialVirtualTranslate === 'undefined') {
                s.params.virtualTranslate = true;
            }
        }

        // Grab Cursor
        if (s.params.grabCursor && s.support.touch) {
            s.params.grabCursor = false;
        }

        // Wrapper
        s.wrapper = s.container.children('.' + s.params.wrapperClass);

        // Pagination
        if (s.params.pagination) {
            s.paginationContainer = $(s.params.pagination);
            if (s.params.uniqueNavElements && typeof s.params.pagination === 'string' && s.paginationContainer.length > 1 && s.container.find(s.params.pagination).length === 1) {
                s.paginationContainer = s.container.find(s.params.pagination);
            }

            if (s.params.paginationType === 'bullets' && s.params.paginationClickable) {
                s.paginationContainer.addClass('swiper-pagination-clickable');
            }
            else {
                s.params.paginationClickable = false;
            }
            s.paginationContainer.addClass('swiper-pagination-' + s.params.paginationType);
        }
        // Next/Prev Buttons
        if (s.params.nextButton || s.params.prevButton) {
            if (s.params.nextButton) {
                s.nextButton = $(s.params.nextButton);
                if (s.params.uniqueNavElements && typeof s.params.nextButton === 'string' && s.nextButton.length > 1 && s.container.find(s.params.nextButton).length === 1) {
                    s.nextButton = s.container.find(s.params.nextButton);
                }
            }
            if (s.params.prevButton) {
                s.prevButton = $(s.params.prevButton);
                if (s.params.uniqueNavElements && typeof s.params.prevButton === 'string' && s.prevButton.length > 1 && s.container.find(s.params.prevButton).length === 1) {
                    s.prevButton = s.container.find(s.params.prevButton);
                }
            }
        }

        // Is Horizontal
        s.isHorizontal = function () {
            return s.params.direction === 'horizontal';
        };
        // s.isH = isH;

        // RTL
        s.rtl = s.isHorizontal() && (s.container[0].dir.toLowerCase() === 'rtl' || s.container.css('direction') === 'rtl');
        if (s.rtl) {
            s.classNames.push('swiper-container-rtl');
        }

        // Wrong RTL support
        if (s.rtl) {
            s.wrongRTL = s.wrapper.css('display') === '-webkit-box';
        }

        // Columns
        if (s.params.slidesPerColumn > 1) {
            s.classNames.push('swiper-container-multirow');
        }

        // Check for Android
        if (s.device.android) {
            s.classNames.push('swiper-container-android');
        }

        // Add classes
        s.container.addClass(s.classNames.join(' '));

        // Translate
        s.translate = 0;

        // Progress
        s.progress = 0;

        // Velocity
        s.velocity = 0;

        /*=========================
          Locks, unlocks
          ===========================*/
        s.lockSwipeToNext = function () {
            s.params.allowSwipeToNext = false;
        };
        s.lockSwipeToPrev = function () {
            s.params.allowSwipeToPrev = false;
        };
        s.lockSwipes = function () {
            s.params.allowSwipeToNext = s.params.allowSwipeToPrev = false;
        };
        s.unlockSwipeToNext = function () {
            s.params.allowSwipeToNext = true;
        };
        s.unlockSwipeToPrev = function () {
            s.params.allowSwipeToPrev = true;
        };
        s.unlockSwipes = function () {
            s.params.allowSwipeToNext = s.params.allowSwipeToPrev = true;
        };

        /*=========================
          Round helper
          ===========================*/
        function round(a) {
            return Math.floor(a);
        }
        /*=========================
          Set grab cursor
          ===========================*/
        if (s.params.grabCursor) {
            s.container[0].style.cursor = 'move';
            s.container[0].style.cursor = '-webkit-grab';
            s.container[0].style.cursor = '-moz-grab';
            s.container[0].style.cursor = 'grab';
        }
        /*=========================
          Update on Images Ready
          ===========================*/
        s.imagesToLoad = [];
        s.imagesLoaded = 0;

        s.loadImage = function (imgElement, src, srcset, checkForComplete, callback) {
            var image;
            function onReady () {
                if (callback) callback();
            }
            if (!imgElement.complete || !checkForComplete) {
                if (src) {
                    image = new window.Image();
                    image.onload = onReady;
                    image.onerror = onReady;
                    if (srcset) {
                        image.srcset = srcset;
                    }
                    if (src) {
                        image.src = src;
                    }
                } else {
                    onReady();
                }

            } else {//image already loaded...
                onReady();
            }
        };
        s.preloadImages = function () {
            s.imagesToLoad = s.container.find('img');
            function _onReady() {
                if (typeof s === 'undefined' || s === null) return;
                if (s.imagesLoaded !== undefined) s.imagesLoaded++;
                if (s.imagesLoaded === s.imagesToLoad.length) {
                    if (s.params.updateOnImagesReady) s.update();
                    s.emit('onImagesReady', s);
                }
            }
            for (var i = 0; i < s.imagesToLoad.length; i++) {
                s.loadImage(s.imagesToLoad[i], (s.imagesToLoad[i].currentSrc || s.imagesToLoad[i].getAttribute('src')), (s.imagesToLoad[i].srcset || s.imagesToLoad[i].getAttribute('srcset')), true, _onReady);
            }
        };

        /*=========================
          Autoplay
          ===========================*/
        s.autoplayTimeoutId = undefined;
        s.autoplaying = false;
        s.autoplayPaused = false;
        function autoplay() {
            s.autoplayTimeoutId = setTimeout(function () {
                if (s.params.loop) {
                    s.fixLoop();
                    s._slideNext();
                    s.emit('onAutoplay', s);
                }
                else {
                    if (!s.isEnd) {
                        s._slideNext();
                        s.emit('onAutoplay', s);
                    }
                    else {
                        if (!params.autoplayStopOnLast) {
                            s._slideTo(0);
                            s.emit('onAutoplay', s);
                        }
                        else {
                            s.stopAutoplay();
                        }
                    }
                }
            }, s.params.autoplay);
        }
        s.startAutoplay = function () {
            if (typeof s.autoplayTimeoutId !== 'undefined') return false;
            if (!s.params.autoplay) return false;
            if (s.autoplaying) return false;
            s.autoplaying = true;
            s.emit('onAutoplayStart', s);
            autoplay();
        };
        s.stopAutoplay = function (internal) {
            if (!s.autoplayTimeoutId) return;
            if (s.autoplayTimeoutId) clearTimeout(s.autoplayTimeoutId);
            s.autoplaying = false;
            s.autoplayTimeoutId = undefined;
            s.emit('onAutoplayStop', s);
        };
        s.pauseAutoplay = function (speed) {
            if (s.autoplayPaused) return;
            if (s.autoplayTimeoutId) clearTimeout(s.autoplayTimeoutId);
            s.autoplayPaused = true;
            if (speed === 0) {
                s.autoplayPaused = false;
                autoplay();
            }
            else {
                s.wrapper.transitionEnd(function () {
                    if (!s) return;
                    s.autoplayPaused = false;
                    if (!s.autoplaying) {
                        s.stopAutoplay();
                    }
                    else {
                        autoplay();
                    }
                });
            }
        };
        /*=========================
          Min/Max Translate
          ===========================*/
        s.minTranslate = function () {
            return (-s.snapGrid[0]);
        };
        s.maxTranslate = function () {
            return (-s.snapGrid[s.snapGrid.length - 1]);
        };
        /*=========================
          Slider/slides sizes
          ===========================*/
        s.updateAutoHeight = function () {
            // Update Height
            var slide = s.slides.eq(s.activeIndex)[0];
            if (typeof slide !== 'undefined') {
                var newHeight = slide.offsetHeight;
                if (newHeight) s.wrapper.css('height', newHeight + 'px');
            }
        };
        s.updateContainerSize = function () {
            var width, height;
            if (typeof s.params.width !== 'undefined') {
                width = s.params.width;
            }
            else {
                width = s.container[0].clientWidth;
            }
            if (typeof s.params.height !== 'undefined') {
                height = s.params.height;
            }
            else {
                height = s.container[0].clientHeight;
            }
            if (width === 0 && s.isHorizontal() || height === 0 && !s.isHorizontal()) {
                return;
            }

            //Subtract paddings
            width = width - parseInt(s.container.css('padding-left'), 10) - parseInt(s.container.css('padding-right'), 10);
            height = height - parseInt(s.container.css('padding-top'), 10) - parseInt(s.container.css('padding-bottom'), 10);

            // Store values
            s.width = width;
            s.height = height;
            s.size = s.isHorizontal() ? s.width : s.height;
        };

        s.updateSlidesSize = function () {
            s.slides = s.wrapper.children('.' + s.params.slideClass);
            s.snapGrid = [];
            s.slidesGrid = [];
            s.slidesSizesGrid = [];

            var spaceBetween = s.params.spaceBetween,
                slidePosition = -s.params.slidesOffsetBefore,
                i,
                prevSlideSize = 0,
                index = 0;
            if (typeof s.size === 'undefined') return;
            if (typeof spaceBetween === 'string' && spaceBetween.indexOf('%') >= 0) {
                spaceBetween = parseFloat(spaceBetween.replace('%', '')) / 100 * s.size;
            }

            s.virtualSize = -spaceBetween;
            // reset margins
            if (s.rtl) s.slides.css({marginLeft: '', marginTop: ''});
            else s.slides.css({marginRight: '', marginBottom: ''});

            var slidesNumberEvenToRows;
            if (s.params.slidesPerColumn > 1) {
                if (Math.floor(s.slides.length / s.params.slidesPerColumn) === s.slides.length / s.params.slidesPerColumn) {
                    slidesNumberEvenToRows = s.slides.length;
                }
                else {
                    slidesNumberEvenToRows = Math.ceil(s.slides.length / s.params.slidesPerColumn) * s.params.slidesPerColumn;
                }
                if (s.params.slidesPerView !== 'auto' && s.params.slidesPerColumnFill === 'row') {
                    slidesNumberEvenToRows = Math.max(slidesNumberEvenToRows, s.params.slidesPerView * s.params.slidesPerColumn);
                }
            }

            // Calc slides
            var slideSize;
            var slidesPerColumn = s.params.slidesPerColumn;
            var slidesPerRow = slidesNumberEvenToRows / slidesPerColumn;
            var numFullColumns = slidesPerRow - (s.params.slidesPerColumn * slidesPerRow - s.slides.length);
            for (i = 0; i < s.slides.length; i++) {
                slideSize = 0;
                var slide = s.slides.eq(i);
                if (s.params.slidesPerColumn > 1) {
                    // Set slides order
                    var newSlideOrderIndex;
                    var column, row;
                    if (s.params.slidesPerColumnFill === 'column') {
                        column = Math.floor(i / slidesPerColumn);
                        row = i - column * slidesPerColumn;
                        if (column > numFullColumns || (column === numFullColumns && row === slidesPerColumn-1)) {
                            if (++row >= slidesPerColumn) {
                                row = 0;
                                column++;
                            }
                        }
                        newSlideOrderIndex = column + row * slidesNumberEvenToRows / slidesPerColumn;
                        slide
                            .css({
                                '-webkit-box-ordinal-group': newSlideOrderIndex,
                                '-moz-box-ordinal-group': newSlideOrderIndex,
                                '-ms-flex-order': newSlideOrderIndex,
                                '-webkit-order': newSlideOrderIndex,
                                'order': newSlideOrderIndex
                            });
                    }
                    else {
                        row = Math.floor(i / slidesPerRow);
                        column = i - row * slidesPerRow;
                    }
                    slide
                        .css({
                            'margin-top': (row !== 0 && s.params.spaceBetween) && (s.params.spaceBetween + 'px')
                        })
                        .attr('data-swiper-column', column)
                        .attr('data-swiper-row', row);

                }
                if (slide.css('display') === 'none') continue;
                if (s.params.slidesPerView === 'auto') {
                    slideSize = s.isHorizontal() ? slide.outerWidth(true) : slide.outerHeight(true);
                    if (s.params.roundLengths) slideSize = round(slideSize);
                }
                else {
                    slideSize = (s.size - (s.params.slidesPerView - 1) * spaceBetween) / s.params.slidesPerView;
                    if (s.params.roundLengths) slideSize = round(slideSize);

                    if (s.isHorizontal()) {
                        s.slides[i].style.width = slideSize + 'px';
                    }
                    else {
                        s.slides[i].style.height = slideSize + 'px';
                    }
                }
                s.slides[i].swiperSlideSize = slideSize;
                s.slidesSizesGrid.push(slideSize);


                if (s.params.centeredSlides) {
                    slidePosition = slidePosition + slideSize / 2 + prevSlideSize / 2 + spaceBetween;
                    if (i === 0) slidePosition = slidePosition - s.size / 2 - spaceBetween;
                    if (Math.abs(slidePosition) < 1 / 1000) slidePosition = 0;
                    if ((index) % s.params.slidesPerGroup === 0) s.snapGrid.push(slidePosition);
                    s.slidesGrid.push(slidePosition);
                }
                else {
                    if ((index) % s.params.slidesPerGroup === 0) s.snapGrid.push(slidePosition);
                    s.slidesGrid.push(slidePosition);
                    slidePosition = slidePosition + slideSize + spaceBetween;
                }

                s.virtualSize += slideSize + spaceBetween;

                prevSlideSize = slideSize;

                index ++;
            }
            s.virtualSize = Math.max(s.virtualSize, s.size) + s.params.slidesOffsetAfter;
            var newSlidesGrid;

            if (
                s.rtl && s.wrongRTL && (s.params.effect === 'slide' || s.params.effect === 'coverflow')) {
                s.wrapper.css({width: s.virtualSize + s.params.spaceBetween + 'px'});
            }
            if (!s.support.flexbox || s.params.setWrapperSize) {
                if (s.isHorizontal()) s.wrapper.css({width: s.virtualSize + s.params.spaceBetween + 'px'});
                else s.wrapper.css({height: s.virtualSize + s.params.spaceBetween + 'px'});
            }

            if (s.params.slidesPerColumn > 1) {
                s.virtualSize = (slideSize + s.params.spaceBetween) * slidesNumberEvenToRows;
                s.virtualSize = Math.ceil(s.virtualSize / s.params.slidesPerColumn) - s.params.spaceBetween;
                s.wrapper.css({width: s.virtualSize + s.params.spaceBetween + 'px'});
                if (s.params.centeredSlides) {
                    newSlidesGrid = [];
                    for (i = 0; i < s.snapGrid.length; i++) {
                        if (s.snapGrid[i] < s.virtualSize + s.snapGrid[0]) newSlidesGrid.push(s.snapGrid[i]);
                    }
                    s.snapGrid = newSlidesGrid;
                }
            }

            // Remove last grid elements depending on width
            if (!s.params.centeredSlides) {
                newSlidesGrid = [];
                for (i = 0; i < s.snapGrid.length; i++) {
                    if (s.snapGrid[i] <= s.virtualSize - s.size) {
                        newSlidesGrid.push(s.snapGrid[i]);
                    }
                }
                s.snapGrid = newSlidesGrid;
                if (Math.floor(s.virtualSize - s.size) - Math.floor(s.snapGrid[s.snapGrid.length - 1]) > 1) {
                    s.snapGrid.push(s.virtualSize - s.size);
                }
            }
            if (s.snapGrid.length === 0) s.snapGrid = [0];

            if (s.params.spaceBetween !== 0) {
                if (s.isHorizontal()) {
                    if (s.rtl) s.slides.css({marginLeft: spaceBetween + 'px'});
                    else s.slides.css({marginRight: spaceBetween + 'px'});
                }
                else s.slides.css({marginBottom: spaceBetween + 'px'});
            }
            if (s.params.watchSlidesProgress) {
                s.updateSlidesOffset();
            }
        };
        s.updateSlidesOffset = function () {
            for (var i = 0; i < s.slides.length; i++) {
                s.slides[i].swiperSlideOffset = s.isHorizontal() ? s.slides[i].offsetLeft : s.slides[i].offsetTop;
            }
        };

        /*=========================
          Slider/slides progress
          ===========================*/
        s.updateSlidesProgress = function (translate) {
            if (typeof translate === 'undefined') {
                translate = s.translate || 0;
            }
            if (s.slides.length === 0) return;
            if (typeof s.slides[0].swiperSlideOffset === 'undefined') s.updateSlidesOffset();

            var offsetCenter = -translate;
            if (s.rtl) offsetCenter = translate;

            // Visible Slides
            s.slides.removeClass(s.params.slideVisibleClass);
            for (var i = 0; i < s.slides.length; i++) {
                var slide = s.slides[i];
                var slideProgress = (offsetCenter - slide.swiperSlideOffset) / (slide.swiperSlideSize + s.params.spaceBetween);
                if (s.params.watchSlidesVisibility) {
                    var slideBefore = -(offsetCenter - slide.swiperSlideOffset);
                    var slideAfter = slideBefore + s.slidesSizesGrid[i];
                    var isVisible =
                        (slideBefore >= 0 && slideBefore < s.size) ||
                        (slideAfter > 0 && slideAfter <= s.size) ||
                        (slideBefore <= 0 && slideAfter >= s.size);
                    if (isVisible) {
                        s.slides.eq(i).addClass(s.params.slideVisibleClass);
                    }
                }
                slide.progress = s.rtl ? -slideProgress : slideProgress;
            }
        };
        s.updateProgress = function (translate) {
            if (typeof translate === 'undefined') {
                translate = s.translate || 0;
            }
            var translatesDiff = s.maxTranslate() - s.minTranslate();
            var wasBeginning = s.isBeginning;
            var wasEnd = s.isEnd;
            if (translatesDiff === 0) {
                s.progress = 0;
                s.isBeginning = s.isEnd = true;
            }
            else {
                s.progress = (translate - s.minTranslate()) / (translatesDiff);
                s.isBeginning = s.progress <= 0;
                s.isEnd = s.progress >= 1;
            }
            if (s.isBeginning && !wasBeginning) s.emit('onReachBeginning', s);
            if (s.isEnd && !wasEnd) s.emit('onReachEnd', s);

            if (s.params.watchSlidesProgress) s.updateSlidesProgress(translate);
            s.emit('onProgress', s, s.progress);
        };
        s.updateActiveIndex = function () {
            var translate = s.rtl ? s.translate : -s.translate;
            var newActiveIndex, i, snapIndex;
            for (i = 0; i < s.slidesGrid.length; i ++) {
                if (typeof s.slidesGrid[i + 1] !== 'undefined') {
                    if (translate >= s.slidesGrid[i] && translate < s.slidesGrid[i + 1] - (s.slidesGrid[i + 1] - s.slidesGrid[i]) / 2) {
                        newActiveIndex = i;
                    }
                    else if (translate >= s.slidesGrid[i] && translate < s.slidesGrid[i + 1]) {
                        newActiveIndex = i + 1;
                    }
                }
                else {
                    if (translate >= s.slidesGrid[i]) {
                        newActiveIndex = i;
                    }
                }
            }
            // Normalize slideIndex
            if (newActiveIndex < 0 || typeof newActiveIndex === 'undefined') newActiveIndex = 0;
            // for (i = 0; i < s.slidesGrid.length; i++) {
                // if (- translate >= s.slidesGrid[i]) {
                    // newActiveIndex = i;
                // }
            // }
            snapIndex = Math.floor(newActiveIndex / s.params.slidesPerGroup);
            if (snapIndex >= s.snapGrid.length) snapIndex = s.snapGrid.length - 1;

            if (newActiveIndex === s.activeIndex) {
                return;
            }
            s.snapIndex = snapIndex;
            s.previousIndex = s.activeIndex;
            s.activeIndex = newActiveIndex;
            s.updateClasses();
        };

        /*=========================
          Classes
          ===========================*/
        s.updateClasses = function () {
            s.slides.removeClass(s.params.slideActiveClass + ' ' + s.params.slideNextClass + ' ' + s.params.slidePrevClass);
            var activeSlide = s.slides.eq(s.activeIndex);
            // Active classes
            activeSlide.addClass(s.params.slideActiveClass);
            // Next Slide
            var nextSlide = activeSlide.next('.' + s.params.slideClass).addClass(s.params.slideNextClass);
            if (s.params.loop && nextSlide.length === 0) {
                s.slides.eq(0).addClass(s.params.slideNextClass);
            }
            // Prev Slide
            var prevSlide = activeSlide.prev('.' + s.params.slideClass).addClass(s.params.slidePrevClass);
            if (s.params.loop && prevSlide.length === 0) {
                s.slides.eq(-1).addClass(s.params.slidePrevClass);
            }

            // Pagination
            if (s.paginationContainer && s.paginationContainer.length > 0) {
                // Current/Total
                var current,
                    total = s.params.loop ? Math.ceil((s.slides.length - s.loopedSlides * 2) / s.params.slidesPerGroup) : s.snapGrid.length;
                if (s.params.loop) {
                    current = Math.ceil((s.activeIndex - s.loopedSlides)/s.params.slidesPerGroup);
                    if (current > s.slides.length - 1 - s.loopedSlides * 2) {
                        current = current - (s.slides.length - s.loopedSlides * 2);
                    }
                    if (current > total - 1) current = current - total;
                    if (current < 0 && s.params.paginationType !== 'bullets') current = total + current;
                }
                else {
                    if (typeof s.snapIndex !== 'undefined') {
                        current = s.snapIndex;
                    }
                    else {
                        current = s.activeIndex || 0;
                    }
                }
                // Types
                if (s.params.paginationType === 'bullets' && s.bullets && s.bullets.length > 0) {
                    s.bullets.removeClass(s.params.bulletActiveClass);
                    if (s.paginationContainer.length > 1) {
                        s.bullets.each(function () {
                            if ($(this).index() === current) $(this).addClass(s.params.bulletActiveClass);
                        });
                    }
                    else {
                        s.bullets.eq(current).addClass(s.params.bulletActiveClass);
                    }
                }
                if (s.params.paginationType === 'fraction') {
                    s.paginationContainer.find('.' + s.params.paginationCurrentClass).text(current + 1);
                    s.paginationContainer.find('.' + s.params.paginationTotalClass).text(total);
                }
                if (s.params.paginationType === 'progress') {
                    var scale = (current + 1) / total,
                        scaleX = scale,
                        scaleY = 1;
                    if (!s.isHorizontal()) {
                        scaleY = scale;
                        scaleX = 1;
                    }
                    s.paginationContainer.find('.' + s.params.paginationProgressbarClass).transform('translate3d(0,0,0) scaleX(' + scaleX + ') scaleY(' + scaleY + ')').transition(s.params.speed);
                }
                if (s.params.paginationType === 'custom' && s.params.paginationCustomRender) {
                    s.paginationContainer.html(s.params.paginationCustomRender(s, current + 1, total));
                    s.emit('onPaginationRendered', s, s.paginationContainer[0]);
                }
            }

            // Next/active buttons
            if (!s.params.loop) {
                if (s.params.prevButton && s.prevButton && s.prevButton.length > 0) {
                    if (s.isBeginning) {
                        s.prevButton.addClass(s.params.buttonDisabledClass);
                        if (s.params.a11y && s.a11y) s.a11y.disable(s.prevButton);
                    }
                    else {
                        s.prevButton.removeClass(s.params.buttonDisabledClass);
                        if (s.params.a11y && s.a11y) s.a11y.enable(s.prevButton);
                    }
                }
                if (s.params.nextButton && s.nextButton && s.nextButton.length > 0) {
                    if (s.isEnd) {
                        s.nextButton.addClass(s.params.buttonDisabledClass);
                        if (s.params.a11y && s.a11y) s.a11y.disable(s.nextButton);
                    }
                    else {
                        s.nextButton.removeClass(s.params.buttonDisabledClass);
                        if (s.params.a11y && s.a11y) s.a11y.enable(s.nextButton);
                    }
                }
            }
        };

        /*=========================
          Pagination
          ===========================*/
        s.updatePagination = function () {
            if (!s.params.pagination) return;
            if (s.paginationContainer && s.paginationContainer.length > 0) {
                var paginationHTML = '';
                if (s.params.paginationType === 'bullets') {
                    var numberOfBullets = s.params.loop ? Math.ceil((s.slides.length - s.loopedSlides * 2) / s.params.slidesPerGroup) : s.snapGrid.length;
                    for (var i = 0; i < numberOfBullets; i++) {
                        if (s.params.paginationBulletRender) {
                            paginationHTML += s.params.paginationBulletRender(i, s.params.bulletClass);
                        }
                        else {
                            paginationHTML += '<' + s.params.paginationElement+' class="' + s.params.bulletClass + '"></' + s.params.paginationElement + '>';
                        }
                    }
                    s.paginationContainer.html(paginationHTML);
                    s.bullets = s.paginationContainer.find('.' + s.params.bulletClass);
                    if (s.params.paginationClickable && s.params.a11y && s.a11y) {
                        s.a11y.initPagination();
                    }
                }
                if (s.params.paginationType === 'fraction') {
                    if (s.params.paginationFractionRender) {
                        paginationHTML = s.params.paginationFractionRender(s, s.params.paginationCurrentClass, s.params.paginationTotalClass);
                    }
                    else {
                        paginationHTML =
                            '<span class="' + s.params.paginationCurrentClass + '"></span>' +
                            ' / ' +
                            '<span class="' + s.params.paginationTotalClass+'"></span>';
                    }
                    s.paginationContainer.html(paginationHTML);
                }
                if (s.params.paginationType === 'progress') {
                    if (s.params.paginationProgressRender) {
                        paginationHTML = s.params.paginationProgressRender(s, s.params.paginationProgressbarClass);
                    }
                    else {
                        paginationHTML = '<span class="' + s.params.paginationProgressbarClass + '"></span>';
                    }
                    s.paginationContainer.html(paginationHTML);
                }
                if (s.params.paginationType !== 'custom') {
                    s.emit('onPaginationRendered', s, s.paginationContainer[0]);
                }
            }
        };
        /*=========================
          Common update method
          ===========================*/
        s.update = function (updateTranslate) {
            s.updateContainerSize();
            s.updateSlidesSize();
            s.updateProgress();
            s.updatePagination();
            s.updateClasses();
            if (s.params.scrollbar && s.scrollbar) {
                s.scrollbar.set();
            }
            function forceSetTranslate() {
                newTranslate = Math.min(Math.max(s.translate, s.maxTranslate()), s.minTranslate());
                s.setWrapperTranslate(newTranslate);
                s.updateActiveIndex();
                s.updateClasses();
            }
            if (updateTranslate) {
                var translated, newTranslate;
                if (s.controller && s.controller.spline) {
                    s.controller.spline = undefined;
                }
                if (s.params.freeMode) {
                    forceSetTranslate();
                    if (s.params.autoHeight) {
                        s.updateAutoHeight();
                    }
                }
                else {
                    if ((s.params.slidesPerView === 'auto' || s.params.slidesPerView > 1) && s.isEnd && !s.params.centeredSlides) {
                        translated = s.slideTo(s.slides.length - 1, 0, false, true);
                    }
                    else {
                        translated = s.slideTo(s.activeIndex, 0, false, true);
                    }
                    if (!translated) {
                        forceSetTranslate();
                    }
                }
            }
            else if (s.params.autoHeight) {
                s.updateAutoHeight();
            }
        };

        /*=========================
          Resize Handler
          ===========================*/
        s.onResize = function (forceUpdatePagination) {
            //Breakpoints
            if (s.params.breakpoints) {
                s.setBreakpoint();
            }

            // Disable locks on resize
            var allowSwipeToPrev = s.params.allowSwipeToPrev;
            var allowSwipeToNext = s.params.allowSwipeToNext;
            s.params.allowSwipeToPrev = s.params.allowSwipeToNext = true;

            s.updateContainerSize();
            s.updateSlidesSize();
            if (s.params.slidesPerView === 'auto' || s.params.freeMode || forceUpdatePagination) s.updatePagination();
            if (s.params.scrollbar && s.scrollbar) {
                s.scrollbar.set();
            }
            if (s.controller && s.controller.spline) {
                s.controller.spline = undefined;
            }
            var slideChangedBySlideTo = false;
            if (s.params.freeMode) {
                var newTranslate = Math.min(Math.max(s.translate, s.maxTranslate()), s.minTranslate());
                s.setWrapperTranslate(newTranslate);
                s.updateActiveIndex();
                s.updateClasses();

                if (s.params.autoHeight) {
                    s.updateAutoHeight();
                }
            }
            else {
                s.updateClasses();
                if ((s.params.slidesPerView === 'auto' || s.params.slidesPerView > 1) && s.isEnd && !s.params.centeredSlides) {
                    slideChangedBySlideTo = s.slideTo(s.slides.length - 1, 0, false, true);
                }
                else {
                    slideChangedBySlideTo = s.slideTo(s.activeIndex, 0, false, true);
                }
            }
            if (s.params.lazyLoading && !slideChangedBySlideTo && s.lazy) {
                s.lazy.load();
            }
            // Return locks after resize
            s.params.allowSwipeToPrev = allowSwipeToPrev;
            s.params.allowSwipeToNext = allowSwipeToNext;
        };

        /*=========================
          Events
          ===========================*/

        //Define Touch Events
        var desktopEvents = ['mousedown', 'mousemove', 'mouseup'];
        if (window.navigator.pointerEnabled) desktopEvents = ['pointerdown', 'pointermove', 'pointerup'];
        else if (window.navigator.msPointerEnabled) desktopEvents = ['MSPointerDown', 'MSPointerMove', 'MSPointerUp'];
        s.touchEvents = {
            start : s.support.touch || !s.params.simulateTouch  ? 'touchstart' : desktopEvents[0],
            move : s.support.touch || !s.params.simulateTouch ? 'touchmove' : desktopEvents[1],
            end : s.support.touch || !s.params.simulateTouch ? 'touchend' : desktopEvents[2]
        };


        // WP8 Touch Events Fix
        if (window.navigator.pointerEnabled || window.navigator.msPointerEnabled) {
            (s.params.touchEventsTarget === 'container' ? s.container : s.wrapper).addClass('swiper-wp8-' + s.params.direction);
        }

        // Attach/detach events
        s.initEvents = function (detach) {
            var actionDom = detach ? 'off' : 'on';
            var action = detach ? 'removeEventListener' : 'addEventListener';
            var touchEventsTarget = s.params.touchEventsTarget === 'container' ? s.container[0] : s.wrapper[0];
            var target = s.support.touch ? touchEventsTarget : document;

            var moveCapture = s.params.nested ? true : false;

            //Touch Events
            if (s.browser.ie) {
                touchEventsTarget[action](s.touchEvents.start, s.onTouchStart, false);
                target[action](s.touchEvents.move, s.onTouchMove, moveCapture);
                target[action](s.touchEvents.end, s.onTouchEnd, false);
            }
            else {
                if (s.support.touch) {
                    touchEventsTarget[action](s.touchEvents.start, s.onTouchStart, false);
                    touchEventsTarget[action](s.touchEvents.move, s.onTouchMove, moveCapture);
                    touchEventsTarget[action](s.touchEvents.end, s.onTouchEnd, false);
                }
                if (params.simulateTouch && !s.device.ios && !s.device.android) {
                    touchEventsTarget[action]('mousedown', s.onTouchStart, false);
                    document[action]('mousemove', s.onTouchMove, moveCapture);
                    document[action]('mouseup', s.onTouchEnd, false);
                }
            }
            window[action]('resize', s.onResize);

            // Next, Prev, Index
            if (s.params.nextButton && s.nextButton && s.nextButton.length > 0) {
                s.nextButton[actionDom]('click', s.onClickNext);
                if (s.params.a11y && s.a11y) s.nextButton[actionDom]('keydown', s.a11y.onEnterKey);
            }
            if (s.params.prevButton && s.prevButton && s.prevButton.length > 0) {
                s.prevButton[actionDom]('click', s.onClickPrev);
                if (s.params.a11y && s.a11y) s.prevButton[actionDom]('keydown', s.a11y.onEnterKey);
            }
            if (s.params.pagination && s.params.paginationClickable) {
                s.paginationContainer[actionDom]('click', '.' + s.params.bulletClass, s.onClickIndex);
                if (s.params.a11y && s.a11y) s.paginationContainer[actionDom]('keydown', '.' + s.params.bulletClass, s.a11y.onEnterKey);
            }

            // Prevent Links Clicks
            if (s.params.preventClicks || s.params.preventClicksPropagation) touchEventsTarget[action]('click', s.preventClicks, true);
        };
        s.attachEvents = function () {
            s.initEvents();
        };
        s.detachEvents = function () {
            s.initEvents(true);
        };

        /*=========================
          Handle Clicks
          ===========================*/
        // Prevent Clicks
        s.allowClick = true;
        s.preventClicks = function (e) {
            if (!s.allowClick) {
                if (s.params.preventClicks) e.preventDefault();
                if (s.params.preventClicksPropagation && s.animating) {
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                }
            }
        };
        // Clicks
        s.onClickNext = function (e) {
            e.preventDefault();
            if (s.isEnd && !s.params.loop) return;
            s.slideNext();
        };
        s.onClickPrev = function (e) {
            e.preventDefault();
            if (s.isBeginning && !s.params.loop) return;
            s.slidePrev();
        };
        s.onClickIndex = function (e) {
            e.preventDefault();
            var index = $(this).index() * s.params.slidesPerGroup;
            if (s.params.loop) index = index + s.loopedSlides;
            s.slideTo(index);
        };

        /*=========================
          Handle Touches
          ===========================*/
        function findElementInEvent(e, selector) {
            var el = $(e.target);
            if (!el.is(selector)) {
                if (typeof selector === 'string') {
                    el = el.parents(selector);
                }
                else if (selector.nodeType) {
                    var found;
                    el.parents().each(function (index, _el) {
                        if (_el === selector) found = selector;
                    });
                    if (!found) return undefined;
                    else return selector;
                }
            }
            if (el.length === 0) {
                return undefined;
            }
            return el[0];
        }
        s.updateClickedSlide = function (e) {
            var slide = findElementInEvent(e, '.' + s.params.slideClass);
            var slideFound = false;
            if (slide) {
                for (var i = 0; i < s.slides.length; i++) {
                    if (s.slides[i] === slide) slideFound = true;
                }
            }

            if (slide && slideFound) {
                s.clickedSlide = slide;
                s.clickedIndex = $(slide).index();
            }
            else {
                s.clickedSlide = undefined;
                s.clickedIndex = undefined;
                return;
            }
            if (s.params.slideToClickedSlide && s.clickedIndex !== undefined && s.clickedIndex !== s.activeIndex) {
                var slideToIndex = s.clickedIndex,
                    realIndex,
                    duplicatedSlides;
                if (s.params.loop) {
                    if (s.animating) return;
                    realIndex = $(s.clickedSlide).attr('data-swiper-slide-index');
                    if (s.params.centeredSlides) {
                        if ((slideToIndex < s.loopedSlides - s.params.slidesPerView/2) || (slideToIndex > s.slides.length - s.loopedSlides + s.params.slidesPerView/2)) {
                            s.fixLoop();
                            slideToIndex = s.wrapper.children('.' + s.params.slideClass + '[data-swiper-slide-index="' + realIndex + '"]:not(.swiper-slide-duplicate)').eq(0).index();
                            setTimeout(function () {
                                s.slideTo(slideToIndex);
                            }, 0);
                        }
                        else {
                            s.slideTo(slideToIndex);
                        }
                    }
                    else {
                        if (slideToIndex > s.slides.length - s.params.slidesPerView) {
                            s.fixLoop();
                            slideToIndex = s.wrapper.children('.' + s.params.slideClass + '[data-swiper-slide-index="' + realIndex + '"]:not(.swiper-slide-duplicate)').eq(0).index();
                            setTimeout(function () {
                                s.slideTo(slideToIndex);
                            }, 0);
                        }
                        else {
                            s.slideTo(slideToIndex);
                        }
                    }
                }
                else {
                    s.slideTo(slideToIndex);
                }
            }
        };

        var isTouched,
            isMoved,
            allowTouchCallbacks,
            touchStartTime,
            isScrolling,
            currentTranslate,
            startTranslate,
            allowThresholdMove,
            // Form elements to match
            formElements = 'input, select, textarea, button',
            // Last click time
            lastClickTime = Date.now(), clickTimeout,
            //Velocities
            velocities = [],
            allowMomentumBounce;

        // Animating Flag
        s.animating = false;

        // Touches information
        s.touches = {
            startX: 0,
            startY: 0,
            currentX: 0,
            currentY: 0,
            diff: 0
        };

        // Touch handlers
        var isTouchEvent, startMoving;
        s.onTouchStart = function (e) {
            if (e.originalEvent) e = e.originalEvent;
            isTouchEvent = e.type === 'touchstart';
            if (!isTouchEvent && 'which' in e && e.which === 3) return;
            if (s.params.noSwiping && findElementInEvent(e, '.' + s.params.noSwipingClass)) {
                s.allowClick = true;
                return;
            }
            if (s.params.swipeHandler) {
                if (!findElementInEvent(e, s.params.swipeHandler)) return;
            }

            var startX = s.touches.currentX = e.type === 'touchstart' ? e.targetTouches[0].pageX : e.pageX;
            var startY = s.touches.currentY = e.type === 'touchstart' ? e.targetTouches[0].pageY : e.pageY;

            // Do NOT start if iOS edge swipe is detected. Otherwise iOS app (UIWebView) cannot swipe-to-go-back anymore
            if(s.device.ios && s.params.iOSEdgeSwipeDetection && startX <= s.params.iOSEdgeSwipeThreshold) {
                return;
            }

            isTouched = true;
            isMoved = false;
            allowTouchCallbacks = true;
            isScrolling = undefined;
            startMoving = undefined;
            s.touches.startX = startX;
            s.touches.startY = startY;
            touchStartTime = Date.now();
            s.allowClick = true;
            s.updateContainerSize();
            s.swipeDirection = undefined;
            if (s.params.threshold > 0) allowThresholdMove = false;
            if (e.type !== 'touchstart') {
                var preventDefault = true;
                if ($(e.target).is(formElements)) preventDefault = false;
                if (document.activeElement && $(document.activeElement).is(formElements)) {
                    document.activeElement.blur();
                }
                if (preventDefault) {
                    e.preventDefault();
                }
            }
            s.emit('onTouchStart', s, e);
        };

        s.onTouchMove = function (e) {
            if (e.originalEvent) e = e.originalEvent;
            if (isTouchEvent && e.type === 'mousemove') return;
            if (e.preventedByNestedSwiper) {
                s.touches.startX = e.type === 'touchmove' ? e.targetTouches[0].pageX : e.pageX;
                s.touches.startY = e.type === 'touchmove' ? e.targetTouches[0].pageY : e.pageY;
                return;
            }
            if (s.params.onlyExternal) {
                // isMoved = true;
                s.allowClick = false;
                if (isTouched) {
                    s.touches.startX = s.touches.currentX = e.type === 'touchmove' ? e.targetTouches[0].pageX : e.pageX;
                    s.touches.startY = s.touches.currentY = e.type === 'touchmove' ? e.targetTouches[0].pageY : e.pageY;
                    touchStartTime = Date.now();
                }
                return;
            }
            if (isTouchEvent && document.activeElement) {
                if (e.target === document.activeElement && $(e.target).is(formElements)) {
                    isMoved = true;
                    s.allowClick = false;
                    return;
                }
            }
            if (allowTouchCallbacks) {
                s.emit('onTouchMove', s, e);
            }
            if (e.targetTouches && e.targetTouches.length > 1) return;

            s.touches.currentX = e.type === 'touchmove' ? e.targetTouches[0].pageX : e.pageX;
            s.touches.currentY = e.type === 'touchmove' ? e.targetTouches[0].pageY : e.pageY;

            if (typeof isScrolling === 'undefined') {
                var touchAngle = Math.atan2(Math.abs(s.touches.currentY - s.touches.startY), Math.abs(s.touches.currentX - s.touches.startX)) * 180 / Math.PI;
                isScrolling = s.isHorizontal() ? touchAngle > s.params.touchAngle : (90 - touchAngle > s.params.touchAngle);
            }
            if (isScrolling) {
                s.emit('onTouchMoveOpposite', s, e);
            }
            if (typeof startMoving === 'undefined' && s.browser.ieTouch) {
                if (s.touches.currentX !== s.touches.startX || s.touches.currentY !== s.touches.startY) {
                    startMoving = true;
                }
            }
            if (!isTouched) return;
            if (isScrolling)  {
                isTouched = false;
                return;
            }
            if (!startMoving && s.browser.ieTouch) {
                return;
            }
            s.allowClick = false;
            s.emit('onSliderMove', s, e);
            e.preventDefault();
            if (s.params.touchMoveStopPropagation && !s.params.nested) {
                e.stopPropagation();
            }

            if (!isMoved) {
                if (params.loop) {
                    s.fixLoop();
                }
                startTranslate = s.getWrapperTranslate();
                s.setWrapperTransition(0);
                if (s.animating) {
                    s.wrapper.trigger('webkitTransitionEnd transitionend oTransitionEnd MSTransitionEnd msTransitionEnd');
                }
                if (s.params.autoplay && s.autoplaying) {
                    if (s.params.autoplayDisableOnInteraction) {
                        s.stopAutoplay();
                    }
                    else {
                        s.pauseAutoplay();
                    }
                }
                allowMomentumBounce = false;
                //Grab Cursor
                if (s.params.grabCursor) {
                    s.container[0].style.cursor = 'move';
                    s.container[0].style.cursor = '-webkit-grabbing';
                    s.container[0].style.cursor = '-moz-grabbin';
                    s.container[0].style.cursor = 'grabbing';
                }
            }
            isMoved = true;

            var diff = s.touches.diff = s.isHorizontal() ? s.touches.currentX - s.touches.startX : s.touches.currentY - s.touches.startY;

            diff = diff * s.params.touchRatio;
            if (s.rtl) diff = -diff;

            s.swipeDirection = diff > 0 ? 'prev' : 'next';
            currentTranslate = diff + startTranslate;

            var disableParentSwiper = true;
            if ((diff > 0 && currentTranslate > s.minTranslate())) {
                disableParentSwiper = false;
                if (s.params.resistance) currentTranslate = s.minTranslate() - 1 + Math.pow(-s.minTranslate() + startTranslate + diff, s.params.resistanceRatio);
            }
            else if (diff < 0 && currentTranslate < s.maxTranslate()) {
                disableParentSwiper = false;
                if (s.params.resistance) currentTranslate = s.maxTranslate() + 1 - Math.pow(s.maxTranslate() - startTranslate - diff, s.params.resistanceRatio);
            }

            if (disableParentSwiper) {
                e.preventedByNestedSwiper = true;
            }

            // Directions locks
            if (!s.params.allowSwipeToNext && s.swipeDirection === 'next' && currentTranslate < startTranslate) {
                currentTranslate = startTranslate;
            }
            if (!s.params.allowSwipeToPrev && s.swipeDirection === 'prev' && currentTranslate > startTranslate) {
                currentTranslate = startTranslate;
            }

            if (!s.params.followFinger) return;

            // Threshold
            if (s.params.threshold > 0) {
                if (Math.abs(diff) > s.params.threshold || allowThresholdMove) {
                    if (!allowThresholdMove) {
                        allowThresholdMove = true;
                        s.touches.startX = s.touches.currentX;
                        s.touches.startY = s.touches.currentY;
                        currentTranslate = startTranslate;
                        s.touches.diff = s.isHorizontal() ? s.touches.currentX - s.touches.startX : s.touches.currentY - s.touches.startY;
                        return;
                    }
                }
                else {
                    currentTranslate = startTranslate;
                    return;
                }
            }
            // Update active index in free mode
            if (s.params.freeMode || s.params.watchSlidesProgress) {
                s.updateActiveIndex();
            }
            if (s.params.freeMode) {
                //Velocity
                if (velocities.length === 0) {
                    velocities.push({
                        position: s.touches[s.isHorizontal() ? 'startX' : 'startY'],
                        time: touchStartTime
                    });
                }
                velocities.push({
                    position: s.touches[s.isHorizontal() ? 'currentX' : 'currentY'],
                    time: (new window.Date()).getTime()
                });
            }
            // Update progress
            s.updateProgress(currentTranslate);
            // Update translate
            s.setWrapperTranslate(currentTranslate);
        };
        s.onTouchEnd = function (e) {
            if (e.originalEvent) e = e.originalEvent;
            if (allowTouchCallbacks) {
                s.emit('onTouchEnd', s, e);
            }
            allowTouchCallbacks = false;
            if (!isTouched) return;
            //Return Grab Cursor
            if (s.params.grabCursor && isMoved && isTouched) {
                s.container[0].style.cursor = 'move';
                s.container[0].style.cursor = '-webkit-grab';
                s.container[0].style.cursor = '-moz-grab';
                s.container[0].style.cursor = 'grab';
            }

            // Time diff
            var touchEndTime = Date.now();
            var timeDiff = touchEndTime - touchStartTime;

            // Tap, doubleTap, Click
            if (s.allowClick) {
                s.updateClickedSlide(e);
                s.emit('onTap', s, e);
                if (timeDiff < 300 && (touchEndTime - lastClickTime) > 300) {
                    if (clickTimeout) clearTimeout(clickTimeout);
                    clickTimeout = setTimeout(function () {
                        if (!s) return;
                        if (s.params.paginationHide && s.paginationContainer.length > 0 && !$(e.target).hasClass(s.params.bulletClass)) {
                            s.paginationContainer.toggleClass(s.params.paginationHiddenClass);
                        }
                        s.emit('onClick', s, e);
                    }, 300);

                }
                if (timeDiff < 300 && (touchEndTime - lastClickTime) < 300) {
                    if (clickTimeout) clearTimeout(clickTimeout);
                    s.emit('onDoubleTap', s, e);
                }
            }

            lastClickTime = Date.now();
            setTimeout(function () {
                if (s) s.allowClick = true;
            }, 0);

            if (!isTouched || !isMoved || !s.swipeDirection || s.touches.diff === 0 || currentTranslate === startTranslate) {
                isTouched = isMoved = false;
                return;
            }
            isTouched = isMoved = false;

            var currentPos;
            if (s.params.followFinger) {
                currentPos = s.rtl ? s.translate : -s.translate;
            }
            else {
                currentPos = -currentTranslate;
            }
            if (s.params.freeMode) {
                if (currentPos < -s.minTranslate()) {
                    s.slideTo(s.activeIndex);
                    return;
                }
                else if (currentPos > -s.maxTranslate()) {
                    if (s.slides.length < s.snapGrid.length) {
                        s.slideTo(s.snapGrid.length - 1);
                    }
                    else {
                        s.slideTo(s.slides.length - 1);
                    }
                    return;
                }

                if (s.params.freeModeMomentum) {
                    if (velocities.length > 1) {
                        var lastMoveEvent = velocities.pop(), velocityEvent = velocities.pop();

                        var distance = lastMoveEvent.position - velocityEvent.position;
                        var time = lastMoveEvent.time - velocityEvent.time;
                        s.velocity = distance / time;
                        s.velocity = s.velocity / 2;
                        if (Math.abs(s.velocity) < s.params.freeModeMinimumVelocity) {
                            s.velocity = 0;
                        }
                        // this implies that the user stopped moving a finger then released.
                        // There would be no events with distance zero, so the last event is stale.
                        if (time > 150 || (new window.Date().getTime() - lastMoveEvent.time) > 300) {
                            s.velocity = 0;
                        }
                    } else {
                        s.velocity = 0;
                    }

                    velocities.length = 0;
                    var momentumDuration = 1000 * s.params.freeModeMomentumRatio;
                    var momentumDistance = s.velocity * momentumDuration;

                    var newPosition = s.translate + momentumDistance;
                    if (s.rtl) newPosition = - newPosition;
                    var doBounce = false;
                    var afterBouncePosition;
                    var bounceAmount = Math.abs(s.velocity) * 20 * s.params.freeModeMomentumBounceRatio;
                    if (newPosition < s.maxTranslate()) {
                        if (s.params.freeModeMomentumBounce) {
                            if (newPosition + s.maxTranslate() < -bounceAmount) {
                                newPosition = s.maxTranslate() - bounceAmount;
                            }
                            afterBouncePosition = s.maxTranslate();
                            doBounce = true;
                            allowMomentumBounce = true;
                        }
                        else {
                            newPosition = s.maxTranslate();
                        }
                    }
                    else if (newPosition > s.minTranslate()) {
                        if (s.params.freeModeMomentumBounce) {
                            if (newPosition - s.minTranslate() > bounceAmount) {
                                newPosition = s.minTranslate() + bounceAmount;
                            }
                            afterBouncePosition = s.minTranslate();
                            doBounce = true;
                            allowMomentumBounce = true;
                        }
                        else {
                            newPosition = s.minTranslate();
                        }
                    }
                    else if (s.params.freeModeSticky) {
                        var j = 0,
                            nextSlide;
                        for (j = 0; j < s.snapGrid.length; j += 1) {
                            if (s.snapGrid[j] > -newPosition) {
                                nextSlide = j;
                                break;
                            }

                        }
                        if (Math.abs(s.snapGrid[nextSlide] - newPosition) < Math.abs(s.snapGrid[nextSlide - 1] - newPosition) || s.swipeDirection === 'next') {
                            newPosition = s.snapGrid[nextSlide];
                        } else {
                            newPosition = s.snapGrid[nextSlide - 1];
                        }
                        if (!s.rtl) newPosition = - newPosition;
                    }
                    //Fix duration
                    if (s.velocity !== 0) {
                        if (s.rtl) {
                            momentumDuration = Math.abs((-newPosition - s.translate) / s.velocity);
                        }
                        else {
                            momentumDuration = Math.abs((newPosition - s.translate) / s.velocity);
                        }
                    }
                    else if (s.params.freeModeSticky) {
                        s.slideReset();
                        return;
                    }

                    if (s.params.freeModeMomentumBounce && doBounce) {
                        s.updateProgress(afterBouncePosition);
                        s.setWrapperTransition(momentumDuration);
                        s.setWrapperTranslate(newPosition);
                        s.onTransitionStart();
                        s.animating = true;
                        s.wrapper.transitionEnd(function () {
                            if (!s || !allowMomentumBounce) return;
                            s.emit('onMomentumBounce', s);

                            s.setWrapperTransition(s.params.speed);
                            s.setWrapperTranslate(afterBouncePosition);
                            s.wrapper.transitionEnd(function () {
                                if (!s) return;
                                s.onTransitionEnd();
                            });
                        });
                    } else if (s.velocity) {
                        s.updateProgress(newPosition);
                        s.setWrapperTransition(momentumDuration);
                        s.setWrapperTranslate(newPosition);
                        s.onTransitionStart();
                        if (!s.animating) {
                            s.animating = true;
                            s.wrapper.transitionEnd(function () {
                                if (!s) return;
                                s.onTransitionEnd();
                            });
                        }

                    } else {
                        s.updateProgress(newPosition);
                    }

                    s.updateActiveIndex();
                }
                if (!s.params.freeModeMomentum || timeDiff >= s.params.longSwipesMs) {
                    s.updateProgress();
                    s.updateActiveIndex();
                }
                return;
            }

            // Find current slide
            var i, stopIndex = 0, groupSize = s.slidesSizesGrid[0];
            for (i = 0; i < s.slidesGrid.length; i += s.params.slidesPerGroup) {
                if (typeof s.slidesGrid[i + s.params.slidesPerGroup] !== 'undefined') {
                    if (currentPos >= s.slidesGrid[i] && currentPos < s.slidesGrid[i + s.params.slidesPerGroup]) {
                        stopIndex = i;
                        groupSize = s.slidesGrid[i + s.params.slidesPerGroup] - s.slidesGrid[i];
                    }
                }
                else {
                    if (currentPos >= s.slidesGrid[i]) {
                        stopIndex = i;
                        groupSize = s.slidesGrid[s.slidesGrid.length - 1] - s.slidesGrid[s.slidesGrid.length - 2];
                    }
                }
            }

            // Find current slide size
            var ratio = (currentPos - s.slidesGrid[stopIndex]) / groupSize;

            if (timeDiff > s.params.longSwipesMs) {
                // Long touches
                if (!s.params.longSwipes) {
                    s.slideTo(s.activeIndex);
                    return;
                }
                if (s.swipeDirection === 'next') {
                    if (ratio >= s.params.longSwipesRatio) s.slideTo(stopIndex + s.params.slidesPerGroup);
                    else s.slideTo(stopIndex);

                }
                if (s.swipeDirection === 'prev') {
                    if (ratio > (1 - s.params.longSwipesRatio)) s.slideTo(stopIndex + s.params.slidesPerGroup);
                    else s.slideTo(stopIndex);
                }
            }
            else {
                // Short swipes
                if (!s.params.shortSwipes) {
                    s.slideTo(s.activeIndex);
                    return;
                }
                if (s.swipeDirection === 'next') {
                    s.slideTo(stopIndex + s.params.slidesPerGroup);

                }
                if (s.swipeDirection === 'prev') {
                    s.slideTo(stopIndex);
                }
            }
        };
        /*=========================
          Transitions
          ===========================*/
        s._slideTo = function (slideIndex, speed) {
            return s.slideTo(slideIndex, speed, true, true);
        };
        s.slideTo = function (slideIndex, speed, runCallbacks, internal) {
            if (typeof runCallbacks === 'undefined') runCallbacks = true;
            if (typeof slideIndex === 'undefined') slideIndex = 0;
            if (slideIndex < 0) slideIndex = 0;
            s.snapIndex = Math.floor(slideIndex / s.params.slidesPerGroup);
            if (s.snapIndex >= s.snapGrid.length) s.snapIndex = s.snapGrid.length - 1;

            var translate = - s.snapGrid[s.snapIndex];
            // Stop autoplay
            if (s.params.autoplay && s.autoplaying) {
                if (internal || !s.params.autoplayDisableOnInteraction) {
                    s.pauseAutoplay(speed);
                }
                else {
                    s.stopAutoplay();
                }
            }
            // Update progress
            s.updateProgress(translate);

            // Normalize slideIndex
            for (var i = 0; i < s.slidesGrid.length; i++) {
                if (- Math.floor(translate * 100) >= Math.floor(s.slidesGrid[i] * 100)) {
                    slideIndex = i;
                }
            }

            // Directions locks
            if (!s.params.allowSwipeToNext && translate < s.translate && translate < s.minTranslate()) {
                return false;
            }
            if (!s.params.allowSwipeToPrev && translate > s.translate && translate > s.maxTranslate()) {
                if ((s.activeIndex || 0) !== slideIndex ) return false;
            }

            // Update Index
            if (typeof speed === 'undefined') speed = s.params.speed;
            s.previousIndex = s.activeIndex || 0;
            s.activeIndex = slideIndex;

            if ((s.rtl && -translate === s.translate) || (!s.rtl && translate === s.translate)) {
                // Update Height
                if (s.params.autoHeight) {
                    s.updateAutoHeight();
                }
                s.updateClasses();
                if (s.params.effect !== 'slide') {
                    s.setWrapperTranslate(translate);
                }
                return false;
            }
            s.updateClasses();
            s.onTransitionStart(runCallbacks);

            if (speed === 0) {
                s.setWrapperTranslate(translate);
                s.setWrapperTransition(0);
                s.onTransitionEnd(runCallbacks);
            }
            else {
                s.setWrapperTranslate(translate);
                s.setWrapperTransition(speed);
                if (!s.animating) {
                    s.animating = true;
                    s.wrapper.transitionEnd(function () {
                        if (!s) return;
                        s.onTransitionEnd(runCallbacks);
                    });
                }

            }

            return true;
        };

        s.onTransitionStart = function (runCallbacks) {
            if (typeof runCallbacks === 'undefined') runCallbacks = true;
            if (s.params.autoHeight) {
                s.updateAutoHeight();
            }
            if (s.lazy) s.lazy.onTransitionStart();
            if (runCallbacks) {
                s.emit('onTransitionStart', s);
                if (s.activeIndex !== s.previousIndex) {
                    s.emit('onSlideChangeStart', s);
                    if (s.activeIndex > s.previousIndex) {
                        s.emit('onSlideNextStart', s);
                    }
                    else {
                        s.emit('onSlidePrevStart', s);
                    }
                }

            }
        };
        s.onTransitionEnd = function (runCallbacks) {
            s.animating = false;
            s.setWrapperTransition(0);
            if (typeof runCallbacks === 'undefined') runCallbacks = true;
            if (s.lazy) s.lazy.onTransitionEnd();
            if (runCallbacks) {
                s.emit('onTransitionEnd', s);
                if (s.activeIndex !== s.previousIndex) {
                    s.emit('onSlideChangeEnd', s);
                    if (s.activeIndex > s.previousIndex) {
                        s.emit('onSlideNextEnd', s);
                    }
                    else {
                        s.emit('onSlidePrevEnd', s);
                    }
                }
            }
            if (s.params.hashnav && s.hashnav) {
                s.hashnav.setHash();
            }

        };
        s.slideNext = function (runCallbacks, speed, internal) {
            if (s.params.loop) {
                if (s.animating) return false;
                s.fixLoop();
                var clientLeft = s.container[0].clientLeft;
                return s.slideTo(s.activeIndex + s.params.slidesPerGroup, speed, runCallbacks, internal);
            }
            else return s.slideTo(s.activeIndex + s.params.slidesPerGroup, speed, runCallbacks, internal);
        };
        s._slideNext = function (speed) {
            return s.slideNext(true, speed, true);
        };
        s.slidePrev = function (runCallbacks, speed, internal) {
            if (s.params.loop) {
                if (s.animating) return false;
                s.fixLoop();
                var clientLeft = s.container[0].clientLeft;
                return s.slideTo(s.activeIndex - 1, speed, runCallbacks, internal);
            }
            else return s.slideTo(s.activeIndex - 1, speed, runCallbacks, internal);
        };
        s._slidePrev = function (speed) {
            return s.slidePrev(true, speed, true);
        };
        s.slideReset = function (runCallbacks, speed, internal) {
            return s.slideTo(s.activeIndex, speed, runCallbacks);
        };

        /*=========================
          Translate/transition helpers
          ===========================*/
        s.setWrapperTransition = function (duration, byController) {
            s.wrapper.transition(duration);
            if (s.params.effect !== 'slide' && s.effects[s.params.effect]) {
                s.effects[s.params.effect].setTransition(duration);
            }
            if (s.params.parallax && s.parallax) {
                s.parallax.setTransition(duration);
            }
            if (s.params.scrollbar && s.scrollbar) {
                s.scrollbar.setTransition(duration);
            }
            if (s.params.control && s.controller) {
                s.controller.setTransition(duration, byController);
            }
            s.emit('onSetTransition', s, duration);
        };
        s.setWrapperTranslate = function (translate, updateActiveIndex, byController) {
            var x = 0, y = 0, z = 0;
            if (s.isHorizontal()) {
                x = s.rtl ? -translate : translate;
            }
            else {
                y = translate;
            }

            if (s.params.roundLengths) {
                x = round(x);
                y = round(y);
            }

            if (!s.params.virtualTranslate) {
                if (s.support.transforms3d) s.wrapper.transform('translate3d(' + x + 'px, ' + y + 'px, ' + z + 'px)');
                else s.wrapper.transform('translate(' + x + 'px, ' + y + 'px)');
            }

            s.translate = s.isHorizontal() ? x : y;

            // Check if we need to update progress
            var progress;
            var translatesDiff = s.maxTranslate() - s.minTranslate();
            if (translatesDiff === 0) {
                progress = 0;
            }
            else {
                progress = (translate - s.minTranslate()) / (translatesDiff);
            }
            if (progress !== s.progress) {
                s.updateProgress(translate);
            }

            if (updateActiveIndex) s.updateActiveIndex();
            if (s.params.effect !== 'slide' && s.effects[s.params.effect]) {
                s.effects[s.params.effect].setTranslate(s.translate);
            }
            if (s.params.parallax && s.parallax) {
                s.parallax.setTranslate(s.translate);
            }
            if (s.params.scrollbar && s.scrollbar) {
                s.scrollbar.setTranslate(s.translate);
            }
            if (s.params.control && s.controller) {
                s.controller.setTranslate(s.translate, byController);
            }
            s.emit('onSetTranslate', s, s.translate);
        };

        s.getTranslate = function (el, axis) {
            var matrix, curTransform, curStyle, transformMatrix;

            // automatic axis detection
            if (typeof axis === 'undefined') {
                axis = 'x';
            }

            if (s.params.virtualTranslate) {
                return s.rtl ? -s.translate : s.translate;
            }

            curStyle = window.getComputedStyle(el, null);
            if (window.WebKitCSSMatrix) {
                curTransform = curStyle.transform || curStyle.webkitTransform;
                if (curTransform.split(',').length > 6) {
                    curTransform = curTransform.split(', ').map(function(a){
                        return a.replace(',','.');
                    }).join(', ');
                }
                // Some old versions of Webkit choke when 'none' is passed; pass
                // empty string instead in this case
                transformMatrix = new window.WebKitCSSMatrix(curTransform === 'none' ? '' : curTransform);
            }
            else {
                transformMatrix = curStyle.MozTransform || curStyle.OTransform || curStyle.MsTransform || curStyle.msTransform  || curStyle.transform || curStyle.getPropertyValue('transform').replace('translate(', 'matrix(1, 0, 0, 1,');
                matrix = transformMatrix.toString().split(',');
            }

            if (axis === 'x') {
                //Latest Chrome and webkits Fix
                if (window.WebKitCSSMatrix)
                    curTransform = transformMatrix.m41;
                //Crazy IE10 Matrix
                else if (matrix.length === 16)
                    curTransform = parseFloat(matrix[12]);
                //Normal Browsers
                else
                    curTransform = parseFloat(matrix[4]);
            }
            if (axis === 'y') {
                //Latest Chrome and webkits Fix
                if (window.WebKitCSSMatrix)
                    curTransform = transformMatrix.m42;
                //Crazy IE10 Matrix
                else if (matrix.length === 16)
                    curTransform = parseFloat(matrix[13]);
                //Normal Browsers
                else
                    curTransform = parseFloat(matrix[5]);
            }
            if (s.rtl && curTransform) curTransform = -curTransform;
            return curTransform || 0;
        };
        s.getWrapperTranslate = function (axis) {
            if (typeof axis === 'undefined') {
                axis = s.isHorizontal() ? 'x' : 'y';
            }
            return s.getTranslate(s.wrapper[0], axis);
        };

        /*=========================
          Observer
          ===========================*/
        s.observers = [];
        function initObserver(target, options) {
            options = options || {};
            // create an observer instance
            var ObserverFunc = window.MutationObserver || window.WebkitMutationObserver;
            var observer = new ObserverFunc(function (mutations) {
                mutations.forEach(function (mutation) {
                    s.onResize(true);
                    s.emit('onObserverUpdate', s, mutation);
                });
            });

            observer.observe(target, {
                attributes: typeof options.attributes === 'undefined' ? true : options.attributes,
                childList: typeof options.childList === 'undefined' ? true : options.childList,
                characterData: typeof options.characterData === 'undefined' ? true : options.characterData
            });

            s.observers.push(observer);
        }
        s.initObservers = function () {
            if (s.params.observeParents) {
                var containerParents = s.container.parents();
                for (var i = 0; i < containerParents.length; i++) {
                    initObserver(containerParents[i]);
                }
            }

            // Observe container
            initObserver(s.container[0], {childList: false});

            // Observe wrapper
            initObserver(s.wrapper[0], {attributes: false});
        };
        s.disconnectObservers = function () {
            for (var i = 0; i < s.observers.length; i++) {
                s.observers[i].disconnect();
            }
            s.observers = [];
        };
        /*=========================
          Loop
          ===========================*/
        // Create looped slides
        s.createLoop = function () {
            // Remove duplicated slides
            s.wrapper.children('.' + s.params.slideClass + '.' + s.params.slideDuplicateClass).remove();

            var slides = s.wrapper.children('.' + s.params.slideClass);

            if(s.params.slidesPerView === 'auto' && !s.params.loopedSlides) s.params.loopedSlides = slides.length;

            s.loopedSlides = parseInt(s.params.loopedSlides || s.params.slidesPerView, 10);
            s.loopedSlides = s.loopedSlides + s.params.loopAdditionalSlides;
            if (s.loopedSlides > slides.length) {
                s.loopedSlides = slides.length;
            }

            var prependSlides = [], appendSlides = [], i;
            slides.each(function (index, el) {
                var slide = $(this);
                if (index < s.loopedSlides) appendSlides.push(el);
                if (index < slides.length && index >= slides.length - s.loopedSlides) prependSlides.push(el);
                slide.attr('data-swiper-slide-index', index);
            });
            for (i = 0; i < appendSlides.length; i++) {
                s.wrapper.append($(appendSlides[i].cloneNode(true)).addClass(s.params.slideDuplicateClass));
            }
            for (i = prependSlides.length - 1; i >= 0; i--) {
                s.wrapper.prepend($(prependSlides[i].cloneNode(true)).addClass(s.params.slideDuplicateClass));
            }
        };
        s.destroyLoop = function () {
            s.wrapper.children('.' + s.params.slideClass + '.' + s.params.slideDuplicateClass).remove();
            s.slides.removeAttr('data-swiper-slide-index');
        };
        s.reLoop = function (updatePosition) {
            var oldIndex = s.activeIndex - s.loopedSlides;
            s.destroyLoop();
            s.createLoop();
            s.updateSlidesSize();
            if (updatePosition) {
                s.slideTo(oldIndex + s.loopedSlides, 0, false);
            }

        };
        s.fixLoop = function () {
            var newIndex;
            //Fix For Negative Oversliding
            if (s.activeIndex < s.loopedSlides) {
                newIndex = s.slides.length - s.loopedSlides * 3 + s.activeIndex;
                newIndex = newIndex + s.loopedSlides;
                s.slideTo(newIndex, 0, false, true);
            }
            //Fix For Positive Oversliding
            else if ((s.params.slidesPerView === 'auto' && s.activeIndex >= s.loopedSlides * 2) || (s.activeIndex > s.slides.length - s.params.slidesPerView * 2)) {
                newIndex = -s.slides.length + s.activeIndex + s.loopedSlides;
                newIndex = newIndex + s.loopedSlides;
                s.slideTo(newIndex, 0, false, true);
            }
        };
        /*=========================
          Append/Prepend/Remove Slides
          ===========================*/
        s.appendSlide = function (slides) {
            if (s.params.loop) {
                s.destroyLoop();
            }
            if (typeof slides === 'object' && slides.length) {
                for (var i = 0; i < slides.length; i++) {
                    if (slides[i]) s.wrapper.append(slides[i]);
                }
            }
            else {
                s.wrapper.append(slides);
            }
            if (s.params.loop) {
                s.createLoop();
            }
            if (!(s.params.observer && s.support.observer)) {
                s.update(true);
            }
        };
        s.prependSlide = function (slides) {
            if (s.params.loop) {
                s.destroyLoop();
            }
            var newActiveIndex = s.activeIndex + 1;
            if (typeof slides === 'object' && slides.length) {
                for (var i = 0; i < slides.length; i++) {
                    if (slides[i]) s.wrapper.prepend(slides[i]);
                }
                newActiveIndex = s.activeIndex + slides.length;
            }
            else {
                s.wrapper.prepend(slides);
            }
            if (s.params.loop) {
                s.createLoop();
            }
            if (!(s.params.observer && s.support.observer)) {
                s.update(true);
            }
            s.slideTo(newActiveIndex, 0, false);
        };
        s.removeSlide = function (slidesIndexes) {
            if (s.params.loop) {
                s.destroyLoop();
                s.slides = s.wrapper.children('.' + s.params.slideClass);
            }
            var newActiveIndex = s.activeIndex,
                indexToRemove;
            if (typeof slidesIndexes === 'object' && slidesIndexes.length) {
                for (var i = 0; i < slidesIndexes.length; i++) {
                    indexToRemove = slidesIndexes[i];
                    if (s.slides[indexToRemove]) s.slides.eq(indexToRemove).remove();
                    if (indexToRemove < newActiveIndex) newActiveIndex--;
                }
                newActiveIndex = Math.max(newActiveIndex, 0);
            }
            else {
                indexToRemove = slidesIndexes;
                if (s.slides[indexToRemove]) s.slides.eq(indexToRemove).remove();
                if (indexToRemove < newActiveIndex) newActiveIndex--;
                newActiveIndex = Math.max(newActiveIndex, 0);
            }

            if (s.params.loop) {
                s.createLoop();
            }

            if (!(s.params.observer && s.support.observer)) {
                s.update(true);
            }
            if (s.params.loop) {
                s.slideTo(newActiveIndex + s.loopedSlides, 0, false);
            }
            else {
                s.slideTo(newActiveIndex, 0, false);
            }

        };
        s.removeAllSlides = function () {
            var slidesIndexes = [];
            for (var i = 0; i < s.slides.length; i++) {
                slidesIndexes.push(i);
            }
            s.removeSlide(slidesIndexes);
        };


        /*=========================
          Effects
          ===========================*/
        s.effects = {
            fade: {
                setTranslate: function () {
                    for (var i = 0; i < s.slides.length; i++) {
                        var slide = s.slides.eq(i);
                        var offset = slide[0].swiperSlideOffset;
                        var tx = -offset;
                        if (!s.params.virtualTranslate) tx = tx - s.translate;
                        var ty = 0;
                        if (!s.isHorizontal()) {
                            ty = tx;
                            tx = 0;
                        }
                        var slideOpacity = s.params.fade.crossFade ?
                                Math.max(1 - Math.abs(slide[0].progress), 0) :
                                1 + Math.min(Math.max(slide[0].progress, -1), 0);
                        slide
                            .css({
                                opacity: slideOpacity
                            })
                            .transform('translate3d(' + tx + 'px, ' + ty + 'px, 0px)');

                    }

                },
                setTransition: function (duration) {
                    s.slides.transition(duration);
                    if (s.params.virtualTranslate && duration !== 0) {
                        var eventTriggered = false;
                        s.slides.transitionEnd(function () {
                            if (eventTriggered) return;
                            if (!s) return;
                            eventTriggered = true;
                            s.animating = false;
                            var triggerEvents = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'];
                            for (var i = 0; i < triggerEvents.length; i++) {
                                s.wrapper.trigger(triggerEvents[i]);
                            }
                        });
                    }
                }
            },
            flip: {
                setTranslate: function () {
                    for (var i = 0; i < s.slides.length; i++) {
                        var slide = s.slides.eq(i);
                        var progress = slide[0].progress;
                        if (s.params.flip.limitRotation) {
                            progress = Math.max(Math.min(slide[0].progress, 1), -1);
                        }
                        var offset = slide[0].swiperSlideOffset;
                        var rotate = -180 * progress,
                            rotateY = rotate,
                            rotateX = 0,
                            tx = -offset,
                            ty = 0;
                        if (!s.isHorizontal()) {
                            ty = tx;
                            tx = 0;
                            rotateX = -rotateY;
                            rotateY = 0;
                        }
                        else if (s.rtl) {
                            rotateY = -rotateY;
                        }

                        slide[0].style.zIndex = -Math.abs(Math.round(progress)) + s.slides.length;

                        if (s.params.flip.slideShadows) {
                            //Set shadows
                            var shadowBefore = s.isHorizontal() ? slide.find('.swiper-slide-shadow-left') : slide.find('.swiper-slide-shadow-top');
                            var shadowAfter = s.isHorizontal() ? slide.find('.swiper-slide-shadow-right') : slide.find('.swiper-slide-shadow-bottom');
                            if (shadowBefore.length === 0) {
                                shadowBefore = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? 'left' : 'top') + '"></div>');
                                slide.append(shadowBefore);
                            }
                            if (shadowAfter.length === 0) {
                                shadowAfter = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? 'right' : 'bottom') + '"></div>');
                                slide.append(shadowAfter);
                            }
                            if (shadowBefore.length) shadowBefore[0].style.opacity = Math.max(-progress, 0);
                            if (shadowAfter.length) shadowAfter[0].style.opacity = Math.max(progress, 0);
                        }

                        slide
                            .transform('translate3d(' + tx + 'px, ' + ty + 'px, 0px) rotateX(' + rotateX + 'deg) rotateY(' + rotateY + 'deg)');
                    }
                },
                setTransition: function (duration) {
                    s.slides.transition(duration).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(duration);
                    if (s.params.virtualTranslate && duration !== 0) {
                        var eventTriggered = false;
                        s.slides.eq(s.activeIndex).transitionEnd(function () {
                            if (eventTriggered) return;
                            if (!s) return;
                            if (!$(this).hasClass(s.params.slideActiveClass)) return;
                            eventTriggered = true;
                            s.animating = false;
                            var triggerEvents = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'];
                            for (var i = 0; i < triggerEvents.length; i++) {
                                s.wrapper.trigger(triggerEvents[i]);
                            }
                        });
                    }
                }
            },
            cube: {
                setTranslate: function () {
                    var wrapperRotate = 0, cubeShadow;
                    if (s.params.cube.shadow) {
                        if (s.isHorizontal()) {
                            cubeShadow = s.wrapper.find('.swiper-cube-shadow');
                            if (cubeShadow.length === 0) {
                                cubeShadow = $('<div class="swiper-cube-shadow"></div>');
                                s.wrapper.append(cubeShadow);
                            }
                            cubeShadow.css({height: s.width + 'px'});
                        }
                        else {
                            cubeShadow = s.container.find('.swiper-cube-shadow');
                            if (cubeShadow.length === 0) {
                                cubeShadow = $('<div class="swiper-cube-shadow"></div>');
                                s.container.append(cubeShadow);
                            }
                        }
                    }
                    for (var i = 0; i < s.slides.length; i++) {
                        var slide = s.slides.eq(i);
                        var slideAngle = i * 90;
                        var round = Math.floor(slideAngle / 360);
                        if (s.rtl) {
                            slideAngle = -slideAngle;
                            round = Math.floor(-slideAngle / 360);
                        }
                        var progress = Math.max(Math.min(slide[0].progress, 1), -1);
                        var tx = 0, ty = 0, tz = 0;
                        if (i % 4 === 0) {
                            tx = - round * 4 * s.size;
                            tz = 0;
                        }
                        else if ((i - 1) % 4 === 0) {
                            tx = 0;
                            tz = - round * 4 * s.size;
                        }
                        else if ((i - 2) % 4 === 0) {
                            tx = s.size + round * 4 * s.size;
                            tz = s.size;
                        }
                        else if ((i - 3) % 4 === 0) {
                            tx = - s.size;
                            tz = 3 * s.size + s.size * 4 * round;
                        }
                        if (s.rtl) {
                            tx = -tx;
                        }

                        if (!s.isHorizontal()) {
                            ty = tx;
                            tx = 0;
                        }

                        var transform = 'rotateX(' + (s.isHorizontal() ? 0 : -slideAngle) + 'deg) rotateY(' + (s.isHorizontal() ? slideAngle : 0) + 'deg) translate3d(' + tx + 'px, ' + ty + 'px, ' + tz + 'px)';
                        if (progress <= 1 && progress > -1) {
                            wrapperRotate = i * 90 + progress * 90;
                            if (s.rtl) wrapperRotate = -i * 90 - progress * 90;
                        }
                        slide.transform(transform);
                        if (s.params.cube.slideShadows) {
                            //Set shadows
                            var shadowBefore = s.isHorizontal() ? slide.find('.swiper-slide-shadow-left') : slide.find('.swiper-slide-shadow-top');
                            var shadowAfter = s.isHorizontal() ? slide.find('.swiper-slide-shadow-right') : slide.find('.swiper-slide-shadow-bottom');
                            if (shadowBefore.length === 0) {
                                shadowBefore = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? 'left' : 'top') + '"></div>');
                                slide.append(shadowBefore);
                            }
                            if (shadowAfter.length === 0) {
                                shadowAfter = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? 'right' : 'bottom') + '"></div>');
                                slide.append(shadowAfter);
                            }
                            if (shadowBefore.length) shadowBefore[0].style.opacity = Math.max(-progress, 0);
                            if (shadowAfter.length) shadowAfter[0].style.opacity = Math.max(progress, 0);
                        }
                    }
                    s.wrapper.css({
                        '-webkit-transform-origin': '50% 50% -' + (s.size / 2) + 'px',
                        '-moz-transform-origin': '50% 50% -' + (s.size / 2) + 'px',
                        '-ms-transform-origin': '50% 50% -' + (s.size / 2) + 'px',
                        'transform-origin': '50% 50% -' + (s.size / 2) + 'px'
                    });

                    if (s.params.cube.shadow) {
                        if (s.isHorizontal()) {
                            cubeShadow.transform('translate3d(0px, ' + (s.width / 2 + s.params.cube.shadowOffset) + 'px, ' + (-s.width / 2) + 'px) rotateX(90deg) rotateZ(0deg) scale(' + (s.params.cube.shadowScale) + ')');
                        }
                        else {
                            var shadowAngle = Math.abs(wrapperRotate) - Math.floor(Math.abs(wrapperRotate) / 90) * 90;
                            var multiplier = 1.5 - (Math.sin(shadowAngle * 2 * Math.PI / 360) / 2 + Math.cos(shadowAngle * 2 * Math.PI / 360) / 2);
                            var scale1 = s.params.cube.shadowScale,
                                scale2 = s.params.cube.shadowScale / multiplier,
                                offset = s.params.cube.shadowOffset;
                            cubeShadow.transform('scale3d(' + scale1 + ', 1, ' + scale2 + ') translate3d(0px, ' + (s.height / 2 + offset) + 'px, ' + (-s.height / 2 / scale2) + 'px) rotateX(-90deg)');
                        }
                    }
                    var zFactor = (s.isSafari || s.isUiWebView) ? (-s.size / 2) : 0;
                    s.wrapper.transform('translate3d(0px,0,' + zFactor + 'px) rotateX(' + (s.isHorizontal() ? 0 : wrapperRotate) + 'deg) rotateY(' + (s.isHorizontal() ? -wrapperRotate : 0) + 'deg)');
                },
                setTransition: function (duration) {
                    s.slides.transition(duration).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(duration);
                    if (s.params.cube.shadow && !s.isHorizontal()) {
                        s.container.find('.swiper-cube-shadow').transition(duration);
                    }
                }
            },
            coverflow: {
                setTranslate: function () {
                    var transform = s.translate;
                    var center = s.isHorizontal() ? -transform + s.width / 2 : -transform + s.height / 2;
                    var rotate = s.isHorizontal() ? s.params.coverflow.rotate: -s.params.coverflow.rotate;
                    var translate = s.params.coverflow.depth;
                    //Each slide offset from center
                    for (var i = 0, length = s.slides.length; i < length; i++) {
                        var slide = s.slides.eq(i);
                        var slideSize = s.slidesSizesGrid[i];
                        var slideOffset = slide[0].swiperSlideOffset;
                        var offsetMultiplier = (center - slideOffset - slideSize / 2) / slideSize * s.params.coverflow.modifier;

                        var rotateY = s.isHorizontal() ? rotate * offsetMultiplier : 0;
                        var rotateX = s.isHorizontal() ? 0 : rotate * offsetMultiplier;
                        // var rotateZ = 0
                        var translateZ = -translate * Math.abs(offsetMultiplier);

                        var translateY = s.isHorizontal() ? 0 : s.params.coverflow.stretch * (offsetMultiplier);
                        var translateX = s.isHorizontal() ? s.params.coverflow.stretch * (offsetMultiplier) : 0;

                        //Fix for ultra small values
                        if (Math.abs(translateX) < 0.001) translateX = 0;
                        if (Math.abs(translateY) < 0.001) translateY = 0;
                        if (Math.abs(translateZ) < 0.001) translateZ = 0;
                        if (Math.abs(rotateY) < 0.001) rotateY = 0;
                        if (Math.abs(rotateX) < 0.001) rotateX = 0;

                        var slideTransform = 'translate3d(' + translateX + 'px,' + translateY + 'px,' + translateZ + 'px)  rotateX(' + rotateX + 'deg) rotateY(' + rotateY + 'deg)';

                        slide.transform(slideTransform);
                        slide[0].style.zIndex = -Math.abs(Math.round(offsetMultiplier)) + 1;
                        if (s.params.coverflow.slideShadows) {
                            //Set shadows
                            var shadowBefore = s.isHorizontal() ? slide.find('.swiper-slide-shadow-left') : slide.find('.swiper-slide-shadow-top');
                            var shadowAfter = s.isHorizontal() ? slide.find('.swiper-slide-shadow-right') : slide.find('.swiper-slide-shadow-bottom');
                            if (shadowBefore.length === 0) {
                                shadowBefore = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? 'left' : 'top') + '"></div>');
                                slide.append(shadowBefore);
                            }
                            if (shadowAfter.length === 0) {
                                shadowAfter = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? 'right' : 'bottom') + '"></div>');
                                slide.append(shadowAfter);
                            }
                            if (shadowBefore.length) shadowBefore[0].style.opacity = offsetMultiplier > 0 ? offsetMultiplier : 0;
                            if (shadowAfter.length) shadowAfter[0].style.opacity = (-offsetMultiplier) > 0 ? -offsetMultiplier : 0;
                        }
                    }

                    //Set correct perspective for IE10
                    if (s.browser.ie) {
                        var ws = s.wrapper[0].style;
                        ws.perspectiveOrigin = center + 'px 50%';
                    }
                },
                setTransition: function (duration) {
                    s.slides.transition(duration).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(duration);
                }
            }
        };

        /*=========================
          Images Lazy Loading
          ===========================*/
        s.lazy = {
            initialImageLoaded: false,
            loadImageInSlide: function (index, loadInDuplicate) {
                if (typeof index === 'undefined') return;
                if (typeof loadInDuplicate === 'undefined') loadInDuplicate = true;
                if (s.slides.length === 0) return;

                var slide = s.slides.eq(index);
                var img = slide.find('.swiper-lazy:not(.swiper-lazy-loaded):not(.swiper-lazy-loading)');
                if (slide.hasClass('swiper-lazy') && !slide.hasClass('swiper-lazy-loaded') && !slide.hasClass('swiper-lazy-loading')) {
                    img = img.add(slide[0]);
                }
                if (img.length === 0) return;

                img.each(function () {
                    var _img = $(this);
                    _img.addClass('swiper-lazy-loading');
                    var background = _img.attr('data-background');
                    var src = _img.attr('data-src'),
                        srcset = _img.attr('data-srcset');
                    s.loadImage(_img[0], (src || background), srcset, false, function () {
                        if (background) {
                            _img.css('background-image', 'url("' + background + '")');
                            _img.removeAttr('data-background');
                        }
                        else {
                            if (srcset) {
                                _img.attr('srcset', srcset);
                                _img.removeAttr('data-srcset');
                            }
                            if (src) {
                                _img.attr('src', src);
                                _img.removeAttr('data-src');
                            }

                        }

                        _img.addClass('swiper-lazy-loaded').removeClass('swiper-lazy-loading');
                        slide.find('.swiper-lazy-preloader, .preloader').remove();
                        if (s.params.loop && loadInDuplicate) {
                            var slideOriginalIndex = slide.attr('data-swiper-slide-index');
                            if (slide.hasClass(s.params.slideDuplicateClass)) {
                                var originalSlide = s.wrapper.children('[data-swiper-slide-index="' + slideOriginalIndex + '"]:not(.' + s.params.slideDuplicateClass + ')');
                                s.lazy.loadImageInSlide(originalSlide.index(), false);
                            }
                            else {
                                var duplicatedSlide = s.wrapper.children('.' + s.params.slideDuplicateClass + '[data-swiper-slide-index="' + slideOriginalIndex + '"]');
                                s.lazy.loadImageInSlide(duplicatedSlide.index(), false);
                            }
                        }
                        s.emit('onLazyImageReady', s, slide[0], _img[0]);
                    });

                    s.emit('onLazyImageLoad', s, slide[0], _img[0]);
                });

            },
            load: function () {
                var i;
                if (s.params.watchSlidesVisibility) {
                    s.wrapper.children('.' + s.params.slideVisibleClass).each(function () {
                        s.lazy.loadImageInSlide($(this).index());
                    });
                }
                else {
                    if (s.params.slidesPerView > 1) {
                        for (i = s.activeIndex; i < s.activeIndex + s.params.slidesPerView ; i++) {
                            if (s.slides[i]) s.lazy.loadImageInSlide(i);
                        }
                    }
                    else {
                        s.lazy.loadImageInSlide(s.activeIndex);
                    }
                }
                if (s.params.lazyLoadingInPrevNext) {
                    if (s.params.slidesPerView > 1 || (s.params.lazyLoadingInPrevNextAmount && s.params.lazyLoadingInPrevNextAmount > 1)) {
                        console.log('ok ok');
                        var amount = s.params.lazyLoadingInPrevNextAmount;
                        var spv = s.params.slidesPerView;
                        var maxIndex = Math.min(s.activeIndex + spv + Math.max(amount, spv), s.slides.length);
                        var minIndex = Math.max(s.activeIndex - Math.max(spv, amount), 0);
                        // Next Slides
                        for (i = s.activeIndex + s.params.slidesPerView; i < maxIndex; i++) {
                            if (s.slides[i]) s.lazy.loadImageInSlide(i);
                        }
                        // Prev Slides
                        for (i = minIndex; i < s.activeIndex ; i++) {
                            if (s.slides[i]) s.lazy.loadImageInSlide(i);
                        }
                    }
                    else {
                        var nextSlide = s.wrapper.children('.' + s.params.slideNextClass);
                        if (nextSlide.length > 0) s.lazy.loadImageInSlide(nextSlide.index());

                        var prevSlide = s.wrapper.children('.' + s.params.slidePrevClass);
                        if (prevSlide.length > 0) s.lazy.loadImageInSlide(prevSlide.index());
                    }
                }
            },
            onTransitionStart: function () {
                if (s.params.lazyLoading) {
                    if (s.params.lazyLoadingOnTransitionStart || (!s.params.lazyLoadingOnTransitionStart && !s.lazy.initialImageLoaded)) {
                        s.lazy.load();
                    }
                }
            },
            onTransitionEnd: function () {
                if (s.params.lazyLoading && !s.params.lazyLoadingOnTransitionStart) {
                    s.lazy.load();
                }
            }
        };


        /*=========================
          Scrollbar
          ===========================*/
        s.scrollbar = {
            isTouched: false,
            setDragPosition: function (e) {
                var sb = s.scrollbar;
                var x = 0, y = 0;
                var translate;
                var pointerPosition = s.isHorizontal() ?
                    ((e.type === 'touchstart' || e.type === 'touchmove') ? e.targetTouches[0].pageX : e.pageX || e.clientX) :
                    ((e.type === 'touchstart' || e.type === 'touchmove') ? e.targetTouches[0].pageY : e.pageY || e.clientY) ;
                var position = (pointerPosition) - sb.track.offset()[s.isHorizontal() ? 'left' : 'top'] - sb.dragSize / 2;
                var positionMin = -s.minTranslate() * sb.moveDivider;
                var positionMax = -s.maxTranslate() * sb.moveDivider;
                if (position < positionMin) {
                    position = positionMin;
                }
                else if (position > positionMax) {
                    position = positionMax;
                }
                position = -position / sb.moveDivider;
                s.updateProgress(position);
                s.setWrapperTranslate(position, true);
            },
            dragStart: function (e) {
                var sb = s.scrollbar;
                sb.isTouched = true;
                e.preventDefault();
                e.stopPropagation();

                sb.setDragPosition(e);
                clearTimeout(sb.dragTimeout);

                sb.track.transition(0);
                if (s.params.scrollbarHide) {
                    sb.track.css('opacity', 1);
                }
                s.wrapper.transition(100);
                sb.drag.transition(100);
                s.emit('onScrollbarDragStart', s);
            },
            dragMove: function (e) {
                var sb = s.scrollbar;
                if (!sb.isTouched) return;
                if (e.preventDefault) e.preventDefault();
                else e.returnValue = false;
                sb.setDragPosition(e);
                s.wrapper.transition(0);
                sb.track.transition(0);
                sb.drag.transition(0);
                s.emit('onScrollbarDragMove', s);
            },
            dragEnd: function (e) {
                var sb = s.scrollbar;
                if (!sb.isTouched) return;
                sb.isTouched = false;
                if (s.params.scrollbarHide) {
                    clearTimeout(sb.dragTimeout);
                    sb.dragTimeout = setTimeout(function () {
                        sb.track.css('opacity', 0);
                        sb.track.transition(400);
                    }, 1000);

                }
                s.emit('onScrollbarDragEnd', s);
                if (s.params.scrollbarSnapOnRelease) {
                    s.slideReset();
                }
            },
            enableDraggable: function () {
                var sb = s.scrollbar;
                var target = s.support.touch ? sb.track : document;
                $(sb.track).on(s.touchEvents.start, sb.dragStart);
                $(target).on(s.touchEvents.move, sb.dragMove);
                $(target).on(s.touchEvents.end, sb.dragEnd);
            },
            disableDraggable: function () {
                var sb = s.scrollbar;
                var target = s.support.touch ? sb.track : document;
                $(sb.track).off(s.touchEvents.start, sb.dragStart);
                $(target).off(s.touchEvents.move, sb.dragMove);
                $(target).off(s.touchEvents.end, sb.dragEnd);
            },
            set: function () {
                if (!s.params.scrollbar) return;
                var sb = s.scrollbar;
                sb.track = $(s.params.scrollbar);
                if (s.params.uniqueNavElements && typeof s.params.scrollbar === 'string' && sb.track.length > 1 && s.container.find(s.params.scrollbar).length === 1) {
                    sb.track = s.container.find(s.params.scrollbar);
                }
                sb.drag = sb.track.find('.swiper-scrollbar-drag');
                if (sb.drag.length === 0) {
                    sb.drag = $('<div class="swiper-scrollbar-drag"></div>');
                    sb.track.append(sb.drag);
                }
                sb.drag[0].style.width = '';
                sb.drag[0].style.height = '';
                sb.trackSize = s.isHorizontal() ? sb.track[0].offsetWidth : sb.track[0].offsetHeight;

                sb.divider = s.size / s.virtualSize;
                sb.moveDivider = sb.divider * (sb.trackSize / s.size);
                sb.dragSize = sb.trackSize * sb.divider;

                if (s.isHorizontal()) {
                    sb.drag[0].style.width = sb.dragSize + 'px';
                }
                else {
                    sb.drag[0].style.height = sb.dragSize + 'px';
                }

                if (sb.divider >= 1) {
                    sb.track[0].style.display = 'none';
                }
                else {
                    sb.track[0].style.display = '';
                }
                if (s.params.scrollbarHide) {
                    sb.track[0].style.opacity = 0;
                }
            },
            setTranslate: function () {
                if (!s.params.scrollbar) return;
                var diff;
                var sb = s.scrollbar;
                var translate = s.translate || 0;
                var newPos;

                var newSize = sb.dragSize;
                newPos = (sb.trackSize - sb.dragSize) * s.progress;
                if (s.rtl && s.isHorizontal()) {
                    newPos = -newPos;
                    if (newPos > 0) {
                        newSize = sb.dragSize - newPos;
                        newPos = 0;
                    }
                    else if (-newPos + sb.dragSize > sb.trackSize) {
                        newSize = sb.trackSize + newPos;
                    }
                }
                else {
                    if (newPos < 0) {
                        newSize = sb.dragSize + newPos;
                        newPos = 0;
                    }
                    else if (newPos + sb.dragSize > sb.trackSize) {
                        newSize = sb.trackSize - newPos;
                    }
                }
                if (s.isHorizontal()) {
                    if (s.support.transforms3d) {
                        sb.drag.transform('translate3d(' + (newPos) + 'px, 0, 0)');
                    }
                    else {
                        sb.drag.transform('translateX(' + (newPos) + 'px)');
                    }
                    sb.drag[0].style.width = newSize + 'px';
                }
                else {
                    if (s.support.transforms3d) {
                        sb.drag.transform('translate3d(0px, ' + (newPos) + 'px, 0)');
                    }
                    else {
                        sb.drag.transform('translateY(' + (newPos) + 'px)');
                    }
                    sb.drag[0].style.height = newSize + 'px';
                }
                if (s.params.scrollbarHide) {
                    clearTimeout(sb.timeout);
                    sb.track[0].style.opacity = 1;
                    sb.timeout = setTimeout(function () {
                        sb.track[0].style.opacity = 0;
                        sb.track.transition(400);
                    }, 1000);
                }
            },
            setTransition: function (duration) {
                if (!s.params.scrollbar) return;
                s.scrollbar.drag.transition(duration);
            }
        };

        /*=========================
          Controller
          ===========================*/
        s.controller = {
            LinearSpline: function (x, y) {
                this.x = x;
                this.y = y;
                this.lastIndex = x.length - 1;
                // Given an x value (x2), return the expected y2 value:
                // (x1,y1) is the known point before given value,
                // (x3,y3) is the known point after given value.
                var i1, i3;
                var l = this.x.length;

                this.interpolate = function (x2) {
                    if (!x2) return 0;

                    // Get the indexes of x1 and x3 (the array indexes before and after given x2):
                    i3 = binarySearch(this.x, x2);
                    i1 = i3 - 1;

                    // We have our indexes i1 & i3, so we can calculate already:
                    // y2 := ((x2−x1) × (y3−y1)) ÷ (x3−x1) + y1
                    return ((x2 - this.x[i1]) * (this.y[i3] - this.y[i1])) / (this.x[i3] - this.x[i1]) + this.y[i1];
                };

                var binarySearch = (function() {
                    var maxIndex, minIndex, guess;
                    return function(array, val) {
                        minIndex = -1;
                        maxIndex = array.length;
                        while (maxIndex - minIndex > 1)
                            if (array[guess = maxIndex + minIndex >> 1] <= val) {
                                minIndex = guess;
                            } else {
                                maxIndex = guess;
                            }
                        return maxIndex;
                    };
                })();
            },
            //xxx: for now i will just save one spline function to to
            getInterpolateFunction: function(c){
                if(!s.controller.spline) s.controller.spline = s.params.loop ?
                    new s.controller.LinearSpline(s.slidesGrid, c.slidesGrid) :
                    new s.controller.LinearSpline(s.snapGrid, c.snapGrid);
            },
            setTranslate: function (translate, byController) {
               var controlled = s.params.control;
               var multiplier, controlledTranslate;
               function setControlledTranslate(c) {
                    // this will create an Interpolate function based on the snapGrids
                    // x is the Grid of the scrolled scroller and y will be the controlled scroller
                    // it makes sense to create this only once and recall it for the interpolation
                    // the function does a lot of value caching for performance
                    translate = c.rtl && c.params.direction === 'horizontal' ? -s.translate : s.translate;
                    if (s.params.controlBy === 'slide') {
                        s.controller.getInterpolateFunction(c);
                        // i am not sure why the values have to be multiplicated this way, tried to invert the snapGrid
                        // but it did not work out
                        controlledTranslate = -s.controller.spline.interpolate(-translate);
                    }

                    if(!controlledTranslate || s.params.controlBy === 'container'){
                        multiplier = (c.maxTranslate() - c.minTranslate()) / (s.maxTranslate() - s.minTranslate());
                        controlledTranslate = (translate - s.minTranslate()) * multiplier + c.minTranslate();
                    }

                    if (s.params.controlInverse) {
                        controlledTranslate = c.maxTranslate() - controlledTranslate;
                    }
                    c.updateProgress(controlledTranslate);
                    c.setWrapperTranslate(controlledTranslate, false, s);
                    c.updateActiveIndex();
               }
               if (s.isArray(controlled)) {
                   for (var i = 0; i < controlled.length; i++) {
                       if (controlled[i] !== byController && controlled[i] instanceof Swiper) {
                           setControlledTranslate(controlled[i]);
                       }
                   }
               }
               else if (controlled instanceof Swiper && byController !== controlled) {

                   setControlledTranslate(controlled);
               }
            },
            setTransition: function (duration, byController) {
                var controlled = s.params.control;
                var i;
                function setControlledTransition(c) {
                    c.setWrapperTransition(duration, s);
                    if (duration !== 0) {
                        c.onTransitionStart();
                        c.wrapper.transitionEnd(function(){
                            if (!controlled) return;
                            if (c.params.loop && s.params.controlBy === 'slide') {
                                c.fixLoop();
                            }
                            c.onTransitionEnd();

                        });
                    }
                }
                if (s.isArray(controlled)) {
                    for (i = 0; i < controlled.length; i++) {
                        if (controlled[i] !== byController && controlled[i] instanceof Swiper) {
                            setControlledTransition(controlled[i]);
                        }
                    }
                }
                else if (controlled instanceof Swiper && byController !== controlled) {
                    setControlledTransition(controlled);
                }
            }
        };

        /*=========================
          Hash Navigation
          ===========================*/
        s.hashnav = {
            init: function () {
                if (!s.params.hashnav) return;
                s.hashnav.initialized = true;
                var hash = document.location.hash.replace('#', '');
                if (!hash) return;
                var speed = 0;
                for (var i = 0, length = s.slides.length; i < length; i++) {
                    var slide = s.slides.eq(i);
                    var slideHash = slide.attr('data-hash');
                    if (slideHash === hash && !slide.hasClass(s.params.slideDuplicateClass)) {
                        var index = slide.index();
                        s.slideTo(index, speed, s.params.runCallbacksOnInit, true);
                    }
                }
            },
            setHash: function () {
                if (!s.hashnav.initialized || !s.params.hashnav) return;
                document.location.hash = s.slides.eq(s.activeIndex).attr('data-hash') || '';
            }
        };

        /*=========================
          Keyboard Control
          ===========================*/
        function handleKeyboard(e) {
            if (e.originalEvent) e = e.originalEvent; //jquery fix
            var kc = e.keyCode || e.charCode;
            // Directions locks
            if (!s.params.allowSwipeToNext && (s.isHorizontal() && kc === 39 || !s.isHorizontal() && kc === 40)) {
                return false;
            }
            if (!s.params.allowSwipeToPrev && (s.isHorizontal() && kc === 37 || !s.isHorizontal() && kc === 38)) {
                return false;
            }
            if (e.shiftKey || e.altKey || e.ctrlKey || e.metaKey) {
                return;
            }
            if (document.activeElement && document.activeElement.nodeName && (document.activeElement.nodeName.toLowerCase() === 'input' || document.activeElement.nodeName.toLowerCase() === 'textarea')) {
                return;
            }
            if (kc === 37 || kc === 39 || kc === 38 || kc === 40) {
                var inView = false;
                //Check that swiper should be inside of visible area of window
                if (s.container.parents('.swiper-slide').length > 0 && s.container.parents('.swiper-slide-active').length === 0) {
                    return;
                }
                var windowScroll = {
                    left: window.pageXOffset,
                    top: window.pageYOffset
                };
                var windowWidth = window.innerWidth;
                var windowHeight = window.innerHeight;
                var swiperOffset = s.container.offset();
                if (s.rtl) swiperOffset.left = swiperOffset.left - s.container[0].scrollLeft;
                var swiperCoord = [
                    [swiperOffset.left, swiperOffset.top],
                    [swiperOffset.left + s.width, swiperOffset.top],
                    [swiperOffset.left, swiperOffset.top + s.height],
                    [swiperOffset.left + s.width, swiperOffset.top + s.height]
                ];
                for (var i = 0; i < swiperCoord.length; i++) {
                    var point = swiperCoord[i];
                    if (
                        point[0] >= windowScroll.left && point[0] <= windowScroll.left + windowWidth &&
                        point[1] >= windowScroll.top && point[1] <= windowScroll.top + windowHeight
                    ) {
                        inView = true;
                    }

                }
                if (!inView) return;
            }
            if (s.isHorizontal()) {
                if (kc === 37 || kc === 39) {
                    if (e.preventDefault) e.preventDefault();
                    else e.returnValue = false;
                }
                if ((kc === 39 && !s.rtl) || (kc === 37 && s.rtl)) s.slideNext();
                if ((kc === 37 && !s.rtl) || (kc === 39 && s.rtl)) s.slidePrev();
            }
            else {
                if (kc === 38 || kc === 40) {
                    if (e.preventDefault) e.preventDefault();
                    else e.returnValue = false;
                }
                if (kc === 40) s.slideNext();
                if (kc === 38) s.slidePrev();
            }
        }
        s.disableKeyboardControl = function () {
            s.params.keyboardControl = false;
            $(document).off('keydown', handleKeyboard);
        };
        s.enableKeyboardControl = function () {
            s.params.keyboardControl = true;
            $(document).on('keydown', handleKeyboard);
        };


        /*=========================
          Mousewheel Control
          ===========================*/
        s.mousewheel = {
            event: false,
            lastScrollTime: (new window.Date()).getTime()
        };
        if (s.params.mousewheelControl) {
            try {
                new window.WheelEvent('wheel');
                s.mousewheel.event = 'wheel';
            } catch (e) {
                if (window.WheelEvent || (s.container[0] && 'wheel' in s.container[0])) {
                    s.mousewheel.event = 'wheel';
                }
            }
            if (!s.mousewheel.event && window.WheelEvent) {

            }
            if (!s.mousewheel.event && document.onmousewheel !== undefined) {
                s.mousewheel.event = 'mousewheel';
            }
            if (!s.mousewheel.event) {
                s.mousewheel.event = 'DOMMouseScroll';
            }
        }
        function handleMousewheel(e) {
            if (e.originalEvent) e = e.originalEvent; //jquery fix
            var we = s.mousewheel.event;
            var delta = 0;
            var rtlFactor = s.rtl ? -1 : 1;

            //WebKits
            if (we === 'mousewheel') {
                if (s.params.mousewheelForceToAxis) {
                    if (s.isHorizontal()) {
                        if (Math.abs(e.wheelDeltaX) > Math.abs(e.wheelDeltaY)) delta = e.wheelDeltaX * rtlFactor;
                        else return;
                    }
                    else {
                        if (Math.abs(e.wheelDeltaY) > Math.abs(e.wheelDeltaX)) delta = e.wheelDeltaY;
                        else return;
                    }
                }
                else {
                    delta = Math.abs(e.wheelDeltaX) > Math.abs(e.wheelDeltaY) ? - e.wheelDeltaX * rtlFactor : - e.wheelDeltaY;
                }
            }
            //Old FireFox
            else if (we === 'DOMMouseScroll') delta = -e.detail;
            //New FireFox
            else if (we === 'wheel') {
                if (s.params.mousewheelForceToAxis) {
                    if (s.isHorizontal()) {
                        if (Math.abs(e.deltaX) > Math.abs(e.deltaY)) delta = -e.deltaX * rtlFactor;
                        else return;
                    }
                    else {
                        if (Math.abs(e.deltaY) > Math.abs(e.deltaX)) delta = -e.deltaY;
                        else return;
                    }
                }
                else {
                    delta = Math.abs(e.deltaX) > Math.abs(e.deltaY) ? - e.deltaX * rtlFactor : - e.deltaY;
                }
            }
            if (delta === 0) return;

            if (s.params.mousewheelInvert) delta = -delta;

            if (!s.params.freeMode) {
                if ((new window.Date()).getTime() - s.mousewheel.lastScrollTime > 60) {
                    if (delta < 0) {
                        if ((!s.isEnd || s.params.loop) && !s.animating) s.slideNext();
                        else if (s.params.mousewheelReleaseOnEdges) return true;
                    }
                    else {
                        if ((!s.isBeginning || s.params.loop) && !s.animating) s.slidePrev();
                        else if (s.params.mousewheelReleaseOnEdges) return true;
                    }
                }
                s.mousewheel.lastScrollTime = (new window.Date()).getTime();

            }
            else {
                //Freemode or scrollContainer:
                var position = s.getWrapperTranslate() + delta * s.params.mousewheelSensitivity;
                var wasBeginning = s.isBeginning,
                    wasEnd = s.isEnd;

                if (position >= s.minTranslate()) position = s.minTranslate();
                if (position <= s.maxTranslate()) position = s.maxTranslate();

                s.setWrapperTransition(0);
                s.setWrapperTranslate(position);
                s.updateProgress();
                s.updateActiveIndex();

                if (!wasBeginning && s.isBeginning || !wasEnd && s.isEnd) {
                    s.updateClasses();
                }

                if (s.params.freeModeSticky) {
                    clearTimeout(s.mousewheel.timeout);
                    s.mousewheel.timeout = setTimeout(function () {
                        s.slideReset();
                    }, 300);
                }
                else {
                    if (s.params.lazyLoading && s.lazy) {
                        s.lazy.load();
                    }
                }

                // Return page scroll on edge positions
                if (position === 0 || position === s.maxTranslate()) return;
            }
            if (s.params.autoplay) s.stopAutoplay();

            if (e.preventDefault) e.preventDefault();
            else e.returnValue = false;
            return false;
        }
        s.disableMousewheelControl = function () {
            if (!s.mousewheel.event) return false;
            s.container.off(s.mousewheel.event, handleMousewheel);
            return true;
        };

        s.enableMousewheelControl = function () {
            if (!s.mousewheel.event) return false;
            s.container.on(s.mousewheel.event, handleMousewheel);
            return true;
        };


        /*=========================
          Parallax
          ===========================*/
        function setParallaxTransform(el, progress) {
            el = $(el);
            var p, pX, pY;
            var rtlFactor = s.rtl ? -1 : 1;

            p = el.attr('data-swiper-parallax') || '0';
            pX = el.attr('data-swiper-parallax-x');
            pY = el.attr('data-swiper-parallax-y');
            if (pX || pY) {
                pX = pX || '0';
                pY = pY || '0';
            }
            else {
                if (s.isHorizontal()) {
                    pX = p;
                    pY = '0';
                }
                else {
                    pY = p;
                    pX = '0';
                }
            }

            if ((pX).indexOf('%') >= 0) {
                pX = parseInt(pX, 10) * progress * rtlFactor + '%';
            }
            else {
                pX = pX * progress * rtlFactor + 'px' ;
            }
            if ((pY).indexOf('%') >= 0) {
                pY = parseInt(pY, 10) * progress + '%';
            }
            else {
                pY = pY * progress + 'px' ;
            }

            el.transform('translate3d(' + pX + ', ' + pY + ',0px)');
        }
        s.parallax = {
            setTranslate: function () {
                s.container.children('[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]').each(function(){
                    setParallaxTransform(this, s.progress);

                });
                s.slides.each(function () {
                    var slide = $(this);
                    slide.find('[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]').each(function () {
                        var progress = Math.min(Math.max(slide[0].progress, -1), 1);
                        setParallaxTransform(this, progress);
                    });
                });
            },
            setTransition: function (duration) {
                if (typeof duration === 'undefined') duration = s.params.speed;
                s.container.find('[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]').each(function(){
                    var el = $(this);
                    var parallaxDuration = parseInt(el.attr('data-swiper-parallax-duration'), 10) || duration;
                    if (duration === 0) parallaxDuration = 0;
                    el.transition(parallaxDuration);
                });
            }
        };


        /*=========================
          Plugins API. Collect all and init all plugins
          ===========================*/
        s._plugins = [];
        for (var plugin in s.plugins) {
            var p = s.plugins[plugin](s, s.params[plugin]);
            if (p) s._plugins.push(p);
        }
        // Method to call all plugins event/method
        s.callPlugins = function (eventName) {
            for (var i = 0; i < s._plugins.length; i++) {
                if (eventName in s._plugins[i]) {
                    s._plugins[i][eventName](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                }
            }
        };

        /*=========================
          Events/Callbacks/Plugins Emitter
          ===========================*/
        function normalizeEventName (eventName) {
            if (eventName.indexOf('on') !== 0) {
                if (eventName[0] !== eventName[0].toUpperCase()) {
                    eventName = 'on' + eventName[0].toUpperCase() + eventName.substring(1);
                }
                else {
                    eventName = 'on' + eventName;
                }
            }
            return eventName;
        }
        s.emitterEventListeners = {

        };
        s.emit = function (eventName) {
            // Trigger callbacks
            if (s.params[eventName]) {
                s.params[eventName](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
            }
            var i;
            // Trigger events
            if (s.emitterEventListeners[eventName]) {
                for (i = 0; i < s.emitterEventListeners[eventName].length; i++) {
                    s.emitterEventListeners[eventName][i](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                }
            }
            // Trigger plugins
            if (s.callPlugins) s.callPlugins(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
        };
        s.on = function (eventName, handler) {
            eventName = normalizeEventName(eventName);
            if (!s.emitterEventListeners[eventName]) s.emitterEventListeners[eventName] = [];
            s.emitterEventListeners[eventName].push(handler);
            return s;
        };
        s.off = function (eventName, handler) {
            var i;
            eventName = normalizeEventName(eventName);
            if (typeof handler === 'undefined') {
                // Remove all handlers for such event
                s.emitterEventListeners[eventName] = [];
                return s;
            }
            if (!s.emitterEventListeners[eventName] || s.emitterEventListeners[eventName].length === 0) return;
            for (i = 0; i < s.emitterEventListeners[eventName].length; i++) {
                if(s.emitterEventListeners[eventName][i] === handler) s.emitterEventListeners[eventName].splice(i, 1);
            }
            return s;
        };
        s.once = function (eventName, handler) {
            eventName = normalizeEventName(eventName);
            var _handler = function () {
                handler(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
                s.off(eventName, _handler);
            };
            s.on(eventName, _handler);
            return s;
        };

        // Accessibility tools
        s.a11y = {
            makeFocusable: function ($el) {
                $el.attr('tabIndex', '0');
                return $el;
            },
            addRole: function ($el, role) {
                $el.attr('role', role);
                return $el;
            },

            addLabel: function ($el, label) {
                $el.attr('aria-label', label);
                return $el;
            },

            disable: function ($el) {
                $el.attr('aria-disabled', true);
                return $el;
            },

            enable: function ($el) {
                $el.attr('aria-disabled', false);
                return $el;
            },

            onEnterKey: function (event) {
                if (event.keyCode !== 13) return;
                if ($(event.target).is(s.params.nextButton)) {
                    s.onClickNext(event);
                    if (s.isEnd) {
                        s.a11y.notify(s.params.lastSlideMessage);
                    }
                    else {
                        s.a11y.notify(s.params.nextSlideMessage);
                    }
                }
                else if ($(event.target).is(s.params.prevButton)) {
                    s.onClickPrev(event);
                    if (s.isBeginning) {
                        s.a11y.notify(s.params.firstSlideMessage);
                    }
                    else {
                        s.a11y.notify(s.params.prevSlideMessage);
                    }
                }
                if ($(event.target).is('.' + s.params.bulletClass)) {
                    $(event.target)[0].click();
                }
            },

            liveRegion: $('<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>'),

            notify: function (message) {
                var notification = s.a11y.liveRegion;
                if (notification.length === 0) return;
                notification.html('');
                notification.html(message);
            },
            init: function () {
                // Setup accessibility
                if (s.params.nextButton && s.nextButton && s.nextButton.length > 0) {
                    s.a11y.makeFocusable(s.nextButton);
                    s.a11y.addRole(s.nextButton, 'button');
                    s.a11y.addLabel(s.nextButton, s.params.nextSlideMessage);
                }
                if (s.params.prevButton && s.prevButton && s.prevButton.length > 0) {
                    s.a11y.makeFocusable(s.prevButton);
                    s.a11y.addRole(s.prevButton, 'button');
                    s.a11y.addLabel(s.prevButton, s.params.prevSlideMessage);
                }

                $(s.container).append(s.a11y.liveRegion);
            },
            initPagination: function () {
                if (s.params.pagination && s.params.paginationClickable && s.bullets && s.bullets.length) {
                    s.bullets.each(function () {
                        var bullet = $(this);
                        s.a11y.makeFocusable(bullet);
                        s.a11y.addRole(bullet, 'button');
                        s.a11y.addLabel(bullet, s.params.paginationBulletMessage.replace(/{{index}}/, bullet.index() + 1));
                    });
                }
            },
            destroy: function () {
                if (s.a11y.liveRegion && s.a11y.liveRegion.length > 0) s.a11y.liveRegion.remove();
            }
        };


        /*=========================
          Init/Destroy
          ===========================*/
        s.init = function () {
            if (s.params.loop) s.createLoop();
            s.updateContainerSize();
            s.updateSlidesSize();
            s.updatePagination();
            if (s.params.scrollbar && s.scrollbar) {
                s.scrollbar.set();
                if (s.params.scrollbarDraggable) {
                    s.scrollbar.enableDraggable();
                }
            }
            if (s.params.effect !== 'slide' && s.effects[s.params.effect]) {
                if (!s.params.loop) s.updateProgress();
                s.effects[s.params.effect].setTranslate();
            }
            if (s.params.loop) {
                s.slideTo(s.params.initialSlide + s.loopedSlides, 0, s.params.runCallbacksOnInit);
            }
            else {
                s.slideTo(s.params.initialSlide, 0, s.params.runCallbacksOnInit);
                if (s.params.initialSlide === 0) {
                    if (s.parallax && s.params.parallax) s.parallax.setTranslate();
                    if (s.lazy && s.params.lazyLoading) {
                        s.lazy.load();
                        s.lazy.initialImageLoaded = true;
                    }
                }
            }
            s.attachEvents();
            if (s.params.observer && s.support.observer) {
                s.initObservers();
            }
            if (s.params.preloadImages && !s.params.lazyLoading) {
                s.preloadImages();
            }
            if (s.params.autoplay) {
                s.startAutoplay();
            }
            if (s.params.keyboardControl) {
                if (s.enableKeyboardControl) s.enableKeyboardControl();
            }
            if (s.params.mousewheelControl) {
                if (s.enableMousewheelControl) s.enableMousewheelControl();
            }
            if (s.params.hashnav) {
                if (s.hashnav) s.hashnav.init();
            }
            if (s.params.a11y && s.a11y) s.a11y.init();
            s.emit('onInit', s);
        };

        // Cleanup dynamic styles
        s.cleanupStyles = function () {
            // Container
            s.container.removeClass(s.classNames.join(' ')).removeAttr('style');

            // Wrapper
            s.wrapper.removeAttr('style');

            // Slides
            if (s.slides && s.slides.length) {
                s.slides
                    .removeClass([
                      s.params.slideVisibleClass,
                      s.params.slideActiveClass,
                      s.params.slideNextClass,
                      s.params.slidePrevClass
                    ].join(' '))
                    .removeAttr('style')
                    .removeAttr('data-swiper-column')
                    .removeAttr('data-swiper-row');
            }

            // Pagination/Bullets
            if (s.paginationContainer && s.paginationContainer.length) {
                s.paginationContainer.removeClass(s.params.paginationHiddenClass);
            }
            if (s.bullets && s.bullets.length) {
                s.bullets.removeClass(s.params.bulletActiveClass);
            }

            // Buttons
            if (s.params.prevButton) $(s.params.prevButton).removeClass(s.params.buttonDisabledClass);
            if (s.params.nextButton) $(s.params.nextButton).removeClass(s.params.buttonDisabledClass);

            // Scrollbar
            if (s.params.scrollbar && s.scrollbar) {
                if (s.scrollbar.track && s.scrollbar.track.length) s.scrollbar.track.removeAttr('style');
                if (s.scrollbar.drag && s.scrollbar.drag.length) s.scrollbar.drag.removeAttr('style');
            }
        };

        // Destroy
        s.destroy = function (deleteInstance, cleanupStyles) {
            // Detach evebts
            s.detachEvents();
            // Stop autoplay
            s.stopAutoplay();
            // Disable draggable
            if (s.params.scrollbar && s.scrollbar) {
                if (s.params.scrollbarDraggable) {
                    s.scrollbar.disableDraggable();
                }
            }
            // Destroy loop
            if (s.params.loop) {
                s.destroyLoop();
            }
            // Cleanup styles
            if (cleanupStyles) {
                s.cleanupStyles();
            }
            // Disconnect observer
            s.disconnectObservers();
            // Disable keyboard/mousewheel
            if (s.params.keyboardControl) {
                if (s.disableKeyboardControl) s.disableKeyboardControl();
            }
            if (s.params.mousewheelControl) {
                if (s.disableMousewheelControl) s.disableMousewheelControl();
            }
            // Disable a11y
            if (s.params.a11y && s.a11y) s.a11y.destroy();
            // Destroy callback
            s.emit('onDestroy');
            // Delete instance
            if (deleteInstance !== false) s = null;
        };

        s.init();



        // Return swiper instance
        return s;
    };


    /*==================================================
        Prototype
    ====================================================*/
    Swiper.prototype = {
        isSafari: (function () {
            var ua = navigator.userAgent.toLowerCase();
            return (ua.indexOf('safari') >= 0 && ua.indexOf('chrome') < 0 && ua.indexOf('android') < 0);
        })(),
        isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent),
        isArray: function (arr) {
            return Object.prototype.toString.apply(arr) === '[object Array]';
        },
        /*==================================================
        Browser
        ====================================================*/
        browser: {
            ie: window.navigator.pointerEnabled || window.navigator.msPointerEnabled,
            ieTouch: (window.navigator.msPointerEnabled && window.navigator.msMaxTouchPoints > 1) || (window.navigator.pointerEnabled && window.navigator.maxTouchPoints > 1)
        },
        /*==================================================
        Devices
        ====================================================*/
        device: (function () {
            var ua = navigator.userAgent;
            var android = ua.match(/(Android);?[\s\/]+([\d.]+)?/);
            var ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
            var ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/);
            var iphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/);
            return {
                ios: ipad || iphone || ipod,
                android: android
            };
        })(),
        /*==================================================
        Feature Detection
        ====================================================*/
        support: {
            touch : (window.Modernizr && Modernizr.touch === true) || (function () {
                return !!(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch);
            })(),

            transforms3d : (window.Modernizr && Modernizr.csstransforms3d === true) || (function () {
                var div = document.createElement('div').style;
                return ('webkitPerspective' in div || 'MozPerspective' in div || 'OPerspective' in div || 'MsPerspective' in div || 'perspective' in div);
            })(),

            flexbox: (function () {
                var div = document.createElement('div').style;
                var styles = ('alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient').split(' ');
                for (var i = 0; i < styles.length; i++) {
                    if (styles[i] in div) return true;
                }
            })(),

            observer: (function () {
                return ('MutationObserver' in window || 'WebkitMutationObserver' in window);
            })()
        },
        /*==================================================
        Plugins
        ====================================================*/
        plugins: {}
    };


    /*===========================
     Get Dom libraries
     ===========================*/
    var swiperDomPlugins = ['jQuery', 'Zepto', 'Dom7'];
    for (var i = 0; i < swiperDomPlugins.length; i++) {
    	if (window[swiperDomPlugins[i]]) {
    		addLibraryPlugin(window[swiperDomPlugins[i]]);
    	}
    }
    // Required DOM Plugins
    var domLib;
    if (typeof Dom7 === 'undefined') {
    	domLib = window.Dom7 || window.Zepto || window.jQuery;
    }
    else {
    	domLib = Dom7;
    }

    /*===========================
    Add .swiper plugin from Dom libraries
    ===========================*/
    function addLibraryPlugin(lib) {
        lib.fn.swiper = function (params) {
            var firstInstance;
            lib(this).each(function () {
                var s = new Swiper(this, params);
                if (!firstInstance) firstInstance = s;
            });
            return firstInstance;
        };
    }

    if (domLib) {
        if (!('transitionEnd' in domLib.fn)) {
            domLib.fn.transitionEnd = function (callback) {
                var events = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'],
                    i, j, dom = this;
                function fireCallBack(e) {
                    /*jshint validthis:true */
                    if (e.target !== this) return;
                    callback.call(this, e);
                    for (i = 0; i < events.length; i++) {
                        dom.off(events[i], fireCallBack);
                    }
                }
                if (callback) {
                    for (i = 0; i < events.length; i++) {
                        dom.on(events[i], fireCallBack);
                    }
                }
                return this;
            };
        }
        if (!('transform' in domLib.fn)) {
            domLib.fn.transform = function (transform) {
                for (var i = 0; i < this.length; i++) {
                    var elStyle = this[i].style;
                    elStyle.webkitTransform = elStyle.MsTransform = elStyle.msTransform = elStyle.MozTransform = elStyle.OTransform = elStyle.transform = transform;
                }
                return this;
            };
        }
        if (!('transition' in domLib.fn)) {
            domLib.fn.transition = function (duration) {
                if (typeof duration !== 'string') {
                    duration = duration + 'ms';
                }
                for (var i = 0; i < this.length; i++) {
                    var elStyle = this[i].style;
                    elStyle.webkitTransitionDuration = elStyle.MsTransitionDuration = elStyle.msTransitionDuration = elStyle.MozTransitionDuration = elStyle.OTransitionDuration = elStyle.transitionDuration = duration;
                }
                return this;
            };
        }
    }

    window.Swiper = Swiper;
})();
/*===========================
Swiper AMD Export
===========================*/
if (typeof(module) !== 'undefined')
{
    module.exports = window.Swiper;
}
else if (typeof define === 'function' && define.amd) {
    define([], function () {
        'use strict';
        return window.Swiper;
    });
}
//# sourceMappingURL=maps/swiper.jquery.js.map

;(function () {
	'use strict';

	/**
	 * @preserve FastClick: polyfill to remove click delays on browsers with touch UIs.
	 *
	 * @codingstandard ftlabs-jsv2
	 * @copyright The Financial Times Limited [All Rights Reserved]
	 * @license MIT License (see LICENSE.txt)
	 */

	/*jslint browser:true, node:true*/
	/*global define, Event, Node*/


	/**
	 * Instantiate fast-clicking listeners on the specified layer.
	 *
	 * @constructor
	 * @param {Element} layer The layer to listen on
	 * @param {Object} [options={}] The options to override the defaults
	 */
	function FastClick(layer, options) {
		var oldOnClick;

		options = options || {};

		/**
		 * Whether a click is currently being tracked.
		 *
		 * @type boolean
		 */
		this.trackingClick = false;


		/**
		 * Timestamp for when click tracking started.
		 *
		 * @type number
		 */
		this.trackingClickStart = 0;


		/**
		 * The element being tracked for a click.
		 *
		 * @type EventTarget
		 */
		this.targetElement = null;


		/**
		 * X-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartX = 0;


		/**
		 * Y-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartY = 0;


		/**
		 * ID of the last touch, retrieved from Touch.identifier.
		 *
		 * @type number
		 */
		this.lastTouchIdentifier = 0;


		/**
		 * Touchmove boundary, beyond which a click will be cancelled.
		 *
		 * @type number
		 */
		this.touchBoundary = options.touchBoundary || 10;


		/**
		 * The FastClick layer.
		 *
		 * @type Element
		 */
		this.layer = layer;

		/**
		 * The minimum time between tap(touchstart and touchend) events
		 *
		 * @type number
		 */
		this.tapDelay = options.tapDelay || 200;

		/**
		 * The maximum time for a tap
		 *
		 * @type number
		 */
		this.tapTimeout = options.tapTimeout || 700;

		if (FastClick.notNeeded(layer)) {
			return;
		}

		// Some old versions of Android don't have Function.prototype.bind
		function bind(method, context) {
			return function() { return method.apply(context, arguments); };
		}


		var methods = ['onMouse', 'onClick', 'onTouchStart', 'onTouchMove', 'onTouchEnd', 'onTouchCancel'];
		var context = this;
		for (var i = 0, l = methods.length; i < l; i++) {
			context[methods[i]] = bind(context[methods[i]], context);
		}

		// Set up event handlers as required
		if (deviceIsAndroid) {
			layer.addEventListener('mouseover', this.onMouse, true);
			layer.addEventListener('mousedown', this.onMouse, true);
			layer.addEventListener('mouseup', this.onMouse, true);
		}

		layer.addEventListener('click', this.onClick, true);
		layer.addEventListener('touchstart', this.onTouchStart, false);
		layer.addEventListener('touchmove', this.onTouchMove, false);
		layer.addEventListener('touchend', this.onTouchEnd, false);
		layer.addEventListener('touchcancel', this.onTouchCancel, false);

		// Hack is required for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
		// which is how FastClick normally stops click events bubbling to callbacks registered on the FastClick
		// layer when they are cancelled.
		if (!Event.prototype.stopImmediatePropagation) {
			layer.removeEventListener = function(type, callback, capture) {
				var rmv = Node.prototype.removeEventListener;
				if (type === 'click') {
					rmv.call(layer, type, callback.hijacked || callback, capture);
				} else {
					rmv.call(layer, type, callback, capture);
				}
			};

			layer.addEventListener = function(type, callback, capture) {
				var adv = Node.prototype.addEventListener;
				if (type === 'click') {
					adv.call(layer, type, callback.hijacked || (callback.hijacked = function(event) {
						if (!event.propagationStopped) {
							callback(event);
						}
					}), capture);
				} else {
					adv.call(layer, type, callback, capture);
				}
			};
		}

		// If a handler is already declared in the element's onclick attribute, it will be fired before
		// FastClick's onClick handler. Fix this by pulling out the user-defined handler function and
		// adding it as listener.
		if (typeof layer.onclick === 'function') {

			// Android browser on at least 3.2 requires a new reference to the function in layer.onclick
			// - the old one won't work if passed to addEventListener directly.
			oldOnClick = layer.onclick;
			layer.addEventListener('click', function(event) {
				oldOnClick(event);
			}, false);
			layer.onclick = null;
		}
	}

	/**
	* Windows Phone 8.1 fakes user agent string to look like Android and iPhone.
	*
	* @type boolean
	*/
	var deviceIsWindowsPhone = navigator.userAgent.indexOf("Windows Phone") >= 0;

	/**
	 * Android requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsAndroid = navigator.userAgent.indexOf('Android') > 0 && !deviceIsWindowsPhone;


	/**
	 * iOS requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent) && !deviceIsWindowsPhone;


	/**
	 * iOS 4 requires an exception for select elements.
	 *
	 * @type boolean
	 */
	var deviceIsIOS4 = deviceIsIOS && (/OS 4_\d(_\d)?/).test(navigator.userAgent);


	/**
	 * iOS 6.0-7.* requires the target element to be manually derived
	 *
	 * @type boolean
	 */
	var deviceIsIOSWithBadTarget = deviceIsIOS && (/OS [6-7]_\d/).test(navigator.userAgent);

	/**
	 * BlackBerry requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsBlackBerry10 = navigator.userAgent.indexOf('BB10') > 0;

	/**
	 * Determine whether a given element requires a native click.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element needs a native click
	 */
	FastClick.prototype.needsClick = function(target) {
		switch (target.nodeName.toLowerCase()) {

		// Don't send a synthetic click to disabled inputs (issue #62)
		case 'button':
		case 'select':
		case 'textarea':
			if (target.disabled) {
				return true;
			}

			break;
		case 'input':

			// File inputs need real clicks on iOS 6 due to a browser bug (issue #68)
			if ((deviceIsIOS && target.type === 'file') || target.disabled) {
				return true;
			}

			break;
		case 'label':
		case 'iframe': // iOS8 homescreen apps can prevent events bubbling into frames
		case 'video':
			return true;
		}

		return (/\bneedsclick\b/).test(target.className);
	};


	/**
	 * Determine whether a given element requires a call to focus to simulate click into element.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element requires a call to focus to simulate native click.
	 */
	FastClick.prototype.needsFocus = function(target) {
		switch (target.nodeName.toLowerCase()) {
		case 'textarea':
			return true;
		case 'select':
			return !deviceIsAndroid;
		case 'input':
			switch (target.type) {
			case 'button':
			case 'checkbox':
			case 'file':
			case 'image':
			case 'radio':
			case 'submit':
				return false;
			}

			// No point in attempting to focus disabled inputs
			return !target.disabled && !target.readOnly;
		default:
			return (/\bneedsfocus\b/).test(target.className);
		}
	};


	/**
	 * Send a click event to the specified element.
	 *
	 * @param {EventTarget|Element} targetElement
	 * @param {Event} event
	 */
	FastClick.prototype.sendClick = function(targetElement, event) {
		var clickEvent, touch;

		// On some Android devices activeElement needs to be blurred otherwise the synthetic click will have no effect (#24)
		if (document.activeElement && document.activeElement !== targetElement) {
			document.activeElement.blur();
		}

		touch = event.changedTouches[0];

		// Synthesise a click event, with an extra attribute so it can be tracked
		clickEvent = document.createEvent('MouseEvents');
		clickEvent.initMouseEvent(this.determineEventType(targetElement), true, true, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, false, false, false, false, 0, null);
		clickEvent.forwardedTouchEvent = true;
		targetElement.dispatchEvent(clickEvent);
	};

	FastClick.prototype.determineEventType = function(targetElement) {

		//Issue #159: Android Chrome Select Box does not open with a synthetic click event
		if (deviceIsAndroid && targetElement.tagName.toLowerCase() === 'select') {
			return 'mousedown';
		}

		return 'click';
	};


	/**
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.focus = function(targetElement) {
		var length;

		// Issue #160: on iOS 7, some input elements (e.g. date datetime month) throw a vague TypeError on setSelectionRange. These elements don't have an integer value for the selectionStart and selectionEnd properties, but unfortunately that can't be used for detection because accessing the properties also throws a TypeError. Just check the type instead. Filed as Apple bug #15122724.
		if (deviceIsIOS && targetElement.setSelectionRange && targetElement.type.indexOf('date') !== 0 && targetElement.type !== 'time' && targetElement.type !== 'month') {
			length = targetElement.value.length;
			targetElement.setSelectionRange(length, length);
		} else {
			targetElement.focus();
		}
	};


	/**
	 * Check whether the given target element is a child of a scrollable layer and if so, set a flag on it.
	 *
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.updateScrollParent = function(targetElement) {
		var scrollParent, parentElement;

		scrollParent = targetElement.fastClickScrollParent;

		// Attempt to discover whether the target element is contained within a scrollable layer. Re-check if the
		// target element was moved to another parent.
		if (!scrollParent || !scrollParent.contains(targetElement)) {
			parentElement = targetElement;
			do {
				if (parentElement.scrollHeight > parentElement.offsetHeight) {
					scrollParent = parentElement;
					targetElement.fastClickScrollParent = parentElement;
					break;
				}

				parentElement = parentElement.parentElement;
			} while (parentElement);
		}

		// Always update the scroll top tracker if possible.
		if (scrollParent) {
			scrollParent.fastClickLastScrollTop = scrollParent.scrollTop;
		}
	};


	/**
	 * @param {EventTarget} targetElement
	 * @returns {Element|EventTarget}
	 */
	FastClick.prototype.getTargetElementFromEventTarget = function(eventTarget) {

		// On some older browsers (notably Safari on iOS 4.1 - see issue #56) the event target may be a text node.
		if (eventTarget.nodeType === Node.TEXT_NODE) {
			return eventTarget.parentNode;
		}

		return eventTarget;
	};


	/**
	 * On touch start, record the position and scroll offset.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchStart = function(event) {
		var targetElement, touch, selection;

		// Ignore multiple touches, otherwise pinch-to-zoom is prevented if both fingers are on the FastClick element (issue #111).
		if (event.targetTouches.length > 1) {
			return true;
		}

		targetElement = this.getTargetElementFromEventTarget(event.target);
		touch = event.targetTouches[0];

		if (deviceIsIOS) {

			// Only trusted events will deselect text on iOS (issue #49)
			selection = window.getSelection();
			if (selection.rangeCount && !selection.isCollapsed) {
				return true;
			}

			if (!deviceIsIOS4) {

				// Weird things happen on iOS when an alert or confirm dialog is opened from a click event callback (issue #23):
				// when the user next taps anywhere else on the page, new touchstart and touchend events are dispatched
				// with the same identifier as the touch event that previously triggered the click that triggered the alert.
				// Sadly, there is an issue on iOS 4 that causes some normal touch events to have the same identifier as an
				// immediately preceeding touch event (issue #52), so this fix is unavailable on that platform.
				// Issue 120: touch.identifier is 0 when Chrome dev tools 'Emulate touch events' is set with an iOS device UA string,
				// which causes all touch events to be ignored. As this block only applies to iOS, and iOS identifiers are always long,
				// random integers, it's safe to to continue if the identifier is 0 here.
				if (touch.identifier && touch.identifier === this.lastTouchIdentifier) {
					event.preventDefault();
					return false;
				}

				this.lastTouchIdentifier = touch.identifier;

				// If the target element is a child of a scrollable layer (using -webkit-overflow-scrolling: touch) and:
				// 1) the user does a fling scroll on the scrollable layer
				// 2) the user stops the fling scroll with another tap
				// then the event.target of the last 'touchend' event will be the element that was under the user's finger
				// when the fling scroll was started, causing FastClick to send a click event to that layer - unless a check
				// is made to ensure that a parent layer was not scrolled before sending a synthetic click (issue #42).
				this.updateScrollParent(targetElement);
			}
		}

		this.trackingClick = true;
		this.trackingClickStart = event.timeStamp;
		this.targetElement = targetElement;

		this.touchStartX = touch.pageX;
		this.touchStartY = touch.pageY;

		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			event.preventDefault();
		}

		return true;
	};


	/**
	 * Based on a touchmove event object, check whether the touch has moved past a boundary since it started.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.touchHasMoved = function(event) {
		var touch = event.changedTouches[0], boundary = this.touchBoundary;

		if (Math.abs(touch.pageX - this.touchStartX) > boundary || Math.abs(touch.pageY - this.touchStartY) > boundary) {
			return true;
		}

		return false;
	};


	/**
	 * Update the last position.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchMove = function(event) {
		if (!this.trackingClick) {
			return true;
		}

		// If the touch has moved, cancel the click tracking
		if (this.targetElement !== this.getTargetElementFromEventTarget(event.target) || this.touchHasMoved(event)) {
			this.trackingClick = false;
			this.targetElement = null;
		}

		return true;
	};


	/**
	 * Attempt to find the labelled control for the given label element.
	 *
	 * @param {EventTarget|HTMLLabelElement} labelElement
	 * @returns {Element|null}
	 */
	FastClick.prototype.findControl = function(labelElement) {

		// Fast path for newer browsers supporting the HTML5 control attribute
		if (labelElement.control !== undefined) {
			return labelElement.control;
		}

		// All browsers under test that support touch events also support the HTML5 htmlFor attribute
		if (labelElement.htmlFor) {
			return document.getElementById(labelElement.htmlFor);
		}

		// If no for attribute exists, attempt to retrieve the first labellable descendant element
		// the list of which is defined here: http://www.w3.org/TR/html5/forms.html#category-label
		return labelElement.querySelector('button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea');
	};


	/**
	 * On touch end, determine whether to send a click event at once.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchEnd = function(event) {
		var forElement, trackingClickStart, targetTagName, scrollParent, touch, targetElement = this.targetElement;

		if (!this.trackingClick) {
			return true;
		}

		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			this.cancelNextClick = true;
			return true;
		}

		if ((event.timeStamp - this.trackingClickStart) > this.tapTimeout) {
			return true;
		}

		// Reset to prevent wrong click cancel on input (issue #156).
		this.cancelNextClick = false;

		this.lastClickTime = event.timeStamp;

		trackingClickStart = this.trackingClickStart;
		this.trackingClick = false;
		this.trackingClickStart = 0;

		// On some iOS devices, the targetElement supplied with the event is invalid if the layer
		// is performing a transition or scroll, and has to be re-detected manually. Note that
		// for this to function correctly, it must be called *after* the event target is checked!
		// See issue #57; also filed as rdar://13048589 .
		if (deviceIsIOSWithBadTarget) {
			touch = event.changedTouches[0];

			// In certain cases arguments of elementFromPoint can be negative, so prevent setting targetElement to null
			targetElement = document.elementFromPoint(touch.pageX - window.pageXOffset, touch.pageY - window.pageYOffset) || targetElement;
			targetElement.fastClickScrollParent = this.targetElement.fastClickScrollParent;
		}

		targetTagName = targetElement.tagName.toLowerCase();
		if (targetTagName === 'label') {
			forElement = this.findControl(targetElement);
			if (forElement) {
				this.focus(targetElement);
				if (deviceIsAndroid) {
					return false;
				}

				targetElement = forElement;
			}
		} else if (this.needsFocus(targetElement)) {

			// Case 1: If the touch started a while ago (best guess is 100ms based on tests for issue #36) then focus will be triggered anyway. Return early and unset the target element reference so that the subsequent click will be allowed through.
			// Case 2: Without this exception for input elements tapped when the document is contained in an iframe, then any inputted text won't be visible even though the value attribute is updated as the user types (issue #37).
			if ((event.timeStamp - trackingClickStart) > 100 || (deviceIsIOS && window.top !== window && targetTagName === 'input')) {
				this.targetElement = null;
				return false;
			}

			this.focus(targetElement);
			this.sendClick(targetElement, event);

			// Select elements need the event to go through on iOS 4, otherwise the selector menu won't open.
			// Also this breaks opening selects when VoiceOver is active on iOS6, iOS7 (and possibly others)
			if (!deviceIsIOS || targetTagName !== 'select') {
				this.targetElement = null;
				event.preventDefault();
			}

			return false;
		}

		if (deviceIsIOS && !deviceIsIOS4) {

			// Don't send a synthetic click event if the target element is contained within a parent layer that was scrolled
			// and this tap is being used to stop the scrolling (usually initiated by a fling - issue #42).
			scrollParent = targetElement.fastClickScrollParent;
			if (scrollParent && scrollParent.fastClickLastScrollTop !== scrollParent.scrollTop) {
				return true;
			}
		}

		// Prevent the actual click from going though - unless the target node is marked as requiring
		// real clicks or if it is in the whitelist in which case only non-programmatic clicks are permitted.
		if (!this.needsClick(targetElement)) {
			event.preventDefault();
			this.sendClick(targetElement, event);
		}

		return false;
	};


	/**
	 * On touch cancel, stop tracking the click.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.onTouchCancel = function() {
		this.trackingClick = false;
		this.targetElement = null;
	};


	/**
	 * Determine mouse events which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onMouse = function(event) {

		// If a target element was never set (because a touch event was never fired) allow the event
		if (!this.targetElement) {
			return true;
		}

		if (event.forwardedTouchEvent) {
			return true;
		}

		// Programmatically generated events targeting a specific element should be permitted
		if (!event.cancelable) {
			return true;
		}

		// Derive and check the target element to see whether the mouse event needs to be permitted;
		// unless explicitly enabled, prevent non-touch click events from triggering actions,
		// to prevent ghost/doubleclicks.
		if (!this.needsClick(this.targetElement) || this.cancelNextClick) {

			// Prevent any user-added listeners declared on FastClick element from being fired.
			if (event.stopImmediatePropagation) {
				event.stopImmediatePropagation();
			} else {

				// Part of the hack for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
				event.propagationStopped = true;
			}

			// Cancel the event
			event.stopPropagation();
			event.preventDefault();

			return false;
		}

		// If the mouse event is permitted, return true for the action to go through.
		return true;
	};


	/**
	 * On actual clicks, determine whether this is a touch-generated click, a click action occurring
	 * naturally after a delay after a touch (which needs to be cancelled to avoid duplication), or
	 * an actual click which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onClick = function(event) {
		var permitted;

		// It's possible for another FastClick-like library delivered with third-party code to fire a click event before FastClick does (issue #44). In that case, set the click-tracking flag back to false and return early. This will cause onTouchEnd to return early.
		if (this.trackingClick) {
			this.targetElement = null;
			this.trackingClick = false;
			return true;
		}

		// Very odd behaviour on iOS (issue #18): if a submit element is present inside a form and the user hits enter in the iOS simulator or clicks the Go button on the pop-up OS keyboard the a kind of 'fake' click event will be triggered with the submit-type input element as the target.
		if (event.target.type === 'submit' && event.detail === 0) {
			return true;
		}

		permitted = this.onMouse(event);

		// Only unset targetElement if the click is not permitted. This will ensure that the check for !targetElement in onMouse fails and the browser's click doesn't go through.
		if (!permitted) {
			this.targetElement = null;
		}

		// If clicks are permitted, return true for the action to go through.
		return permitted;
	};


	/**
	 * Remove all FastClick's event listeners.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.destroy = function() {
		var layer = this.layer;

		if (deviceIsAndroid) {
			layer.removeEventListener('mouseover', this.onMouse, true);
			layer.removeEventListener('mousedown', this.onMouse, true);
			layer.removeEventListener('mouseup', this.onMouse, true);
		}

		layer.removeEventListener('click', this.onClick, true);
		layer.removeEventListener('touchstart', this.onTouchStart, false);
		layer.removeEventListener('touchmove', this.onTouchMove, false);
		layer.removeEventListener('touchend', this.onTouchEnd, false);
		layer.removeEventListener('touchcancel', this.onTouchCancel, false);
	};


	/**
	 * Check whether FastClick is needed.
	 *
	 * @param {Element} layer The layer to listen on
	 */
	FastClick.notNeeded = function(layer) {
		var metaViewport;
		var chromeVersion;
		var blackberryVersion;
		var firefoxVersion;

		// Devices that don't support touch don't need FastClick
		if (typeof window.ontouchstart === 'undefined') {
			return true;
		}

		// Chrome version - zero for other browsers
		chromeVersion = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [,0])[1];

		if (chromeVersion) {

			if (deviceIsAndroid) {
				metaViewport = document.querySelector('meta[name=viewport]');

				if (metaViewport) {
					// Chrome on Android with user-scalable="no" doesn't need FastClick (issue #89)
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// Chrome 32 and above with width=device-width or less don't need FastClick
					if (chromeVersion > 31 && document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}

			// Chrome desktop doesn't need FastClick (issue #15)
			} else {
				return true;
			}
		}

		if (deviceIsBlackBerry10) {
			blackberryVersion = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/);

			// BlackBerry 10.3+ does not require Fastclick library.
			// https://github.com/ftlabs/fastclick/issues/251
			if (blackberryVersion[1] >= 10 && blackberryVersion[2] >= 3) {
				metaViewport = document.querySelector('meta[name=viewport]');

				if (metaViewport) {
					// user-scalable=no eliminates click delay.
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// width=device-width (or less than device-width) eliminates click delay.
					if (document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}
			}
		}

		// IE10 with -ms-touch-action: none or manipulation, which disables double-tap-to-zoom (issue #97)
		if (layer.style.msTouchAction === 'none' || layer.style.touchAction === 'manipulation') {
			return true;
		}

		// Firefox version - zero for other browsers
		firefoxVersion = +(/Firefox\/([0-9]+)/.exec(navigator.userAgent) || [,0])[1];

		if (firefoxVersion >= 27) {
			// Firefox 27+ does not have tap delay if the content is not zoomable - https://bugzilla.mozilla.org/show_bug.cgi?id=922896

			metaViewport = document.querySelector('meta[name=viewport]');
			if (metaViewport && (metaViewport.content.indexOf('user-scalable=no') !== -1 || document.documentElement.scrollWidth <= window.outerWidth)) {
				return true;
			}
		}

		// IE11: prefixed -ms-touch-action is no longer supported and it's recomended to use non-prefixed version
		// http://msdn.microsoft.com/en-us/library/windows/apps/Hh767313.aspx
		if (layer.style.touchAction === 'none' || layer.style.touchAction === 'manipulation') {
			return true;
		}

		return false;
	};


	/**
	 * Factory method for creating a FastClick object
	 *
	 * @param {Element} layer The layer to listen on
	 * @param {Object} [options={}] The options to override the defaults
	 */
	FastClick.attach = function(layer, options) {
		return new FastClick(layer, options);
	};


	if (typeof define === 'function' && typeof define.amd === 'object' && define.amd) {

		// AMD. Register as an anonymous module.
		define(function() {
			return FastClick;
		});
	} else if (typeof module !== 'undefined' && module.exports) {
		module.exports = FastClick.attach;
		module.exports.FastClick = FastClick;
	} else {
		window.FastClick = FastClick;
	}
}());

(function(window, document, undefined){
	/*jshint eqnull:true */
	'use strict';
	var polyfill;
	var config = (window.lazySizes && lazySizes.cfg) || window.lazySizesConfig;
	var img = document.createElement('img');
	var supportSrcset = ('sizes' in img) && ('srcset' in img);
	var regHDesc = /\s+\d+h/g;
	var fixEdgeHDescriptor = (function(){
		var regDescriptors = /\s+(\d+)(w|h)\s+(\d+)(w|h)/;
		var forEach = Array.prototype.forEach;

		return function(edgeMatch){
			var img = document.createElement('img');
			var removeHDescriptors = function(source){
				var ratio;
				var srcset = source.getAttribute(lazySizesConfig.srcsetAttr);
				if(srcset){
					if(srcset.match(regDescriptors)){
						if(RegExp.$2 == 'w'){
							ratio = RegExp.$1 / RegExp.$3;
						} else {
							ratio = RegExp.$3 / RegExp.$1;
						}

						if(ratio){
							source.setAttribute('data-aspectratio', ratio);
						}
					}
					source.setAttribute(lazySizesConfig.srcsetAttr, srcset.replace(regHDesc, ''));
				}
			};
			var handler = function(e){
				var picture = e.target.parentNode;

				if(picture && picture.nodeName == 'PICTURE'){
					forEach.call(picture.getElementsByTagName('source'), removeHDescriptors);
				}
				removeHDescriptors(e.target);
			};

			var test = function(){
				if(!!img.currentSrc){
					document.removeEventListener('lazybeforeunveil', handler);
				}
			};

			if(edgeMatch[1]){
				document.addEventListener('lazybeforeunveil', handler);

				if(true || edgeMatch[1] > 14){
					img.onload = test;
					img.onerror = test;

					img.srcset = 'data:,a 1w 1h';

					if(img.complete){
						test();
					}
				}
			}
		};
	})();


	if(!config){
		config = {};
		window.lazySizesConfig = config;
	}

	if(!config.supportsType){
		config.supportsType = function(type/*, elem*/){
			return !type;
		};
	}

	if(window.picturefill || config.pf){return;}

	if(window.HTMLPictureElement && supportSrcset){

		if(document.msElementsFromPoint){
			fixEdgeHDescriptor(navigator.userAgent.match(/Edge\/(\d+)/));
		}

		config.pf = function(){};
		return;
	}

	config.pf = function(options){
		var i, len;
		if(window.picturefill){return;}
		for(i = 0, len = options.elements.length; i < len; i++){
			polyfill(options.elements[i]);
		}
	};

	// partial polyfill
	polyfill = (function(){
		var ascendingSort = function( a, b ) {
			return a.w - b.w;
		};
		var regPxLength = /^\s*\d+\.*\d*px\s*$/;
		var reduceCandidate = function (srces) {
			var lowerCandidate, bonusFactor;
			var len = srces.length;
			var candidate = srces[len -1];
			var i = 0;

			for(i; i < len;i++){
				candidate = srces[i];
				candidate.d = candidate.w / srces.w;

				if(candidate.d >= srces.d){
					if(!candidate.cached && (lowerCandidate = srces[i - 1]) &&
						lowerCandidate.d > srces.d - (0.13 * Math.pow(srces.d, 2.2))){

						bonusFactor = Math.pow(lowerCandidate.d - 0.6, 1.6);

						if(lowerCandidate.cached) {
							lowerCandidate.d += 0.15 * bonusFactor;
						}

						if(lowerCandidate.d + ((candidate.d - srces.d) * bonusFactor) > srces.d){
							candidate = lowerCandidate;
						}
					}
					break;
				}
			}
			return candidate;
		};

		var parseWsrcset = (function(){
			var candidates;
			var regWCandidates = /(([^,\s].[^\s]+)\s+(\d+)w)/g;
			var regMultiple = /\s/;
			var addCandidate = function(match, candidate, url, wDescriptor){
				candidates.push({
					c: candidate,
					u: url,
					w: wDescriptor * 1
				});
			};

			return function(input){
				candidates = [];
				input = input.trim();
				input
					.replace(regHDesc, '')
					.replace(regWCandidates, addCandidate)
				;

				if(!candidates.length && input && !regMultiple.test(input)){
					candidates.push({
						c: input,
						u: input,
						w: 99
					});
				}

				return candidates;
			};
		})();

		var runMatchMedia = function(){
			if(runMatchMedia.init){return;}

			runMatchMedia.init = true;
			addEventListener('resize', (function(){
				var timer;
				var matchMediaElems = document.getElementsByClassName('lazymatchmedia');
				var run = function(){
					var i, len;
					for(i = 0, len = matchMediaElems.length; i < len; i++){
						polyfill(matchMediaElems[i]);
					}
				};

				return function(){
					clearTimeout(timer);
					timer = setTimeout(run, 66);
				};
			})());
		};

		var createSrcset = function(elem, isImage){
			var parsedSet;
			var srcSet = elem.getAttribute('srcset') || elem.getAttribute(config.srcsetAttr);

			if(!srcSet && isImage){
				srcSet = !elem._lazypolyfill ?
					(elem.getAttribute(config.srcAttr) || elem.getAttribute('src')) :
					elem._lazypolyfill._set
				;
			}

			if(!elem._lazypolyfill || elem._lazypolyfill._set != srcSet){

				parsedSet = parseWsrcset( srcSet || '' );
				if(isImage && elem.parentNode){
					parsedSet.isPicture = elem.parentNode.nodeName.toUpperCase() == 'PICTURE';

					if(parsedSet.isPicture){
						if(window.matchMedia){
							lazySizes.aC(elem, 'lazymatchmedia');
							runMatchMedia();
						}
					}
				}

				parsedSet._set = srcSet;
				Object.defineProperty(elem, '_lazypolyfill', {
					value: parsedSet,
					writable: true
				});
			}
		};

		var getX = function(elem){
			var dpr = window.devicePixelRatio || 1;
			var optimum = lazySizes.getX && lazySizes.getX(elem);
			return Math.min(optimum || dpr, 2.5, dpr);
		};

		var matchesMedia = function(media){
			if(window.matchMedia){
				matchesMedia = function(media){
					return !media || (matchMedia(media) || {}).matches;
				};
			} else {
				return !media;
			}

			return matchesMedia(media);
		};

		var getCandidate = function(elem){
			var sources, i, len, media, source, srces, src, width;

			source = elem;
			createSrcset(source, true);
			srces = source._lazypolyfill;

			if(srces.isPicture){
				for(i = 0, sources = elem.parentNode.getElementsByTagName('source'), len = sources.length; i < len; i++){
					if( config.supportsType(sources[i].getAttribute('type'), elem) && matchesMedia( sources[i].getAttribute('media')) ){
						source = sources[i];
						createSrcset(source);
						srces = source._lazypolyfill;
						break;
					}
				}
			}

			if(srces.length > 1){
				width = source.getAttribute('sizes') || '';
				width = regPxLength.test(width) && parseInt(width, 10) || lazySizes.gW(elem, elem.parentNode);
				srces.d = getX(elem);
				if(!srces.src || !srces.w || srces.w < width){
					srces.w = width;
					src = reduceCandidate(srces.sort(ascendingSort));
					srces.src = src;
				} else {
					src = srces.src;
				}
			} else {
				src = srces[0];
			}

			return src;
		};

		var p = function(elem){
			if(supportSrcset && elem.parentNode && elem.parentNode.nodeName.toUpperCase() != 'PICTURE'){return;}
			var candidate = getCandidate(elem);

			if(candidate && candidate.u && elem._lazypolyfill.cur != candidate.u){
				elem._lazypolyfill.cur = candidate.u;
				candidate.cached = true;
				elem.setAttribute(config.srcAttr, candidate.u);
				elem.setAttribute('src', candidate.u);
			}
		};

		p.parse = parseWsrcset;

		return p;
	})();

	if(config.loadedClass && config.loadingClass){
		(function(){
			var sels = [];
			['img[sizes$="px"][srcset].', 'picture > img:not([srcset]).'].forEach(function(sel){
				sels.push(sel + config.loadedClass);
				sels.push(sel + config.loadingClass);
			});
			config.pf({
				elements: document.querySelectorAll(sels.join(', '))
			});
		})();

	}
})(window, document);

/**
 * Some versions of iOS (8.1-) do load the first candidate of a srcset candidate list, if width descriptors with the sizes attribute is used.
 * This tiny extension prevents this wasted download by creating a picture structure around the image.
 * Note: This extension is already included in the ls.respimg.js file.
 *
 * Usage:
 *
 * <img
 * 	class="lazyload"
 * 	data-sizes="auto"
 * 	data-srcset="small.jpg 640px,
 * 		medium.jpg 980w,
 * 		large.jpg 1280w"
 * 	/>
 */

(function(document){
	'use strict';
	var regPicture;
	var img = document.createElement('img');

	if(('srcset' in img) && !('sizes' in img) && !window.HTMLPictureElement){
		regPicture = /^picture$/i;
		document.addEventListener('lazybeforeunveil', function(e){
			var elem, parent, srcset, sizes, isPicture;
			var picture, source;
			if(e.defaultPrevented ||
				lazySizesConfig.noIOSFix ||
				!(elem = e.target) ||
				!(srcset = elem.getAttribute(lazySizesConfig.srcsetAttr)) ||
				!(parent = elem.parentNode) ||
				(
					!(isPicture = regPicture.test(parent.nodeName || '')) &&
					!(sizes = elem.getAttribute('sizes') || elem.getAttribute(lazySizesConfig.sizesAttr))
				)
			){return;}

			picture = isPicture ? parent : document.createElement('picture');

			if(!elem._lazyImgSrc){
				Object.defineProperty(elem, '_lazyImgSrc', {
					value: document.createElement('source'),
					writable: true
				});
			}
			source = elem._lazyImgSrc;

			if(sizes){
				source.setAttribute('sizes', sizes);
			}

			source.setAttribute(lazySizesConfig.srcsetAttr, srcset);
			elem.setAttribute('data-pfsrcset', srcset);
			elem.removeAttribute(lazySizesConfig.srcsetAttr);

			if(!isPicture){
				parent.insertBefore(picture, elem);
				picture.appendChild(elem);
			}
			picture.insertBefore(source, elem);
		});
	}
})(document);

(function(window, factory) {
	var lazySizes = factory(window, window.document);
	window.lazySizes = lazySizes;
	if(typeof module == 'object' && module.exports){
		module.exports = lazySizes;
	}
}(window, function l(window, document) {
	'use strict';
	/*jshint eqnull:true */
	if(!document.getElementsByClassName){return;}

	var lazySizesConfig;

	var docElem = document.documentElement;

	var Date = window.Date;

	var supportPicture = window.HTMLPictureElement;

	var _addEventListener = 'addEventListener';

	var _getAttribute = 'getAttribute';

	var addEventListener = window[_addEventListener];

	var setTimeout = window.setTimeout;

	var requestAnimationFrame = window.requestAnimationFrame || setTimeout;

	var requestIdleCallback = window.requestIdleCallback;

	var regPicture = /^picture$/i;

	var loadEvents = ['load', 'error', 'lazyincluded', '_lazyloaded'];

	var regClassCache = {};

	var forEach = Array.prototype.forEach;

	var hasClass = function(ele, cls) {
		if(!regClassCache[cls]){
			regClassCache[cls] = new RegExp('(\\s|^)'+cls+'(\\s|$)');
		}
		return regClassCache[cls].test(ele[_getAttribute]('class') || '') && regClassCache[cls];
	};

	var addClass = function(ele, cls) {
		if (!hasClass(ele, cls)){
			ele.setAttribute('class', (ele[_getAttribute]('class') || '').trim() + ' ' + cls);
		}
	};

	var removeClass = function(ele, cls) {
		var reg;
		if ((reg = hasClass(ele,cls))) {
			ele.setAttribute('class', (ele[_getAttribute]('class') || '').replace(reg, ' '));
		}
	};

	var addRemoveLoadEvents = function(dom, fn, add){
		var action = add ? _addEventListener : 'removeEventListener';
		if(add){
			addRemoveLoadEvents(dom, fn);
		}
		loadEvents.forEach(function(evt){
			dom[action](evt, fn);
		});
	};

	var triggerEvent = function(elem, name, detail, noBubbles, noCancelable){
		var event = document.createEvent('CustomEvent');

		event.initCustomEvent(name, !noBubbles, !noCancelable, detail || {});

		elem.dispatchEvent(event);
		return event;
	};

	var updatePolyfill = function (el, full){
		var polyfill;
		if( !supportPicture && ( polyfill = (window.picturefill || lazySizesConfig.pf) ) ){
			polyfill({reevaluate: true, elements: [el]});
		} else if(full && full.src){
			el.src = full.src;
		}
	};

	var getCSS = function (elem, style){
		return (getComputedStyle(elem, null) || {})[style];
	};

	var getWidth = function(elem, parent, width){
		width = width || elem.offsetWidth;

		while(width < lazySizesConfig.minSize && parent && !elem._lazysizesWidth){
			width =  parent.offsetWidth;
			parent = parent.parentNode;
		}

		return width;
	};

	var rAF = (function(){
		var running, waiting;
		var firstFns = [];
		var secondFns = [];
		var fns = firstFns;

		var run = function(){
			var runFns = fns;

			fns = firstFns.length ? secondFns : firstFns;

			running = true;
			waiting = false;

			while(runFns.length){
				runFns.shift()();
			}

			running = false;
		};

		var rafBatch = function(fn, queue){
			if(running && !queue){
				fn.apply(this, arguments);
			} else {
				fns.push(fn);

				if(!waiting){
					waiting = true;
					(document.hidden ? setTimeout : requestAnimationFrame)(run);
				}
			}
		};

		rafBatch._lsFlush = run;

		return rafBatch;
	})();

	var rAFIt = function(fn, simple){
		return simple ?
			function() {
				rAF(fn);
			} :
			function(){
				var that = this;
				var args = arguments;
				rAF(function(){
					fn.apply(that, args);
				});
			}
		;
	};

	var throttle = function(fn){
		var running;
		var lastTime = 0;
		var gDelay = 125;
		var RIC_DEFAULT_TIMEOUT = 666;
		var rICTimeout = RIC_DEFAULT_TIMEOUT;
		var run = function(){
			running = false;
			lastTime = Date.now();
			fn();
		};
		var idleCallback = requestIdleCallback ?
			function(){
				requestIdleCallback(run, {timeout: rICTimeout});
				if(rICTimeout !== RIC_DEFAULT_TIMEOUT){
					rICTimeout = RIC_DEFAULT_TIMEOUT;
				}
			}:
			rAFIt(function(){
				setTimeout(run);
			}, true)
		;

		return function(isPriority){
			var delay;
			if((isPriority = isPriority === true)){
				rICTimeout = 44;
			}

			if(running){
				return;
			}

			running =  true;

			delay = gDelay - (Date.now() - lastTime);

			if(delay < 0){
				delay = 0;
			}

			if(isPriority || (delay < 9 && requestIdleCallback)){
				idleCallback();
			} else {
				setTimeout(idleCallback, delay);
			}
		};
	};

	//based on http://modernjavascript.blogspot.de/2013/08/building-better-debounce.html
	var debounce = function(func) {
		var timeout, timestamp;
		var wait = 99;
		var run = function(){
			timeout = null;
			func();
		};
		var later = function() {
			var last = Date.now() - timestamp;

			if (last < wait) {
				setTimeout(later, wait - last);
			} else {
				(requestIdleCallback || run)(run);
			}
		};

		return function() {
			timestamp = Date.now();

			if (!timeout) {
				timeout = setTimeout(later, wait);
			}
		};
	};


	var loader = (function(){
		var lazyloadElems, preloadElems, isCompleted, resetPreloadingTimer, loadMode, started;

		var eLvW, elvH, eLtop, eLleft, eLright, eLbottom;

		var defaultExpand, preloadExpand, hFac;

		var regImg = /^img$/i;
		var regIframe = /^iframe$/i;

		var supportScroll = ('onscroll' in window) && !(/glebot/.test(navigator.userAgent));

		var shrinkExpand = 0;
		var currentExpand = 0;

		var isLoading = 0;
		var lowRuns = -1;

		var resetPreloading = function(e){
			isLoading--;
			if(e && e.target){
				addRemoveLoadEvents(e.target, resetPreloading);
			}

			if(!e || isLoading < 0 || !e.target){
				isLoading = 0;
			}
		};

		var isNestedVisible = function(elem, elemExpand){
			var outerRect;
			var parent = elem;
			var visible = getCSS(document.body, 'visibility') == 'hidden' || getCSS(elem, 'visibility') != 'hidden';

			eLtop -= elemExpand;
			eLbottom += elemExpand;
			eLleft -= elemExpand;
			eLright += elemExpand;

			while(visible && (parent = parent.offsetParent) && parent != document.body && parent != docElem){
				visible = ((getCSS(parent, 'opacity') || 1) > 0);

				if(visible && getCSS(parent, 'overflow') != 'visible'){
					outerRect = parent.getBoundingClientRect();
					visible = eLright > outerRect.left &&
						eLleft < outerRect.right &&
						eLbottom > outerRect.top - 1 &&
						eLtop < outerRect.bottom + 1
					;
				}
			}

			return visible;
		};

		var checkElements = function() {
			var eLlen, i, rect, autoLoadElem, loadedSomething, elemExpand, elemNegativeExpand, elemExpandVal, beforeExpandVal;

			if((loadMode = lazySizesConfig.loadMode) && isLoading < 8 && (eLlen = lazyloadElems.length)){

				i = 0;

				lowRuns++;

				if(preloadExpand == null){
					if(!('expand' in lazySizesConfig)){
						lazySizesConfig.expand = docElem.clientHeight > 500 && docElem.clientWidth > 500 ? 500 : 370;
					}

					defaultExpand = lazySizesConfig.expand;
					preloadExpand = defaultExpand * lazySizesConfig.expFactor;
				}

				if(currentExpand < preloadExpand && isLoading < 1 && lowRuns > 2 && loadMode > 2 && !document.hidden){
					currentExpand = preloadExpand;
					lowRuns = 0;
				} else if(loadMode > 1 && lowRuns > 1 && isLoading < 6){
					currentExpand = defaultExpand;
				} else {
					currentExpand = shrinkExpand;
				}

				for(; i < eLlen; i++){

					if(!lazyloadElems[i] || lazyloadElems[i]._lazyRace){continue;}

					if(!supportScroll){unveilElement(lazyloadElems[i]);continue;}

					if(!(elemExpandVal = lazyloadElems[i][_getAttribute]('data-expand')) || !(elemExpand = elemExpandVal * 1)){
						elemExpand = currentExpand;
					}

					if(beforeExpandVal !== elemExpand){
						eLvW = innerWidth + (elemExpand * hFac);
						elvH = innerHeight + elemExpand;
						elemNegativeExpand = elemExpand * -1;
						beforeExpandVal = elemExpand;
					}

					rect = lazyloadElems[i].getBoundingClientRect();

					if ((eLbottom = rect.bottom) >= elemNegativeExpand &&
						(eLtop = rect.top) <= elvH &&
						(eLright = rect.right) >= elemNegativeExpand * hFac &&
						(eLleft = rect.left) <= eLvW &&
						(eLbottom || eLright || eLleft || eLtop) &&
						((isCompleted && isLoading < 3 && !elemExpandVal && (loadMode < 3 || lowRuns < 4)) || isNestedVisible(lazyloadElems[i], elemExpand))){
						unveilElement(lazyloadElems[i]);
						loadedSomething = true;
						if(isLoading > 9){break;}
					} else if(!loadedSomething && isCompleted && !autoLoadElem &&
						isLoading < 4 && lowRuns < 4 && loadMode > 2 &&
						(preloadElems[0] || lazySizesConfig.preloadAfterLoad) &&
						(preloadElems[0] || (!elemExpandVal && ((eLbottom || eLright || eLleft || eLtop) || lazyloadElems[i][_getAttribute](lazySizesConfig.sizesAttr) != 'auto')))){
						autoLoadElem = preloadElems[0] || lazyloadElems[i];
					}
				}

				if(autoLoadElem && !loadedSomething){
					unveilElement(autoLoadElem);
				}
			}
		};

		var throttledCheckElements = throttle(checkElements);

		var switchLoadingClass = function(e){
			addClass(e.target, lazySizesConfig.loadedClass);
			removeClass(e.target, lazySizesConfig.loadingClass);
			addRemoveLoadEvents(e.target, rafSwitchLoadingClass);
		};
		var rafedSwitchLoadingClass = rAFIt(switchLoadingClass);
		var rafSwitchLoadingClass = function(e){
			rafedSwitchLoadingClass({target: e.target});
		};

		var changeIframeSrc = function(elem, src){
			try {
				elem.contentWindow.location.replace(src);
			} catch(e){
				elem.src = src;
			}
		};

		var handleSources = function(source){
			var customMedia, parent;

			var sourceSrcset = source[_getAttribute](lazySizesConfig.srcsetAttr);

			if( (customMedia = lazySizesConfig.customMedia[source[_getAttribute]('data-media') || source[_getAttribute]('media')]) ){
				source.setAttribute('media', customMedia);
			}

			if(sourceSrcset){
				source.setAttribute('srcset', sourceSrcset);
			}

			//https://bugzilla.mozilla.org/show_bug.cgi?id=1170572
			if(customMedia){
				parent = source.parentNode;
				parent.insertBefore(source.cloneNode(), source);
				parent.removeChild(source);
			}
		};

		var lazyUnveil = rAFIt(function (elem, detail, isAuto, sizes, isImg){
			var src, srcset, parent, isPicture, event, firesLoad;

			if(!(event = triggerEvent(elem, 'lazybeforeunveil', detail)).defaultPrevented){

				if(sizes){
					if(isAuto){
						addClass(elem, lazySizesConfig.autosizesClass);
					} else {
						elem.setAttribute('sizes', sizes);
					}
				}

				srcset = elem[_getAttribute](lazySizesConfig.srcsetAttr);
				src = elem[_getAttribute](lazySizesConfig.srcAttr);

				if(isImg) {
					parent = elem.parentNode;
					isPicture = parent && regPicture.test(parent.nodeName || '');
				}

				firesLoad = detail.firesLoad || (('src' in elem) && (srcset || src || isPicture));

				event = {target: elem};

				if(firesLoad){
					addRemoveLoadEvents(elem, resetPreloading, true);
					clearTimeout(resetPreloadingTimer);
					resetPreloadingTimer = setTimeout(resetPreloading, 2500);

					addClass(elem, lazySizesConfig.loadingClass);
					addRemoveLoadEvents(elem, rafSwitchLoadingClass, true);
				}

				if(isPicture){
					forEach.call(parent.getElementsByTagName('source'), handleSources);
				}

				if(srcset){
					elem.setAttribute('srcset', srcset);
				} else if(src && !isPicture){
					if(regIframe.test(elem.nodeName)){
						changeIframeSrc(elem, src);
					} else {
						elem.src = src;
					}
				}

				if(srcset || isPicture){
					updatePolyfill(elem, {src: src});
				}
			}

			if(elem._lazyRace){
				delete elem._lazyRace;
			}
			removeClass(elem, lazySizesConfig.lazyClass);

			rAF(function(){
				if( !firesLoad || (elem.complete && elem.naturalWidth > 1)){
					if(firesLoad){
						resetPreloading(event);
					} else {
						isLoading--;
					}
					switchLoadingClass(event);
				}
			}, true);
		});

		var unveilElement = function (elem){
			var detail;

			var isImg = regImg.test(elem.nodeName);

			//allow using sizes="auto", but don't use. it's invalid. Use data-sizes="auto" or a valid value for sizes instead (i.e.: sizes="80vw")
			var sizes = isImg && (elem[_getAttribute](lazySizesConfig.sizesAttr) || elem[_getAttribute]('sizes'));
			var isAuto = sizes == 'auto';

			if( (isAuto || !isCompleted) && isImg && (elem.src || elem.srcset) && !elem.complete && !hasClass(elem, lazySizesConfig.errorClass)){return;}

			detail = triggerEvent(elem, 'lazyunveilread').detail;

			if(isAuto){
				 autoSizer.updateElem(elem, true, elem.offsetWidth);
			}

			elem._lazyRace = true;
			isLoading++;

			lazyUnveil(elem, detail, isAuto, sizes, isImg);
		};

		var onload = function(){
			if(isCompleted){return;}
			if(Date.now() - started < 999){
				setTimeout(onload, 999);
				return;
			}
			var afterScroll = debounce(function(){
				lazySizesConfig.loadMode = 3;
				throttledCheckElements();
			});

			isCompleted = true;

			lazySizesConfig.loadMode = 3;

			throttledCheckElements();

			addEventListener('scroll', function(){
				if(lazySizesConfig.loadMode == 3){
					lazySizesConfig.loadMode = 2;
				}
				afterScroll();
			}, true);
		};

		return {
			_: function(){
				started = Date.now();

				lazyloadElems = document.getElementsByClassName(lazySizesConfig.lazyClass);
				preloadElems = document.getElementsByClassName(lazySizesConfig.lazyClass + ' ' + lazySizesConfig.preloadClass);
				hFac = lazySizesConfig.hFac;

				addEventListener('scroll', throttledCheckElements, true);

				addEventListener('resize', throttledCheckElements, true);

				if(window.MutationObserver){
					new MutationObserver( throttledCheckElements ).observe( docElem, {childList: true, subtree: true, attributes: true} );
				} else {
					docElem[_addEventListener]('DOMNodeInserted', throttledCheckElements, true);
					docElem[_addEventListener]('DOMAttrModified', throttledCheckElements, true);
					setInterval(throttledCheckElements, 999);
				}

				addEventListener('hashchange', throttledCheckElements, true);

				//, 'fullscreenchange'
				['focus', 'mouseover', 'click', 'load', 'transitionend', 'animationend', 'webkitAnimationEnd'].forEach(function(name){
					document[_addEventListener](name, throttledCheckElements, true);
				});

				if((/d$|^c/.test(document.readyState))){
					onload();
				} else {
					addEventListener('load', onload);
					document[_addEventListener]('DOMContentLoaded', throttledCheckElements);
					setTimeout(onload, 20000);
				}

				if(lazyloadElems.length){
					checkElements();
					rAF._lsFlush();
				} else {
					throttledCheckElements();
				}
			},
			checkElems: throttledCheckElements,
			unveil: unveilElement
		};
	})();


	var autoSizer = (function(){
		var autosizesElems;

		var sizeElement = rAFIt(function(elem, parent, event, width){
			var sources, i, len;
			elem._lazysizesWidth = width;
			width += 'px';

			elem.setAttribute('sizes', width);

			if(regPicture.test(parent.nodeName || '')){
				sources = parent.getElementsByTagName('source');
				for(i = 0, len = sources.length; i < len; i++){
					sources[i].setAttribute('sizes', width);
				}
			}

			if(!event.detail.dataAttr){
				updatePolyfill(elem, event.detail);
			}
		});
		var getSizeElement = function (elem, dataAttr, width){
			var event;
			var parent = elem.parentNode;

			if(parent){
				width = getWidth(elem, parent, width);
				event = triggerEvent(elem, 'lazybeforesizes', {width: width, dataAttr: !!dataAttr});

				if(!event.defaultPrevented){
					width = event.detail.width;

					if(width && width !== elem._lazysizesWidth){
						sizeElement(elem, parent, event, width);
					}
				}
			}
		};

		var updateElementsSizes = function(){
			var i;
			var len = autosizesElems.length;
			if(len){
				i = 0;

				for(; i < len; i++){
					getSizeElement(autosizesElems[i]);
				}
			}
		};

		var debouncedUpdateElementsSizes = debounce(updateElementsSizes);

		return {
			_: function(){
				autosizesElems = document.getElementsByClassName(lazySizesConfig.autosizesClass);
				addEventListener('resize', debouncedUpdateElementsSizes);
			},
			checkElems: debouncedUpdateElementsSizes,
			updateElem: getSizeElement
		};
	})();

	var init = function(){
		if(!init.i){
			init.i = true;
			autoSizer._();
			loader._();
		}
	};

	(function(){
		var prop;

		var lazySizesDefaults = {
			lazyClass: 'lazyload',
			loadedClass: 'lazyloaded',
			loadingClass: 'lazyloading',
			preloadClass: 'lazypreload',
			errorClass: 'lazyerror',
			//strictClass: 'lazystrict',
			autosizesClass: 'lazyautosizes',
			srcAttr: 'data-src',
			srcsetAttr: 'data-srcset',
			sizesAttr: 'data-sizes',
			//preloadAfterLoad: false,
			minSize: 40,
			customMedia: {},
			init: true,
			expFactor: 1.5,
			hFac: 0.8,
			loadMode: 2
		};

		lazySizesConfig = window.lazySizesConfig || window.lazysizesConfig || {};

		for(prop in lazySizesDefaults){
			if(!(prop in lazySizesConfig)){
				lazySizesConfig[prop] = lazySizesDefaults[prop];
			}
		}

		window.lazySizesConfig = lazySizesConfig;

		setTimeout(function(){
			if(lazySizesConfig.init){
				init();
			}
		});
	})();

	return {
		cfg: lazySizesConfig,
		autoSizer: autoSizer,
		loader: loader,
		init: init,
		uP: updatePolyfill,
		aC: addClass,
		rC: removeClass,
		hC: hasClass,
		fire: triggerEvent,
		gW: getWidth,
		rAF: rAF,
	};
}
));

/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas, David Knight. Dual MIT/BSD license */
window.matchMedia||(window.matchMedia=function(){"use strict";var a=window.styleMedia||window.media;if(!a){var b=document.createElement("style"),c=document.getElementsByTagName("script")[0],d=null;b.type="text/css",b.id="matchmediajs-test",c.parentNode.insertBefore(b,c),d="getComputedStyle"in window&&window.getComputedStyle(b,null)||b.currentStyle,a={matchMedium:function(a){var c="@media "+a+"{ #matchmediajs-test { width: 1px; } }";return b.styleSheet?b.styleSheet.cssText=c:b.textContent=c,"1px"===d.width}}}return function(b){return{matches:a.matchMedium(b||"all"),media:b||"all"}}}()),function(a,b,c){var d=window.matchMedia;"undefined"!=typeof module&&module.exports?module.exports=c(d):"function"==typeof define&&define.amd?define(function(){return b[a]=c(d)}):b[a]=c(d)}("enquire",this,function(a){"use strict";function b(a,b){var c,d=0,e=a.length;for(d;d<e&&(c=b(a[d],d),c!==!1);d++);}function c(a){return"[object Array]"===Object.prototype.toString.apply(a)}function d(a){return"function"==typeof a}function e(a){this.options=a,!a.deferSetup&&this.setup()}function f(b,c){this.query=b,this.isUnconditional=c,this.handlers=[],this.mql=a(b);var d=this;this.listener=function(a){d.mql=a,d.assess()},this.mql.addListener(this.listener)}function g(){if(!a)throw new Error("matchMedia not present, legacy browsers require a polyfill");this.queries={},this.browserIsIncapable=!a("only all").matches}return e.prototype={setup:function(){this.options.setup&&this.options.setup(),this.initialised=!0},on:function(){!this.initialised&&this.setup(),this.options.match&&this.options.match()},off:function(){this.options.unmatch&&this.options.unmatch()},destroy:function(){this.options.destroy?this.options.destroy():this.off()},equals:function(a){return this.options===a||this.options.match===a}},f.prototype={addHandler:function(a){var b=new e(a);this.handlers.push(b),this.matches()&&b.on()},removeHandler:function(a){var c=this.handlers;b(c,function(b,d){if(b.equals(a))return b.destroy(),!c.splice(d,1)})},matches:function(){return this.mql.matches||this.isUnconditional},clear:function(){b(this.handlers,function(a){a.destroy()}),this.mql.removeListener(this.listener),this.handlers.length=0},assess:function(){var a=this.matches()?"on":"off";b(this.handlers,function(b){b[a]()})}},g.prototype={register:function(a,e,g){var h=this.queries,i=g&&this.browserIsIncapable;return h[a]||(h[a]=new f(a,i)),d(e)&&(e={match:e}),c(e)||(e=[e]),b(e,function(b){d(b)&&(b={match:b}),h[a].addHandler(b)}),this},unregister:function(a,b){var c=this.queries[a];return c&&(b?c.removeHandler(b):(c.clear(),delete this.queries[a])),this}},new g}),$("body").on("input propertychange",".fl-form-group",function(a){$(this).toggleClass("fl-form-group-with-value",!!$(a.target).val())}).on("focus",".fl-form-group",function(){$(this).addClass("fl-form-group-with-focus")}).on("blur",".fl-form-group",function(){$(this).removeClass("fl-form-group-with-focus")}),function(){"use strict";function a(a){a.fn.swiper=function(b){var d;return a(this).each(function(){var a=new c(this,b);d||(d=a)}),d}}var b,c=function(a,d){function e(a){return Math.floor(a)}function f(){t.autoplayTimeoutId=setTimeout(function(){t.params.loop?(t.fixLoop(),t._slideNext(),t.emit("onAutoplay",t)):t.isEnd?d.autoplayStopOnLast?t.stopAutoplay():(t._slideTo(0),t.emit("onAutoplay",t)):(t._slideNext(),t.emit("onAutoplay",t))},t.params.autoplay)}function g(a,c){var d=b(a.target);if(!d.is(c))if("string"==typeof c)d=d.parents(c);else if(c.nodeType){var e;return d.parents().each(function(a,b){b===c&&(e=c)}),e?c:void 0}if(0!==d.length)return d[0]}function h(a,b){b=b||{};var c=window.MutationObserver||window.WebkitMutationObserver,d=new c(function(a){a.forEach(function(a){t.onResize(!0),t.emit("onObserverUpdate",t,a)})});d.observe(a,{attributes:"undefined"==typeof b.attributes||b.attributes,childList:"undefined"==typeof b.childList||b.childList,characterData:"undefined"==typeof b.characterData||b.characterData}),t.observers.push(d)}function i(a){a.originalEvent&&(a=a.originalEvent);var b=a.keyCode||a.charCode;if(!t.params.allowSwipeToNext&&(t.isHorizontal()&&39===b||!t.isHorizontal()&&40===b))return!1;if(!t.params.allowSwipeToPrev&&(t.isHorizontal()&&37===b||!t.isHorizontal()&&38===b))return!1;if(!(a.shiftKey||a.altKey||a.ctrlKey||a.metaKey||document.activeElement&&document.activeElement.nodeName&&("input"===document.activeElement.nodeName.toLowerCase()||"textarea"===document.activeElement.nodeName.toLowerCase()))){if(37===b||39===b||38===b||40===b){var c=!1;if(t.container.parents(".swiper-slide").length>0&&0===t.container.parents(".swiper-slide-active").length)return;var d={left:window.pageXOffset,top:window.pageYOffset},e=window.innerWidth,f=window.innerHeight,g=t.container.offset();t.rtl&&(g.left=g.left-t.container[0].scrollLeft);for(var h=[[g.left,g.top],[g.left+t.width,g.top],[g.left,g.top+t.height],[g.left+t.width,g.top+t.height]],i=0;i<h.length;i++){var j=h[i];j[0]>=d.left&&j[0]<=d.left+e&&j[1]>=d.top&&j[1]<=d.top+f&&(c=!0)}if(!c)return}t.isHorizontal()?(37!==b&&39!==b||(a.preventDefault?a.preventDefault():a.returnValue=!1),(39===b&&!t.rtl||37===b&&t.rtl)&&t.slideNext(),(37===b&&!t.rtl||39===b&&t.rtl)&&t.slidePrev()):(38!==b&&40!==b||(a.preventDefault?a.preventDefault():a.returnValue=!1),40===b&&t.slideNext(),38===b&&t.slidePrev())}}function j(a){a.originalEvent&&(a=a.originalEvent);var b=t.mousewheel.event,c=0,d=t.rtl?-1:1;if("mousewheel"===b)if(t.params.mousewheelForceToAxis)if(t.isHorizontal()){if(!(Math.abs(a.wheelDeltaX)>Math.abs(a.wheelDeltaY)))return;c=a.wheelDeltaX*d}else{if(!(Math.abs(a.wheelDeltaY)>Math.abs(a.wheelDeltaX)))return;c=a.wheelDeltaY}else c=Math.abs(a.wheelDeltaX)>Math.abs(a.wheelDeltaY)?-a.wheelDeltaX*d:-a.wheelDeltaY;else if("DOMMouseScroll"===b)c=-a.detail;else if("wheel"===b)if(t.params.mousewheelForceToAxis)if(t.isHorizontal()){if(!(Math.abs(a.deltaX)>Math.abs(a.deltaY)))return;c=-a.deltaX*d}else{if(!(Math.abs(a.deltaY)>Math.abs(a.deltaX)))return;c=-a.deltaY}else c=Math.abs(a.deltaX)>Math.abs(a.deltaY)?-a.deltaX*d:-a.deltaY;if(0!==c){if(t.params.mousewheelInvert&&(c=-c),t.params.freeMode){var e=t.getWrapperTranslate()+c*t.params.mousewheelSensitivity,f=t.isBeginning,g=t.isEnd;if(e>=t.minTranslate()&&(e=t.minTranslate()),e<=t.maxTranslate()&&(e=t.maxTranslate()),t.setWrapperTransition(0),t.setWrapperTranslate(e),t.updateProgress(),t.updateActiveIndex(),(!f&&t.isBeginning||!g&&t.isEnd)&&t.updateClasses(),t.params.freeModeSticky?(clearTimeout(t.mousewheel.timeout),t.mousewheel.timeout=setTimeout(function(){t.slideReset()},300)):t.params.lazyLoading&&t.lazy&&t.lazy.load(),0===e||e===t.maxTranslate())return}else{if((new window.Date).getTime()-t.mousewheel.lastScrollTime>60)if(c<0)if(t.isEnd&&!t.params.loop||t.animating){if(t.params.mousewheelReleaseOnEdges)return!0}else t.slideNext();else if(t.isBeginning&&!t.params.loop||t.animating){if(t.params.mousewheelReleaseOnEdges)return!0}else t.slidePrev();t.mousewheel.lastScrollTime=(new window.Date).getTime()}return t.params.autoplay&&t.stopAutoplay(),a.preventDefault?a.preventDefault():a.returnValue=!1,!1}}function k(a,c){a=b(a);var d,e,f,g=t.rtl?-1:1;d=a.attr("data-swiper-parallax")||"0",e=a.attr("data-swiper-parallax-x"),f=a.attr("data-swiper-parallax-y"),e||f?(e=e||"0",f=f||"0"):t.isHorizontal()?(e=d,f="0"):(f=d,e="0"),e=e.indexOf("%")>=0?parseInt(e,10)*c*g+"%":e*c*g+"px",f=f.indexOf("%")>=0?parseInt(f,10)*c+"%":f*c+"px",a.transform("translate3d("+e+", "+f+",0px)")}function l(a){return 0!==a.indexOf("on")&&(a=a[0]!==a[0].toUpperCase()?"on"+a[0].toUpperCase()+a.substring(1):"on"+a),a}if(!(this instanceof c))return new c(a,d);var m={direction:"horizontal",touchEventsTarget:"container",initialSlide:0,speed:300,autoplay:!1,autoplayDisableOnInteraction:!0,autoplayStopOnLast:!1,iOSEdgeSwipeDetection:!1,iOSEdgeSwipeThreshold:20,freeMode:!1,freeModeMomentum:!0,freeModeMomentumRatio:1,freeModeMomentumBounce:!0,freeModeMomentumBounceRatio:1,freeModeSticky:!1,freeModeMinimumVelocity:.02,autoHeight:!1,setWrapperSize:!1,virtualTranslate:!1,effect:"slide",coverflow:{rotate:50,stretch:0,depth:100,modifier:1,slideShadows:!0},flip:{slideShadows:!0,limitRotation:!0},cube:{slideShadows:!0,shadow:!0,shadowOffset:20,shadowScale:.94},fade:{crossFade:!1},parallax:!1,scrollbar:null,scrollbarHide:!0,scrollbarDraggable:!1,scrollbarSnapOnRelease:!1,keyboardControl:!1,mousewheelControl:!1,mousewheelReleaseOnEdges:!1,mousewheelInvert:!1,mousewheelForceToAxis:!1,mousewheelSensitivity:1,hashnav:!1,breakpoints:void 0,spaceBetween:0,slidesPerView:1,slidesPerColumn:1,slidesPerColumnFill:"column",slidesPerGroup:1,centeredSlides:!1,slidesOffsetBefore:0,slidesOffsetAfter:0,roundLengths:!1,touchRatio:1,touchAngle:45,simulateTouch:!0,shortSwipes:!0,longSwipes:!0,longSwipesRatio:.5,longSwipesMs:300,followFinger:!0,onlyExternal:!1,threshold:0,touchMoveStopPropagation:!0,uniqueNavElements:!0,pagination:null,paginationElement:"span",paginationClickable:!1,paginationHide:!1,paginationBulletRender:null,paginationProgressRender:null,paginationFractionRender:null,paginationCustomRender:null,paginationType:"bullets",resistance:!0,resistanceRatio:.85,nextButton:null,prevButton:null,watchSlidesProgress:!1,watchSlidesVisibility:!1,grabCursor:!1,preventClicks:!0,preventClicksPropagation:!0,slideToClickedSlide:!1,lazyLoading:!1,lazyLoadingInPrevNext:!1,lazyLoadingInPrevNextAmount:1,lazyLoadingOnTransitionStart:!1,preloadImages:!0,updateOnImagesReady:!0,loop:!1,loopAdditionalSlides:0,loopedSlides:null,control:void 0,controlInverse:!1,controlBy:"slide",allowSwipeToPrev:!0,allowSwipeToNext:!0,swipeHandler:null,noSwiping:!0,noSwipingClass:"swiper-no-swiping",slideClass:"swiper-slide",slideActiveClass:"swiper-slide-active",slideVisibleClass:"swiper-slide-visible",slideDuplicateClass:"swiper-slide-duplicate",slideNextClass:"swiper-slide-next",slidePrevClass:"swiper-slide-prev",wrapperClass:"swiper-wrapper",bulletClass:"swiper-pagination-bullet",bulletActiveClass:"swiper-pagination-bullet-active",buttonDisabledClass:"swiper-button-disabled",paginationCurrentClass:"swiper-pagination-current",paginationTotalClass:"swiper-pagination-total",paginationHiddenClass:"swiper-pagination-hidden",paginationProgressbarClass:"swiper-pagination-progressbar",observer:!1,observeParents:!1,a11y:!1,prevSlideMessage:"Previous slide",nextSlideMessage:"Next slide",firstSlideMessage:"This is the first slide",lastSlideMessage:"This is the last slide",paginationBulletMessage:"Go to slide {{index}}",runCallbacksOnInit:!0},n=d&&d.virtualTranslate;d=d||{};var o={};for(var p in d)if("object"!=typeof d[p]||null===d[p]||(d[p].nodeType||d[p]===window||d[p]===document||"undefined"!=typeof Dom7&&d[p]instanceof Dom7||"undefined"!=typeof jQuery&&d[p]instanceof jQuery))o[p]=d[p];else{o[p]={};for(var q in d[p])o[p][q]=d[p][q]}for(var r in m)if("undefined"==typeof d[r])d[r]=m[r];else if("object"==typeof d[r])for(var s in m[r])"undefined"==typeof d[r][s]&&(d[r][s]=m[r][s]);var t=this;if(t.params=d,t.originalParams=o,t.classNames=[],"undefined"!=typeof b&&"undefined"!=typeof Dom7&&(b=Dom7),("undefined"!=typeof b||(b="undefined"==typeof Dom7?window.Dom7||window.Zepto||window.jQuery:Dom7))&&(t.$=b,t.currentBreakpoint=void 0,t.getActiveBreakpoint=function(){if(!t.params.breakpoints)return!1;var a,b=!1,c=[];for(a in t.params.breakpoints)t.params.breakpoints.hasOwnProperty(a)&&c.push(a);c.sort(function(a,b){return parseInt(a,10)>parseInt(b,10)});for(var d=0;d<c.length;d++)a=c[d],a>=window.innerWidth&&!b&&(b=a);return b||"max"},t.setBreakpoint=function(){var a=t.getActiveBreakpoint();if(a&&t.currentBreakpoint!==a){var b=a in t.params.breakpoints?t.params.breakpoints[a]:t.originalParams,c=t.params.loop&&b.slidesPerView!==t.params.slidesPerView;for(var d in b)t.params[d]=b[d];t.currentBreakpoint=a,c&&t.destroyLoop&&t.reLoop(!0)}},t.params.breakpoints&&t.setBreakpoint(),t.container=b(a),0!==t.container.length)){if(t.container.length>1){var u=[];return t.container.each(function(){u.push(new c(this,d))}),u}t.container[0].swiper=t,t.container.data("swiper",t),t.classNames.push("swiper-container-"+t.params.direction),t.params.freeMode&&t.classNames.push("swiper-container-free-mode"),t.support.flexbox||(t.classNames.push("swiper-container-no-flexbox"),t.params.slidesPerColumn=1),t.params.autoHeight&&t.classNames.push("swiper-container-autoheight"),(t.params.parallax||t.params.watchSlidesVisibility)&&(t.params.watchSlidesProgress=!0),["cube","coverflow","flip"].indexOf(t.params.effect)>=0&&(t.support.transforms3d?(t.params.watchSlidesProgress=!0,t.classNames.push("swiper-container-3d")):t.params.effect="slide"),"slide"!==t.params.effect&&t.classNames.push("swiper-container-"+t.params.effect),"cube"===t.params.effect&&(t.params.resistanceRatio=0,t.params.slidesPerView=1,t.params.slidesPerColumn=1,t.params.slidesPerGroup=1,t.params.centeredSlides=!1,t.params.spaceBetween=0,t.params.virtualTranslate=!0,t.params.setWrapperSize=!1),"fade"!==t.params.effect&&"flip"!==t.params.effect||(t.params.slidesPerView=1,t.params.slidesPerColumn=1,t.params.slidesPerGroup=1,t.params.watchSlidesProgress=!0,t.params.spaceBetween=0,t.params.setWrapperSize=!1,"undefined"==typeof n&&(t.params.virtualTranslate=!0)),t.params.grabCursor&&t.support.touch&&(t.params.grabCursor=!1),t.wrapper=t.container.children("."+t.params.wrapperClass),t.params.pagination&&(t.paginationContainer=b(t.params.pagination),t.params.uniqueNavElements&&"string"==typeof t.params.pagination&&t.paginationContainer.length>1&&1===t.container.find(t.params.pagination).length&&(t.paginationContainer=t.container.find(t.params.pagination)),"bullets"===t.params.paginationType&&t.params.paginationClickable?t.paginationContainer.addClass("swiper-pagination-clickable"):t.params.paginationClickable=!1,t.paginationContainer.addClass("swiper-pagination-"+t.params.paginationType)),(t.params.nextButton||t.params.prevButton)&&(t.params.nextButton&&(t.nextButton=b(t.params.nextButton),t.params.uniqueNavElements&&"string"==typeof t.params.nextButton&&t.nextButton.length>1&&1===t.container.find(t.params.nextButton).length&&(t.nextButton=t.container.find(t.params.nextButton))),t.params.prevButton&&(t.prevButton=b(t.params.prevButton),t.params.uniqueNavElements&&"string"==typeof t.params.prevButton&&t.prevButton.length>1&&1===t.container.find(t.params.prevButton).length&&(t.prevButton=t.container.find(t.params.prevButton)))),t.isHorizontal=function(){return"horizontal"===t.params.direction},t.rtl=t.isHorizontal()&&("rtl"===t.container[0].dir.toLowerCase()||"rtl"===t.container.css("direction")),t.rtl&&t.classNames.push("swiper-container-rtl"),t.rtl&&(t.wrongRTL="-webkit-box"===t.wrapper.css("display")),t.params.slidesPerColumn>1&&t.classNames.push("swiper-container-multirow"),t.device.android&&t.classNames.push("swiper-container-android"),t.container.addClass(t.classNames.join(" ")),t.translate=0,t.progress=0,t.velocity=0,t.lockSwipeToNext=function(){t.params.allowSwipeToNext=!1},t.lockSwipeToPrev=function(){t.params.allowSwipeToPrev=!1},t.lockSwipes=function(){t.params.allowSwipeToNext=t.params.allowSwipeToPrev=!1},t.unlockSwipeToNext=function(){t.params.allowSwipeToNext=!0},t.unlockSwipeToPrev=function(){t.params.allowSwipeToPrev=!0},t.unlockSwipes=function(){t.params.allowSwipeToNext=t.params.allowSwipeToPrev=!0},t.params.grabCursor&&(t.container[0].style.cursor="move",t.container[0].style.cursor="-webkit-grab",t.container[0].style.cursor="-moz-grab",t.container[0].style.cursor="grab"),t.imagesToLoad=[],t.imagesLoaded=0,t.loadImage=function(a,b,c,d,e){function f(){e&&e()}var g;a.complete&&d?f():b?(g=new window.Image,g.onload=f,g.onerror=f,c&&(g.srcset=c),b&&(g.src=b)):f()},t.preloadImages=function(){function a(){"undefined"!=typeof t&&null!==t&&(void 0!==t.imagesLoaded&&t.imagesLoaded++,t.imagesLoaded===t.imagesToLoad.length&&(t.params.updateOnImagesReady&&t.update(),t.emit("onImagesReady",t)))}t.imagesToLoad=t.container.find("img");for(var b=0;b<t.imagesToLoad.length;b++)t.loadImage(t.imagesToLoad[b],t.imagesToLoad[b].currentSrc||t.imagesToLoad[b].getAttribute("src"),t.imagesToLoad[b].srcset||t.imagesToLoad[b].getAttribute("srcset"),!0,a)},t.autoplayTimeoutId=void 0,t.autoplaying=!1,t.autoplayPaused=!1,t.startAutoplay=function(){return"undefined"==typeof t.autoplayTimeoutId&&(!!t.params.autoplay&&(!t.autoplaying&&(t.autoplaying=!0,t.emit("onAutoplayStart",t),void f())))},t.stopAutoplay=function(a){t.autoplayTimeoutId&&(t.autoplayTimeoutId&&clearTimeout(t.autoplayTimeoutId),t.autoplaying=!1,t.autoplayTimeoutId=void 0,t.emit("onAutoplayStop",t))},t.pauseAutoplay=function(a){t.autoplayPaused||(t.autoplayTimeoutId&&clearTimeout(t.autoplayTimeoutId),t.autoplayPaused=!0,0===a?(t.autoplayPaused=!1,f()):t.wrapper.transitionEnd(function(){t&&(t.autoplayPaused=!1,t.autoplaying?f():t.stopAutoplay())}))},t.minTranslate=function(){return-t.snapGrid[0]},t.maxTranslate=function(){return-t.snapGrid[t.snapGrid.length-1]},t.updateAutoHeight=function(){var a=t.slides.eq(t.activeIndex)[0];if("undefined"!=typeof a){var b=a.offsetHeight;b&&t.wrapper.css("height",b+"px")}},t.updateContainerSize=function(){var a,b;a="undefined"!=typeof t.params.width?t.params.width:t.container[0].clientWidth,b="undefined"!=typeof t.params.height?t.params.height:t.container[0].clientHeight,0===a&&t.isHorizontal()||0===b&&!t.isHorizontal()||(a=a-parseInt(t.container.css("padding-left"),10)-parseInt(t.container.css("padding-right"),10),b=b-parseInt(t.container.css("padding-top"),10)-parseInt(t.container.css("padding-bottom"),10),t.width=a,t.height=b,t.size=t.isHorizontal()?t.width:t.height)},t.updateSlidesSize=function(){t.slides=t.wrapper.children("."+t.params.slideClass),t.snapGrid=[],t.slidesGrid=[],t.slidesSizesGrid=[];var a,b=t.params.spaceBetween,c=-t.params.slidesOffsetBefore,d=0,f=0;if("undefined"!=typeof t.size){"string"==typeof b&&b.indexOf("%")>=0&&(b=parseFloat(b.replace("%",""))/100*t.size),t.virtualSize=-b,t.rtl?t.slides.css({marginLeft:"",marginTop:""}):t.slides.css({marginRight:"",marginBottom:""});var g;t.params.slidesPerColumn>1&&(g=Math.floor(t.slides.length/t.params.slidesPerColumn)===t.slides.length/t.params.slidesPerColumn?t.slides.length:Math.ceil(t.slides.length/t.params.slidesPerColumn)*t.params.slidesPerColumn,"auto"!==t.params.slidesPerView&&"row"===t.params.slidesPerColumnFill&&(g=Math.max(g,t.params.slidesPerView*t.params.slidesPerColumn)));var h,i=t.params.slidesPerColumn,j=g/i,k=j-(t.params.slidesPerColumn*j-t.slides.length);for(a=0;a<t.slides.length;a++){h=0;var l=t.slides.eq(a);if(t.params.slidesPerColumn>1){var m,n,o;"column"===t.params.slidesPerColumnFill?(n=Math.floor(a/i),o=a-n*i,(n>k||n===k&&o===i-1)&&++o>=i&&(o=0,n++),m=n+o*g/i,l.css({"-webkit-box-ordinal-group":m,"-moz-box-ordinal-group":m,"-ms-flex-order":m,"-webkit-order":m,order:m})):(o=Math.floor(a/j),n=a-o*j),l.css({"margin-top":0!==o&&t.params.spaceBetween&&t.params.spaceBetween+"px"}).attr("data-swiper-column",n).attr("data-swiper-row",o)}"none"!==l.css("display")&&("auto"===t.params.slidesPerView?(h=t.isHorizontal()?l.outerWidth(!0):l.outerHeight(!0),t.params.roundLengths&&(h=e(h))):(h=(t.size-(t.params.slidesPerView-1)*b)/t.params.slidesPerView,t.params.roundLengths&&(h=e(h)),t.isHorizontal()?t.slides[a].style.width=h+"px":t.slides[a].style.height=h+"px"),t.slides[a].swiperSlideSize=h,t.slidesSizesGrid.push(h),t.params.centeredSlides?(c=c+h/2+d/2+b,0===a&&(c=c-t.size/2-b),Math.abs(c)<.001&&(c=0),f%t.params.slidesPerGroup===0&&t.snapGrid.push(c),t.slidesGrid.push(c)):(f%t.params.slidesPerGroup===0&&t.snapGrid.push(c),t.slidesGrid.push(c),c=c+h+b),t.virtualSize+=h+b,d=h,f++)}t.virtualSize=Math.max(t.virtualSize,t.size)+t.params.slidesOffsetAfter;var p;if(t.rtl&&t.wrongRTL&&("slide"===t.params.effect||"coverflow"===t.params.effect)&&t.wrapper.css({width:t.virtualSize+t.params.spaceBetween+"px"}),t.support.flexbox&&!t.params.setWrapperSize||(t.isHorizontal()?t.wrapper.css({width:t.virtualSize+t.params.spaceBetween+"px"}):t.wrapper.css({height:t.virtualSize+t.params.spaceBetween+"px"})),t.params.slidesPerColumn>1&&(t.virtualSize=(h+t.params.spaceBetween)*g,t.virtualSize=Math.ceil(t.virtualSize/t.params.slidesPerColumn)-t.params.spaceBetween,t.wrapper.css({width:t.virtualSize+t.params.spaceBetween+"px"}),t.params.centeredSlides)){for(p=[],a=0;a<t.snapGrid.length;a++)t.snapGrid[a]<t.virtualSize+t.snapGrid[0]&&p.push(t.snapGrid[a]);t.snapGrid=p}if(!t.params.centeredSlides){for(p=[],a=0;a<t.snapGrid.length;a++)t.snapGrid[a]<=t.virtualSize-t.size&&p.push(t.snapGrid[a]);t.snapGrid=p,Math.floor(t.virtualSize-t.size)-Math.floor(t.snapGrid[t.snapGrid.length-1])>1&&t.snapGrid.push(t.virtualSize-t.size)}0===t.snapGrid.length&&(t.snapGrid=[0]),0!==t.params.spaceBetween&&(t.isHorizontal()?t.rtl?t.slides.css({marginLeft:b+"px"}):t.slides.css({marginRight:b+"px"}):t.slides.css({marginBottom:b+"px"})),t.params.watchSlidesProgress&&t.updateSlidesOffset()}},t.updateSlidesOffset=function(){for(var a=0;a<t.slides.length;a++)t.slides[a].swiperSlideOffset=t.isHorizontal()?t.slides[a].offsetLeft:t.slides[a].offsetTop},t.updateSlidesProgress=function(a){if("undefined"==typeof a&&(a=t.translate||0),0!==t.slides.length){"undefined"==typeof t.slides[0].swiperSlideOffset&&t.updateSlidesOffset();var b=-a;t.rtl&&(b=a),t.slides.removeClass(t.params.slideVisibleClass);for(var c=0;c<t.slides.length;c++){var d=t.slides[c],e=(b-d.swiperSlideOffset)/(d.swiperSlideSize+t.params.spaceBetween);if(t.params.watchSlidesVisibility){var f=-(b-d.swiperSlideOffset),g=f+t.slidesSizesGrid[c],h=f>=0&&f<t.size||g>0&&g<=t.size||f<=0&&g>=t.size;h&&t.slides.eq(c).addClass(t.params.slideVisibleClass)}d.progress=t.rtl?-e:e}}},t.updateProgress=function(a){"undefined"==typeof a&&(a=t.translate||0);var b=t.maxTranslate()-t.minTranslate(),c=t.isBeginning,d=t.isEnd;0===b?(t.progress=0,t.isBeginning=t.isEnd=!0):(t.progress=(a-t.minTranslate())/b,t.isBeginning=t.progress<=0,t.isEnd=t.progress>=1),t.isBeginning&&!c&&t.emit("onReachBeginning",t),t.isEnd&&!d&&t.emit("onReachEnd",t),t.params.watchSlidesProgress&&t.updateSlidesProgress(a),t.emit("onProgress",t,t.progress)},t.updateActiveIndex=function(){var a,b,c,d=t.rtl?t.translate:-t.translate;for(b=0;b<t.slidesGrid.length;b++)"undefined"!=typeof t.slidesGrid[b+1]?d>=t.slidesGrid[b]&&d<t.slidesGrid[b+1]-(t.slidesGrid[b+1]-t.slidesGrid[b])/2?a=b:d>=t.slidesGrid[b]&&d<t.slidesGrid[b+1]&&(a=b+1):d>=t.slidesGrid[b]&&(a=b);(a<0||"undefined"==typeof a)&&(a=0),c=Math.floor(a/t.params.slidesPerGroup),c>=t.snapGrid.length&&(c=t.snapGrid.length-1),a!==t.activeIndex&&(t.snapIndex=c,t.previousIndex=t.activeIndex,t.activeIndex=a,t.updateClasses())},t.updateClasses=function(){t.slides.removeClass(t.params.slideActiveClass+" "+t.params.slideNextClass+" "+t.params.slidePrevClass);var a=t.slides.eq(t.activeIndex);a.addClass(t.params.slideActiveClass);var c=a.next("."+t.params.slideClass).addClass(t.params.slideNextClass);t.params.loop&&0===c.length&&t.slides.eq(0).addClass(t.params.slideNextClass);var d=a.prev("."+t.params.slideClass).addClass(t.params.slidePrevClass);if(t.params.loop&&0===d.length&&t.slides.eq(-1).addClass(t.params.slidePrevClass),t.paginationContainer&&t.paginationContainer.length>0){var e,f=t.params.loop?Math.ceil((t.slides.length-2*t.loopedSlides)/t.params.slidesPerGroup):t.snapGrid.length;if(t.params.loop?(e=Math.ceil((t.activeIndex-t.loopedSlides)/t.params.slidesPerGroup),e>t.slides.length-1-2*t.loopedSlides&&(e-=t.slides.length-2*t.loopedSlides),e>f-1&&(e-=f),e<0&&"bullets"!==t.params.paginationType&&(e=f+e)):e="undefined"!=typeof t.snapIndex?t.snapIndex:t.activeIndex||0,"bullets"===t.params.paginationType&&t.bullets&&t.bullets.length>0&&(t.bullets.removeClass(t.params.bulletActiveClass),t.paginationContainer.length>1?t.bullets.each(function(){b(this).index()===e&&b(this).addClass(t.params.bulletActiveClass)}):t.bullets.eq(e).addClass(t.params.bulletActiveClass)),"fraction"===t.params.paginationType&&(t.paginationContainer.find("."+t.params.paginationCurrentClass).text(e+1),t.paginationContainer.find("."+t.params.paginationTotalClass).text(f)),"progress"===t.params.paginationType){var g=(e+1)/f,h=g,i=1;t.isHorizontal()||(i=g,h=1),t.paginationContainer.find("."+t.params.paginationProgressbarClass).transform("translate3d(0,0,0) scaleX("+h+") scaleY("+i+")").transition(t.params.speed)}"custom"===t.params.paginationType&&t.params.paginationCustomRender&&(t.paginationContainer.html(t.params.paginationCustomRender(t,e+1,f)),t.emit("onPaginationRendered",t,t.paginationContainer[0]))}t.params.loop||(t.params.prevButton&&t.prevButton&&t.prevButton.length>0&&(t.isBeginning?(t.prevButton.addClass(t.params.buttonDisabledClass),t.params.a11y&&t.a11y&&t.a11y.disable(t.prevButton)):(t.prevButton.removeClass(t.params.buttonDisabledClass),t.params.a11y&&t.a11y&&t.a11y.enable(t.prevButton))),t.params.nextButton&&t.nextButton&&t.nextButton.length>0&&(t.isEnd?(t.nextButton.addClass(t.params.buttonDisabledClass),t.params.a11y&&t.a11y&&t.a11y.disable(t.nextButton)):(t.nextButton.removeClass(t.params.buttonDisabledClass),t.params.a11y&&t.a11y&&t.a11y.enable(t.nextButton))))},t.updatePagination=function(){if(t.params.pagination&&t.paginationContainer&&t.paginationContainer.length>0){var a="";if("bullets"===t.params.paginationType){for(var b=t.params.loop?Math.ceil((t.slides.length-2*t.loopedSlides)/t.params.slidesPerGroup):t.snapGrid.length,c=0;c<b;c++)a+=t.params.paginationBulletRender?t.params.paginationBulletRender(c,t.params.bulletClass):"<"+t.params.paginationElement+' class="'+t.params.bulletClass+'"></'+t.params.paginationElement+">";t.paginationContainer.html(a),t.bullets=t.paginationContainer.find("."+t.params.bulletClass),t.params.paginationClickable&&t.params.a11y&&t.a11y&&t.a11y.initPagination()}"fraction"===t.params.paginationType&&(a=t.params.paginationFractionRender?t.params.paginationFractionRender(t,t.params.paginationCurrentClass,t.params.paginationTotalClass):'<span class="'+t.params.paginationCurrentClass+'"></span> / <span class="'+t.params.paginationTotalClass+'"></span>',t.paginationContainer.html(a)),"progress"===t.params.paginationType&&(a=t.params.paginationProgressRender?t.params.paginationProgressRender(t,t.params.paginationProgressbarClass):'<span class="'+t.params.paginationProgressbarClass+'"></span>',t.paginationContainer.html(a)),"custom"!==t.params.paginationType&&t.emit("onPaginationRendered",t,t.paginationContainer[0])}},t.update=function(a){function b(){d=Math.min(Math.max(t.translate,t.maxTranslate()),t.minTranslate()),t.setWrapperTranslate(d),t.updateActiveIndex(),t.updateClasses()}if(t.updateContainerSize(),t.updateSlidesSize(),t.updateProgress(),t.updatePagination(),t.updateClasses(),t.params.scrollbar&&t.scrollbar&&t.scrollbar.set(),a){var c,d;t.controller&&t.controller.spline&&(t.controller.spline=void 0),t.params.freeMode?(b(),t.params.autoHeight&&t.updateAutoHeight()):(c=("auto"===t.params.slidesPerView||t.params.slidesPerView>1)&&t.isEnd&&!t.params.centeredSlides?t.slideTo(t.slides.length-1,0,!1,!0):t.slideTo(t.activeIndex,0,!1,!0),c||b())}else t.params.autoHeight&&t.updateAutoHeight()},t.onResize=function(a){t.params.breakpoints&&t.setBreakpoint();var b=t.params.allowSwipeToPrev,c=t.params.allowSwipeToNext;t.params.allowSwipeToPrev=t.params.allowSwipeToNext=!0,t.updateContainerSize(),t.updateSlidesSize(),("auto"===t.params.slidesPerView||t.params.freeMode||a)&&t.updatePagination(),t.params.scrollbar&&t.scrollbar&&t.scrollbar.set(),t.controller&&t.controller.spline&&(t.controller.spline=void 0);var d=!1;if(t.params.freeMode){var e=Math.min(Math.max(t.translate,t.maxTranslate()),t.minTranslate());t.setWrapperTranslate(e),t.updateActiveIndex(),t.updateClasses(),t.params.autoHeight&&t.updateAutoHeight()}else t.updateClasses(),d=("auto"===t.params.slidesPerView||t.params.slidesPerView>1)&&t.isEnd&&!t.params.centeredSlides?t.slideTo(t.slides.length-1,0,!1,!0):t.slideTo(t.activeIndex,0,!1,!0);t.params.lazyLoading&&!d&&t.lazy&&t.lazy.load(),t.params.allowSwipeToPrev=b,t.params.allowSwipeToNext=c};var v=["mousedown","mousemove","mouseup"];window.navigator.pointerEnabled?v=["pointerdown","pointermove","pointerup"]:window.navigator.msPointerEnabled&&(v=["MSPointerDown","MSPointerMove","MSPointerUp"]),t.touchEvents={start:t.support.touch||!t.params.simulateTouch?"touchstart":v[0],move:t.support.touch||!t.params.simulateTouch?"touchmove":v[1],end:t.support.touch||!t.params.simulateTouch?"touchend":v[2]},(window.navigator.pointerEnabled||window.navigator.msPointerEnabled)&&("container"===t.params.touchEventsTarget?t.container:t.wrapper).addClass("swiper-wp8-"+t.params.direction),t.initEvents=function(a){var b=a?"off":"on",c=a?"removeEventListener":"addEventListener",e="container"===t.params.touchEventsTarget?t.container[0]:t.wrapper[0],f=t.support.touch?e:document,g=!!t.params.nested;t.browser.ie?(e[c](t.touchEvents.start,t.onTouchStart,!1),f[c](t.touchEvents.move,t.onTouchMove,g),f[c](t.touchEvents.end,t.onTouchEnd,!1)):(t.support.touch&&(e[c](t.touchEvents.start,t.onTouchStart,!1),e[c](t.touchEvents.move,t.onTouchMove,g),e[c](t.touchEvents.end,t.onTouchEnd,!1)),!d.simulateTouch||t.device.ios||t.device.android||(e[c]("mousedown",t.onTouchStart,!1),document[c]("mousemove",t.onTouchMove,g),document[c]("mouseup",t.onTouchEnd,!1))),window[c]("resize",t.onResize),t.params.nextButton&&t.nextButton&&t.nextButton.length>0&&(t.nextButton[b]("click",t.onClickNext),t.params.a11y&&t.a11y&&t.nextButton[b]("keydown",t.a11y.onEnterKey)),t.params.prevButton&&t.prevButton&&t.prevButton.length>0&&(t.prevButton[b]("click",t.onClickPrev),t.params.a11y&&t.a11y&&t.prevButton[b]("keydown",t.a11y.onEnterKey)),t.params.pagination&&t.params.paginationClickable&&(t.paginationContainer[b]("click","."+t.params.bulletClass,t.onClickIndex),t.params.a11y&&t.a11y&&t.paginationContainer[b]("keydown","."+t.params.bulletClass,t.a11y.onEnterKey)),(t.params.preventClicks||t.params.preventClicksPropagation)&&e[c]("click",t.preventClicks,!0)},t.attachEvents=function(){t.initEvents()},t.detachEvents=function(){t.initEvents(!0)},t.allowClick=!0,t.preventClicks=function(a){t.allowClick||(t.params.preventClicks&&a.preventDefault(),t.params.preventClicksPropagation&&t.animating&&(a.stopPropagation(),a.stopImmediatePropagation()))},t.onClickNext=function(a){a.preventDefault(),t.isEnd&&!t.params.loop||t.slideNext()},t.onClickPrev=function(a){a.preventDefault(),t.isBeginning&&!t.params.loop||t.slidePrev()},t.onClickIndex=function(a){a.preventDefault();var c=b(this).index()*t.params.slidesPerGroup;t.params.loop&&(c+=t.loopedSlides),t.slideTo(c)},t.updateClickedSlide=function(a){var c=g(a,"."+t.params.slideClass),d=!1;if(c)for(var e=0;e<t.slides.length;e++)t.slides[e]===c&&(d=!0);if(!c||!d)return t.clickedSlide=void 0,void(t.clickedIndex=void 0);if(t.clickedSlide=c,t.clickedIndex=b(c).index(),t.params.slideToClickedSlide&&void 0!==t.clickedIndex&&t.clickedIndex!==t.activeIndex){var f,h=t.clickedIndex;if(t.params.loop){if(t.animating)return;f=b(t.clickedSlide).attr("data-swiper-slide-index"),t.params.centeredSlides?h<t.loopedSlides-t.params.slidesPerView/2||h>t.slides.length-t.loopedSlides+t.params.slidesPerView/2?(t.fixLoop(),h=t.wrapper.children("."+t.params.slideClass+'[data-swiper-slide-index="'+f+'"]:not(.swiper-slide-duplicate)').eq(0).index(),setTimeout(function(){t.slideTo(h)},0)):t.slideTo(h):h>t.slides.length-t.params.slidesPerView?(t.fixLoop(),h=t.wrapper.children("."+t.params.slideClass+'[data-swiper-slide-index="'+f+'"]:not(.swiper-slide-duplicate)').eq(0).index(),setTimeout(function(){t.slideTo(h)},0)):t.slideTo(h)}else t.slideTo(h)}};var w,x,y,z,A,B,C,D,E,F,G="input, select, textarea, button",H=Date.now(),I=[];t.animating=!1,t.touches={startX:0,startY:0,currentX:0,currentY:0,diff:0};var J,K;if(t.onTouchStart=function(a){if(a.originalEvent&&(a=a.originalEvent),J="touchstart"===a.type,J||!("which"in a)||3!==a.which){if(t.params.noSwiping&&g(a,"."+t.params.noSwipingClass))return void(t.allowClick=!0);if(!t.params.swipeHandler||g(a,t.params.swipeHandler)){var c=t.touches.currentX="touchstart"===a.type?a.targetTouches[0].pageX:a.pageX,d=t.touches.currentY="touchstart"===a.type?a.targetTouches[0].pageY:a.pageY;
if(!(t.device.ios&&t.params.iOSEdgeSwipeDetection&&c<=t.params.iOSEdgeSwipeThreshold)){if(w=!0,x=!1,y=!0,A=void 0,K=void 0,t.touches.startX=c,t.touches.startY=d,z=Date.now(),t.allowClick=!0,t.updateContainerSize(),t.swipeDirection=void 0,t.params.threshold>0&&(D=!1),"touchstart"!==a.type){var e=!0;b(a.target).is(G)&&(e=!1),document.activeElement&&b(document.activeElement).is(G)&&document.activeElement.blur(),e&&a.preventDefault()}t.emit("onTouchStart",t,a)}}}},t.onTouchMove=function(a){if(a.originalEvent&&(a=a.originalEvent),!J||"mousemove"!==a.type){if(a.preventedByNestedSwiper)return t.touches.startX="touchmove"===a.type?a.targetTouches[0].pageX:a.pageX,void(t.touches.startY="touchmove"===a.type?a.targetTouches[0].pageY:a.pageY);if(t.params.onlyExternal)return t.allowClick=!1,void(w&&(t.touches.startX=t.touches.currentX="touchmove"===a.type?a.targetTouches[0].pageX:a.pageX,t.touches.startY=t.touches.currentY="touchmove"===a.type?a.targetTouches[0].pageY:a.pageY,z=Date.now()));if(J&&document.activeElement&&a.target===document.activeElement&&b(a.target).is(G))return x=!0,void(t.allowClick=!1);if(y&&t.emit("onTouchMove",t,a),!(a.targetTouches&&a.targetTouches.length>1)){if(t.touches.currentX="touchmove"===a.type?a.targetTouches[0].pageX:a.pageX,t.touches.currentY="touchmove"===a.type?a.targetTouches[0].pageY:a.pageY,"undefined"==typeof A){var c=180*Math.atan2(Math.abs(t.touches.currentY-t.touches.startY),Math.abs(t.touches.currentX-t.touches.startX))/Math.PI;A=t.isHorizontal()?c>t.params.touchAngle:90-c>t.params.touchAngle}if(A&&t.emit("onTouchMoveOpposite",t,a),"undefined"==typeof K&&t.browser.ieTouch&&(t.touches.currentX===t.touches.startX&&t.touches.currentY===t.touches.startY||(K=!0)),w){if(A)return void(w=!1);if(K||!t.browser.ieTouch){t.allowClick=!1,t.emit("onSliderMove",t,a),a.preventDefault(),t.params.touchMoveStopPropagation&&!t.params.nested&&a.stopPropagation(),x||(d.loop&&t.fixLoop(),C=t.getWrapperTranslate(),t.setWrapperTransition(0),t.animating&&t.wrapper.trigger("webkitTransitionEnd transitionend oTransitionEnd MSTransitionEnd msTransitionEnd"),t.params.autoplay&&t.autoplaying&&(t.params.autoplayDisableOnInteraction?t.stopAutoplay():t.pauseAutoplay()),F=!1,t.params.grabCursor&&(t.container[0].style.cursor="move",t.container[0].style.cursor="-webkit-grabbing",t.container[0].style.cursor="-moz-grabbin",t.container[0].style.cursor="grabbing")),x=!0;var e=t.touches.diff=t.isHorizontal()?t.touches.currentX-t.touches.startX:t.touches.currentY-t.touches.startY;e*=t.params.touchRatio,t.rtl&&(e=-e),t.swipeDirection=e>0?"prev":"next",B=e+C;var f=!0;if(e>0&&B>t.minTranslate()?(f=!1,t.params.resistance&&(B=t.minTranslate()-1+Math.pow(-t.minTranslate()+C+e,t.params.resistanceRatio))):e<0&&B<t.maxTranslate()&&(f=!1,t.params.resistance&&(B=t.maxTranslate()+1-Math.pow(t.maxTranslate()-C-e,t.params.resistanceRatio))),f&&(a.preventedByNestedSwiper=!0),!t.params.allowSwipeToNext&&"next"===t.swipeDirection&&B<C&&(B=C),!t.params.allowSwipeToPrev&&"prev"===t.swipeDirection&&B>C&&(B=C),t.params.followFinger){if(t.params.threshold>0){if(!(Math.abs(e)>t.params.threshold||D))return void(B=C);if(!D)return D=!0,t.touches.startX=t.touches.currentX,t.touches.startY=t.touches.currentY,B=C,void(t.touches.diff=t.isHorizontal()?t.touches.currentX-t.touches.startX:t.touches.currentY-t.touches.startY)}(t.params.freeMode||t.params.watchSlidesProgress)&&t.updateActiveIndex(),t.params.freeMode&&(0===I.length&&I.push({position:t.touches[t.isHorizontal()?"startX":"startY"],time:z}),I.push({position:t.touches[t.isHorizontal()?"currentX":"currentY"],time:(new window.Date).getTime()})),t.updateProgress(B),t.setWrapperTranslate(B)}}}}}},t.onTouchEnd=function(a){if(a.originalEvent&&(a=a.originalEvent),y&&t.emit("onTouchEnd",t,a),y=!1,w){t.params.grabCursor&&x&&w&&(t.container[0].style.cursor="move",t.container[0].style.cursor="-webkit-grab",t.container[0].style.cursor="-moz-grab",t.container[0].style.cursor="grab");var c=Date.now(),d=c-z;if(t.allowClick&&(t.updateClickedSlide(a),t.emit("onTap",t,a),d<300&&c-H>300&&(E&&clearTimeout(E),E=setTimeout(function(){t&&(t.params.paginationHide&&t.paginationContainer.length>0&&!b(a.target).hasClass(t.params.bulletClass)&&t.paginationContainer.toggleClass(t.params.paginationHiddenClass),t.emit("onClick",t,a))},300)),d<300&&c-H<300&&(E&&clearTimeout(E),t.emit("onDoubleTap",t,a))),H=Date.now(),setTimeout(function(){t&&(t.allowClick=!0)},0),!w||!x||!t.swipeDirection||0===t.touches.diff||B===C)return void(w=x=!1);w=x=!1;var e;if(e=t.params.followFinger?t.rtl?t.translate:-t.translate:-B,t.params.freeMode){if(e<-t.minTranslate())return void t.slideTo(t.activeIndex);if(e>-t.maxTranslate())return void(t.slides.length<t.snapGrid.length?t.slideTo(t.snapGrid.length-1):t.slideTo(t.slides.length-1));if(t.params.freeModeMomentum){if(I.length>1){var f=I.pop(),g=I.pop(),h=f.position-g.position,i=f.time-g.time;t.velocity=h/i,t.velocity=t.velocity/2,Math.abs(t.velocity)<t.params.freeModeMinimumVelocity&&(t.velocity=0),(i>150||(new window.Date).getTime()-f.time>300)&&(t.velocity=0)}else t.velocity=0;I.length=0;var j=1e3*t.params.freeModeMomentumRatio,k=t.velocity*j,l=t.translate+k;t.rtl&&(l=-l);var m,n=!1,o=20*Math.abs(t.velocity)*t.params.freeModeMomentumBounceRatio;if(l<t.maxTranslate())t.params.freeModeMomentumBounce?(l+t.maxTranslate()<-o&&(l=t.maxTranslate()-o),m=t.maxTranslate(),n=!0,F=!0):l=t.maxTranslate();else if(l>t.minTranslate())t.params.freeModeMomentumBounce?(l-t.minTranslate()>o&&(l=t.minTranslate()+o),m=t.minTranslate(),n=!0,F=!0):l=t.minTranslate();else if(t.params.freeModeSticky){var p,q=0;for(q=0;q<t.snapGrid.length;q+=1)if(t.snapGrid[q]>-l){p=q;break}l=Math.abs(t.snapGrid[p]-l)<Math.abs(t.snapGrid[p-1]-l)||"next"===t.swipeDirection?t.snapGrid[p]:t.snapGrid[p-1],t.rtl||(l=-l)}if(0!==t.velocity)j=t.rtl?Math.abs((-l-t.translate)/t.velocity):Math.abs((l-t.translate)/t.velocity);else if(t.params.freeModeSticky)return void t.slideReset();t.params.freeModeMomentumBounce&&n?(t.updateProgress(m),t.setWrapperTransition(j),t.setWrapperTranslate(l),t.onTransitionStart(),t.animating=!0,t.wrapper.transitionEnd(function(){t&&F&&(t.emit("onMomentumBounce",t),t.setWrapperTransition(t.params.speed),t.setWrapperTranslate(m),t.wrapper.transitionEnd(function(){t&&t.onTransitionEnd()}))})):t.velocity?(t.updateProgress(l),t.setWrapperTransition(j),t.setWrapperTranslate(l),t.onTransitionStart(),t.animating||(t.animating=!0,t.wrapper.transitionEnd(function(){t&&t.onTransitionEnd()}))):t.updateProgress(l),t.updateActiveIndex()}return void((!t.params.freeModeMomentum||d>=t.params.longSwipesMs)&&(t.updateProgress(),t.updateActiveIndex()))}var r,s=0,u=t.slidesSizesGrid[0];for(r=0;r<t.slidesGrid.length;r+=t.params.slidesPerGroup)"undefined"!=typeof t.slidesGrid[r+t.params.slidesPerGroup]?e>=t.slidesGrid[r]&&e<t.slidesGrid[r+t.params.slidesPerGroup]&&(s=r,u=t.slidesGrid[r+t.params.slidesPerGroup]-t.slidesGrid[r]):e>=t.slidesGrid[r]&&(s=r,u=t.slidesGrid[t.slidesGrid.length-1]-t.slidesGrid[t.slidesGrid.length-2]);var v=(e-t.slidesGrid[s])/u;if(d>t.params.longSwipesMs){if(!t.params.longSwipes)return void t.slideTo(t.activeIndex);"next"===t.swipeDirection&&(v>=t.params.longSwipesRatio?t.slideTo(s+t.params.slidesPerGroup):t.slideTo(s)),"prev"===t.swipeDirection&&(v>1-t.params.longSwipesRatio?t.slideTo(s+t.params.slidesPerGroup):t.slideTo(s))}else{if(!t.params.shortSwipes)return void t.slideTo(t.activeIndex);"next"===t.swipeDirection&&t.slideTo(s+t.params.slidesPerGroup),"prev"===t.swipeDirection&&t.slideTo(s)}}},t._slideTo=function(a,b){return t.slideTo(a,b,!0,!0)},t.slideTo=function(a,b,c,d){"undefined"==typeof c&&(c=!0),"undefined"==typeof a&&(a=0),a<0&&(a=0),t.snapIndex=Math.floor(a/t.params.slidesPerGroup),t.snapIndex>=t.snapGrid.length&&(t.snapIndex=t.snapGrid.length-1);var e=-t.snapGrid[t.snapIndex];t.params.autoplay&&t.autoplaying&&(d||!t.params.autoplayDisableOnInteraction?t.pauseAutoplay(b):t.stopAutoplay()),t.updateProgress(e);for(var f=0;f<t.slidesGrid.length;f++)-Math.floor(100*e)>=Math.floor(100*t.slidesGrid[f])&&(a=f);return!(!t.params.allowSwipeToNext&&e<t.translate&&e<t.minTranslate())&&(!(!t.params.allowSwipeToPrev&&e>t.translate&&e>t.maxTranslate()&&(t.activeIndex||0)!==a)&&("undefined"==typeof b&&(b=t.params.speed),t.previousIndex=t.activeIndex||0,t.activeIndex=a,t.rtl&&-e===t.translate||!t.rtl&&e===t.translate?(t.params.autoHeight&&t.updateAutoHeight(),t.updateClasses(),"slide"!==t.params.effect&&t.setWrapperTranslate(e),!1):(t.updateClasses(),t.onTransitionStart(c),0===b?(t.setWrapperTranslate(e),t.setWrapperTransition(0),t.onTransitionEnd(c)):(t.setWrapperTranslate(e),t.setWrapperTransition(b),t.animating||(t.animating=!0,t.wrapper.transitionEnd(function(){t&&t.onTransitionEnd(c)}))),!0)))},t.onTransitionStart=function(a){"undefined"==typeof a&&(a=!0),t.params.autoHeight&&t.updateAutoHeight(),t.lazy&&t.lazy.onTransitionStart(),a&&(t.emit("onTransitionStart",t),t.activeIndex!==t.previousIndex&&(t.emit("onSlideChangeStart",t),t.activeIndex>t.previousIndex?t.emit("onSlideNextStart",t):t.emit("onSlidePrevStart",t)))},t.onTransitionEnd=function(a){t.animating=!1,t.setWrapperTransition(0),"undefined"==typeof a&&(a=!0),t.lazy&&t.lazy.onTransitionEnd(),a&&(t.emit("onTransitionEnd",t),t.activeIndex!==t.previousIndex&&(t.emit("onSlideChangeEnd",t),t.activeIndex>t.previousIndex?t.emit("onSlideNextEnd",t):t.emit("onSlidePrevEnd",t))),t.params.hashnav&&t.hashnav&&t.hashnav.setHash()},t.slideNext=function(a,b,c){if(t.params.loop){if(t.animating)return!1;t.fixLoop();t.container[0].clientLeft;return t.slideTo(t.activeIndex+t.params.slidesPerGroup,b,a,c)}return t.slideTo(t.activeIndex+t.params.slidesPerGroup,b,a,c)},t._slideNext=function(a){return t.slideNext(!0,a,!0)},t.slidePrev=function(a,b,c){if(t.params.loop){if(t.animating)return!1;t.fixLoop();t.container[0].clientLeft;return t.slideTo(t.activeIndex-1,b,a,c)}return t.slideTo(t.activeIndex-1,b,a,c)},t._slidePrev=function(a){return t.slidePrev(!0,a,!0)},t.slideReset=function(a,b,c){return t.slideTo(t.activeIndex,b,a)},t.setWrapperTransition=function(a,b){t.wrapper.transition(a),"slide"!==t.params.effect&&t.effects[t.params.effect]&&t.effects[t.params.effect].setTransition(a),t.params.parallax&&t.parallax&&t.parallax.setTransition(a),t.params.scrollbar&&t.scrollbar&&t.scrollbar.setTransition(a),t.params.control&&t.controller&&t.controller.setTransition(a,b),t.emit("onSetTransition",t,a)},t.setWrapperTranslate=function(a,b,c){var d=0,f=0,g=0;t.isHorizontal()?d=t.rtl?-a:a:f=a,t.params.roundLengths&&(d=e(d),f=e(f)),t.params.virtualTranslate||(t.support.transforms3d?t.wrapper.transform("translate3d("+d+"px, "+f+"px, "+g+"px)"):t.wrapper.transform("translate("+d+"px, "+f+"px)")),t.translate=t.isHorizontal()?d:f;var h,i=t.maxTranslate()-t.minTranslate();h=0===i?0:(a-t.minTranslate())/i,h!==t.progress&&t.updateProgress(a),b&&t.updateActiveIndex(),"slide"!==t.params.effect&&t.effects[t.params.effect]&&t.effects[t.params.effect].setTranslate(t.translate),t.params.parallax&&t.parallax&&t.parallax.setTranslate(t.translate),t.params.scrollbar&&t.scrollbar&&t.scrollbar.setTranslate(t.translate),t.params.control&&t.controller&&t.controller.setTranslate(t.translate,c),t.emit("onSetTranslate",t,t.translate)},t.getTranslate=function(a,b){var c,d,e,f;return"undefined"==typeof b&&(b="x"),t.params.virtualTranslate?t.rtl?-t.translate:t.translate:(e=window.getComputedStyle(a,null),window.WebKitCSSMatrix?(d=e.transform||e.webkitTransform,d.split(",").length>6&&(d=d.split(", ").map(function(a){return a.replace(",",".")}).join(", ")),f=new window.WebKitCSSMatrix("none"===d?"":d)):(f=e.MozTransform||e.OTransform||e.MsTransform||e.msTransform||e.transform||e.getPropertyValue("transform").replace("translate(","matrix(1, 0, 0, 1,"),c=f.toString().split(",")),"x"===b&&(d=window.WebKitCSSMatrix?f.m41:16===c.length?parseFloat(c[12]):parseFloat(c[4])),"y"===b&&(d=window.WebKitCSSMatrix?f.m42:16===c.length?parseFloat(c[13]):parseFloat(c[5])),t.rtl&&d&&(d=-d),d||0)},t.getWrapperTranslate=function(a){return"undefined"==typeof a&&(a=t.isHorizontal()?"x":"y"),t.getTranslate(t.wrapper[0],a)},t.observers=[],t.initObservers=function(){if(t.params.observeParents)for(var a=t.container.parents(),b=0;b<a.length;b++)h(a[b]);h(t.container[0],{childList:!1}),h(t.wrapper[0],{attributes:!1})},t.disconnectObservers=function(){for(var a=0;a<t.observers.length;a++)t.observers[a].disconnect();t.observers=[]},t.createLoop=function(){t.wrapper.children("."+t.params.slideClass+"."+t.params.slideDuplicateClass).remove();var a=t.wrapper.children("."+t.params.slideClass);"auto"!==t.params.slidesPerView||t.params.loopedSlides||(t.params.loopedSlides=a.length),t.loopedSlides=parseInt(t.params.loopedSlides||t.params.slidesPerView,10),t.loopedSlides=t.loopedSlides+t.params.loopAdditionalSlides,t.loopedSlides>a.length&&(t.loopedSlides=a.length);var c,d=[],e=[];for(a.each(function(c,f){var g=b(this);c<t.loopedSlides&&e.push(f),c<a.length&&c>=a.length-t.loopedSlides&&d.push(f),g.attr("data-swiper-slide-index",c)}),c=0;c<e.length;c++)t.wrapper.append(b(e[c].cloneNode(!0)).addClass(t.params.slideDuplicateClass));for(c=d.length-1;c>=0;c--)t.wrapper.prepend(b(d[c].cloneNode(!0)).addClass(t.params.slideDuplicateClass))},t.destroyLoop=function(){t.wrapper.children("."+t.params.slideClass+"."+t.params.slideDuplicateClass).remove(),t.slides.removeAttr("data-swiper-slide-index")},t.reLoop=function(a){var b=t.activeIndex-t.loopedSlides;t.destroyLoop(),t.createLoop(),t.updateSlidesSize(),a&&t.slideTo(b+t.loopedSlides,0,!1)},t.fixLoop=function(){var a;t.activeIndex<t.loopedSlides?(a=t.slides.length-3*t.loopedSlides+t.activeIndex,a+=t.loopedSlides,t.slideTo(a,0,!1,!0)):("auto"===t.params.slidesPerView&&t.activeIndex>=2*t.loopedSlides||t.activeIndex>t.slides.length-2*t.params.slidesPerView)&&(a=-t.slides.length+t.activeIndex+t.loopedSlides,a+=t.loopedSlides,t.slideTo(a,0,!1,!0))},t.appendSlide=function(a){if(t.params.loop&&t.destroyLoop(),"object"==typeof a&&a.length)for(var b=0;b<a.length;b++)a[b]&&t.wrapper.append(a[b]);else t.wrapper.append(a);t.params.loop&&t.createLoop(),t.params.observer&&t.support.observer||t.update(!0)},t.prependSlide=function(a){t.params.loop&&t.destroyLoop();var b=t.activeIndex+1;if("object"==typeof a&&a.length){for(var c=0;c<a.length;c++)a[c]&&t.wrapper.prepend(a[c]);b=t.activeIndex+a.length}else t.wrapper.prepend(a);t.params.loop&&t.createLoop(),t.params.observer&&t.support.observer||t.update(!0),t.slideTo(b,0,!1)},t.removeSlide=function(a){t.params.loop&&(t.destroyLoop(),t.slides=t.wrapper.children("."+t.params.slideClass));var b,c=t.activeIndex;if("object"==typeof a&&a.length){for(var d=0;d<a.length;d++)b=a[d],t.slides[b]&&t.slides.eq(b).remove(),b<c&&c--;c=Math.max(c,0)}else b=a,t.slides[b]&&t.slides.eq(b).remove(),b<c&&c--,c=Math.max(c,0);t.params.loop&&t.createLoop(),t.params.observer&&t.support.observer||t.update(!0),t.params.loop?t.slideTo(c+t.loopedSlides,0,!1):t.slideTo(c,0,!1)},t.removeAllSlides=function(){for(var a=[],b=0;b<t.slides.length;b++)a.push(b);t.removeSlide(a)},t.effects={fade:{setTranslate:function(){for(var a=0;a<t.slides.length;a++){var b=t.slides.eq(a),c=b[0].swiperSlideOffset,d=-c;t.params.virtualTranslate||(d-=t.translate);var e=0;t.isHorizontal()||(e=d,d=0);var f=t.params.fade.crossFade?Math.max(1-Math.abs(b[0].progress),0):1+Math.min(Math.max(b[0].progress,-1),0);b.css({opacity:f}).transform("translate3d("+d+"px, "+e+"px, 0px)")}},setTransition:function(a){if(t.slides.transition(a),t.params.virtualTranslate&&0!==a){var b=!1;t.slides.transitionEnd(function(){if(!b&&t){b=!0,t.animating=!1;for(var a=["webkitTransitionEnd","transitionend","oTransitionEnd","MSTransitionEnd","msTransitionEnd"],c=0;c<a.length;c++)t.wrapper.trigger(a[c])}})}}},flip:{setTranslate:function(){for(var a=0;a<t.slides.length;a++){var c=t.slides.eq(a),d=c[0].progress;t.params.flip.limitRotation&&(d=Math.max(Math.min(c[0].progress,1),-1));var e=c[0].swiperSlideOffset,f=-180*d,g=f,h=0,i=-e,j=0;if(t.isHorizontal()?t.rtl&&(g=-g):(j=i,i=0,h=-g,g=0),c[0].style.zIndex=-Math.abs(Math.round(d))+t.slides.length,t.params.flip.slideShadows){var k=t.isHorizontal()?c.find(".swiper-slide-shadow-left"):c.find(".swiper-slide-shadow-top"),l=t.isHorizontal()?c.find(".swiper-slide-shadow-right"):c.find(".swiper-slide-shadow-bottom");0===k.length&&(k=b('<div class="swiper-slide-shadow-'+(t.isHorizontal()?"left":"top")+'"></div>'),c.append(k)),0===l.length&&(l=b('<div class="swiper-slide-shadow-'+(t.isHorizontal()?"right":"bottom")+'"></div>'),c.append(l)),k.length&&(k[0].style.opacity=Math.max(-d,0)),l.length&&(l[0].style.opacity=Math.max(d,0))}c.transform("translate3d("+i+"px, "+j+"px, 0px) rotateX("+h+"deg) rotateY("+g+"deg)")}},setTransition:function(a){if(t.slides.transition(a).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(a),t.params.virtualTranslate&&0!==a){var c=!1;t.slides.eq(t.activeIndex).transitionEnd(function(){if(!c&&t&&b(this).hasClass(t.params.slideActiveClass)){c=!0,t.animating=!1;for(var a=["webkitTransitionEnd","transitionend","oTransitionEnd","MSTransitionEnd","msTransitionEnd"],d=0;d<a.length;d++)t.wrapper.trigger(a[d])}})}}},cube:{setTranslate:function(){var a,c=0;t.params.cube.shadow&&(t.isHorizontal()?(a=t.wrapper.find(".swiper-cube-shadow"),0===a.length&&(a=b('<div class="swiper-cube-shadow"></div>'),t.wrapper.append(a)),a.css({height:t.width+"px"})):(a=t.container.find(".swiper-cube-shadow"),0===a.length&&(a=b('<div class="swiper-cube-shadow"></div>'),t.container.append(a))));for(var d=0;d<t.slides.length;d++){var e=t.slides.eq(d),f=90*d,g=Math.floor(f/360);t.rtl&&(f=-f,g=Math.floor(-f/360));var h=Math.max(Math.min(e[0].progress,1),-1),i=0,j=0,k=0;d%4===0?(i=4*-g*t.size,k=0):(d-1)%4===0?(i=0,k=4*-g*t.size):(d-2)%4===0?(i=t.size+4*g*t.size,k=t.size):(d-3)%4===0&&(i=-t.size,k=3*t.size+4*t.size*g),t.rtl&&(i=-i),t.isHorizontal()||(j=i,i=0);var l="rotateX("+(t.isHorizontal()?0:-f)+"deg) rotateY("+(t.isHorizontal()?f:0)+"deg) translate3d("+i+"px, "+j+"px, "+k+"px)";if(h<=1&&h>-1&&(c=90*d+90*h,t.rtl&&(c=90*-d-90*h)),e.transform(l),t.params.cube.slideShadows){var m=t.isHorizontal()?e.find(".swiper-slide-shadow-left"):e.find(".swiper-slide-shadow-top"),n=t.isHorizontal()?e.find(".swiper-slide-shadow-right"):e.find(".swiper-slide-shadow-bottom");0===m.length&&(m=b('<div class="swiper-slide-shadow-'+(t.isHorizontal()?"left":"top")+'"></div>'),e.append(m)),0===n.length&&(n=b('<div class="swiper-slide-shadow-'+(t.isHorizontal()?"right":"bottom")+'"></div>'),e.append(n)),m.length&&(m[0].style.opacity=Math.max(-h,0)),n.length&&(n[0].style.opacity=Math.max(h,0))}}if(t.wrapper.css({"-webkit-transform-origin":"50% 50% -"+t.size/2+"px","-moz-transform-origin":"50% 50% -"+t.size/2+"px","-ms-transform-origin":"50% 50% -"+t.size/2+"px","transform-origin":"50% 50% -"+t.size/2+"px"}),t.params.cube.shadow)if(t.isHorizontal())a.transform("translate3d(0px, "+(t.width/2+t.params.cube.shadowOffset)+"px, "+-t.width/2+"px) rotateX(90deg) rotateZ(0deg) scale("+t.params.cube.shadowScale+")");else{var o=Math.abs(c)-90*Math.floor(Math.abs(c)/90),p=1.5-(Math.sin(2*o*Math.PI/360)/2+Math.cos(2*o*Math.PI/360)/2),q=t.params.cube.shadowScale,r=t.params.cube.shadowScale/p,s=t.params.cube.shadowOffset;a.transform("scale3d("+q+", 1, "+r+") translate3d(0px, "+(t.height/2+s)+"px, "+-t.height/2/r+"px) rotateX(-90deg)")}var u=t.isSafari||t.isUiWebView?-t.size/2:0;t.wrapper.transform("translate3d(0px,0,"+u+"px) rotateX("+(t.isHorizontal()?0:c)+"deg) rotateY("+(t.isHorizontal()?-c:0)+"deg)")},setTransition:function(a){t.slides.transition(a).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(a),t.params.cube.shadow&&!t.isHorizontal()&&t.container.find(".swiper-cube-shadow").transition(a)}},coverflow:{setTranslate:function(){for(var a=t.translate,c=t.isHorizontal()?-a+t.width/2:-a+t.height/2,d=t.isHorizontal()?t.params.coverflow.rotate:-t.params.coverflow.rotate,e=t.params.coverflow.depth,f=0,g=t.slides.length;f<g;f++){var h=t.slides.eq(f),i=t.slidesSizesGrid[f],j=h[0].swiperSlideOffset,k=(c-j-i/2)/i*t.params.coverflow.modifier,l=t.isHorizontal()?d*k:0,m=t.isHorizontal()?0:d*k,n=-e*Math.abs(k),o=t.isHorizontal()?0:t.params.coverflow.stretch*k,p=t.isHorizontal()?t.params.coverflow.stretch*k:0;Math.abs(p)<.001&&(p=0),Math.abs(o)<.001&&(o=0),Math.abs(n)<.001&&(n=0),Math.abs(l)<.001&&(l=0),Math.abs(m)<.001&&(m=0);var q="translate3d("+p+"px,"+o+"px,"+n+"px)  rotateX("+m+"deg) rotateY("+l+"deg)";if(h.transform(q),h[0].style.zIndex=-Math.abs(Math.round(k))+1,t.params.coverflow.slideShadows){var r=t.isHorizontal()?h.find(".swiper-slide-shadow-left"):h.find(".swiper-slide-shadow-top"),s=t.isHorizontal()?h.find(".swiper-slide-shadow-right"):h.find(".swiper-slide-shadow-bottom");0===r.length&&(r=b('<div class="swiper-slide-shadow-'+(t.isHorizontal()?"left":"top")+'"></div>'),h.append(r)),0===s.length&&(s=b('<div class="swiper-slide-shadow-'+(t.isHorizontal()?"right":"bottom")+'"></div>'),h.append(s)),r.length&&(r[0].style.opacity=k>0?k:0),s.length&&(s[0].style.opacity=-k>0?-k:0)}}if(t.browser.ie){var u=t.wrapper[0].style;u.perspectiveOrigin=c+"px 50%"}},setTransition:function(a){t.slides.transition(a).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(a)}}},t.lazy={initialImageLoaded:!1,loadImageInSlide:function(a,c){if("undefined"!=typeof a&&("undefined"==typeof c&&(c=!0),0!==t.slides.length)){var d=t.slides.eq(a),e=d.find(".swiper-lazy:not(.swiper-lazy-loaded):not(.swiper-lazy-loading)");!d.hasClass("swiper-lazy")||d.hasClass("swiper-lazy-loaded")||d.hasClass("swiper-lazy-loading")||(e=e.add(d[0])),0!==e.length&&e.each(function(){var a=b(this);a.addClass("swiper-lazy-loading");var e=a.attr("data-background"),f=a.attr("data-src"),g=a.attr("data-srcset");t.loadImage(a[0],f||e,g,!1,function(){if(e?(a.css("background-image",'url("'+e+'")'),a.removeAttr("data-background")):(g&&(a.attr("srcset",g),a.removeAttr("data-srcset")),f&&(a.attr("src",f),a.removeAttr("data-src"))),a.addClass("swiper-lazy-loaded").removeClass("swiper-lazy-loading"),d.find(".swiper-lazy-preloader, .preloader").remove(),t.params.loop&&c){var b=d.attr("data-swiper-slide-index");if(d.hasClass(t.params.slideDuplicateClass)){var h=t.wrapper.children('[data-swiper-slide-index="'+b+'"]:not(.'+t.params.slideDuplicateClass+")");t.lazy.loadImageInSlide(h.index(),!1)}else{var i=t.wrapper.children("."+t.params.slideDuplicateClass+'[data-swiper-slide-index="'+b+'"]');t.lazy.loadImageInSlide(i.index(),!1)}}t.emit("onLazyImageReady",t,d[0],a[0])}),t.emit("onLazyImageLoad",t,d[0],a[0])})}},load:function(){var a;if(t.params.watchSlidesVisibility)t.wrapper.children("."+t.params.slideVisibleClass).each(function(){t.lazy.loadImageInSlide(b(this).index())});else if(t.params.slidesPerView>1)for(a=t.activeIndex;a<t.activeIndex+t.params.slidesPerView;a++)t.slides[a]&&t.lazy.loadImageInSlide(a);else t.lazy.loadImageInSlide(t.activeIndex);if(t.params.lazyLoadingInPrevNext)if(t.params.slidesPerView>1||t.params.lazyLoadingInPrevNextAmount&&t.params.lazyLoadingInPrevNextAmount>1){console.log("ok ok");var c=t.params.lazyLoadingInPrevNextAmount,d=t.params.slidesPerView,e=Math.min(t.activeIndex+d+Math.max(c,d),t.slides.length),f=Math.max(t.activeIndex-Math.max(d,c),0);for(a=t.activeIndex+t.params.slidesPerView;a<e;a++)t.slides[a]&&t.lazy.loadImageInSlide(a);for(a=f;a<t.activeIndex;a++)t.slides[a]&&t.lazy.loadImageInSlide(a)}else{var g=t.wrapper.children("."+t.params.slideNextClass);g.length>0&&t.lazy.loadImageInSlide(g.index());var h=t.wrapper.children("."+t.params.slidePrevClass);h.length>0&&t.lazy.loadImageInSlide(h.index())}},onTransitionStart:function(){t.params.lazyLoading&&(t.params.lazyLoadingOnTransitionStart||!t.params.lazyLoadingOnTransitionStart&&!t.lazy.initialImageLoaded)&&t.lazy.load()},onTransitionEnd:function(){t.params.lazyLoading&&!t.params.lazyLoadingOnTransitionStart&&t.lazy.load()}},t.scrollbar={isTouched:!1,setDragPosition:function(a){var b=t.scrollbar,c=t.isHorizontal()?"touchstart"===a.type||"touchmove"===a.type?a.targetTouches[0].pageX:a.pageX||a.clientX:"touchstart"===a.type||"touchmove"===a.type?a.targetTouches[0].pageY:a.pageY||a.clientY,d=c-b.track.offset()[t.isHorizontal()?"left":"top"]-b.dragSize/2,e=-t.minTranslate()*b.moveDivider,f=-t.maxTranslate()*b.moveDivider;d<e?d=e:d>f&&(d=f),d=-d/b.moveDivider,t.updateProgress(d),t.setWrapperTranslate(d,!0)},dragStart:function(a){var b=t.scrollbar;b.isTouched=!0,a.preventDefault(),a.stopPropagation(),b.setDragPosition(a),clearTimeout(b.dragTimeout),b.track.transition(0),t.params.scrollbarHide&&b.track.css("opacity",1),t.wrapper.transition(100),b.drag.transition(100),t.emit("onScrollbarDragStart",t)},dragMove:function(a){var b=t.scrollbar;b.isTouched&&(a.preventDefault?a.preventDefault():a.returnValue=!1,b.setDragPosition(a),t.wrapper.transition(0),b.track.transition(0),b.drag.transition(0),t.emit("onScrollbarDragMove",t))},dragEnd:function(a){var b=t.scrollbar;b.isTouched&&(b.isTouched=!1,t.params.scrollbarHide&&(clearTimeout(b.dragTimeout),b.dragTimeout=setTimeout(function(){b.track.css("opacity",0),b.track.transition(400)},1e3)),t.emit("onScrollbarDragEnd",t),t.params.scrollbarSnapOnRelease&&t.slideReset())},enableDraggable:function(){var a=t.scrollbar,c=t.support.touch?a.track:document;b(a.track).on(t.touchEvents.start,a.dragStart),b(c).on(t.touchEvents.move,a.dragMove),b(c).on(t.touchEvents.end,a.dragEnd)},disableDraggable:function(){var a=t.scrollbar,c=t.support.touch?a.track:document;b(a.track).off(t.touchEvents.start,a.dragStart),b(c).off(t.touchEvents.move,a.dragMove),b(c).off(t.touchEvents.end,a.dragEnd)},set:function(){if(t.params.scrollbar){var a=t.scrollbar;a.track=b(t.params.scrollbar),t.params.uniqueNavElements&&"string"==typeof t.params.scrollbar&&a.track.length>1&&1===t.container.find(t.params.scrollbar).length&&(a.track=t.container.find(t.params.scrollbar)),a.drag=a.track.find(".swiper-scrollbar-drag"),0===a.drag.length&&(a.drag=b('<div class="swiper-scrollbar-drag"></div>'),a.track.append(a.drag)),a.drag[0].style.width="",a.drag[0].style.height="",a.trackSize=t.isHorizontal()?a.track[0].offsetWidth:a.track[0].offsetHeight,a.divider=t.size/t.virtualSize,a.moveDivider=a.divider*(a.trackSize/t.size),a.dragSize=a.trackSize*a.divider,t.isHorizontal()?a.drag[0].style.width=a.dragSize+"px":a.drag[0].style.height=a.dragSize+"px",a.divider>=1?a.track[0].style.display="none":a.track[0].style.display="",t.params.scrollbarHide&&(a.track[0].style.opacity=0)}},setTranslate:function(){if(t.params.scrollbar){var a,b=t.scrollbar,c=(t.translate||0,b.dragSize);a=(b.trackSize-b.dragSize)*t.progress,t.rtl&&t.isHorizontal()?(a=-a,a>0?(c=b.dragSize-a,a=0):-a+b.dragSize>b.trackSize&&(c=b.trackSize+a)):a<0?(c=b.dragSize+a,a=0):a+b.dragSize>b.trackSize&&(c=b.trackSize-a),t.isHorizontal()?(t.support.transforms3d?b.drag.transform("translate3d("+a+"px, 0, 0)"):b.drag.transform("translateX("+a+"px)"),b.drag[0].style.width=c+"px"):(t.support.transforms3d?b.drag.transform("translate3d(0px, "+a+"px, 0)"):b.drag.transform("translateY("+a+"px)"),b.drag[0].style.height=c+"px"),t.params.scrollbarHide&&(clearTimeout(b.timeout),b.track[0].style.opacity=1,b.timeout=setTimeout(function(){b.track[0].style.opacity=0,b.track.transition(400)},1e3))}},setTransition:function(a){t.params.scrollbar&&t.scrollbar.drag.transition(a)}},t.controller={LinearSpline:function(a,b){this.x=a,this.y=b,this.lastIndex=a.length-1;var c,d;this.x.length;this.interpolate=function(a){return a?(d=e(this.x,a),c=d-1,(a-this.x[c])*(this.y[d]-this.y[c])/(this.x[d]-this.x[c])+this.y[c]):0};var e=function(){var a,b,c;return function(d,e){for(b=-1,a=d.length;a-b>1;)d[c=a+b>>1]<=e?b=c:a=c;return a}}()},getInterpolateFunction:function(a){t.controller.spline||(t.controller.spline=t.params.loop?new t.controller.LinearSpline(t.slidesGrid,a.slidesGrid):new t.controller.LinearSpline(t.snapGrid,a.snapGrid))},setTranslate:function(a,b){function d(b){a=b.rtl&&"horizontal"===b.params.direction?-t.translate:t.translate,"slide"===t.params.controlBy&&(t.controller.getInterpolateFunction(b),f=-t.controller.spline.interpolate(-a)),f&&"container"!==t.params.controlBy||(e=(b.maxTranslate()-b.minTranslate())/(t.maxTranslate()-t.minTranslate()),f=(a-t.minTranslate())*e+b.minTranslate()),t.params.controlInverse&&(f=b.maxTranslate()-f),b.updateProgress(f),b.setWrapperTranslate(f,!1,t),b.updateActiveIndex()}var e,f,g=t.params.control;if(t.isArray(g))for(var h=0;h<g.length;h++)g[h]!==b&&g[h]instanceof c&&d(g[h]);else g instanceof c&&b!==g&&d(g)},setTransition:function(a,b){function d(b){b.setWrapperTransition(a,t),0!==a&&(b.onTransitionStart(),b.wrapper.transitionEnd(function(){f&&(b.params.loop&&"slide"===t.params.controlBy&&b.fixLoop(),b.onTransitionEnd())}))}var e,f=t.params.control;if(t.isArray(f))for(e=0;e<f.length;e++)f[e]!==b&&f[e]instanceof c&&d(f[e]);else f instanceof c&&b!==f&&d(f)}},t.hashnav={init:function(){if(t.params.hashnav){t.hashnav.initialized=!0;var a=document.location.hash.replace("#","");if(a)for(var b=0,c=0,d=t.slides.length;c<d;c++){var e=t.slides.eq(c),f=e.attr("data-hash");if(f===a&&!e.hasClass(t.params.slideDuplicateClass)){var g=e.index();t.slideTo(g,b,t.params.runCallbacksOnInit,!0)}}}},setHash:function(){t.hashnav.initialized&&t.params.hashnav&&(document.location.hash=t.slides.eq(t.activeIndex).attr("data-hash")||"")}},t.disableKeyboardControl=function(){t.params.keyboardControl=!1,b(document).off("keydown",i)},t.enableKeyboardControl=function(){t.params.keyboardControl=!0,b(document).on("keydown",i)},t.mousewheel={event:!1,lastScrollTime:(new window.Date).getTime()},t.params.mousewheelControl){try{new window.WheelEvent("wheel"),t.mousewheel.event="wheel"}catch(L){(window.WheelEvent||t.container[0]&&"wheel"in t.container[0])&&(t.mousewheel.event="wheel")}!t.mousewheel.event&&window.WheelEvent,t.mousewheel.event||void 0===document.onmousewheel||(t.mousewheel.event="mousewheel"),t.mousewheel.event||(t.mousewheel.event="DOMMouseScroll")}t.disableMousewheelControl=function(){return!!t.mousewheel.event&&(t.container.off(t.mousewheel.event,j),!0)},t.enableMousewheelControl=function(){return!!t.mousewheel.event&&(t.container.on(t.mousewheel.event,j),!0)},t.parallax={setTranslate:function(){t.container.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(){k(this,t.progress)}),t.slides.each(function(){var a=b(this);a.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(){var b=Math.min(Math.max(a[0].progress,-1),1);k(this,b)})})},setTransition:function(a){"undefined"==typeof a&&(a=t.params.speed),t.container.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(){var c=b(this),d=parseInt(c.attr("data-swiper-parallax-duration"),10)||a;0===a&&(d=0),c.transition(d)})}},t._plugins=[];for(var M in t.plugins){var N=t.plugins[M](t,t.params[M]);N&&t._plugins.push(N)}return t.callPlugins=function(a){for(var b=0;b<t._plugins.length;b++)a in t._plugins[b]&&t._plugins[b][a](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5])},t.emitterEventListeners={},t.emit=function(a){t.params[a]&&t.params[a](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]);var b;if(t.emitterEventListeners[a])for(b=0;b<t.emitterEventListeners[a].length;b++)t.emitterEventListeners[a][b](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]);t.callPlugins&&t.callPlugins(a,arguments[1],arguments[2],arguments[3],arguments[4],arguments[5])},t.on=function(a,b){return a=l(a),t.emitterEventListeners[a]||(t.emitterEventListeners[a]=[]),t.emitterEventListeners[a].push(b),t},t.off=function(a,b){var c;if(a=l(a),"undefined"==typeof b)return t.emitterEventListeners[a]=[],t;if(t.emitterEventListeners[a]&&0!==t.emitterEventListeners[a].length){for(c=0;c<t.emitterEventListeners[a].length;c++)t.emitterEventListeners[a][c]===b&&t.emitterEventListeners[a].splice(c,1);
return t}},t.once=function(a,b){a=l(a);var c=function(){b(arguments[0],arguments[1],arguments[2],arguments[3],arguments[4]),t.off(a,c)};return t.on(a,c),t},t.a11y={makeFocusable:function(a){return a.attr("tabIndex","0"),a},addRole:function(a,b){return a.attr("role",b),a},addLabel:function(a,b){return a.attr("aria-label",b),a},disable:function(a){return a.attr("aria-disabled",!0),a},enable:function(a){return a.attr("aria-disabled",!1),a},onEnterKey:function(a){13===a.keyCode&&(b(a.target).is(t.params.nextButton)?(t.onClickNext(a),t.isEnd?t.a11y.notify(t.params.lastSlideMessage):t.a11y.notify(t.params.nextSlideMessage)):b(a.target).is(t.params.prevButton)&&(t.onClickPrev(a),t.isBeginning?t.a11y.notify(t.params.firstSlideMessage):t.a11y.notify(t.params.prevSlideMessage)),b(a.target).is("."+t.params.bulletClass)&&b(a.target)[0].click())},liveRegion:b('<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>'),notify:function(a){var b=t.a11y.liveRegion;0!==b.length&&(b.html(""),b.html(a))},init:function(){t.params.nextButton&&t.nextButton&&t.nextButton.length>0&&(t.a11y.makeFocusable(t.nextButton),t.a11y.addRole(t.nextButton,"button"),t.a11y.addLabel(t.nextButton,t.params.nextSlideMessage)),t.params.prevButton&&t.prevButton&&t.prevButton.length>0&&(t.a11y.makeFocusable(t.prevButton),t.a11y.addRole(t.prevButton,"button"),t.a11y.addLabel(t.prevButton,t.params.prevSlideMessage)),b(t.container).append(t.a11y.liveRegion)},initPagination:function(){t.params.pagination&&t.params.paginationClickable&&t.bullets&&t.bullets.length&&t.bullets.each(function(){var a=b(this);t.a11y.makeFocusable(a),t.a11y.addRole(a,"button"),t.a11y.addLabel(a,t.params.paginationBulletMessage.replace(/{{index}}/,a.index()+1))})},destroy:function(){t.a11y.liveRegion&&t.a11y.liveRegion.length>0&&t.a11y.liveRegion.remove()}},t.init=function(){t.params.loop&&t.createLoop(),t.updateContainerSize(),t.updateSlidesSize(),t.updatePagination(),t.params.scrollbar&&t.scrollbar&&(t.scrollbar.set(),t.params.scrollbarDraggable&&t.scrollbar.enableDraggable()),"slide"!==t.params.effect&&t.effects[t.params.effect]&&(t.params.loop||t.updateProgress(),t.effects[t.params.effect].setTranslate()),t.params.loop?t.slideTo(t.params.initialSlide+t.loopedSlides,0,t.params.runCallbacksOnInit):(t.slideTo(t.params.initialSlide,0,t.params.runCallbacksOnInit),0===t.params.initialSlide&&(t.parallax&&t.params.parallax&&t.parallax.setTranslate(),t.lazy&&t.params.lazyLoading&&(t.lazy.load(),t.lazy.initialImageLoaded=!0))),t.attachEvents(),t.params.observer&&t.support.observer&&t.initObservers(),t.params.preloadImages&&!t.params.lazyLoading&&t.preloadImages(),t.params.autoplay&&t.startAutoplay(),t.params.keyboardControl&&t.enableKeyboardControl&&t.enableKeyboardControl(),t.params.mousewheelControl&&t.enableMousewheelControl&&t.enableMousewheelControl(),t.params.hashnav&&t.hashnav&&t.hashnav.init(),t.params.a11y&&t.a11y&&t.a11y.init(),t.emit("onInit",t)},t.cleanupStyles=function(){t.container.removeClass(t.classNames.join(" ")).removeAttr("style"),t.wrapper.removeAttr("style"),t.slides&&t.slides.length&&t.slides.removeClass([t.params.slideVisibleClass,t.params.slideActiveClass,t.params.slideNextClass,t.params.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-column").removeAttr("data-swiper-row"),t.paginationContainer&&t.paginationContainer.length&&t.paginationContainer.removeClass(t.params.paginationHiddenClass),t.bullets&&t.bullets.length&&t.bullets.removeClass(t.params.bulletActiveClass),t.params.prevButton&&b(t.params.prevButton).removeClass(t.params.buttonDisabledClass),t.params.nextButton&&b(t.params.nextButton).removeClass(t.params.buttonDisabledClass),t.params.scrollbar&&t.scrollbar&&(t.scrollbar.track&&t.scrollbar.track.length&&t.scrollbar.track.removeAttr("style"),t.scrollbar.drag&&t.scrollbar.drag.length&&t.scrollbar.drag.removeAttr("style"))},t.destroy=function(a,b){t.detachEvents(),t.stopAutoplay(),t.params.scrollbar&&t.scrollbar&&t.params.scrollbarDraggable&&t.scrollbar.disableDraggable(),t.params.loop&&t.destroyLoop(),b&&t.cleanupStyles(),t.disconnectObservers(),t.params.keyboardControl&&t.disableKeyboardControl&&t.disableKeyboardControl(),t.params.mousewheelControl&&t.disableMousewheelControl&&t.disableMousewheelControl(),t.params.a11y&&t.a11y&&t.a11y.destroy(),t.emit("onDestroy"),a!==!1&&(t=null)},t.init(),t}};c.prototype={isSafari:function(){var a=navigator.userAgent.toLowerCase();return a.indexOf("safari")>=0&&a.indexOf("chrome")<0&&a.indexOf("android")<0}(),isUiWebView:/(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent),isArray:function(a){return"[object Array]"===Object.prototype.toString.apply(a)},browser:{ie:window.navigator.pointerEnabled||window.navigator.msPointerEnabled,ieTouch:window.navigator.msPointerEnabled&&window.navigator.msMaxTouchPoints>1||window.navigator.pointerEnabled&&window.navigator.maxTouchPoints>1},device:function(){var a=navigator.userAgent,b=a.match(/(Android);?[\s\/]+([\d.]+)?/),c=a.match(/(iPad).*OS\s([\d_]+)/),d=a.match(/(iPod)(.*OS\s([\d_]+))?/),e=!c&&a.match(/(iPhone\sOS)\s([\d_]+)/);return{ios:c||e||d,android:b}}(),support:{touch:window.Modernizr&&Modernizr.touch===!0||function(){return!!("ontouchstart"in window||window.DocumentTouch&&document instanceof DocumentTouch)}(),transforms3d:window.Modernizr&&Modernizr.csstransforms3d===!0||function(){var a=document.createElement("div").style;return"webkitPerspective"in a||"MozPerspective"in a||"OPerspective"in a||"MsPerspective"in a||"perspective"in a}(),flexbox:function(){for(var a=document.createElement("div").style,b="alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "),c=0;c<b.length;c++)if(b[c]in a)return!0}(),observer:function(){return"MutationObserver"in window||"WebkitMutationObserver"in window}()},plugins:{}};for(var d=["jQuery","Zepto","Dom7"],e=0;e<d.length;e++)window[d[e]]&&a(window[d[e]]);var f;f="undefined"==typeof Dom7?window.Dom7||window.Zepto||window.jQuery:Dom7,f&&("transitionEnd"in f.fn||(f.fn.transitionEnd=function(a){function b(f){if(f.target===this)for(a.call(this,f),c=0;c<d.length;c++)e.off(d[c],b)}var c,d=["webkitTransitionEnd","transitionend","oTransitionEnd","MSTransitionEnd","msTransitionEnd"],e=this;if(a)for(c=0;c<d.length;c++)e.on(d[c],b);return this}),"transform"in f.fn||(f.fn.transform=function(a){for(var b=0;b<this.length;b++){var c=this[b].style;c.webkitTransform=c.MsTransform=c.msTransform=c.MozTransform=c.OTransform=c.transform=a}return this}),"transition"in f.fn||(f.fn.transition=function(a){"string"!=typeof a&&(a+="ms");for(var b=0;b<this.length;b++){var c=this[b].style;c.webkitTransitionDuration=c.MsTransitionDuration=c.msTransitionDuration=c.MozTransitionDuration=c.OTransitionDuration=c.transitionDuration=a}return this})),window.Swiper=c}(),"undefined"!=typeof module?module.exports=window.Swiper:"function"==typeof define&&define.amd&&define([],function(){"use strict";return window.Swiper}),function(){"use strict";/**
	 * @preserve FastClick: polyfill to remove click delays on browsers with touch UIs.
	 *
	 * @codingstandard ftlabs-jsv2
	 * @copyright The Financial Times Limited [All Rights Reserved]
	 * @license MIT License (see LICENSE.txt)
	 */
function a(b,d){function e(a,b){return function(){return a.apply(b,arguments)}}var f;if(d=d||{},this.trackingClick=!1,this.trackingClickStart=0,this.targetElement=null,this.touchStartX=0,this.touchStartY=0,this.lastTouchIdentifier=0,this.touchBoundary=d.touchBoundary||10,this.layer=b,this.tapDelay=d.tapDelay||200,this.tapTimeout=d.tapTimeout||700,!a.notNeeded(b)){for(var g=["onMouse","onClick","onTouchStart","onTouchMove","onTouchEnd","onTouchCancel"],h=this,i=0,j=g.length;i<j;i++)h[g[i]]=e(h[g[i]],h);c&&(b.addEventListener("mouseover",this.onMouse,!0),b.addEventListener("mousedown",this.onMouse,!0),b.addEventListener("mouseup",this.onMouse,!0)),b.addEventListener("click",this.onClick,!0),b.addEventListener("touchstart",this.onTouchStart,!1),b.addEventListener("touchmove",this.onTouchMove,!1),b.addEventListener("touchend",this.onTouchEnd,!1),b.addEventListener("touchcancel",this.onTouchCancel,!1),Event.prototype.stopImmediatePropagation||(b.removeEventListener=function(a,c,d){var e=Node.prototype.removeEventListener;"click"===a?e.call(b,a,c.hijacked||c,d):e.call(b,a,c,d)},b.addEventListener=function(a,c,d){var e=Node.prototype.addEventListener;"click"===a?e.call(b,a,c.hijacked||(c.hijacked=function(a){a.propagationStopped||c(a)}),d):e.call(b,a,c,d)}),"function"==typeof b.onclick&&(f=b.onclick,b.addEventListener("click",function(a){f(a)},!1),b.onclick=null)}}var b=navigator.userAgent.indexOf("Windows Phone")>=0,c=navigator.userAgent.indexOf("Android")>0&&!b,d=/iP(ad|hone|od)/.test(navigator.userAgent)&&!b,e=d&&/OS 4_\d(_\d)?/.test(navigator.userAgent),f=d&&/OS [6-7]_\d/.test(navigator.userAgent),g=navigator.userAgent.indexOf("BB10")>0;a.prototype.needsClick=function(a){switch(a.nodeName.toLowerCase()){case"button":case"select":case"textarea":if(a.disabled)return!0;break;case"input":if(d&&"file"===a.type||a.disabled)return!0;break;case"label":case"iframe":case"video":return!0}return/\bneedsclick\b/.test(a.className)},a.prototype.needsFocus=function(a){switch(a.nodeName.toLowerCase()){case"textarea":return!0;case"select":return!c;case"input":switch(a.type){case"button":case"checkbox":case"file":case"image":case"radio":case"submit":return!1}return!a.disabled&&!a.readOnly;default:return/\bneedsfocus\b/.test(a.className)}},a.prototype.sendClick=function(a,b){var c,d;document.activeElement&&document.activeElement!==a&&document.activeElement.blur(),d=b.changedTouches[0],c=document.createEvent("MouseEvents"),c.initMouseEvent(this.determineEventType(a),!0,!0,window,1,d.screenX,d.screenY,d.clientX,d.clientY,!1,!1,!1,!1,0,null),c.forwardedTouchEvent=!0,a.dispatchEvent(c)},a.prototype.determineEventType=function(a){return c&&"select"===a.tagName.toLowerCase()?"mousedown":"click"},a.prototype.focus=function(a){var b;d&&a.setSelectionRange&&0!==a.type.indexOf("date")&&"time"!==a.type&&"month"!==a.type?(b=a.value.length,a.setSelectionRange(b,b)):a.focus()},a.prototype.updateScrollParent=function(a){var b,c;if(b=a.fastClickScrollParent,!b||!b.contains(a)){c=a;do{if(c.scrollHeight>c.offsetHeight){b=c,a.fastClickScrollParent=c;break}c=c.parentElement}while(c)}b&&(b.fastClickLastScrollTop=b.scrollTop)},a.prototype.getTargetElementFromEventTarget=function(a){return a.nodeType===Node.TEXT_NODE?a.parentNode:a},a.prototype.onTouchStart=function(a){var b,c,f;if(a.targetTouches.length>1)return!0;if(b=this.getTargetElementFromEventTarget(a.target),c=a.targetTouches[0],d){if(f=window.getSelection(),f.rangeCount&&!f.isCollapsed)return!0;if(!e){if(c.identifier&&c.identifier===this.lastTouchIdentifier)return a.preventDefault(),!1;this.lastTouchIdentifier=c.identifier,this.updateScrollParent(b)}}return this.trackingClick=!0,this.trackingClickStart=a.timeStamp,this.targetElement=b,this.touchStartX=c.pageX,this.touchStartY=c.pageY,a.timeStamp-this.lastClickTime<this.tapDelay&&a.preventDefault(),!0},a.prototype.touchHasMoved=function(a){var b=a.changedTouches[0],c=this.touchBoundary;return Math.abs(b.pageX-this.touchStartX)>c||Math.abs(b.pageY-this.touchStartY)>c},a.prototype.onTouchMove=function(a){return!this.trackingClick||((this.targetElement!==this.getTargetElementFromEventTarget(a.target)||this.touchHasMoved(a))&&(this.trackingClick=!1,this.targetElement=null),!0)},a.prototype.findControl=function(a){return void 0!==a.control?a.control:a.htmlFor?document.getElementById(a.htmlFor):a.querySelector("button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea")},a.prototype.onTouchEnd=function(a){var b,g,h,i,j,k=this.targetElement;if(!this.trackingClick)return!0;if(a.timeStamp-this.lastClickTime<this.tapDelay)return this.cancelNextClick=!0,!0;if(a.timeStamp-this.trackingClickStart>this.tapTimeout)return!0;if(this.cancelNextClick=!1,this.lastClickTime=a.timeStamp,g=this.trackingClickStart,this.trackingClick=!1,this.trackingClickStart=0,f&&(j=a.changedTouches[0],k=document.elementFromPoint(j.pageX-window.pageXOffset,j.pageY-window.pageYOffset)||k,k.fastClickScrollParent=this.targetElement.fastClickScrollParent),h=k.tagName.toLowerCase(),"label"===h){if(b=this.findControl(k)){if(this.focus(k),c)return!1;k=b}}else if(this.needsFocus(k))return a.timeStamp-g>100||d&&window.top!==window&&"input"===h?(this.targetElement=null,!1):(this.focus(k),this.sendClick(k,a),d&&"select"===h||(this.targetElement=null,a.preventDefault()),!1);return!(!d||e||(i=k.fastClickScrollParent,!i||i.fastClickLastScrollTop===i.scrollTop))||(this.needsClick(k)||(a.preventDefault(),this.sendClick(k,a)),!1)},a.prototype.onTouchCancel=function(){this.trackingClick=!1,this.targetElement=null},a.prototype.onMouse=function(a){return!this.targetElement||(!!a.forwardedTouchEvent||(!a.cancelable||(!(!this.needsClick(this.targetElement)||this.cancelNextClick)||(a.stopImmediatePropagation?a.stopImmediatePropagation():a.propagationStopped=!0,a.stopPropagation(),a.preventDefault(),!1))))},a.prototype.onClick=function(a){var b;return this.trackingClick?(this.targetElement=null,this.trackingClick=!1,!0):"submit"===a.target.type&&0===a.detail||(b=this.onMouse(a),b||(this.targetElement=null),b)},a.prototype.destroy=function(){var a=this.layer;c&&(a.removeEventListener("mouseover",this.onMouse,!0),a.removeEventListener("mousedown",this.onMouse,!0),a.removeEventListener("mouseup",this.onMouse,!0)),a.removeEventListener("click",this.onClick,!0),a.removeEventListener("touchstart",this.onTouchStart,!1),a.removeEventListener("touchmove",this.onTouchMove,!1),a.removeEventListener("touchend",this.onTouchEnd,!1),a.removeEventListener("touchcancel",this.onTouchCancel,!1)},a.notNeeded=function(a){var b,d,e,f;if("undefined"==typeof window.ontouchstart)return!0;if(d=+(/Chrome\/([0-9]+)/.exec(navigator.userAgent)||[,0])[1]){if(!c)return!0;if(b=document.querySelector("meta[name=viewport]")){if(b.content.indexOf("user-scalable=no")!==-1)return!0;if(d>31&&document.documentElement.scrollWidth<=window.outerWidth)return!0}}if(g&&(e=navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/),e[1]>=10&&e[2]>=3&&(b=document.querySelector("meta[name=viewport]")))){if(b.content.indexOf("user-scalable=no")!==-1)return!0;if(document.documentElement.scrollWidth<=window.outerWidth)return!0}return"none"===a.style.msTouchAction||"manipulation"===a.style.touchAction||(f=+(/Firefox\/([0-9]+)/.exec(navigator.userAgent)||[,0])[1],!!(f>=27&&(b=document.querySelector("meta[name=viewport]"),b&&(b.content.indexOf("user-scalable=no")!==-1||document.documentElement.scrollWidth<=window.outerWidth)))||("none"===a.style.touchAction||"manipulation"===a.style.touchAction))},a.attach=function(b,c){return new a(b,c)},"function"==typeof define&&"object"==typeof define.amd&&define.amd?define(function(){return a}):"undefined"!=typeof module&&module.exports?(module.exports=a.attach,module.exports.FastClick=a):window.FastClick=a}(),function(a,b,c){"use strict";var d,e=a.lazySizes&&lazySizes.cfg||a.lazySizesConfig,f=b.createElement("img"),g="sizes"in f&&"srcset"in f,h=/\s+\d+h/g,i=function(){var a=/\s+(\d+)(w|h)\s+(\d+)(w|h)/,c=Array.prototype.forEach;return function(d){var e=b.createElement("img"),f=function(b){var c,d=b.getAttribute(lazySizesConfig.srcsetAttr);d&&(d.match(a)&&(c="w"==RegExp.$2?RegExp.$1/RegExp.$3:RegExp.$3/RegExp.$1,c&&b.setAttribute("data-aspectratio",c)),b.setAttribute(lazySizesConfig.srcsetAttr,d.replace(h,"")))},g=function(a){var b=a.target.parentNode;b&&"PICTURE"==b.nodeName&&c.call(b.getElementsByTagName("source"),f),f(a.target)},i=function(){e.currentSrc&&b.removeEventListener("lazybeforeunveil",g)};d[1]&&(b.addEventListener("lazybeforeunveil",g),e.onload=i,e.onerror=i,e.srcset="data:,a 1w 1h",e.complete&&i())}}();if(e||(e={},a.lazySizesConfig=e),e.supportsType||(e.supportsType=function(a){return!a}),!a.picturefill&&!e.pf){if(a.HTMLPictureElement&&g)return b.msElementsFromPoint&&i(navigator.userAgent.match(/Edge\/(\d+)/)),void(e.pf=function(){});e.pf=function(b){var c,e;if(!a.picturefill)for(c=0,e=b.elements.length;c<e;c++)d(b.elements[c])},d=function(){var c=function(a,b){return a.w-b.w},f=/^\s*\d+\.*\d*px\s*$/,i=function(a){var b,c,d=a.length,e=a[d-1],f=0;for(f;f<d;f++)if(e=a[f],e.d=e.w/a.w,e.d>=a.d){!e.cached&&(b=a[f-1])&&b.d>a.d-.13*Math.pow(a.d,2.2)&&(c=Math.pow(b.d-.6,1.6),b.cached&&(b.d+=.15*c),b.d+(e.d-a.d)*c>a.d&&(e=b));break}return e},j=function(){var a,b=/(([^,\s].[^\s]+)\s+(\d+)w)/g,c=/\s/,d=function(b,c,d,e){a.push({c:c,u:d,w:1*e})};return function(e){return a=[],e=e.trim(),e.replace(h,"").replace(b,d),a.length||!e||c.test(e)||a.push({c:e,u:e,w:99}),a}}(),k=function(){k.init||(k.init=!0,addEventListener("resize",function(){var a,c=b.getElementsByClassName("lazymatchmedia"),e=function(){var a,b;for(a=0,b=c.length;a<b;a++)d(c[a])};return function(){clearTimeout(a),a=setTimeout(e,66)}}()))},l=function(b,c){var d,f=b.getAttribute("srcset")||b.getAttribute(e.srcsetAttr);!f&&c&&(f=b._lazypolyfill?b._lazypolyfill._set:b.getAttribute(e.srcAttr)||b.getAttribute("src")),b._lazypolyfill&&b._lazypolyfill._set==f||(d=j(f||""),c&&b.parentNode&&(d.isPicture="PICTURE"==b.parentNode.nodeName.toUpperCase(),d.isPicture&&a.matchMedia&&(lazySizes.aC(b,"lazymatchmedia"),k())),d._set=f,Object.defineProperty(b,"_lazypolyfill",{value:d,writable:!0}))},m=function(b){var c=a.devicePixelRatio||1,d=lazySizes.getX&&lazySizes.getX(b);return Math.min(d||c,2.5,c)},n=function(b){return a.matchMedia?(n=function(a){return!a||(matchMedia(a)||{}).matches})(b):!b},o=function(a){var b,d,g,h,j,k,o;if(h=a,l(h,!0),j=h._lazypolyfill,j.isPicture)for(d=0,b=a.parentNode.getElementsByTagName("source"),g=b.length;d<g;d++)if(e.supportsType(b[d].getAttribute("type"),a)&&n(b[d].getAttribute("media"))){h=b[d],l(h),j=h._lazypolyfill;break}return j.length>1?(o=h.getAttribute("sizes")||"",o=f.test(o)&&parseInt(o,10)||lazySizes.gW(a,a.parentNode),j.d=m(a),!j.src||!j.w||j.w<o?(j.w=o,k=i(j.sort(c)),j.src=k):k=j.src):k=j[0],k},p=function(a){if(!g||!a.parentNode||"PICTURE"==a.parentNode.nodeName.toUpperCase()){var b=o(a);b&&b.u&&a._lazypolyfill.cur!=b.u&&(a._lazypolyfill.cur=b.u,b.cached=!0,a.setAttribute(e.srcAttr,b.u),a.setAttribute("src",b.u))}};return p.parse=j,p}(),e.loadedClass&&e.loadingClass&&!function(){var a=[];['img[sizes$="px"][srcset].',"picture > img:not([srcset])."].forEach(function(b){a.push(b+e.loadedClass),a.push(b+e.loadingClass)}),e.pf({elements:b.querySelectorAll(a.join(", "))})}()}}(window,document),function(a){"use strict";var b,c=a.createElement("img");!("srcset"in c)||"sizes"in c||window.HTMLPictureElement||(b=/^picture$/i,a.addEventListener("lazybeforeunveil",function(c){var d,e,f,g,h,i,j;!c.defaultPrevented&&!lazySizesConfig.noIOSFix&&(d=c.target)&&(f=d.getAttribute(lazySizesConfig.srcsetAttr))&&(e=d.parentNode)&&((h=b.test(e.nodeName||""))||(g=d.getAttribute("sizes")||d.getAttribute(lazySizesConfig.sizesAttr)))&&(i=h?e:a.createElement("picture"),d._lazyImgSrc||Object.defineProperty(d,"_lazyImgSrc",{value:a.createElement("source"),writable:!0}),j=d._lazyImgSrc,g&&j.setAttribute("sizes",g),j.setAttribute(lazySizesConfig.srcsetAttr,f),d.setAttribute("data-pfsrcset",f),d.removeAttribute(lazySizesConfig.srcsetAttr),h||(e.insertBefore(i,d),i.appendChild(d)),i.insertBefore(j,d))}))}(document),function(a,b){var c=b(a,a.document);a.lazySizes=c,"object"==typeof module&&module.exports&&(module.exports=c)}(window,function(a,b){"use strict";if(b.getElementsByClassName){var c,d=b.documentElement,e=a.Date,f=a.HTMLPictureElement,g="addEventListener",h="getAttribute",i=a[g],j=a.setTimeout,k=a.requestAnimationFrame||j,l=a.requestIdleCallback,m=/^picture$/i,n=["load","error","lazyincluded","_lazyloaded"],o={},p=Array.prototype.forEach,q=function(a,b){return o[b]||(o[b]=new RegExp("(\\s|^)"+b+"(\\s|$)")),o[b].test(a[h]("class")||"")&&o[b]},r=function(a,b){q(a,b)||a.setAttribute("class",(a[h]("class")||"").trim()+" "+b)},s=function(a,b){var c;(c=q(a,b))&&a.setAttribute("class",(a[h]("class")||"").replace(c," "))},t=function(a,b,c){var d=c?g:"removeEventListener";c&&t(a,b),n.forEach(function(c){a[d](c,b)})},u=function(a,c,d,e,f){var g=b.createEvent("CustomEvent");return g.initCustomEvent(c,!e,!f,d||{}),a.dispatchEvent(g),g},v=function(b,d){var e;!f&&(e=a.picturefill||c.pf)?e({reevaluate:!0,elements:[b]}):d&&d.src&&(b.src=d.src)},w=function(a,b){return(getComputedStyle(a,null)||{})[b]},x=function(a,b,d){for(d=d||a.offsetWidth;d<c.minSize&&b&&!a._lazysizesWidth;)d=b.offsetWidth,b=b.parentNode;return d},y=function(){var a,c,d=[],e=[],f=d,g=function(){var b=f;for(f=d.length?e:d,a=!0,c=!1;b.length;)b.shift()();a=!1},h=function(d,e){a&&!e?d.apply(this,arguments):(f.push(d),c||(c=!0,(b.hidden?j:k)(g)))};return h._lsFlush=g,h}(),z=function(a,b){return b?function(){y(a)}:function(){var b=this,c=arguments;y(function(){a.apply(b,c)})}},A=function(a){var b,c=0,d=125,f=666,g=f,h=function(){b=!1,c=e.now(),a()},i=l?function(){l(h,{timeout:g}),g!==f&&(g=f)}:z(function(){j(h)},!0);return function(a){var f;(a=a===!0)&&(g=44),b||(b=!0,f=d-(e.now()-c),f<0&&(f=0),a||f<9&&l?i():j(i,f))}},B=function(a){var b,c,d=99,f=function(){b=null,a()},g=function(){var a=e.now()-c;a<d?j(g,d-a):(l||f)(f)};return function(){c=e.now(),b||(b=j(g,d))}},C=function(){var f,k,l,n,o,x,C,E,F,G,H,I,J,K,L,M=/^img$/i,N=/^iframe$/i,O="onscroll"in a&&!/glebot/.test(navigator.userAgent),P=0,Q=0,R=0,S=-1,T=function(a){R--,a&&a.target&&t(a.target,T),(!a||R<0||!a.target)&&(R=0)},U=function(a,c){var e,f=a,g="hidden"==w(b.body,"visibility")||"hidden"!=w(a,"visibility");for(F-=c,I+=c,G-=c,H+=c;g&&(f=f.offsetParent)&&f!=b.body&&f!=d;)g=(w(f,"opacity")||1)>0,g&&"visible"!=w(f,"overflow")&&(e=f.getBoundingClientRect(),g=H>e.left&&G<e.right&&I>e.top-1&&F<e.bottom+1);return g},V=function(){var a,e,g,i,j,m,n,p,q;if((o=c.loadMode)&&R<8&&(a=f.length)){e=0,S++,null==K&&("expand"in c||(c.expand=d.clientHeight>500&&d.clientWidth>500?500:370),J=c.expand,K=J*c.expFactor),Q<K&&R<1&&S>2&&o>2&&!b.hidden?(Q=K,S=0):Q=o>1&&S>1&&R<6?J:P;for(;e<a;e++)if(f[e]&&!f[e]._lazyRace)if(O)if((p=f[e][h]("data-expand"))&&(m=1*p)||(m=Q),q!==m&&(C=innerWidth+m*L,E=innerHeight+m,n=m*-1,q=m),g=f[e].getBoundingClientRect(),(I=g.bottom)>=n&&(F=g.top)<=E&&(H=g.right)>=n*L&&(G=g.left)<=C&&(I||H||G||F)&&(l&&R<3&&!p&&(o<3||S<4)||U(f[e],m))){if(ba(f[e]),j=!0,R>9)break}else!j&&l&&!i&&R<4&&S<4&&o>2&&(k[0]||c.preloadAfterLoad)&&(k[0]||!p&&(I||H||G||F||"auto"!=f[e][h](c.sizesAttr)))&&(i=k[0]||f[e]);else ba(f[e]);i&&!j&&ba(i)}},W=A(V),X=function(a){r(a.target,c.loadedClass),s(a.target,c.loadingClass),t(a.target,Z)},Y=z(X),Z=function(a){Y({target:a.target})},$=function(a,b){try{a.contentWindow.location.replace(b)}catch(c){a.src=b}},_=function(a){var b,d,e=a[h](c.srcsetAttr);(b=c.customMedia[a[h]("data-media")||a[h]("media")])&&a.setAttribute("media",b),e&&a.setAttribute("srcset",e),b&&(d=a.parentNode,d.insertBefore(a.cloneNode(),a),d.removeChild(a))},aa=z(function(a,b,d,e,f){var g,i,k,l,o,q;(o=u(a,"lazybeforeunveil",b)).defaultPrevented||(e&&(d?r(a,c.autosizesClass):a.setAttribute("sizes",e)),i=a[h](c.srcsetAttr),g=a[h](c.srcAttr),f&&(k=a.parentNode,l=k&&m.test(k.nodeName||"")),q=b.firesLoad||"src"in a&&(i||g||l),o={target:a},q&&(t(a,T,!0),clearTimeout(n),n=j(T,2500),r(a,c.loadingClass),t(a,Z,!0)),l&&p.call(k.getElementsByTagName("source"),_),i?a.setAttribute("srcset",i):g&&!l&&(N.test(a.nodeName)?$(a,g):a.src=g),(i||l)&&v(a,{src:g})),a._lazyRace&&delete a._lazyRace,s(a,c.lazyClass),y(function(){(!q||a.complete&&a.naturalWidth>1)&&(q?T(o):R--,X(o))},!0)}),ba=function(a){var b,d=M.test(a.nodeName),e=d&&(a[h](c.sizesAttr)||a[h]("sizes")),f="auto"==e;(!f&&l||!d||!a.src&&!a.srcset||a.complete||q(a,c.errorClass))&&(b=u(a,"lazyunveilread").detail,f&&D.updateElem(a,!0,a.offsetWidth),a._lazyRace=!0,R++,aa(a,b,f,e,d))},ca=function(){if(!l){if(e.now()-x<999)return void j(ca,999);var a=B(function(){c.loadMode=3,W()});l=!0,c.loadMode=3,W(),i("scroll",function(){3==c.loadMode&&(c.loadMode=2),a()},!0)}};return{_:function(){x=e.now(),f=b.getElementsByClassName(c.lazyClass),k=b.getElementsByClassName(c.lazyClass+" "+c.preloadClass),L=c.hFac,i("scroll",W,!0),i("resize",W,!0),a.MutationObserver?new MutationObserver(W).observe(d,{childList:!0,subtree:!0,attributes:!0}):(d[g]("DOMNodeInserted",W,!0),d[g]("DOMAttrModified",W,!0),setInterval(W,999)),i("hashchange",W,!0),["focus","mouseover","click","load","transitionend","animationend","webkitAnimationEnd"].forEach(function(a){b[g](a,W,!0)}),/d$|^c/.test(b.readyState)?ca():(i("load",ca),b[g]("DOMContentLoaded",W),j(ca,2e4)),f.length?(V(),y._lsFlush()):W()},checkElems:W,unveil:ba}}(),D=function(){var a,d=z(function(a,b,c,d){var e,f,g;if(a._lazysizesWidth=d,d+="px",a.setAttribute("sizes",d),m.test(b.nodeName||""))for(e=b.getElementsByTagName("source"),f=0,g=e.length;f<g;f++)e[f].setAttribute("sizes",d);c.detail.dataAttr||v(a,c.detail)}),e=function(a,b,c){var e,f=a.parentNode;f&&(c=x(a,f,c),e=u(a,"lazybeforesizes",{width:c,dataAttr:!!b}),e.defaultPrevented||(c=e.detail.width,c&&c!==a._lazysizesWidth&&d(a,f,e,c)))},f=function(){var b,c=a.length;if(c)for(b=0;b<c;b++)e(a[b])},g=B(f);return{_:function(){a=b.getElementsByClassName(c.autosizesClass),i("resize",g)},checkElems:g,updateElem:e}}(),E=function(){E.i||(E.i=!0,D._(),C._())};return function(){var b,d={lazyClass:"lazyload",loadedClass:"lazyloaded",loadingClass:"lazyloading",preloadClass:"lazypreload",errorClass:"lazyerror",autosizesClass:"lazyautosizes",srcAttr:"data-src",srcsetAttr:"data-srcset",sizesAttr:"data-sizes",minSize:40,customMedia:{},init:!0,expFactor:1.5,hFac:.8,loadMode:2};c=a.lazySizesConfig||a.lazysizesConfig||{};for(b in d)b in c||(c[b]=d[b]);a.lazySizesConfig=c,j(function(){c.init&&E()})}(),{cfg:c,autoSizer:D,loader:C,init:E,uP:v,aC:r,rC:s,hC:q,fire:u,gW:x,rAF:y}}});
(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['exports', 'module', './util'], factory);
  } else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
    factory(exports, module, require('./util'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, mod, global.Util);
    global.alert = mod.exports;
  }
})(this, function (exports, module, _util) {
  'use strict';

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

  var _Util = _interopRequireDefault(_util);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.0.0-alpha.2): alert.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Alert = (function ($) {

    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */

    var NAME = 'alert';
    var VERSION = '4.0.0-alpha.2';
    var DATA_KEY = 'bs.alert';
    var EVENT_KEY = '.' + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $.fn[NAME];
    var TRANSITION_DURATION = 150;

    var Selector = {
      DISMISS: '[data-dismiss="alert"]'
    };

    var Event = {
      CLOSE: 'close' + EVENT_KEY,
      CLOSED: 'closed' + EVENT_KEY,
      CLICK_DATA_API: 'click' + EVENT_KEY + DATA_API_KEY
    };

    var ClassName = {
      ALERT: 'alert',
      FADE: 'fade',
      IN: 'in'
    };

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

    var Alert = (function () {
      function Alert(element) {
        _classCallCheck(this, Alert);

        this._element = element;
      }

      /**
       * ------------------------------------------------------------------------
       * Data Api implementation
       * ------------------------------------------------------------------------
       */

      // getters

      _createClass(Alert, [{
        key: 'close',

        // public

        value: function close(element) {
          element = element || this._element;

          var rootElement = this._getRootElement(element);
          var customEvent = this._triggerCloseEvent(rootElement);

          if (customEvent.isDefaultPrevented()) {
            return;
          }

          this._removeElement(rootElement);
        }
      }, {
        key: 'dispose',
        value: function dispose() {
          $.removeData(this._element, DATA_KEY);
          this._element = null;
        }

        // private

      }, {
        key: '_getRootElement',
        value: function _getRootElement(element) {
          var selector = _Util['default'].getSelectorFromElement(element);
          var parent = false;

          if (selector) {
            parent = $(selector)[0];
          }

          if (!parent) {
            parent = $(element).closest('.' + ClassName.ALERT)[0];
          }

          return parent;
        }
      }, {
        key: '_triggerCloseEvent',
        value: function _triggerCloseEvent(element) {
          var closeEvent = $.Event(Event.CLOSE);

          $(element).trigger(closeEvent);
          return closeEvent;
        }
      }, {
        key: '_removeElement',
        value: function _removeElement(element) {
          $(element).removeClass(ClassName.IN);

          if (!_Util['default'].supportsTransitionEnd() || !$(element).hasClass(ClassName.FADE)) {
            this._destroyElement(element);
            return;
          }

          $(element).one(_Util['default'].TRANSITION_END, $.proxy(this._destroyElement, this, element)).emulateTransitionEnd(TRANSITION_DURATION);
        }
      }, {
        key: '_destroyElement',
        value: function _destroyElement(element) {
          $(element).detach().trigger(Event.CLOSED).remove();
        }

        // static

      }], [{
        key: '_jQueryInterface',
        value: function _jQueryInterface(config) {
          return this.each(function () {
            var $element = $(this);
            var data = $element.data(DATA_KEY);

            if (!data) {
              data = new Alert(this);
              $element.data(DATA_KEY, data);
            }

            if (config === 'close') {
              data[config](this);
            }
          });
        }
      }, {
        key: '_handleDismiss',
        value: function _handleDismiss(alertInstance) {
          return function (event) {
            if (event) {
              event.preventDefault();
            }

            alertInstance.close(this);
          };
        }
      }, {
        key: 'VERSION',
        get: function get() {
          return VERSION;
        }
      }]);

      return Alert;
    })();

    $(document).on(Event.CLICK_DATA_API, Selector.DISMISS, Alert._handleDismiss(new Alert()));

    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $.fn[NAME] = Alert._jQueryInterface;
    $.fn[NAME].Constructor = Alert;
    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return Alert._jQueryInterface;
    };

    return Alert;
  })(jQuery);

  module.exports = Alert;
});

(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['exports', 'module'], factory);
  } else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
    factory(exports, module);
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, mod);
    global.button = mod.exports;
  }
})(this, function (exports, module) {
  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.0.0-alpha.2): button.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  'use strict';

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

  var Button = (function ($) {

    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */

    var NAME = 'button';
    var VERSION = '4.0.0-alpha.2';
    var DATA_KEY = 'bs.button';
    var EVENT_KEY = '.' + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $.fn[NAME];

    var ClassName = {
      ACTIVE: 'active',
      BUTTON: 'btn',
      FOCUS: 'focus'
    };

    var Selector = {
      DATA_TOGGLE_CARROT: '[data-toggle^="button"]',
      DATA_TOGGLE: '[data-toggle="buttons"]',
      INPUT: 'input',
      ACTIVE: '.active',
      BUTTON: '.btn'
    };

    var Event = {
      CLICK_DATA_API: 'click' + EVENT_KEY + DATA_API_KEY,
      FOCUS_BLUR_DATA_API: 'focus' + EVENT_KEY + DATA_API_KEY + ' ' + ('blur' + EVENT_KEY + DATA_API_KEY)
    };

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

    var Button = (function () {
      function Button(element) {
        _classCallCheck(this, Button);

        this._element = element;
      }

      /**
       * ------------------------------------------------------------------------
       * Data Api implementation
       * ------------------------------------------------------------------------
       */

      // getters

      _createClass(Button, [{
        key: 'toggle',

        // public

        value: function toggle() {
          var triggerChangeEvent = true;
          var rootElement = $(this._element).closest(Selector.DATA_TOGGLE)[0];

          if (rootElement) {
            var input = $(this._element).find(Selector.INPUT)[0];

            if (input) {
              if (input.type === 'radio') {
                if (input.checked && $(this._element).hasClass(ClassName.ACTIVE)) {
                  triggerChangeEvent = false;
                } else {
                  var activeElement = $(rootElement).find(Selector.ACTIVE)[0];

                  if (activeElement) {
                    $(activeElement).removeClass(ClassName.ACTIVE);
                  }
                }
              }

              if (triggerChangeEvent) {
                input.checked = !$(this._element).hasClass(ClassName.ACTIVE);
                $(this._element).trigger('change');
              }

              input.focus();
            }
          } else {
            this._element.setAttribute('aria-pressed', !$(this._element).hasClass(ClassName.ACTIVE));
          }

          if (triggerChangeEvent) {
            $(this._element).toggleClass(ClassName.ACTIVE);
          }
        }
      }, {
        key: 'dispose',
        value: function dispose() {
          $.removeData(this._element, DATA_KEY);
          this._element = null;
        }

        // static

      }], [{
        key: '_jQueryInterface',
        value: function _jQueryInterface(config) {
          return this.each(function () {
            var data = $(this).data(DATA_KEY);

            if (!data) {
              data = new Button(this);
              $(this).data(DATA_KEY, data);
            }

            if (config === 'toggle') {
              data[config]();
            }
          });
        }
      }, {
        key: 'VERSION',
        get: function get() {
          return VERSION;
        }
      }]);

      return Button;
    })();

    $(document).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE_CARROT, function (event) {
      event.preventDefault();

      var button = event.target;

      if (!$(button).hasClass(ClassName.BUTTON)) {
        button = $(button).closest(Selector.BUTTON);
      }

      Button._jQueryInterface.call($(button), 'toggle');
    }).on(Event.FOCUS_BLUR_DATA_API, Selector.DATA_TOGGLE_CARROT, function (event) {
      var button = $(event.target).closest(Selector.BUTTON)[0];
      $(button).toggleClass(ClassName.FOCUS, /^focus(in)?$/.test(event.type));
    });

    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $.fn[NAME] = Button._jQueryInterface;
    $.fn[NAME].Constructor = Button;
    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return Button._jQueryInterface;
    };

    return Button;
  })(jQuery);

  module.exports = Button;
});

(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['exports', 'module', './util'], factory);
  } else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
    factory(exports, module, require('./util'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, mod, global.Util);
    global.carousel = mod.exports;
  }
})(this, function (exports, module, _util) {
  'use strict';

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

  var _Util = _interopRequireDefault(_util);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.0.0-alpha.2): carousel.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Carousel = (function ($) {

    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */

    var NAME = 'carousel';
    var VERSION = '4.0.0-alpha.2';
    var DATA_KEY = 'bs.carousel';
    var EVENT_KEY = '.' + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $.fn[NAME];
    var TRANSITION_DURATION = 600;

    var Default = {
      interval: 5000,
      keyboard: true,
      slide: false,
      pause: 'hover',
      wrap: true
    };

    var DefaultType = {
      interval: '(number|boolean)',
      keyboard: 'boolean',
      slide: '(boolean|string)',
      pause: '(string|boolean)',
      wrap: 'boolean'
    };

    var Direction = {
      NEXT: 'next',
      PREVIOUS: 'prev'
    };

    var Event = {
      SLIDE: 'slide' + EVENT_KEY,
      SLID: 'slid' + EVENT_KEY,
      KEYDOWN: 'keydown' + EVENT_KEY,
      MOUSEENTER: 'mouseenter' + EVENT_KEY,
      MOUSELEAVE: 'mouseleave' + EVENT_KEY,
      LOAD_DATA_API: 'load' + EVENT_KEY + DATA_API_KEY,
      CLICK_DATA_API: 'click' + EVENT_KEY + DATA_API_KEY
    };

    var ClassName = {
      CAROUSEL: 'carousel',
      ACTIVE: 'active',
      SLIDE: 'slide',
      RIGHT: 'right',
      LEFT: 'left',
      ITEM: 'carousel-item'
    };

    var Selector = {
      ACTIVE: '.active',
      ACTIVE_ITEM: '.active.carousel-item',
      ITEM: '.carousel-item',
      NEXT_PREV: '.next, .prev',
      INDICATORS: '.carousel-indicators',
      DATA_SLIDE: '[data-slide], [data-slide-to]',
      DATA_RIDE: '[data-ride="carousel"]'
    };

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

    var Carousel = (function () {
      function Carousel(element, config) {
        _classCallCheck(this, Carousel);

        this._items = null;
        this._interval = null;
        this._activeElement = null;

        this._isPaused = false;
        this._isSliding = false;

        this._config = this._getConfig(config);
        this._element = $(element)[0];
        this._indicatorsElement = $(this._element).find(Selector.INDICATORS)[0];

        this._addEventListeners();
      }

      /**
       * ------------------------------------------------------------------------
       * Data Api implementation
       * ------------------------------------------------------------------------
       */

      // getters

      _createClass(Carousel, [{
        key: 'next',

        // public

        value: function next() {
          if (!this._isSliding) {
            this._slide(Direction.NEXT);
          }
        }
      }, {
        key: 'nextWhenVisible',
        value: function nextWhenVisible() {
          // Don't call next when the page isn't visible
          if (!document.hidden) {
            this.next();
          }
        }
      }, {
        key: 'prev',
        value: function prev() {
          if (!this._isSliding) {
            this._slide(Direction.PREVIOUS);
          }
        }
      }, {
        key: 'pause',
        value: function pause(event) {
          if (!event) {
            this._isPaused = true;
          }

          if ($(this._element).find(Selector.NEXT_PREV)[0] && _Util['default'].supportsTransitionEnd()) {
            _Util['default'].triggerTransitionEnd(this._element);
            this.cycle(true);
          }

          clearInterval(this._interval);
          this._interval = null;
        }
      }, {
        key: 'cycle',
        value: function cycle(event) {
          if (!event) {
            this._isPaused = false;
          }

          if (this._interval) {
            clearInterval(this._interval);
            this._interval = null;
          }

          if (this._config.interval && !this._isPaused) {
            this._interval = setInterval($.proxy(document.visibilityState ? this.nextWhenVisible : this.next, this), this._config.interval);
          }
        }
      }, {
        key: 'to',
        value: function to(index) {
          var _this = this;

          this._activeElement = $(this._element).find(Selector.ACTIVE_ITEM)[0];

          var activeIndex = this._getItemIndex(this._activeElement);

          if (index > this._items.length - 1 || index < 0) {
            return;
          }

          if (this._isSliding) {
            $(this._element).one(Event.SLID, function () {
              return _this.to(index);
            });
            return;
          }

          if (activeIndex === index) {
            this.pause();
            this.cycle();
            return;
          }

          var direction = index > activeIndex ? Direction.NEXT : Direction.PREVIOUS;

          this._slide(direction, this._items[index]);
        }
      }, {
        key: 'dispose',
        value: function dispose() {
          $(this._element).off(EVENT_KEY);
          $.removeData(this._element, DATA_KEY);

          this._items = null;
          this._config = null;
          this._element = null;
          this._interval = null;
          this._isPaused = null;
          this._isSliding = null;
          this._activeElement = null;
          this._indicatorsElement = null;
        }

        // private

      }, {
        key: '_getConfig',
        value: function _getConfig(config) {
          config = $.extend({}, Default, config);
          _Util['default'].typeCheckConfig(NAME, config, DefaultType);
          return config;
        }
      }, {
        key: '_addEventListeners',
        value: function _addEventListeners() {
          if (this._config.keyboard) {
            $(this._element).on(Event.KEYDOWN, $.proxy(this._keydown, this));
          }

          if (this._config.pause === 'hover' && !('ontouchstart' in document.documentElement)) {
            $(this._element).on(Event.MOUSEENTER, $.proxy(this.pause, this)).on(Event.MOUSELEAVE, $.proxy(this.cycle, this));
          }
        }
      }, {
        key: '_keydown',
        value: function _keydown(event) {
          event.preventDefault();

          if (/input|textarea/i.test(event.target.tagName)) {
            return;
          }

          switch (event.which) {
            case 37:
              this.prev();break;
            case 39:
              this.next();break;
            default:
              return;
          }
        }
      }, {
        key: '_getItemIndex',
        value: function _getItemIndex(element) {
          this._items = $.makeArray($(element).parent().find(Selector.ITEM));
          return this._items.indexOf(element);
        }
      }, {
        key: '_getItemByDirection',
        value: function _getItemByDirection(direction, activeElement) {
          var isNextDirection = direction === Direction.NEXT;
          var isPrevDirection = direction === Direction.PREVIOUS;
          var activeIndex = this._getItemIndex(activeElement);
          var lastItemIndex = this._items.length - 1;
          var isGoingToWrap = isPrevDirection && activeIndex === 0 || isNextDirection && activeIndex === lastItemIndex;

          if (isGoingToWrap && !this._config.wrap) {
            return activeElement;
          }

          var delta = direction === Direction.PREVIOUS ? -1 : 1;
          var itemIndex = (activeIndex + delta) % this._items.length;

          return itemIndex === -1 ? this._items[this._items.length - 1] : this._items[itemIndex];
        }
      }, {
        key: '_triggerSlideEvent',
        value: function _triggerSlideEvent(relatedTarget, directionalClassname) {
          var slideEvent = $.Event(Event.SLIDE, {
            relatedTarget: relatedTarget,
            direction: directionalClassname
          });

          $(this._element).trigger(slideEvent);

          return slideEvent;
        }
      }, {
        key: '_setActiveIndicatorElement',
        value: function _setActiveIndicatorElement(element) {
          if (this._indicatorsElement) {
            $(this._indicatorsElement).find(Selector.ACTIVE).removeClass(ClassName.ACTIVE);

            var nextIndicator = this._indicatorsElement.children[this._getItemIndex(element)];

            if (nextIndicator) {
              $(nextIndicator).addClass(ClassName.ACTIVE);
            }
          }
        }
      }, {
        key: '_slide',
        value: function _slide(direction, element) {
          var _this2 = this;

          var activeElement = $(this._element).find(Selector.ACTIVE_ITEM)[0];
          var nextElement = element || activeElement && this._getItemByDirection(direction, activeElement);

          var isCycling = Boolean(this._interval);

          var directionalClassName = direction === Direction.NEXT ? ClassName.LEFT : ClassName.RIGHT;

          if (nextElement && $(nextElement).hasClass(ClassName.ACTIVE)) {
            this._isSliding = false;
            return;
          }

          var slideEvent = this._triggerSlideEvent(nextElement, directionalClassName);
          if (slideEvent.isDefaultPrevented()) {
            return;
          }

          if (!activeElement || !nextElement) {
            // some weirdness is happening, so we bail
            return;
          }

          this._isSliding = true;

          if (isCycling) {
            this.pause();
          }

          this._setActiveIndicatorElement(nextElement);

          var slidEvent = $.Event(Event.SLID, {
            relatedTarget: nextElement,
            direction: directionalClassName
          });

          if (_Util['default'].supportsTransitionEnd() && $(this._element).hasClass(ClassName.SLIDE)) {

            $(nextElement).addClass(direction);

            _Util['default'].reflow(nextElement);

            $(activeElement).addClass(directionalClassName);
            $(nextElement).addClass(directionalClassName);

            $(activeElement).one(_Util['default'].TRANSITION_END, function () {
              $(nextElement).removeClass(directionalClassName).removeClass(direction);

              $(nextElement).addClass(ClassName.ACTIVE);

              $(activeElement).removeClass(ClassName.ACTIVE).removeClass(direction).removeClass(directionalClassName);

              _this2._isSliding = false;

              setTimeout(function () {
                return $(_this2._element).trigger(slidEvent);
              }, 0);
            }).emulateTransitionEnd(TRANSITION_DURATION);
          } else {
            $(activeElement).removeClass(ClassName.ACTIVE);
            $(nextElement).addClass(ClassName.ACTIVE);

            this._isSliding = false;
            $(this._element).trigger(slidEvent);
          }

          if (isCycling) {
            this.cycle();
          }
        }

        // static

      }], [{
        key: '_jQueryInterface',
        value: function _jQueryInterface(config) {
          return this.each(function () {
            var data = $(this).data(DATA_KEY);
            var _config = $.extend({}, Default, $(this).data());

            if (typeof config === 'object') {
              $.extend(_config, config);
            }

            var action = typeof config === 'string' ? config : _config.slide;

            if (!data) {
              data = new Carousel(this, _config);
              $(this).data(DATA_KEY, data);
            }

            if (typeof config === 'number') {
              data.to(config);
            } else if (typeof action === 'string') {
              if (data[action] === undefined) {
                throw new Error('No method named "' + action + '"');
              }
              data[action]();
            } else if (_config.interval) {
              data.pause();
              data.cycle();
            }
          });
        }
      }, {
        key: '_dataApiClickHandler',
        value: function _dataApiClickHandler(event) {
          var selector = _Util['default'].getSelectorFromElement(this);

          if (!selector) {
            return;
          }

          var target = $(selector)[0];

          if (!target || !$(target).hasClass(ClassName.CAROUSEL)) {
            return;
          }

          var config = $.extend({}, $(target).data(), $(this).data());
          var slideIndex = this.getAttribute('data-slide-to');

          if (slideIndex) {
            config.interval = false;
          }

          Carousel._jQueryInterface.call($(target), config);

          if (slideIndex) {
            $(target).data(DATA_KEY).to(slideIndex);
          }

          event.preventDefault();
        }
      }, {
        key: 'VERSION',
        get: function get() {
          return VERSION;
        }
      }, {
        key: 'Default',
        get: function get() {
          return Default;
        }
      }]);

      return Carousel;
    })();

    $(document).on(Event.CLICK_DATA_API, Selector.DATA_SLIDE, Carousel._dataApiClickHandler);

    $(window).on(Event.LOAD_DATA_API, function () {
      $(Selector.DATA_RIDE).each(function () {
        var $carousel = $(this);
        Carousel._jQueryInterface.call($carousel, $carousel.data());
      });
    });

    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $.fn[NAME] = Carousel._jQueryInterface;
    $.fn[NAME].Constructor = Carousel;
    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return Carousel._jQueryInterface;
    };

    return Carousel;
  })(jQuery);

  module.exports = Carousel;
});

(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['exports', 'module', './util'], factory);
  } else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
    factory(exports, module, require('./util'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, mod, global.Util);
    global.collapse = mod.exports;
  }
})(this, function (exports, module, _util) {
  'use strict';

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

  var _Util = _interopRequireDefault(_util);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.0.0-alpha.2): collapse.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Collapse = (function ($) {

    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */

    var NAME = 'collapse';
    var VERSION = '4.0.0-alpha.2';
    var DATA_KEY = 'bs.collapse';
    var EVENT_KEY = '.' + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $.fn[NAME];
    var TRANSITION_DURATION = 600;

    var Default = {
      toggle: true,
      parent: ''
    };

    var DefaultType = {
      toggle: 'boolean',
      parent: 'string'
    };

    var Event = {
      SHOW: 'show' + EVENT_KEY,
      SHOWN: 'shown' + EVENT_KEY,
      HIDE: 'hide' + EVENT_KEY,
      HIDDEN: 'hidden' + EVENT_KEY,
      CLICK_DATA_API: 'click' + EVENT_KEY + DATA_API_KEY
    };

    var ClassName = {
      IN: 'in',
      COLLAPSE: 'collapse',
      COLLAPSING: 'collapsing',
      COLLAPSED: 'collapsed'
    };

    var Dimension = {
      WIDTH: 'width',
      HEIGHT: 'height'
    };

    var Selector = {
      ACTIVES: '.panel > .in, .panel > .collapsing',
      DATA_TOGGLE: '[data-toggle="collapse"]'
    };

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

    var Collapse = (function () {
      function Collapse(element, config) {
        _classCallCheck(this, Collapse);

        this._isTransitioning = false;
        this._element = element;
        this._config = this._getConfig(config);
        this._triggerArray = $.makeArray($('[data-toggle="collapse"][href="#' + element.id + '"],' + ('[data-toggle="collapse"][data-target="#' + element.id + '"]')));

        this._parent = this._config.parent ? this._getParent() : null;

        if (!this._config.parent) {
          this._addAriaAndCollapsedClass(this._element, this._triggerArray);
        }

        if (this._config.toggle) {
          this.toggle();
        }
      }

      /**
       * ------------------------------------------------------------------------
       * Data Api implementation
       * ------------------------------------------------------------------------
       */

      // getters

      _createClass(Collapse, [{
        key: 'toggle',

        // public

        value: function toggle() {
          if ($(this._element).hasClass(ClassName.IN)) {
            this.hide();
          } else {
            this.show();
          }
        }
      }, {
        key: 'show',
        value: function show() {
          var _this = this;

          if (this._isTransitioning || $(this._element).hasClass(ClassName.IN)) {
            return;
          }

          var actives = undefined;
          var activesData = undefined;

          if (this._parent) {
            actives = $.makeArray($(Selector.ACTIVES));
            if (!actives.length) {
              actives = null;
            }
          }

          if (actives) {
            activesData = $(actives).data(DATA_KEY);
            if (activesData && activesData._isTransitioning) {
              return;
            }
          }

          var startEvent = $.Event(Event.SHOW);
          $(this._element).trigger(startEvent);
          if (startEvent.isDefaultPrevented()) {
            return;
          }

          if (actives) {
            Collapse._jQueryInterface.call($(actives), 'hide');
            if (!activesData) {
              $(actives).data(DATA_KEY, null);
            }
          }

          var dimension = this._getDimension();

          $(this._element).removeClass(ClassName.COLLAPSE).addClass(ClassName.COLLAPSING);

          this._element.style[dimension] = 0;
          this._element.setAttribute('aria-expanded', true);

          if (this._triggerArray.length) {
            $(this._triggerArray).removeClass(ClassName.COLLAPSED).attr('aria-expanded', true);
          }

          this.setTransitioning(true);

          var complete = function complete() {
            $(_this._element).removeClass(ClassName.COLLAPSING).addClass(ClassName.COLLAPSE).addClass(ClassName.IN);

            _this._element.style[dimension] = '';

            _this.setTransitioning(false);

            $(_this._element).trigger(Event.SHOWN);
          };

          if (!_Util['default'].supportsTransitionEnd()) {
            complete();
            return;
          }

          var capitalizedDimension = dimension[0].toUpperCase() + dimension.slice(1);
          var scrollSize = 'scroll' + capitalizedDimension;

          $(this._element).one(_Util['default'].TRANSITION_END, complete).emulateTransitionEnd(TRANSITION_DURATION);

          this._element.style[dimension] = this._element[scrollSize] + 'px';
        }
      }, {
        key: 'hide',
        value: function hide() {
          var _this2 = this;

          if (this._isTransitioning || !$(this._element).hasClass(ClassName.IN)) {
            return;
          }

          var startEvent = $.Event(Event.HIDE);
          $(this._element).trigger(startEvent);
          if (startEvent.isDefaultPrevented()) {
            return;
          }

          var dimension = this._getDimension();
          var offsetDimension = dimension === Dimension.WIDTH ? 'offsetWidth' : 'offsetHeight';

          this._element.style[dimension] = this._element[offsetDimension] + 'px';

          _Util['default'].reflow(this._element);

          $(this._element).addClass(ClassName.COLLAPSING).removeClass(ClassName.COLLAPSE).removeClass(ClassName.IN);

          this._element.setAttribute('aria-expanded', false);

          if (this._triggerArray.length) {
            $(this._triggerArray).addClass(ClassName.COLLAPSED).attr('aria-expanded', false);
          }

          this.setTransitioning(true);

          var complete = function complete() {
            _this2.setTransitioning(false);
            $(_this2._element).removeClass(ClassName.COLLAPSING).addClass(ClassName.COLLAPSE).trigger(Event.HIDDEN);
          };

          this._element.style[dimension] = 0;

          if (!_Util['default'].supportsTransitionEnd()) {
            complete();
            return;
          }

          $(this._element).one(_Util['default'].TRANSITION_END, complete).emulateTransitionEnd(TRANSITION_DURATION);
        }
      }, {
        key: 'setTransitioning',
        value: function setTransitioning(isTransitioning) {
          this._isTransitioning = isTransitioning;
        }
      }, {
        key: 'dispose',
        value: function dispose() {
          $.removeData(this._element, DATA_KEY);

          this._config = null;
          this._parent = null;
          this._element = null;
          this._triggerArray = null;
          this._isTransitioning = null;
        }

        // private

      }, {
        key: '_getConfig',
        value: function _getConfig(config) {
          config = $.extend({}, Default, config);
          config.toggle = Boolean(config.toggle); // coerce string values
          _Util['default'].typeCheckConfig(NAME, config, DefaultType);
          return config;
        }
      }, {
        key: '_getDimension',
        value: function _getDimension() {
          var hasWidth = $(this._element).hasClass(Dimension.WIDTH);
          return hasWidth ? Dimension.WIDTH : Dimension.HEIGHT;
        }
      }, {
        key: '_getParent',
        value: function _getParent() {
          var _this3 = this;

          var parent = $(this._config.parent)[0];
          var selector = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]';

          $(parent).find(selector).each(function (i, element) {
            _this3._addAriaAndCollapsedClass(Collapse._getTargetFromElement(element), [element]);
          });

          return parent;
        }
      }, {
        key: '_addAriaAndCollapsedClass',
        value: function _addAriaAndCollapsedClass(element, triggerArray) {
          if (element) {
            var isOpen = $(element).hasClass(ClassName.IN);
            element.setAttribute('aria-expanded', isOpen);

            if (triggerArray.length) {
              $(triggerArray).toggleClass(ClassName.COLLAPSED, !isOpen).attr('aria-expanded', isOpen);
            }
          }
        }

        // static

      }], [{
        key: '_getTargetFromElement',
        value: function _getTargetFromElement(element) {
          var selector = _Util['default'].getSelectorFromElement(element);
          return selector ? $(selector)[0] : null;
        }
      }, {
        key: '_jQueryInterface',
        value: function _jQueryInterface(config) {
          return this.each(function () {
            var $this = $(this);
            var data = $this.data(DATA_KEY);
            var _config = $.extend({}, Default, $this.data(), typeof config === 'object' && config);

            if (!data && _config.toggle && /show|hide/.test(config)) {
              _config.toggle = false;
            }

            if (!data) {
              data = new Collapse(this, _config);
              $this.data(DATA_KEY, data);
            }

            if (typeof config === 'string') {
              if (data[config] === undefined) {
                throw new Error('No method named "' + config + '"');
              }
              data[config]();
            }
          });
        }
      }, {
        key: 'VERSION',
        get: function get() {
          return VERSION;
        }
      }, {
        key: 'Default',
        get: function get() {
          return Default;
        }
      }]);

      return Collapse;
    })();

    $(document).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE, function (event) {
      event.preventDefault();

      var target = Collapse._getTargetFromElement(this);
      var data = $(target).data(DATA_KEY);
      var config = data ? 'toggle' : $(this).data();

      Collapse._jQueryInterface.call($(target), config);
    });

    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $.fn[NAME] = Collapse._jQueryInterface;
    $.fn[NAME].Constructor = Collapse;
    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return Collapse._jQueryInterface;
    };

    return Collapse;
  })(jQuery);

  module.exports = Collapse;
});

(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['exports', 'module', './util'], factory);
  } else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
    factory(exports, module, require('./util'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, mod, global.Util);
    global.dropdown = mod.exports;
  }
})(this, function (exports, module, _util) {
  'use strict';

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

  var _Util = _interopRequireDefault(_util);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.0.0-alpha.2): dropdown.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Dropdown = (function ($) {

    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */

    var NAME = 'dropdown';
    var VERSION = '4.0.0-alpha.2';
    var DATA_KEY = 'bs.dropdown';
    var EVENT_KEY = '.' + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $.fn[NAME];

    var Event = {
      HIDE: 'hide' + EVENT_KEY,
      HIDDEN: 'hidden' + EVENT_KEY,
      SHOW: 'show' + EVENT_KEY,
      SHOWN: 'shown' + EVENT_KEY,
      CLICK: 'click' + EVENT_KEY,
      CLICK_DATA_API: 'click' + EVENT_KEY + DATA_API_KEY,
      KEYDOWN_DATA_API: 'keydown' + EVENT_KEY + DATA_API_KEY
    };

    var ClassName = {
      BACKDROP: 'dropdown-backdrop',
      DISABLED: 'disabled',
      OPEN: 'open'
    };

    var Selector = {
      BACKDROP: '.dropdown-backdrop',
      DATA_TOGGLE: '[data-toggle="dropdown"]',
      FORM_CHILD: '.dropdown form',
      ROLE_MENU: '[role="menu"]',
      ROLE_LISTBOX: '[role="listbox"]',
      NAVBAR_NAV: '.navbar-nav',
      VISIBLE_ITEMS: '[role="menu"] li:not(.disabled) a, ' + '[role="listbox"] li:not(.disabled) a'
    };

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

    var Dropdown = (function () {
      function Dropdown(element) {
        _classCallCheck(this, Dropdown);

        this._element = element;

        this._addEventListeners();
      }

      /**
       * ------------------------------------------------------------------------
       * Data Api implementation
       * ------------------------------------------------------------------------
       */

      // getters

      _createClass(Dropdown, [{
        key: 'toggle',

        // public

        value: function toggle() {
          if (this.disabled || $(this).hasClass(ClassName.DISABLED)) {
            return false;
          }

          var parent = Dropdown._getParentFromElement(this);
          var isActive = $(parent).hasClass(ClassName.OPEN);

          Dropdown._clearMenus();

          if (isActive) {
            return false;
          }

          if ('ontouchstart' in document.documentElement && !$(parent).closest(Selector.NAVBAR_NAV).length) {

            // if mobile we use a backdrop because click events don't delegate
            var dropdown = document.createElement('div');
            dropdown.className = ClassName.BACKDROP;
            $(dropdown).insertBefore(this);
            $(dropdown).on('click', Dropdown._clearMenus);
          }

          var relatedTarget = { relatedTarget: this };
          var showEvent = $.Event(Event.SHOW, relatedTarget);

          $(parent).trigger(showEvent);

          if (showEvent.isDefaultPrevented()) {
            return false;
          }

          this.focus();
          this.setAttribute('aria-expanded', 'true');

          $(parent).toggleClass(ClassName.OPEN);
          $(parent).trigger($.Event(Event.SHOWN, relatedTarget));

          return false;
        }
      }, {
        key: 'dispose',
        value: function dispose() {
          $.removeData(this._element, DATA_KEY);
          $(this._element).off(EVENT_KEY);
          this._element = null;
        }

        // private

      }, {
        key: '_addEventListeners',
        value: function _addEventListeners() {
          $(this._element).on(Event.CLICK, this.toggle);
        }

        // static

      }], [{
        key: '_jQueryInterface',
        value: function _jQueryInterface(config) {
          return this.each(function () {
            var data = $(this).data(DATA_KEY);

            if (!data) {
              $(this).data(DATA_KEY, data = new Dropdown(this));
            }

            if (typeof config === 'string') {
              if (data[config] === undefined) {
                throw new Error('No method named "' + config + '"');
              }
              data[config].call(this);
            }
          });
        }
      }, {
        key: '_clearMenus',
        value: function _clearMenus(event) {
          if (event && event.which === 3) {
            return;
          }

          var backdrop = $(Selector.BACKDROP)[0];
          if (backdrop) {
            backdrop.parentNode.removeChild(backdrop);
          }

          var toggles = $.makeArray($(Selector.DATA_TOGGLE));

          for (var i = 0; i < toggles.length; i++) {
            var _parent = Dropdown._getParentFromElement(toggles[i]);
            var relatedTarget = { relatedTarget: toggles[i] };

            if (!$(_parent).hasClass(ClassName.OPEN)) {
              continue;
            }

            if (event && event.type === 'click' && /input|textarea/i.test(event.target.tagName) && $.contains(_parent, event.target)) {
              continue;
            }

            var hideEvent = $.Event(Event.HIDE, relatedTarget);
            $(_parent).trigger(hideEvent);
            if (hideEvent.isDefaultPrevented()) {
              continue;
            }

            toggles[i].setAttribute('aria-expanded', 'false');

            $(_parent).removeClass(ClassName.OPEN).trigger($.Event(Event.HIDDEN, relatedTarget));
          }
        }
      }, {
        key: '_getParentFromElement',
        value: function _getParentFromElement(element) {
          var parent = undefined;
          var selector = _Util['default'].getSelectorFromElement(element);

          if (selector) {
            parent = $(selector)[0];
          }

          return parent || element.parentNode;
        }
      }, {
        key: '_dataApiKeydownHandler',
        value: function _dataApiKeydownHandler(event) {
          if (!/(38|40|27|32)/.test(event.which) || /input|textarea/i.test(event.target.tagName)) {
            return;
          }

          event.preventDefault();
          event.stopPropagation();

          if (this.disabled || $(this).hasClass(ClassName.DISABLED)) {
            return;
          }

          var parent = Dropdown._getParentFromElement(this);
          var isActive = $(parent).hasClass(ClassName.OPEN);

          if (!isActive && event.which !== 27 || isActive && event.which === 27) {

            if (event.which === 27) {
              var toggle = $(parent).find(Selector.DATA_TOGGLE)[0];
              $(toggle).trigger('focus');
            }

            $(this).trigger('click');
            return;
          }

          var items = $.makeArray($(Selector.VISIBLE_ITEMS));

          items = items.filter(function (item) {
            return item.offsetWidth || item.offsetHeight;
          });

          if (!items.length) {
            return;
          }

          var index = items.indexOf(event.target);

          if (event.which === 38 && index > 0) {
            // up
            index--;
          }

          if (event.which === 40 && index < items.length - 1) {
            // down
            index++;
          }

          if (index < 0) {
            index = 0;
          }

          items[index].focus();
        }
      }, {
        key: 'VERSION',
        get: function get() {
          return VERSION;
        }
      }]);

      return Dropdown;
    })();

    $(document).on(Event.KEYDOWN_DATA_API, Selector.DATA_TOGGLE, Dropdown._dataApiKeydownHandler).on(Event.KEYDOWN_DATA_API, Selector.ROLE_MENU, Dropdown._dataApiKeydownHandler).on(Event.KEYDOWN_DATA_API, Selector.ROLE_LISTBOX, Dropdown._dataApiKeydownHandler).on(Event.CLICK_DATA_API, Dropdown._clearMenus).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE, Dropdown.prototype.toggle).on(Event.CLICK_DATA_API, Selector.FORM_CHILD, function (e) {
      e.stopPropagation();
    });

    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $.fn[NAME] = Dropdown._jQueryInterface;
    $.fn[NAME].Constructor = Dropdown;
    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return Dropdown._jQueryInterface;
    };

    return Dropdown;
  })(jQuery);

  module.exports = Dropdown;
});

(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['exports', 'module', './util'], factory);
  } else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
    factory(exports, module, require('./util'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, mod, global.Util);
    global.modal = mod.exports;
  }
})(this, function (exports, module, _util) {
  'use strict';

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

  var _Util = _interopRequireDefault(_util);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.0.0-alpha.2): modal.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Modal = (function ($) {

    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */

    var NAME = 'modal';
    var VERSION = '4.0.0-alpha.2';
    var DATA_KEY = 'bs.modal';
    var EVENT_KEY = '.' + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $.fn[NAME];
    var TRANSITION_DURATION = 300;
    var BACKDROP_TRANSITION_DURATION = 150;

    var Default = {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: true
    };

    var DefaultType = {
      backdrop: '(boolean|string)',
      keyboard: 'boolean',
      focus: 'boolean',
      show: 'boolean'
    };

    var Event = {
      HIDE: 'hide' + EVENT_KEY,
      HIDDEN: 'hidden' + EVENT_KEY,
      SHOW: 'show' + EVENT_KEY,
      SHOWN: 'shown' + EVENT_KEY,
      FOCUSIN: 'focusin' + EVENT_KEY,
      RESIZE: 'resize' + EVENT_KEY,
      CLICK_DISMISS: 'click.dismiss' + EVENT_KEY,
      KEYDOWN_DISMISS: 'keydown.dismiss' + EVENT_KEY,
      MOUSEUP_DISMISS: 'mouseup.dismiss' + EVENT_KEY,
      MOUSEDOWN_DISMISS: 'mousedown.dismiss' + EVENT_KEY,
      CLICK_DATA_API: 'click' + EVENT_KEY + DATA_API_KEY
    };

    var ClassName = {
      SCROLLBAR_MEASURER: 'modal-scrollbar-measure',
      BACKDROP: 'modal-backdrop',
      OPEN: 'modal-open',
      FADE: 'fade',
      IN: 'in'
    };

    var Selector = {
      DIALOG: '.modal-dialog',
      DATA_TOGGLE: '[data-toggle="modal"]',
      DATA_DISMISS: '[data-dismiss="modal"]',
      FIXED_CONTENT: '.navbar-fixed-top, .navbar-fixed-bottom, .is-fixed'
    };

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

    var Modal = (function () {
      function Modal(element, config) {
        _classCallCheck(this, Modal);

        this._config = this._getConfig(config);
        this._element = element;
        this._dialog = $(element).find(Selector.DIALOG)[0];
        this._backdrop = null;
        this._isShown = false;
        this._isBodyOverflowing = false;
        this._ignoreBackdropClick = false;
        this._originalBodyPadding = 0;
        this._scrollbarWidth = 0;
      }

      /**
       * ------------------------------------------------------------------------
       * Data Api implementation
       * ------------------------------------------------------------------------
       */

      // getters

      _createClass(Modal, [{
        key: 'toggle',

        // public

        value: function toggle(relatedTarget) {
          return this._isShown ? this.hide() : this.show(relatedTarget);
        }
      }, {
        key: 'show',
        value: function show(relatedTarget) {
          var _this = this;

          var showEvent = $.Event(Event.SHOW, {
            relatedTarget: relatedTarget
          });

          $(this._element).trigger(showEvent);

          if (this._isShown || showEvent.isDefaultPrevented()) {
            return;
          }

          this._isShown = true;

          this._checkScrollbar();
          this._setScrollbar();

          $(document.body).addClass(ClassName.OPEN);

          this._setEscapeEvent();
          this._setResizeEvent();

          $(this._element).on(Event.CLICK_DISMISS, Selector.DATA_DISMISS, $.proxy(this.hide, this));

          $(this._dialog).on(Event.MOUSEDOWN_DISMISS, function () {
            $(_this._element).one(Event.MOUSEUP_DISMISS, function (event) {
              if ($(event.target).is(_this._element)) {
                _this._ignoreBackdropClick = true;
              }
            });
          });

          this._showBackdrop($.proxy(this._showElement, this, relatedTarget));
        }
      }, {
        key: 'hide',
        value: function hide(event) {
          if (event) {
            event.preventDefault();
          }

          var hideEvent = $.Event(Event.HIDE);

          $(this._element).trigger(hideEvent);

          if (!this._isShown || hideEvent.isDefaultPrevented()) {
            return;
          }

          this._isShown = false;

          this._setEscapeEvent();
          this._setResizeEvent();

          $(document).off(Event.FOCUSIN);

          $(this._element).removeClass(ClassName.IN);

          $(this._element).off(Event.CLICK_DISMISS);
          $(this._dialog).off(Event.MOUSEDOWN_DISMISS);

          if (_Util['default'].supportsTransitionEnd() && $(this._element).hasClass(ClassName.FADE)) {

            $(this._element).one(_Util['default'].TRANSITION_END, $.proxy(this._hideModal, this)).emulateTransitionEnd(TRANSITION_DURATION);
          } else {
            this._hideModal();
          }
        }
      }, {
        key: 'dispose',
        value: function dispose() {
          $.removeData(this._element, DATA_KEY);

          $(window).off(EVENT_KEY);
          $(document).off(EVENT_KEY);
          $(this._element).off(EVENT_KEY);
          $(this._backdrop).off(EVENT_KEY);

          this._config = null;
          this._element = null;
          this._dialog = null;
          this._backdrop = null;
          this._isShown = null;
          this._isBodyOverflowing = null;
          this._ignoreBackdropClick = null;
          this._originalBodyPadding = null;
          this._scrollbarWidth = null;
        }

        // private

      }, {
        key: '_getConfig',
        value: function _getConfig(config) {
          config = $.extend({}, Default, config);
          _Util['default'].typeCheckConfig(NAME, config, DefaultType);
          return config;
        }
      }, {
        key: '_showElement',
        value: function _showElement(relatedTarget) {
          var _this2 = this;

          var transition = _Util['default'].supportsTransitionEnd() && $(this._element).hasClass(ClassName.FADE);

          if (!this._element.parentNode || this._element.parentNode.nodeType !== Node.ELEMENT_NODE) {
            // don't move modals dom position
            document.body.appendChild(this._element);
          }

          this._element.style.display = 'block';
          this._element.removeAttribute('aria-hidden');
          this._element.scrollTop = 0;

          if (transition) {
            _Util['default'].reflow(this._element);
          }

          $(this._element).addClass(ClassName.IN);

          if (this._config.focus) {
            this._enforceFocus();
          }

          var shownEvent = $.Event(Event.SHOWN, {
            relatedTarget: relatedTarget
          });

          var transitionComplete = function transitionComplete() {
            if (_this2._config.focus) {
              _this2._element.focus();
            }
            $(_this2._element).trigger(shownEvent);
          };

          if (transition) {
            $(this._dialog).one(_Util['default'].TRANSITION_END, transitionComplete).emulateTransitionEnd(TRANSITION_DURATION);
          } else {
            transitionComplete();
          }
        }
      }, {
        key: '_enforceFocus',
        value: function _enforceFocus() {
          var _this3 = this;

          $(document).off(Event.FOCUSIN) // guard against infinite focus loop
          .on(Event.FOCUSIN, function (event) {
            if (document !== event.target && _this3._element !== event.target && !$(_this3._element).has(event.target).length) {
              _this3._element.focus();
            }
          });
        }
      }, {
        key: '_setEscapeEvent',
        value: function _setEscapeEvent() {
          var _this4 = this;

          if (this._isShown && this._config.keyboard) {
            $(this._element).on(Event.KEYDOWN_DISMISS, function (event) {
              if (event.which === 27) {
                _this4.hide();
              }
            });
          } else if (!this._isShown) {
            $(this._element).off(Event.KEYDOWN_DISMISS);
          }
        }
      }, {
        key: '_setResizeEvent',
        value: function _setResizeEvent() {
          if (this._isShown) {
            $(window).on(Event.RESIZE, $.proxy(this._handleUpdate, this));
          } else {
            $(window).off(Event.RESIZE);
          }
        }
      }, {
        key: '_hideModal',
        value: function _hideModal() {
          var _this5 = this;

          this._element.style.display = 'none';
          this._element.setAttribute('aria-hidden', 'true');
          this._showBackdrop(function () {
            $(document.body).removeClass(ClassName.OPEN);
            _this5._resetAdjustments();
            _this5._resetScrollbar();
            $(_this5._element).trigger(Event.HIDDEN);
          });
        }
      }, {
        key: '_removeBackdrop',
        value: function _removeBackdrop() {
          if (this._backdrop) {
            $(this._backdrop).remove();
            this._backdrop = null;
          }
        }
      }, {
        key: '_showBackdrop',
        value: function _showBackdrop(callback) {
          var _this6 = this;

          var animate = $(this._element).hasClass(ClassName.FADE) ? ClassName.FADE : '';

          if (this._isShown && this._config.backdrop) {
            var doAnimate = _Util['default'].supportsTransitionEnd() && animate;

            this._backdrop = document.createElement('div');
            this._backdrop.className = ClassName.BACKDROP;

            if (animate) {
              $(this._backdrop).addClass(animate);
            }

            $(this._backdrop).appendTo(document.body);

            $(this._element).on(Event.CLICK_DISMISS, function (event) {
              if (_this6._ignoreBackdropClick) {
                _this6._ignoreBackdropClick = false;
                return;
              }
              if (event.target !== event.currentTarget) {
                return;
              }
              if (_this6._config.backdrop === 'static') {
                _this6._element.focus();
              } else {
                _this6.hide();
              }
            });

            if (doAnimate) {
              _Util['default'].reflow(this._backdrop);
            }

            $(this._backdrop).addClass(ClassName.IN);

            if (!callback) {
              return;
            }

            if (!doAnimate) {
              callback();
              return;
            }

            $(this._backdrop).one(_Util['default'].TRANSITION_END, callback).emulateTransitionEnd(BACKDROP_TRANSITION_DURATION);
          } else if (!this._isShown && this._backdrop) {
            $(this._backdrop).removeClass(ClassName.IN);

            var callbackRemove = function callbackRemove() {
              _this6._removeBackdrop();
              if (callback) {
                callback();
              }
            };

            if (_Util['default'].supportsTransitionEnd() && $(this._element).hasClass(ClassName.FADE)) {
              $(this._backdrop).one(_Util['default'].TRANSITION_END, callbackRemove).emulateTransitionEnd(BACKDROP_TRANSITION_DURATION);
            } else {
              callbackRemove();
            }
          } else if (callback) {
            callback();
          }
        }

        // ----------------------------------------------------------------------
        // the following methods are used to handle overflowing modals
        // todo (fat): these should probably be refactored out of modal.js
        // ----------------------------------------------------------------------

      }, {
        key: '_handleUpdate',
        value: function _handleUpdate() {
          this._adjustDialog();
        }
      }, {
        key: '_adjustDialog',
        value: function _adjustDialog() {
          var isModalOverflowing = this._element.scrollHeight > document.documentElement.clientHeight;

          if (!this._isBodyOverflowing && isModalOverflowing) {
            this._element.style.paddingLeft = this._scrollbarWidth + 'px';
          }

          if (this._isBodyOverflowing && !isModalOverflowing) {
            this._element.style.paddingRight = this._scrollbarWidth + 'px~';
          }
        }
      }, {
        key: '_resetAdjustments',
        value: function _resetAdjustments() {
          this._element.style.paddingLeft = '';
          this._element.style.paddingRight = '';
        }
      }, {
        key: '_checkScrollbar',
        value: function _checkScrollbar() {
          this._isBodyOverflowing = document.body.clientWidth < window.innerWidth;
          this._scrollbarWidth = this._getScrollbarWidth();
        }
      }, {
        key: '_setScrollbar',
        value: function _setScrollbar() {
          var bodyPadding = parseInt($(Selector.FIXED_CONTENT).css('padding-right') || 0, 10);

          this._originalBodyPadding = document.body.style.paddingRight || '';

          if (this._isBodyOverflowing) {
            document.body.style.paddingRight = bodyPadding + this._scrollbarWidth + 'px';
          }
        }
      }, {
        key: '_resetScrollbar',
        value: function _resetScrollbar() {
          document.body.style.paddingRight = this._originalBodyPadding;
        }
      }, {
        key: '_getScrollbarWidth',
        value: function _getScrollbarWidth() {
          // thx d.walsh
          var scrollDiv = document.createElement('div');
          scrollDiv.className = ClassName.SCROLLBAR_MEASURER;
          document.body.appendChild(scrollDiv);
          var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
          document.body.removeChild(scrollDiv);
          return scrollbarWidth;
        }

        // static

      }], [{
        key: '_jQueryInterface',
        value: function _jQueryInterface(config, relatedTarget) {
          return this.each(function () {
            var data = $(this).data(DATA_KEY);
            var _config = $.extend({}, Modal.Default, $(this).data(), typeof config === 'object' && config);

            if (!data) {
              data = new Modal(this, _config);
              $(this).data(DATA_KEY, data);
            }

            if (typeof config === 'string') {
              if (data[config] === undefined) {
                throw new Error('No method named "' + config + '"');
              }
              data[config](relatedTarget);
            } else if (_config.show) {
              data.show(relatedTarget);
            }
          });
        }
      }, {
        key: 'VERSION',
        get: function get() {
          return VERSION;
        }
      }, {
        key: 'Default',
        get: function get() {
          return Default;
        }
      }]);

      return Modal;
    })();

    $(document).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE, function (event) {
      var _this7 = this;

      var target = undefined;
      var selector = _Util['default'].getSelectorFromElement(this);

      if (selector) {
        target = $(selector)[0];
      }

      var config = $(target).data(DATA_KEY) ? 'toggle' : $.extend({}, $(target).data(), $(this).data());

      if (this.tagName === 'A') {
        event.preventDefault();
      }

      var $target = $(target).one(Event.SHOW, function (showEvent) {
        if (showEvent.isDefaultPrevented()) {
          // only register focus restorer if modal will actually get shown
          return;
        }

        $target.one(Event.HIDDEN, function () {
          if ($(_this7).is(':visible')) {
            _this7.focus();
          }
        });
      });

      Modal._jQueryInterface.call($(target), config, this);
    });

    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $.fn[NAME] = Modal._jQueryInterface;
    $.fn[NAME].Constructor = Modal;
    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return Modal._jQueryInterface;
    };

    return Modal;
  })(jQuery);

  module.exports = Modal;
});

(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['exports', 'module', './tooltip'], factory);
  } else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
    factory(exports, module, require('./tooltip'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, mod, global.Tooltip);
    global.popover = mod.exports;
  }
})(this, function (exports, module, _tooltip) {
  'use strict';

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

  function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

  var _Tooltip2 = _interopRequireDefault(_tooltip);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.0.0-alpha.2): popover.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Popover = (function ($) {

    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */

    var NAME = 'popover';
    var VERSION = '4.0.0-alpha.2';
    var DATA_KEY = 'bs.popover';
    var EVENT_KEY = '.' + DATA_KEY;
    var JQUERY_NO_CONFLICT = $.fn[NAME];

    var Default = $.extend({}, _Tooltip2['default'].Default, {
      placement: 'right',
      trigger: 'click',
      content: '',
      template: '<div class="popover" role="tooltip">' + '<div class="popover-arrow"></div>' + '<h3 class="popover-title"></h3>' + '<div class="popover-content"></div></div>'
    });

    var DefaultType = $.extend({}, _Tooltip2['default'].DefaultType, {
      content: '(string|element|function)'
    });

    var ClassName = {
      FADE: 'fade',
      IN: 'in'
    };

    var Selector = {
      TITLE: '.popover-title',
      CONTENT: '.popover-content',
      ARROW: '.popover-arrow'
    };

    var Event = {
      HIDE: 'hide' + EVENT_KEY,
      HIDDEN: 'hidden' + EVENT_KEY,
      SHOW: 'show' + EVENT_KEY,
      SHOWN: 'shown' + EVENT_KEY,
      INSERTED: 'inserted' + EVENT_KEY,
      CLICK: 'click' + EVENT_KEY,
      FOCUSIN: 'focusin' + EVENT_KEY,
      FOCUSOUT: 'focusout' + EVENT_KEY,
      MOUSEENTER: 'mouseenter' + EVENT_KEY,
      MOUSELEAVE: 'mouseleave' + EVENT_KEY
    };

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

    var Popover = (function (_Tooltip) {
      _inherits(Popover, _Tooltip);

      function Popover() {
        _classCallCheck(this, Popover);

        _get(Object.getPrototypeOf(Popover.prototype), 'constructor', this).apply(this, arguments);
      }

      /**
       * ------------------------------------------------------------------------
       * jQuery
       * ------------------------------------------------------------------------
       */

      _createClass(Popover, [{
        key: 'isWithContent',

        // overrides

        value: function isWithContent() {
          return this.getTitle() || this._getContent();
        }
      }, {
        key: 'getTipElement',
        value: function getTipElement() {
          return this.tip = this.tip || $(this.config.template)[0];
        }
      }, {
        key: 'setContent',
        value: function setContent() {
          var $tip = $(this.getTipElement());

          // we use append for html objects to maintain js events
          this.setElementContent($tip.find(Selector.TITLE), this.getTitle());
          this.setElementContent($tip.find(Selector.CONTENT), this._getContent());

          $tip.removeClass(ClassName.FADE).removeClass(ClassName.IN);

          this.cleanupTether();
        }

        // private

      }, {
        key: '_getContent',
        value: function _getContent() {
          return this.element.getAttribute('data-content') || (typeof this.config.content === 'function' ? this.config.content.call(this.element) : this.config.content);
        }

        // static

      }], [{
        key: '_jQueryInterface',
        value: function _jQueryInterface(config) {
          return this.each(function () {
            var data = $(this).data(DATA_KEY);
            var _config = typeof config === 'object' ? config : null;

            if (!data && /destroy|hide/.test(config)) {
              return;
            }

            if (!data) {
              data = new Popover(this, _config);
              $(this).data(DATA_KEY, data);
            }

            if (typeof config === 'string') {
              if (data[config] === undefined) {
                throw new Error('No method named "' + config + '"');
              }
              data[config]();
            }
          });
        }
      }, {
        key: 'VERSION',

        // getters

        get: function get() {
          return VERSION;
        }
      }, {
        key: 'Default',
        get: function get() {
          return Default;
        }
      }, {
        key: 'NAME',
        get: function get() {
          return NAME;
        }
      }, {
        key: 'DATA_KEY',
        get: function get() {
          return DATA_KEY;
        }
      }, {
        key: 'Event',
        get: function get() {
          return Event;
        }
      }, {
        key: 'EVENT_KEY',
        get: function get() {
          return EVENT_KEY;
        }
      }, {
        key: 'DefaultType',
        get: function get() {
          return DefaultType;
        }
      }]);

      return Popover;
    })(_Tooltip2['default']);

    $.fn[NAME] = Popover._jQueryInterface;
    $.fn[NAME].Constructor = Popover;
    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return Popover._jQueryInterface;
    };

    return Popover;
  })(jQuery);

  module.exports = Popover;
});

(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['exports', 'module', './util'], factory);
  } else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
    factory(exports, module, require('./util'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, mod, global.Util);
    global.scrollspy = mod.exports;
  }
})(this, function (exports, module, _util) {
  'use strict';

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

  var _Util = _interopRequireDefault(_util);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.0.0-alpha.2): scrollspy.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var ScrollSpy = (function ($) {

    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */

    var NAME = 'scrollspy';
    var VERSION = '4.0.0-alpha.2';
    var DATA_KEY = 'bs.scrollspy';
    var EVENT_KEY = '.' + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $.fn[NAME];

    var Default = {
      offset: 10,
      method: 'auto',
      target: ''
    };

    var DefaultType = {
      offset: 'number',
      method: 'string',
      target: '(string|element)'
    };

    var Event = {
      ACTIVATE: 'activate' + EVENT_KEY,
      SCROLL: 'scroll' + EVENT_KEY,
      LOAD_DATA_API: 'load' + EVENT_KEY + DATA_API_KEY
    };

    var ClassName = {
      DROPDOWN_ITEM: 'dropdown-item',
      DROPDOWN_MENU: 'dropdown-menu',
      NAV_LINK: 'nav-link',
      NAV: 'nav',
      ACTIVE: 'active'
    };

    var Selector = {
      DATA_SPY: '[data-spy="scroll"]',
      ACTIVE: '.active',
      LIST_ITEM: '.list-item',
      LI: 'li',
      LI_DROPDOWN: 'li.dropdown',
      NAV_LINKS: '.nav-link',
      DROPDOWN: '.dropdown',
      DROPDOWN_ITEMS: '.dropdown-item',
      DROPDOWN_TOGGLE: '.dropdown-toggle'
    };

    var OffsetMethod = {
      OFFSET: 'offset',
      POSITION: 'position'
    };

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

    var ScrollSpy = (function () {
      function ScrollSpy(element, config) {
        _classCallCheck(this, ScrollSpy);

        this._element = element;
        this._scrollElement = element.tagName === 'BODY' ? window : element;
        this._config = this._getConfig(config);
        this._selector = this._config.target + ' ' + Selector.NAV_LINKS + ',' + (this._config.target + ' ' + Selector.DROPDOWN_ITEMS);
        this._offsets = [];
        this._targets = [];
        this._activeTarget = null;
        this._scrollHeight = 0;

        $(this._scrollElement).on(Event.SCROLL, $.proxy(this._process, this));

        this.refresh();
        this._process();
      }

      /**
       * ------------------------------------------------------------------------
       * Data Api implementation
       * ------------------------------------------------------------------------
       */

      // getters

      _createClass(ScrollSpy, [{
        key: 'refresh',

        // public

        value: function refresh() {
          var _this = this;

          var autoMethod = this._scrollElement !== this._scrollElement.window ? OffsetMethod.POSITION : OffsetMethod.OFFSET;

          var offsetMethod = this._config.method === 'auto' ? autoMethod : this._config.method;

          var offsetBase = offsetMethod === OffsetMethod.POSITION ? this._getScrollTop() : 0;

          this._offsets = [];
          this._targets = [];

          this._scrollHeight = this._getScrollHeight();

          var targets = $.makeArray($(this._selector));

          targets.map(function (element) {
            var target = undefined;
            var targetSelector = _Util['default'].getSelectorFromElement(element);

            if (targetSelector) {
              target = $(targetSelector)[0];
            }

            if (target && (target.offsetWidth || target.offsetHeight)) {
              // todo (fat): remove sketch reliance on jQuery position/offset
              return [$(target)[offsetMethod]().top + offsetBase, targetSelector];
            }
          }).filter(function (item) {
            return item;
          }).sort(function (a, b) {
            return a[0] - b[0];
          }).forEach(function (item) {
            _this._offsets.push(item[0]);
            _this._targets.push(item[1]);
          });
        }
      }, {
        key: 'dispose',
        value: function dispose() {
          $.removeData(this._element, DATA_KEY);
          $(this._scrollElement).off(EVENT_KEY);

          this._element = null;
          this._scrollElement = null;
          this._config = null;
          this._selector = null;
          this._offsets = null;
          this._targets = null;
          this._activeTarget = null;
          this._scrollHeight = null;
        }

        // private

      }, {
        key: '_getConfig',
        value: function _getConfig(config) {
          config = $.extend({}, Default, config);

          if (typeof config.target !== 'string') {
            var id = $(config.target).attr('id');
            if (!id) {
              id = _Util['default'].getUID(NAME);
              $(config.target).attr('id', id);
            }
            config.target = '#' + id;
          }

          _Util['default'].typeCheckConfig(NAME, config, DefaultType);

          return config;
        }
      }, {
        key: '_getScrollTop',
        value: function _getScrollTop() {
          return this._scrollElement === window ? this._scrollElement.scrollY : this._scrollElement.scrollTop;
        }
      }, {
        key: '_getScrollHeight',
        value: function _getScrollHeight() {
          return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
        }
      }, {
        key: '_process',
        value: function _process() {
          var scrollTop = this._getScrollTop() + this._config.offset;
          var scrollHeight = this._getScrollHeight();
          var maxScroll = this._config.offset + scrollHeight - this._scrollElement.offsetHeight;

          if (this._scrollHeight !== scrollHeight) {
            this.refresh();
          }

          if (scrollTop >= maxScroll) {
            var target = this._targets[this._targets.length - 1];

            if (this._activeTarget !== target) {
              this._activate(target);
            }
          }

          if (this._activeTarget && scrollTop < this._offsets[0]) {
            this._activeTarget = null;
            this._clear();
            return;
          }

          for (var i = this._offsets.length; i--;) {
            var isActiveTarget = this._activeTarget !== this._targets[i] && scrollTop >= this._offsets[i] && (this._offsets[i + 1] === undefined || scrollTop < this._offsets[i + 1]);

            if (isActiveTarget) {
              this._activate(this._targets[i]);
            }
          }
        }
      }, {
        key: '_activate',
        value: function _activate(target) {
          this._activeTarget = target;

          this._clear();

          var queries = this._selector.split(',');
          queries = queries.map(function (selector) {
            return selector + '[data-target="' + target + '"],' + (selector + '[href="' + target + '"]');
          });

          var $link = $(queries.join(','));

          if ($link.hasClass(ClassName.DROPDOWN_ITEM)) {
            $link.closest(Selector.DROPDOWN).find(Selector.DROPDOWN_TOGGLE).addClass(ClassName.ACTIVE);
            $link.addClass(ClassName.ACTIVE);
          } else {
            // todo (fat) this is kinda sus...
            // recursively add actives to tested nav-links
            $link.parents(Selector.LI).find(Selector.NAV_LINKS).addClass(ClassName.ACTIVE);
          }

          $(this._scrollElement).trigger(Event.ACTIVATE, {
            relatedTarget: target
          });
        }
      }, {
        key: '_clear',
        value: function _clear() {
          $(this._selector).filter(Selector.ACTIVE).removeClass(ClassName.ACTIVE);
        }

        // static

      }], [{
        key: '_jQueryInterface',
        value: function _jQueryInterface(config) {
          return this.each(function () {
            var data = $(this).data(DATA_KEY);
            var _config = typeof config === 'object' && config || null;

            if (!data) {
              data = new ScrollSpy(this, _config);
              $(this).data(DATA_KEY, data);
            }

            if (typeof config === 'string') {
              if (data[config] === undefined) {
                throw new Error('No method named "' + config + '"');
              }
              data[config]();
            }
          });
        }
      }, {
        key: 'VERSION',
        get: function get() {
          return VERSION;
        }
      }, {
        key: 'Default',
        get: function get() {
          return Default;
        }
      }]);

      return ScrollSpy;
    })();

    $(window).on(Event.LOAD_DATA_API, function () {
      var scrollSpys = $.makeArray($(Selector.DATA_SPY));

      for (var i = scrollSpys.length; i--;) {
        var $spy = $(scrollSpys[i]);
        ScrollSpy._jQueryInterface.call($spy, $spy.data());
      }
    });

    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $.fn[NAME] = ScrollSpy._jQueryInterface;
    $.fn[NAME].Constructor = ScrollSpy;
    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return ScrollSpy._jQueryInterface;
    };

    return ScrollSpy;
  })(jQuery);

  module.exports = ScrollSpy;
});

(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['exports', 'module', './util'], factory);
  } else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
    factory(exports, module, require('./util'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, mod, global.Util);
    global.tab = mod.exports;
  }
})(this, function (exports, module, _util) {
  'use strict';

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

  var _Util = _interopRequireDefault(_util);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.0.0-alpha.2): tab.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Tab = (function ($) {

    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */

    var NAME = 'tab';
    var VERSION = '4.0.0-alpha.2';
    var DATA_KEY = 'bs.tab';
    var EVENT_KEY = '.' + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $.fn[NAME];
    var TRANSITION_DURATION = 150;

    var Event = {
      HIDE: 'hide' + EVENT_KEY,
      HIDDEN: 'hidden' + EVENT_KEY,
      SHOW: 'show' + EVENT_KEY,
      SHOWN: 'shown' + EVENT_KEY,
      CLICK_DATA_API: 'click' + EVENT_KEY + DATA_API_KEY
    };

    var ClassName = {
      DROPDOWN_MENU: 'dropdown-menu',
      ACTIVE: 'active',
      FADE: 'fade',
      IN: 'in'
    };

    var Selector = {
      A: 'a',
      LI: 'li',
      DROPDOWN: '.dropdown',
      UL: 'ul:not(.dropdown-menu)',
      FADE_CHILD: '> .nav-item .fade, > .fade',
      ACTIVE: '.active',
      ACTIVE_CHILD: '> .nav-item > .active, > .active',
      DATA_TOGGLE: '[data-toggle="tab"], [data-toggle="pill"]',
      DROPDOWN_TOGGLE: '.dropdown-toggle',
      DROPDOWN_ACTIVE_CHILD: '> .dropdown-menu .active'
    };

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

    var Tab = (function () {
      function Tab(element) {
        _classCallCheck(this, Tab);

        this._element = element;
      }

      /**
       * ------------------------------------------------------------------------
       * Data Api implementation
       * ------------------------------------------------------------------------
       */

      // getters

      _createClass(Tab, [{
        key: 'show',

        // public

        value: function show() {
          var _this = this;

          if (this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && $(this._element).hasClass(ClassName.ACTIVE)) {
            return;
          }

          var target = undefined;
          var previous = undefined;
          var ulElement = $(this._element).closest(Selector.UL)[0];
          var selector = _Util['default'].getSelectorFromElement(this._element);

          if (ulElement) {
            previous = $.makeArray($(ulElement).find(Selector.ACTIVE));
            previous = previous[previous.length - 1];
          }

          var hideEvent = $.Event(Event.HIDE, {
            relatedTarget: this._element
          });

          var showEvent = $.Event(Event.SHOW, {
            relatedTarget: previous
          });

          if (previous) {
            $(previous).trigger(hideEvent);
          }

          $(this._element).trigger(showEvent);

          if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) {
            return;
          }

          if (selector) {
            target = $(selector)[0];
          }

          this._activate(this._element, ulElement);

          var complete = function complete() {
            var hiddenEvent = $.Event(Event.HIDDEN, {
              relatedTarget: _this._element
            });

            var shownEvent = $.Event(Event.SHOWN, {
              relatedTarget: previous
            });

            $(previous).trigger(hiddenEvent);
            $(_this._element).trigger(shownEvent);
          };

          if (target) {
            this._activate(target, target.parentNode, complete);
          } else {
            complete();
          }
        }
      }, {
        key: 'dispose',
        value: function dispose() {
          $.removeClass(this._element, DATA_KEY);
          this._element = null;
        }

        // private

      }, {
        key: '_activate',
        value: function _activate(element, container, callback) {
          var active = $(container).find(Selector.ACTIVE_CHILD)[0];
          var isTransitioning = callback && _Util['default'].supportsTransitionEnd() && (active && $(active).hasClass(ClassName.FADE) || Boolean($(container).find(Selector.FADE_CHILD)[0]));

          var complete = $.proxy(this._transitionComplete, this, element, active, isTransitioning, callback);

          if (active && isTransitioning) {
            $(active).one(_Util['default'].TRANSITION_END, complete).emulateTransitionEnd(TRANSITION_DURATION);
          } else {
            complete();
          }

          if (active) {
            $(active).removeClass(ClassName.IN);
          }
        }
      }, {
        key: '_transitionComplete',
        value: function _transitionComplete(element, active, isTransitioning, callback) {
          if (active) {
            $(active).removeClass(ClassName.ACTIVE);

            var dropdownChild = $(active).find(Selector.DROPDOWN_ACTIVE_CHILD)[0];

            if (dropdownChild) {
              $(dropdownChild).removeClass(ClassName.ACTIVE);
            }

            active.setAttribute('aria-expanded', false);
          }

          $(element).addClass(ClassName.ACTIVE);
          element.setAttribute('aria-expanded', true);

          if (isTransitioning) {
            _Util['default'].reflow(element);
            $(element).addClass(ClassName.IN);
          } else {
            $(element).removeClass(ClassName.FADE);
          }

          if (element.parentNode && $(element.parentNode).hasClass(ClassName.DROPDOWN_MENU)) {

            var dropdownElement = $(element).closest(Selector.DROPDOWN)[0];
            if (dropdownElement) {
              $(dropdownElement).find(Selector.DROPDOWN_TOGGLE).addClass(ClassName.ACTIVE);
            }

            element.setAttribute('aria-expanded', true);
          }

          if (callback) {
            callback();
          }
        }

        // static

      }], [{
        key: '_jQueryInterface',
        value: function _jQueryInterface(config) {
          return this.each(function () {
            var $this = $(this);
            var data = $this.data(DATA_KEY);

            if (!data) {
              data = data = new Tab(this);
              $this.data(DATA_KEY, data);
            }

            if (typeof config === 'string') {
              if (data[config] === undefined) {
                throw new Error('No method named "' + config + '"');
              }
              data[config]();
            }
          });
        }
      }, {
        key: 'VERSION',
        get: function get() {
          return VERSION;
        }
      }]);

      return Tab;
    })();

    $(document).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE, function (event) {
      event.preventDefault();
      Tab._jQueryInterface.call($(this), 'show');
    });

    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $.fn[NAME] = Tab._jQueryInterface;
    $.fn[NAME].Constructor = Tab;
    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return Tab._jQueryInterface;
    };

    return Tab;
  })(jQuery);

  module.exports = Tab;
});

(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['exports', 'module', './util'], factory);
  } else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
    factory(exports, module, require('./util'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, mod, global.Util);
    global.tooltip = mod.exports;
  }
})(this, function (exports, module, _util) {
  /* global Tether */

  'use strict';

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

  var _Util = _interopRequireDefault(_util);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.0.0-alpha.2): tooltip.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Tooltip = (function ($) {

    /**
     * Check for Tether dependency
     * Tether - http://github.hubspot.com/tether/
     */
    if (window.Tether === undefined) {
      throw new Error('Bootstrap tooltips require Tether (http://github.hubspot.com/tether/)');
    }

    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */

    var NAME = 'tooltip';
    var VERSION = '4.0.0-alpha.2';
    var DATA_KEY = 'bs.tooltip';
    var EVENT_KEY = '.' + DATA_KEY;
    var JQUERY_NO_CONFLICT = $.fn[NAME];
    var TRANSITION_DURATION = 150;
    var CLASS_PREFIX = 'bs-tether';

    var Default = {
      animation: true,
      template: '<div class="tooltip" role="tooltip">' + '<div class="tooltip-arrow"></div>' + '<div class="tooltip-inner"></div></div>',
      trigger: 'hover focus',
      title: '',
      delay: 0,
      html: false,
      selector: false,
      placement: 'top',
      offset: '0 0',
      constraints: []
    };

    var DefaultType = {
      animation: 'boolean',
      template: 'string',
      title: '(string|element|function)',
      trigger: 'string',
      delay: '(number|object)',
      html: 'boolean',
      selector: '(string|boolean)',
      placement: '(string|function)',
      offset: 'string',
      constraints: 'array'
    };

    var AttachmentMap = {
      TOP: 'bottom center',
      RIGHT: 'middle left',
      BOTTOM: 'top center',
      LEFT: 'middle right'
    };

    var HoverState = {
      IN: 'in',
      OUT: 'out'
    };

    var Event = {
      HIDE: 'hide' + EVENT_KEY,
      HIDDEN: 'hidden' + EVENT_KEY,
      SHOW: 'show' + EVENT_KEY,
      SHOWN: 'shown' + EVENT_KEY,
      INSERTED: 'inserted' + EVENT_KEY,
      CLICK: 'click' + EVENT_KEY,
      FOCUSIN: 'focusin' + EVENT_KEY,
      FOCUSOUT: 'focusout' + EVENT_KEY,
      MOUSEENTER: 'mouseenter' + EVENT_KEY,
      MOUSELEAVE: 'mouseleave' + EVENT_KEY
    };

    var ClassName = {
      FADE: 'fade',
      IN: 'in'
    };

    var Selector = {
      TOOLTIP: '.tooltip',
      TOOLTIP_INNER: '.tooltip-inner'
    };

    var TetherClass = {
      element: false,
      enabled: false
    };

    var Trigger = {
      HOVER: 'hover',
      FOCUS: 'focus',
      CLICK: 'click',
      MANUAL: 'manual'
    };

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

    var Tooltip = (function () {
      function Tooltip(element, config) {
        _classCallCheck(this, Tooltip);

        // private
        this._isEnabled = true;
        this._timeout = 0;
        this._hoverState = '';
        this._activeTrigger = {};
        this._tether = null;

        // protected
        this.element = element;
        this.config = this._getConfig(config);
        this.tip = null;

        this._setListeners();
      }

      /**
       * ------------------------------------------------------------------------
       * jQuery
       * ------------------------------------------------------------------------
       */

      // getters

      _createClass(Tooltip, [{
        key: 'enable',

        // public

        value: function enable() {
          this._isEnabled = true;
        }
      }, {
        key: 'disable',
        value: function disable() {
          this._isEnabled = false;
        }
      }, {
        key: 'toggleEnabled',
        value: function toggleEnabled() {
          this._isEnabled = !this._isEnabled;
        }
      }, {
        key: 'toggle',
        value: function toggle(event) {
          if (event) {
            var dataKey = this.constructor.DATA_KEY;
            var context = $(event.currentTarget).data(dataKey);

            if (!context) {
              context = new this.constructor(event.currentTarget, this._getDelegateConfig());
              $(event.currentTarget).data(dataKey, context);
            }

            context._activeTrigger.click = !context._activeTrigger.click;

            if (context._isWithActiveTrigger()) {
              context._enter(null, context);
            } else {
              context._leave(null, context);
            }
          } else {

            if ($(this.getTipElement()).hasClass(ClassName.IN)) {
              this._leave(null, this);
              return;
            }

            this._enter(null, this);
          }
        }
      }, {
        key: 'dispose',
        value: function dispose() {
          clearTimeout(this._timeout);

          this.cleanupTether();

          $.removeData(this.element, this.constructor.DATA_KEY);

          $(this.element).off(this.constructor.EVENT_KEY);

          if (this.tip) {
            $(this.tip).remove();
          }

          this._isEnabled = null;
          this._timeout = null;
          this._hoverState = null;
          this._activeTrigger = null;
          this._tether = null;

          this.element = null;
          this.config = null;
          this.tip = null;
        }
      }, {
        key: 'show',
        value: function show() {
          var _this = this;

          var showEvent = $.Event(this.constructor.Event.SHOW);

          if (this.isWithContent() && this._isEnabled) {
            $(this.element).trigger(showEvent);

            var isInTheDom = $.contains(this.element.ownerDocument.documentElement, this.element);

            if (showEvent.isDefaultPrevented() || !isInTheDom) {
              return;
            }

            var tip = this.getTipElement();
            var tipId = _Util['default'].getUID(this.constructor.NAME);

            tip.setAttribute('id', tipId);
            this.element.setAttribute('aria-describedby', tipId);

            this.setContent();

            if (this.config.animation) {
              $(tip).addClass(ClassName.FADE);
            }

            var placement = typeof this.config.placement === 'function' ? this.config.placement.call(this, tip, this.element) : this.config.placement;

            var attachment = this._getAttachment(placement);

            $(tip).data(this.constructor.DATA_KEY, this).appendTo(document.body);

            $(this.element).trigger(this.constructor.Event.INSERTED);

            this._tether = new Tether({
              attachment: attachment,
              element: tip,
              target: this.element,
              classes: TetherClass,
              classPrefix: CLASS_PREFIX,
              offset: this.config.offset,
              constraints: this.config.constraints,
              addTargetClasses: false
            });

            _Util['default'].reflow(tip);
            this._tether.position();

            $(tip).addClass(ClassName.IN);

            var complete = function complete() {
              var prevHoverState = _this._hoverState;
              _this._hoverState = null;

              $(_this.element).trigger(_this.constructor.Event.SHOWN);

              if (prevHoverState === HoverState.OUT) {
                _this._leave(null, _this);
              }
            };

            if (_Util['default'].supportsTransitionEnd() && $(this.tip).hasClass(ClassName.FADE)) {
              $(this.tip).one(_Util['default'].TRANSITION_END, complete).emulateTransitionEnd(Tooltip._TRANSITION_DURATION);
              return;
            }

            complete();
          }
        }
      }, {
        key: 'hide',
        value: function hide(callback) {
          var _this2 = this;

          var tip = this.getTipElement();
          var hideEvent = $.Event(this.constructor.Event.HIDE);
          var complete = function complete() {
            if (_this2._hoverState !== HoverState.IN && tip.parentNode) {
              tip.parentNode.removeChild(tip);
            }

            _this2.element.removeAttribute('aria-describedby');
            $(_this2.element).trigger(_this2.constructor.Event.HIDDEN);
            _this2.cleanupTether();

            if (callback) {
              callback();
            }
          };

          $(this.element).trigger(hideEvent);

          if (hideEvent.isDefaultPrevented()) {
            return;
          }

          $(tip).removeClass(ClassName.IN);

          if (_Util['default'].supportsTransitionEnd() && $(this.tip).hasClass(ClassName.FADE)) {

            $(tip).one(_Util['default'].TRANSITION_END, complete).emulateTransitionEnd(TRANSITION_DURATION);
          } else {
            complete();
          }

          this._hoverState = '';
        }

        // protected

      }, {
        key: 'isWithContent',
        value: function isWithContent() {
          return Boolean(this.getTitle());
        }
      }, {
        key: 'getTipElement',
        value: function getTipElement() {
          return this.tip = this.tip || $(this.config.template)[0];
        }
      }, {
        key: 'setContent',
        value: function setContent() {
          var $tip = $(this.getTipElement());

          this.setElementContent($tip.find(Selector.TOOLTIP_INNER), this.getTitle());

          $tip.removeClass(ClassName.FADE).removeClass(ClassName.IN);

          this.cleanupTether();
        }
      }, {
        key: 'setElementContent',
        value: function setElementContent($element, content) {
          var html = this.config.html;
          if (typeof content === 'object' && (content.nodeType || content.jquery)) {
            // content is a DOM node or a jQuery
            if (html) {
              if (!$(content).parent().is($element)) {
                $element.empty().append(content);
              }
            } else {
              $element.text($(content).text());
            }
          } else {
            $element[html ? 'html' : 'text'](content);
          }
        }
      }, {
        key: 'getTitle',
        value: function getTitle() {
          var title = this.element.getAttribute('data-original-title');

          if (!title) {
            title = typeof this.config.title === 'function' ? this.config.title.call(this.element) : this.config.title;
          }

          return title;
        }
      }, {
        key: 'cleanupTether',
        value: function cleanupTether() {
          if (this._tether) {
            this._tether.destroy();
          }
        }

        // private

      }, {
        key: '_getAttachment',
        value: function _getAttachment(placement) {
          return AttachmentMap[placement.toUpperCase()];
        }
      }, {
        key: '_setListeners',
        value: function _setListeners() {
          var _this3 = this;

          var triggers = this.config.trigger.split(' ');

          triggers.forEach(function (trigger) {
            if (trigger === 'click') {
              $(_this3.element).on(_this3.constructor.Event.CLICK, _this3.config.selector, $.proxy(_this3.toggle, _this3));
            } else if (trigger !== Trigger.MANUAL) {
              var eventIn = trigger === Trigger.HOVER ? _this3.constructor.Event.MOUSEENTER : _this3.constructor.Event.FOCUSIN;
              var eventOut = trigger === Trigger.HOVER ? _this3.constructor.Event.MOUSELEAVE : _this3.constructor.Event.FOCUSOUT;

              $(_this3.element).on(eventIn, _this3.config.selector, $.proxy(_this3._enter, _this3)).on(eventOut, _this3.config.selector, $.proxy(_this3._leave, _this3));
            }
          });

          if (this.config.selector) {
            this.config = $.extend({}, this.config, {
              trigger: 'manual',
              selector: ''
            });
          } else {
            this._fixTitle();
          }
        }
      }, {
        key: '_fixTitle',
        value: function _fixTitle() {
          var titleType = typeof this.element.getAttribute('data-original-title');
          if (this.element.getAttribute('title') || titleType !== 'string') {
            this.element.setAttribute('data-original-title', this.element.getAttribute('title') || '');
            this.element.setAttribute('title', '');
          }
        }
      }, {
        key: '_enter',
        value: function _enter(event, context) {
          var dataKey = this.constructor.DATA_KEY;

          context = context || $(event.currentTarget).data(dataKey);

          if (!context) {
            context = new this.constructor(event.currentTarget, this._getDelegateConfig());
            $(event.currentTarget).data(dataKey, context);
          }

          if (event) {
            context._activeTrigger[event.type === 'focusin' ? Trigger.FOCUS : Trigger.HOVER] = true;
          }

          if ($(context.getTipElement()).hasClass(ClassName.IN) || context._hoverState === HoverState.IN) {
            context._hoverState = HoverState.IN;
            return;
          }

          clearTimeout(context._timeout);

          context._hoverState = HoverState.IN;

          if (!context.config.delay || !context.config.delay.show) {
            context.show();
            return;
          }

          context._timeout = setTimeout(function () {
            if (context._hoverState === HoverState.IN) {
              context.show();
            }
          }, context.config.delay.show);
        }
      }, {
        key: '_leave',
        value: function _leave(event, context) {
          var dataKey = this.constructor.DATA_KEY;

          context = context || $(event.currentTarget).data(dataKey);

          if (!context) {
            context = new this.constructor(event.currentTarget, this._getDelegateConfig());
            $(event.currentTarget).data(dataKey, context);
          }

          if (event) {
            context._activeTrigger[event.type === 'focusout' ? Trigger.FOCUS : Trigger.HOVER] = false;
          }

          if (context._isWithActiveTrigger()) {
            return;
          }

          clearTimeout(context._timeout);

          context._hoverState = HoverState.OUT;

          if (!context.config.delay || !context.config.delay.hide) {
            context.hide();
            return;
          }

          context._timeout = setTimeout(function () {
            if (context._hoverState === HoverState.OUT) {
              context.hide();
            }
          }, context.config.delay.hide);
        }
      }, {
        key: '_isWithActiveTrigger',
        value: function _isWithActiveTrigger() {
          for (var trigger in this._activeTrigger) {
            if (this._activeTrigger[trigger]) {
              return true;
            }
          }

          return false;
        }
      }, {
        key: '_getConfig',
        value: function _getConfig(config) {
          config = $.extend({}, this.constructor.Default, $(this.element).data(), config);

          if (config.delay && typeof config.delay === 'number') {
            config.delay = {
              show: config.delay,
              hide: config.delay
            };
          }

          _Util['default'].typeCheckConfig(NAME, config, this.constructor.DefaultType);

          return config;
        }
      }, {
        key: '_getDelegateConfig',
        value: function _getDelegateConfig() {
          var config = {};

          if (this.config) {
            for (var key in this.config) {
              if (this.constructor.Default[key] !== this.config[key]) {
                config[key] = this.config[key];
              }
            }
          }

          return config;
        }

        // static

      }], [{
        key: '_jQueryInterface',
        value: function _jQueryInterface(config) {
          return this.each(function () {
            var data = $(this).data(DATA_KEY);
            var _config = typeof config === 'object' ? config : null;

            if (!data && /destroy|hide/.test(config)) {
              return;
            }

            if (!data) {
              data = new Tooltip(this, _config);
              $(this).data(DATA_KEY, data);
            }

            if (typeof config === 'string') {
              if (data[config] === undefined) {
                throw new Error('No method named "' + config + '"');
              }
              data[config]();
            }
          });
        }
      }, {
        key: 'VERSION',
        get: function get() {
          return VERSION;
        }
      }, {
        key: 'Default',
        get: function get() {
          return Default;
        }
      }, {
        key: 'NAME',
        get: function get() {
          return NAME;
        }
      }, {
        key: 'DATA_KEY',
        get: function get() {
          return DATA_KEY;
        }
      }, {
        key: 'Event',
        get: function get() {
          return Event;
        }
      }, {
        key: 'EVENT_KEY',
        get: function get() {
          return EVENT_KEY;
        }
      }, {
        key: 'DefaultType',
        get: function get() {
          return DefaultType;
        }
      }]);

      return Tooltip;
    })();

    $.fn[NAME] = Tooltip._jQueryInterface;
    $.fn[NAME].Constructor = Tooltip;
    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return Tooltip._jQueryInterface;
    };

    return Tooltip;
  })(jQuery);

  module.exports = Tooltip;
});

(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['exports', 'module'], factory);
  } else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
    factory(exports, module);
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, mod);
    global.util = mod.exports;
  }
})(this, function (exports, module) {
  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.0.0-alpha.2): util.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  'use strict';

  var Util = (function ($) {

    /**
     * ------------------------------------------------------------------------
     * Private TransitionEnd Helpers
     * ------------------------------------------------------------------------
     */

    var transition = false;

    var TransitionEndEvent = {
      WebkitTransition: 'webkitTransitionEnd',
      MozTransition: 'transitionend',
      OTransition: 'oTransitionEnd otransitionend',
      transition: 'transitionend'
    };

    // shoutout AngusCroll (https://goo.gl/pxwQGp)
    function toType(obj) {
      return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
    }

    function isElement(obj) {
      return (obj[0] || obj).nodeType;
    }

    function getSpecialTransitionEndEvent() {
      return {
        bindType: transition.end,
        delegateType: transition.end,
        handle: function handle(event) {
          if ($(event.target).is(this)) {
            return event.handleObj.handler.apply(this, arguments);
          }
        }
      };
    }

    function transitionEndTest() {
      if (window.QUnit) {
        return false;
      }

      var el = document.createElement('bootstrap');

      for (var _name in TransitionEndEvent) {
        if (el.style[_name] !== undefined) {
          return { end: TransitionEndEvent[_name] };
        }
      }

      return false;
    }

    function transitionEndEmulator(duration) {
      var _this = this;

      var called = false;

      $(this).one(Util.TRANSITION_END, function () {
        called = true;
      });

      setTimeout(function () {
        if (!called) {
          Util.triggerTransitionEnd(_this);
        }
      }, duration);

      return this;
    }

    function setTransitionEndSupport() {
      transition = transitionEndTest();

      $.fn.emulateTransitionEnd = transitionEndEmulator;

      if (Util.supportsTransitionEnd()) {
        $.event.special[Util.TRANSITION_END] = getSpecialTransitionEndEvent();
      }
    }

    /**
     * --------------------------------------------------------------------------
     * Public Util Api
     * --------------------------------------------------------------------------
     */

    var Util = {

      TRANSITION_END: 'bsTransitionEnd',

      getUID: function getUID(prefix) {
        do {
          /* eslint-disable no-bitwise */
          prefix += ~ ~(Math.random() * 1000000); // "~~" acts like a faster Math.floor() here
          /* eslint-enable no-bitwise */
        } while (document.getElementById(prefix));
        return prefix;
      },

      getSelectorFromElement: function getSelectorFromElement(element) {
        var selector = element.getAttribute('data-target');

        if (!selector) {
          selector = element.getAttribute('href') || '';
          selector = /^#[a-z]/i.test(selector) ? selector : null;
        }

        return selector;
      },

      reflow: function reflow(element) {
        new Function('bs', 'return bs')(element.offsetHeight);
      },

      triggerTransitionEnd: function triggerTransitionEnd(element) {
        $(element).trigger(transition.end);
      },

      supportsTransitionEnd: function supportsTransitionEnd() {
        return Boolean(transition);
      },

      typeCheckConfig: function typeCheckConfig(componentName, config, configTypes) {
        for (var property in configTypes) {
          if (configTypes.hasOwnProperty(property)) {
            var expectedTypes = configTypes[property];
            var value = config[property];
            var valueType = undefined;

            if (value && isElement(value)) {
              valueType = 'element';
            } else {
              valueType = toType(value);
            }

            if (!new RegExp(expectedTypes).test(valueType)) {
              throw new Error(componentName.toUpperCase() + ': ' + ('Option "' + property + '" provided type "' + valueType + '" ') + ('but expected type "' + expectedTypes + '".'));
            }
          }
        }
      }
    };

    setTransitionEndSupport();

    return Util;
  })(jQuery);

  module.exports = Util;
});
